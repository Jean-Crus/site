import bitmap from "@images/image2020.svg";
import logo from "@images/logo.png";

export const customTheme = {
    primary: "#ef7300",
    secondary: "#ef7300",
    background: "#1f1f1f",
    backgroundHeader: "#0058a8",
    letter: "#FFFFFF",
    letter2: "#6c6c6c",
    newsletterColor: "white",
    letterColor: "#002a50"
};

export const defaultTheme = {
    primary: "#5433f1",
    secondary: "#34b16f",
    background: "#1f1f1f",
    backgroundHeader: "#131313",
    letter: "#FFFFFF",
    letter2: "#6c6c6c"
};

export const header = {
    logo
};

export const footer = {
    text: "Plataforma Tesis"
};

export const sobreCurso = {
    img: bitmap,
    title: "",
    subTitle: " Sobre a Tesis",
    phrase: "Descubra a sua história",
    buttonText: "Quero conhecer"
};

export const carouselStart = {
    img1:
        "https://images.unsplash.com/photo-1515378791036-0648a3ef77b2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
    title1: "Plataforma Tesis",
    phrase1: "Plataforma Tesis",
    img2:
        "https://images.unsplash.com/photo-1558274862-fb21bb917cd3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
    title2: "Plataforma Tesis:",
    phrase2: "Plataforma Tesis",
    img3:
        "https://images.unsplash.com/photo-1554415707-6e8cfc93fe23?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
    title3: "Plataforma Tesis",
    phrase3: "Plataforma Tesis"
};

export const vantagens = {
    titleVantagem: "Vantagens",
    img1: "https://image.flaticon.com/icons/svg/2170/2170182.svg",
    title1: "Maquiagem",
    phrase1: "Frase 1",
    img2: "https://image.flaticon.com/icons/svg/2204/2204515.svg",
    title2: "Corte",
    phrase2: "Frase 2",
    img3: "https://image.flaticon.com/icons/svg/599/599752.svg",
    title3: "Manicure",
    phrase3: "Frase 3",
    img4: "https://image.flaticon.com/icons/svg/42/42006.svg",
    title4: "Penteado",
    phrase4: "Frase 4",
    img5: "https://image.flaticon.com/icons/svg/2181/2181982.svg",
    title5: "Técnicas de Barbearia ",
    phrase5:
        "Título bem grande testando Título bem grande testando Título bem grande testando Título bem grande testando",
    img6: "https://image.flaticon.com/icons/svg/2236/2236200.svg",
    title6: "Técnica de Escova",
    phrase6: "Frase 6"
};

export const faleConosco = {
    img:
        "https://images.unsplash.com/photo-1531537571171-a707bf2683da?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80",
    title: "Fale Conosco",
    subTitle: "Será um prazer falar com você! ",
    phrase: "Frase grande grande"
};

export const newsletter = {
    highlight: "Quer receber notícias sobre ",
    sub: "Plataforma Tesis?",
    doubtPlaceholder: "Plataforma Tesis?",
    doubtValue1: "Plataforma Tesis",
    doubtValue2: "Plataforma Tesis",
    doubtValue3: "Plataforma Tesis",
    doubtValue4: "Plataforma Tesis",
    email: "E-mail",
    buttonTitle: "Inscrever"
};
