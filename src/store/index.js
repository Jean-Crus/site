import { persistStore } from "redux-persist";
import { applyMiddleware, createStore, compose } from "redux";
// import history from '../routes/history';
import createSagaMiddleware from "redux-saga";
import persistReducers from "./PersistReducers";
import createRootReducer from "./modules/rootReducer";
import rootSaga from "./modules/rootSaga";

const sagaMonitor =
    process.env.NODE_ENV === "development"
        ? console.tron.createSagaMonitor()
        : null;
const sagaMiddleware = createSagaMiddleware({ sagaMonitor });
// const routeMiddleware = routerMiddleware(history);

const middlewares = [sagaMiddleware];

const composer =
    process.env.NODE_ENV === "development"
        ? compose(
              console.tron.createEnhancer(),
              applyMiddleware(...middlewares)
          )
        : applyMiddleware(...middlewares);

const store = createStore(persistReducers(createRootReducer), composer);
const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export { store, persistor };
