import bannerProfessor from "@images/bannerProfessor.png";
import image2020 from "@images/image2020.svg";

const initialState = {
    loading: false,
    openChat: false,
    themeHome1: {
        background: { colorOne: "#012a50", colorTwo: "#0058a8" },
        titleColor: { colorOne: "#fff", colorTwo: "#fff" },
        fontTitle: 50,
        circle: { colorOne: "#ef9300", colorTwo: "#ef7300" },
        circleColor: { colorOne: "#fff", colorTwo: "#fff" },
        subtitleColor: { colorOne: "#fff", colorTwo: "#fff" },
        fontSubtitle: 30,
        dotsProgress: { colorOne: "#fff", colorTwo: "#fff" },
        dotsTitle: { colorOne: "#fff", colorTwo: "#fff" },
        dotsSub: { colorOne: "#fff", colorTwo: "#fff" },
        slideArrow: { colorOne: "#fff", colorTwo: "#fff" },
        slides: [
            {
                id: 0,
                title:
                    '<h1 class="ql-indent-1"><br></h1><h1 class="ql-indent-1"><br></h1><h1><strong style="color: rgb(255, 255, 255);" class="ql-font-Montserrat">Plataforma Tesis </strong></h1>',
                subtitle:
                    '<h2><span class="ql-font-Montserrat">Plataforma Tesis</span></h2><p class="ql-indent-2"><br></p><p class="ql-indent-2"><br></p><p class="ql-indent-2"><br></p>',
                titleHighlight: "Plataforma Tesis",
                subtitleHighlight: "Plataforma Tesis",
                button: {
                    title: "Cursos",
                    type: "Modelo 02",
                    cor: "Modelo 02",
                    letterColor: "Modelo 02"
                },
                img: image2020
            },
            {
                id: 1,
                title:
                    '<h1 class="ql-indent-1"><br></h1><h1 class="ql-indent-1"><br></h1><h1><strong class="ql-font-Montserrat" style="color: rgb(255, 255, 255);">Plataforma Tesis</strong></h1>',
                subtitle:
                    '<h2><span class="ql-font-Montserrat">Plataforma Tesis</span></h2><p class="ql-indent-2"><br></p><p class="ql-indent-2"><br></p><p class="ql-indent-2"><br></p>',
                titleHighlight: "Plataforma Tesis",
                subtitleHighlight: "Plataforma Tesis",
                button: {
                    title: "Cursos",
                    type: "Modelo 02",
                    cor: "Modelo 02",
                    letterColor: "Modelo 02"
                },
                img:
                    "https://images.unsplash.com/photo-1477281765962-ef34e8bb0967?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1232&q=80"
            },
            {
                id: 2,
                title:
                    '<h1 class="ql-indent-1"><br></h1><h1 class="ql-indent-1"><br></h1><h1><strong class="ql-font-Montserrat" style="color: rgb(255, 255, 255);">Plataforma Tesis!</strong></h1>',
                subtitle:
                    '<h2><span class="ql-font-Montserrat">Plataforma Tesis</span></h2><p class="ql-indent-2"><br></p><p class="ql-indent-2"><br></p><p class="ql-indent-2"><br></p>',
                titleHighlight: "Plataforma Tesis",
                subtitleHighlight: "Plataforma Tesis",
                button: {
                    title: "Cursos",
                    type: "Modelo 02",
                    cor: "Modelo 02",
                    letterColor: "Modelo 02"
                },
                img:
                    "https://images.unsplash.com/photo-1529007196863-d07650a3f0ea?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80"
            }
        ]
    },
    themeCourseDestaque: {
        titleText: "Curso Destaque",
        background: { colorOne: "#fff", colorTwo: "#fff" },
        titleCardColor: { colorOne: "#1f1f1f", colorTwo: "#1f1f1f" },
        subtitleCheck: true,
        priceHighlightColor: { colorOne: "#1f1f1f", colorTwo: "#1f1f1f" },
        textOffer: "Investimento de:",
        priceOfferColor: { colorOne: "#6c6c6c", colorTwo: "#6c6c6c" },
        textColor: { colorOne: "#6c6c6c", colorTwo: "#6c6c6c" },
        tagsColor: { colorOne: "#6c6c6c", colorTwo: "#6c6c6c" },
        buttonOne: {
            title: "Adicionar ao carrinho",
            type: "Modelo 02",
            cor: "Modelo 02",
            letterColor: "Modelo 02"
        },
        buttonTwo: {
            title: "Saiba mais",
            type: "Modelo 02",
            cor: "Modelo 02",
            letterColor: "Modelo 02"
        },
        cardBackground: { colorOne: "#fff", colorTwo: "#fff" }
    }
};

export default initialState;
