export function getOpenChat() {
    return { type: '@app/OPEN_CHAT' };
}

export function changeGeralHome1(color, name) {
    return {
        type: '@themeHome1/CHANGE_GERAL',
        payload: { color, name },
    };
}

export function changeTextHome1(text, campo, id, campoTexto) {
    return {
        type: '@themeHome1/CHANGE_TEXT',
        payload: { text, campo, id, campoTexto },
    };
}

export function changeFontHome1(value, target) {
    return {
        type: '@themeHome1/CHANGE_FONT',
        payload: { value, target },
    };
}

export function changeImageHome1(img, imgCamp, imgId) {
    return {
        type: '@themeHome1/CHANGE_IMAGE',
        payload: { img, imgCamp, imgId },
    };
}

export function changeButtonHome1(button, buttonId) {
    return {
        type: '@themeHome1/CHANGE_BUTTON',
        payload: { button, buttonId },
    };
}

export function changeButtonDestaque(buttonDestaque, buttonName) {
    return {
        type: '@themeCourseDestaque/CHANGE_BUTTON',
        payload: { buttonDestaque, buttonName },
    };
}

export function changeTextDestaque(text, campo, id, campoTexto) {
    return {
        type: '@themeCourseDestaque/CHANGE_TEXT',
        payload: { text, campo, id, campoTexto },
    };
}

export function changeGeralDestaque(colorDestaque, nameDestaque) {
    return {
        type: '@themeCourseDestaque/CHANGE_GERAL',
        payload: { colorDestaque, nameDestaque },
    };
}
