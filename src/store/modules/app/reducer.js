import produce from 'immer';
import initialState from './initialState';

export default function app(state = initialState, action) {
    return produce(state, draft => {
        switch (action.type) {
            case '@app/OPEN_CHAT':
                draft.openChat = !draft.openChat;
                break;
            case '@themeHome1/CHANGE_GERAL':
                const { color, name } = action.payload;
                if (color.hex) {
                    draft.themeHome1[name] = {
                        colorOne: color.hex,
                        colorTwo: color.hex,
                    };
                } else if (color.colorOne) {
                    draft.themeHome1[name] = color;
                } else {
                    draft.themeHome1[name] = {
                        colorOne: color,
                        colorTwo: color,
                    };
                }
                break;
            case '@themeHome1/CHANGE_TEXT':
                const { text, campo, id, campoTexto } = action.payload;
                draft.themeHome1.slides.map(slide => {
                    if (slide.id === id) {
                        slide[campo] = text;
                        slide[`${campo}Highlight`] = campoTexto;
                    }
                });
                break;
            case '@themeHome1/CHANGE_FONT':
                const { value, target } = action.payload;
                draft.themeHome1[target] = value;
                break;
            case '@themeHome1/CHANGE_IMAGE':
                const { img, imgCamp, imgId } = action.payload;
                draft.themeHome1.slides.map(slide => {
                    if (slide.id === imgId) {
                        slide[imgCamp] = img;
                    }
                });
                break;
            case '@themeHome1/CHANGE_BUTTON':
                const { button, buttonId } = action.payload;
                draft.themeHome1.slides.map(slide => {
                    if (slide.id === buttonId && button.hex) {
                        slide.button = {
                            title: slide.button.title,
                            cor: button.hex,
                            type: button,
                            letterColor: button,
                        };
                    }
                    if (slide.id === buttonId && !button.hex) {
                        slide.button = {
                            title: slide.button.title,
                            cor: button,
                            type: button,
                            letterColor: button,
                        };
                    }
                });
                break;
            case '@themeCourseDestaque/CHANGE_BUTTON':
                const { buttonDestaque, buttonName } = action.payload;
                if (buttonDestaque.hex) {
                    draft.themeCourseDestaque[buttonName] = {
                        title: draft.themeCourseDestaque[buttonName].title,
                        cor: buttonDestaque.hex,
                        type: buttonDestaque,
                        letterColor: buttonDestaque,
                    };
                }
                if (!buttonDestaque.hex) {
                    draft.themeCourseDestaque[buttonName] = {
                        title: draft.themeCourseDestaque[buttonName].title,
                        cor: buttonDestaque,
                        type: buttonDestaque,
                        letterColor: buttonDestaque,
                    };
                }
                break;
            case '@themeCourseDestaque/CHANGE_GERAL':
                const { colorDestaque, nameDestaque } = action.payload;
                if (colorDestaque.hex) {
                    draft.themeCourseDestaque[nameDestaque] = {
                        colorOne: colorDestaque.hex,
                        colorTwo: colorDestaque.hex,
                    };
                } else if (colorDestaque.colorOne) {
                    draft.themeCourseDestaque[nameDestaque] = colorDestaque;
                } else {
                    draft.themeCourseDestaque[nameDestaque] = {
                        colorOne: colorDestaque,
                        colorTwo: colorDestaque,
                    };
                }
                break;
            default:
                return state;
        }
    });
}
