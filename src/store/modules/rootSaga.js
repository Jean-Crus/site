import { all } from 'redux-saga/effects';
import curso from './curso/sagas';
import cursoDetail from './cursoDetail/sagas';
import cart from './cart/sagas';
import app from './app/sagas';
import auth from './auth/sagas';
import user from './user/sagas';
import checkout from './checkout/sagas';
import filterCurso from './filter/sagas';
import order from './order/sagas';

export default function* rootSaga() {
    yield all([
        curso,
        cart,
        cursoDetail,
        app,
        auth,
        checkout,
        user,
        filterCurso,
        order,
    ]);
}
