export function getInstallmentRequest() {
    return { type: "@checkout/GET_INSTALLMENT_REQUEST" };
}

export function getSessionCardRequest() {
    return { type: "@checkout/GET_SESSION_CARD_REQUEST" };
}

export function getSessionCardSuccess() {
    return { type: "@checkout/GET_SESSION_CARD_SUCCESS" };
}

export function getCheckoutRequest(
    nomeTitular,
    numberCard,
    cpf,
    birthDate,
    validade,
    ccv,
    opcaoPagamento,
    cardBrand,
    tel
) {
    return {
        type: "@checkout/GET_CHECKOUT_REQUEST",
        payload: {
            nomeTitular,
            numberCard,
            cpf,
            birthDate,
            validade,
            ccv,
            opcaoPagamento,
            cardBrand,
            tel
        }
    };
}

export function getCheckoutSuccess() {
    return {
        type: "@checkout/GET_CHECKOUT_SUCCESS"
    };
}

export function checkoutFailure() {
    return {
        type: "@checkout/CHECKOUT_FAILURE"
    };
}

export function emptyCart() {
    return {
        type: "@checkout/GET_CHECKOUT_EMPTY"
    };
}

export function getThankuRequest(id) {
    return { type: "@checkout/GET_THANKU_REQUEST", payload: { id } };
}

export function getThankuSuccess(ticket) {
    return { type: "@checkout/GET_THANKU_SUCCESS", payload: { ticket } };
}

export function changePaymentMethod(payMethod) {
    return {
        type: "@checkout/CHANGE_PAYMENT_METHOD",
        payload: { payMethod }
    };
}

export function boletoPaymentRequest() {
    return {
        type: "@checkout/BOLETO_PAYMENT_REQUEST"
    };
}
export function boletoFailure() {
    return {
        type: "@checkout/BOLETO_PAYMENT_FAILURE"
    };
}
export function boletoPaymentSuccess() {
    return {
        type: "@checkout/BOLETO_PAYMENT_SUCCESS"
    };
}

export function getSessionIuguCardRequest(
    nomeTitular,
    numberCard,
    cpf,
    birthDate,
    validade,
    ccv,
    opcaoPagamento,
    cardBrand,
    tel
) {
    return {
        type: "@checkout/GET_SESSION_IUGU_CARD_REQUEST",
        payload: {
            nomeTitular,
            numberCard,
            cpf,
            birthDate,
            validade,
            ccv,
            opcaoPagamento,
            cardBrand,
            tel
        }
    };
}

export function getSessionIuguCardSuccess() {
    return { type: "@checkout/GET_SESSION_IUGU_CARD_SUCCESS" };
}
