const initialState = {
    loading: true,
    completed: false,
    data: [],
    payMethod: "credit-card",
    loadingBoleto: false
};

export default initialState;
