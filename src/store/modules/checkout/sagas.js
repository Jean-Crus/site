import { call, select, put, all, takeLatest } from "redux-saga/effects";

import { toast } from "react-toastify";
import history from "@routes/history";
import { newError } from "@util/error";
import { TestaCPF, createError } from "@util/tools";
import api, { checkoutPayment, checkout } from "@api";
import {
    getSessionCardSuccess,
    getCheckoutSuccess,
    getThankuSuccess,
    checkoutFailure,
    boletoPaymentSuccess,
    boletoFailure
} from "./actions";
import { getInstallmentRequest } from "../cart/actions";
import { checkoutIugu } from "../../../services/api";

function getInstallment(amount, brand = "visa") {
    return new Promise(function promise(resolve, reject) {
        window.PagSeguroDirectPayment.getInstallments({
            amount,
            maxInstallmentNoInterest: 0,
            brand,
            success(response) {
                resolve(response);
            },
            error(error) {
                reject(error);
            },
            complete(response) {
                // Callback para todas chamadas.
            }
        });
    });
}

function criarCardToken(card) {
    return new Promise(function promise(resolve, reject) {
        window.PagSeguroDirectPayment.createCardToken({
            cardNumber: card.cardNumber, // Número do cartão de crédito
            brand: card.brand.name, // Bandeira do cartão
            cvv: card.cvv, // CVV do cartão
            expirationMonth: card.expirationMonth, // Mês da expiração do cartão
            expirationYear: card.expirationYear, // Ano da expiração do cartão, é necessário os 4 dígitos.
            success(response) {
                resolve(response);
            },
            error(error) {
                reject(error);
            },
            complete(response) {}
        });
    });
}
const getCard = async ({
    nomeTitular,
    numberCard,
    cpf,
    birthDate,
    validade,
    ccv,
    opcaoPagamento,
    cardBrand
}) => {
    const card = {};
    card.cardNumber = numberCard;
    card.brand = {
        name: cardBrand
    };
    card.cvv = ccv.trim();
    card.expiration = validade;
    card.installments = opcaoPagamento;

    if (card.expiration) {
        const split = card.expiration.toString().split("/");
        card.expirationMonth = split[0];
        card.expirationYear = split[1];
    }

    return criarCardToken(card)
        .then(response => {
            return {
                senderHash: window.PagSeguroDirectPayment.getSenderHash(),
                cardToken: response.card.token
            };
        })
        .catch(error => {
            return error;
        });
};

function* getSessionCardRequest() {
    try {
        const response = yield call(checkout.get, "session");
        window.PagSeguroDirectPayment.setSessionId(response.data);

        const amount = yield select(state => state.cart.total);
        const responseChama = yield call(getInstallment, amount);

        yield put(getInstallmentRequest(responseChama.installments.visa));

        yield put(getSessionCardSuccess());
    } catch (error) {}
}

function* getCheckoutCard({ payload }) {
    try {
        const {
            nomeTitular,
            numberCard,
            cpf,
            birthDate,
            tel,
            validade,
            ccv,
            opcaoPagamento
        } = payload;

        if (!TestaCPF(cpf)) {
            throw createError({
                cpf: ["CPF inválido"]
            });
        }

        const token = yield select(state => state.auth.token);

        const response = yield call(getCard, payload);
        if (response.error === true) {
            yield newError({
                data: {
                    code: 30400,
                    message: Object.values(response.errors)
                }
            });
            yield put(checkoutFailure());
        } else {
            const items = yield select(state => state.cart.data);
            const profile = yield select(state => state.user.profile);

            const installment = yield select(state => state.cart.installment);
            const newInstallment = installment.filter(item => {
                return item.quantity === opcaoPagamento;
            });

            const resp = yield call(api.post, "checkout", {
                senderName: `${profile.name} ${profile.lastname}`,
                senderPhone: tel,
                senderEmail: profile.email,
                senderCPF: cpf,
                paymentMethod: "credit-card",
                creditCardHolderName: nomeTitular,
                creditCardHolderPhone: tel,
                creditCardHolderCPF: cpf,
                creditCardHolderBirthDate: birthDate,
                creditCardToken: response.cardToken,
                senderHash: response.senderHash,
                installmentQuantity: newInstallment[0].quantity,
                installmentValue: newInstallment[0].installmentAmount,
                items
            });
            yield put(getCheckoutSuccess());
            history.push(`/thankyou/${resp.data.id_order}`);
        }
    } catch (error) {
        yield newError(error.response, "card");
    }
}

function* getTicket({ payload }) {
    try {
        const { id } = payload;
        const token = yield select(state => state.auth.token);
        const response = yield call(api.get, `/users/orders/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        const { data } = response;
        yield put(getThankuSuccess(data.data));
    } catch (error) {
        yield newError(error.response);
    }
}

function* boletoPay({ payload }) {
    try {
        const token = yield select(state => state.auth.token);

        // if (response.error === true) {
        //     yield newError({
        //         data: {
        //             code: 30400,
        //             message: Object.values(response.errors)
        //         }
        //     });
        //     yield put(checkoutFailure());
        // } else {
        const items = yield select(state => state.cart.data);
        const profile = yield select(state => state.user.profile);

        const resp = yield call(api.post, "checkout", {
            senderName: `${profile.name} ${profile.lastname}`,
            senderPhone: profile.telephone,
            senderEmail: profile.email,
            senderCPF: profile.cpf,
            senderHash: window.PagSeguroDirectPayment.getSenderHash(),
            paymentMethod: "boleto",
            items
        });
        yield put(getCheckoutSuccess());
        yield put(boletoPaymentSuccess());
        history.push(`/thankyou/${resp.data.id_order}`);
        // }
    } catch (error) {
        yield newError(error.response, "card");
        yield put(boletoFailure());
    }
}

function* getSessionIuguCardRequest({ payload }) {
    try {
        const {
            nomeTitular,
            numberCard,
            cpf,
            birthDate,
            tel,
            validade,
            ccv,
            opcaoPagamento
        } = payload;

        const monthYear = validade.split("/");

        if (!TestaCPF(cpf)) {
            throw createError({
                cpf: ["CPF inválido"]
            });
        }

        const items = yield select(state => state.cart.data);
        const profile = yield select(state => state.user.profile);

        const installment = yield select(state => state.cart.installment);
        const newInstallment = installment.filter(item => {
            return item.quantity === opcaoPagamento;
        });
        const responseToken = yield call(checkoutIugu.post, "payment_token", {
            account_id: "88BA7377CA8C4AB7BFD1E53DFA297011",
            method: "credit_card",
            test: true,
            data: {
                number: numberCard,
                verification_value: ccv,
                first_name: nomeTitular,
                last_name: "",
                month: monthYear[0],
                year: monthYear[1]
            }
        });

        // const resp = yield call(api.post, "checkout", {
        //     senderName: `${profile.name} ${profile.lastname}`,
        //     senderPhone: tel,
        //     senderEmail: profile.email,
        //     senderCPF: cpf,
        //     paymentMethod: "credit-card",
        //     creditCardToken: responseToken.data.id,
        //     installmentQuantity: newInstallment[0].quantity,
        //     installmentValue: newInstallment[0].installmentAmount,
        //     items
        // });
        // yield put(getCheckoutSuccess());
        // history.push(`/thankyou/${resp.data.id_order}`);
    } catch (error) {
        yield newError(error.response, "card");
    }
}

export default all([
    takeLatest("@checkout/GET_SESSION_CARD_REQUEST", getSessionCardRequest),
    // takeLatest("@checkout/GET_INSTALLMENT_REQUEST", getInstallment),
    takeLatest("@checkout/GET_CHECKOUT_REQUEST", getCheckoutCard),
    takeLatest("@checkout/GET_THANKU_REQUEST", getTicket),
    takeLatest("@checkout/BOLETO_PAYMENT_REQUEST", boletoPay),
    takeLatest(
        "@checkout/GET_SESSION_IUGU_CARD_REQUEST",
        getSessionIuguCardRequest
    )
]);
