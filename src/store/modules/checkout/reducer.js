import produce from "immer";
import initialState from "./initialState";

export default function checkout(state = initialState, action) {
    return produce(state, draft => {
        switch (action.type) {
            case "@checkout/GET_SESSION_CARD_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@checkout/GET_SESSION_CARD_SUCCESS": {
                draft.loading = false;
                break;
            }
            case "@checkout/GET_CHECKOUT_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@checkout/GET_CHECKOUT_SUCCESS": {
                draft.loading = true;
                break;
            }
            case "@checkout/GET_THANKU_REQUEST": {
                draft.loading = true;
                break;
            }

            case "@checkout/GET_THANKU_SUCCESS": {
                const { payload } = action;
                draft.data = payload.ticket;
                draft.loading = false;
                break;
            }
            case "@checkout/CHECKOUT_FAILURE": {
                draft.loading = false;
                break;
            }
            case "@checkout/CHANGE_PAYMENT_METHOD": {
                const { payMethod } = action.payload;
                draft.payMethod = payMethod;
                break;
            }
            case "@checkout/BOLETO_PAYMENT_REQUEST": {
                draft.loadingBoleto = true;
                break;
            }
            case "@checkout/BOLETO_PAYMENT_SUCCESS": {
                draft.loadingBoleto = false;
                break;
            }
            case "@checkout/BOLETO_PAYMENT_FAILURE": {
                draft.loadingBoleto = false;
                break;
            }
            default:
        }
    });
}
