const initialState = {
    depos: [
        {
            id: 1,
            video: 'https://www.youtube.com/watch?v=ZRx4mTDMmzA',
            name: 'Mineiro da Silva',
            city: 'Recife',
            text: 'Nossa, esse mora longe pra caramba!',
            imagem:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
        },
        {
            id: 2,
            video: 'https://www.youtube.com/watch?v=npCMuKdtDKI',
            name: 'Marlon Cabeça',
            city: 'Campinas',
            text: 'Nossa que textão bonito.',
            imagem:
                'https://images.pexels.com/photos/774909/pexels-photo-774909.jpeg?cs=srgb&dl=alegria-atraente-bonita-774909.jpg&fm=jpg',
        },
        {
            id: 3,
            video: 'https://www.youtube.com/watch?v=eVFs6GnJTZs',
            name: 'Roberto Almeida',
            city: 'Campo Largo',
            text: 'Nossa, mas esse cara é bom demais.',
            imagem:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
        },
        {
            id: 4,
            video: 'https://www.youtube.com/watch?v=_f9sK9KMxRI',
            name: 'Dagol Neto',
            city: 'São Paulo',
            text: 'Caramba que coisa mais legal!',
            imagem:
                'https://images.pexels.com/photos/774909/pexels-photo-774909.jpeg?cs=srgb&dl=alegria-atraente-bonita-774909.jpg&fm=jpg',
        },
        {
            id: 5,
            video: 'https://www.youtube.com/watch?v=zjojsT9zo8M',
            name: 'Felisberto Testa',
            city: 'Florianópolis',
            text: 'Esse manja das paradas.',
            imagem:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
        },
    ],
    active: {
        id: 1,
        video: 'https://www.youtube.com/watch?v=ZRx4mTDMmzA',
        name: 'Mineiro da Silva',
        city: 'Recife',
        text: 'Nossa, esse mora longe pra caramba!',
        imagem:
            'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
    },
    deposText: [
        {
            id: 1,
            name: 'Mineiro da Silva',
            city: 'Recife',
            text: 'Nossa, esse mora longe pra caramba!',
            imagem:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
        },
        {
            id: 2,
            name: 'Marlon Cabeça',
            city: 'Campinas',
            text: 'Nossa que textão bonito.',
            imagem:
                'https://images.pexels.com/photos/774909/pexels-photo-774909.jpeg?cs=srgb&dl=alegria-atraente-bonita-774909.jpg&fm=jpg',
        },
        {
            id: 3,
            name: 'Roberto Almeida',
            city: 'Campo Largo',
            text: 'Nossa, mas esse cara é bom demais.',
            imagem:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
        },
        {
            id: 4,
            name: 'Dagol Neto',
            city: 'São Paulo',
            text: 'Caramba que coisa mais legal!',
            imagem:
                'https://images.pexels.com/photos/774909/pexels-photo-774909.jpeg?cs=srgb&dl=alegria-atraente-bonita-774909.jpg&fm=jpg',
        },
        {
            id: 5,
            name: 'Felisberto Testa',
            city: 'Florianópolis',
            text: 'Esse manja das paradas.',
            imagem:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
        },
        {
            id: 6,
            name: 'Ana canhão',
            city: 'Porto Alegre',
            text: 'Esse texto é das paradas.',
            imagem:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
        },
        {
            id: 7,
            name: 'Wilson Jesus',
            city: 'Rio de Janeiro',
            text: 'Nossa que cidade.',
            imagem:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
        },
    ],
    activeText: {
        id: 1,
        name: 'Mineiro da Silva',
        city: 'Recife',
        text: 'Nossa, esse mora longe pra caramba!',
        imagem:
            'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
    },
};

export default initialState;
