export function setDepo(id) {
    return { type: '@depoiments/SET_DEPO', payload: { id } };
}

export function setDepoText(id) {
    return { type: '@depoiments/SET_DEPO_TEXT', payload: { id } };
}
