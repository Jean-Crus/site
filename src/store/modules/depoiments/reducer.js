import { produce } from 'immer';
import initialState from './initialState';

export default function user(state = initialState, action) {
    return produce(state, draft => {
        switch (action.type) {
            case '@depoiments/SET_DEPO':
                const depo = draft.depos.find(
                    item => item.id === action.payload.id
                );
                if (depo !== -1) draft.active = depo;
                break;
            case '@depoiments/SET_DEPO_TEXT':
                const depoText = draft.deposText.find(
                    item => item.id === action.payload.id
                );
                if (depoText !== -1) draft.activeText = depoText;
                break;
            default:
        }
    });
}
