/* eslint-disable no-case-declarations */
import produce from "immer";
import { formatPrice } from "@util/format";
import initialState from "./initialState";

export default function cart(state = initialState, action) {
    return produce(state, draft => {
        switch (action.type) {
            case "@cart/GET_INSTALLMENT_REQUEST":
                const { installment } = action.payload;
                draft.installment = installment;
                break;
            case "@cart/ADD_REQUEST":
                draft.loading = true;
                break;
            case "@cart/ADD_SUCCESS":
                const { product, total, item } = action.payload;
                draft.data.push(product);
                if (item) {
                    draft.cartId.push(item);
                }
                draft.amount += 1;
                draft.total += total;
                draft.totalFormatted = formatPrice(draft.total);
                draft.openCart = true;
                draft.empty = false;
                draft.loading = false;
                break;
            case "@cart/CONT_BUYING":
                draft.openCart = false;
                break;
            case "@cart/REMOVE":
                const { id } = action.payload;
                const productIndex = draft.data.findIndex(p => p.id === id);
                const productData = draft.data.filter(p => p.id === id);

                const oneLess = productData.map(produto =>
                    produto.product.sale_price > 0
                        ? produto.product.sale_price
                        : produto.product.price
                );

                if (productIndex >= 0) {
                    draft.data.splice(productIndex, 1);
                    draft.cartId.splice(productIndex, 1);
                    draft.amount -= 1;
                    draft.total -= oneLess;
                    draft.totalFormatted = formatPrice(draft.total);
                }
                if (draft.amount < 1) draft.empty = true;
                break;

            case "@checkout/GET_CHECKOUT_EMPTY":
                draft.data = [];
                draft.amount = 0;
                draft.empty = true;
                draft.total = 0;
                draft.totalFormatted = "R$ 00,00";
                break;
            case "@cart/OPEN_CART":
                draft.openCart = !draft.openCart;
                break;
            default:
        }
    });
}
