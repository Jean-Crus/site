const initialState = {
    loading: true,
    data: [],
    openCart: false,
    amount: 0,
    cartId: [],
    empty: true,
    total: 0,
    totalFormatted: 'R$ 0,00',
    installment: [],
};

export default initialState;
