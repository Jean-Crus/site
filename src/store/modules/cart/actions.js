export function getInstallmentRequest(installment) {
    return {
        type: "@cart/GET_INSTALLMENT_REQUEST",
        payload: { installment }
    };
}

export function addToCartRequest(id) {
    return {
        type: "@cart/ADD_REQUEST",
        payload: { id }
    };
}

export function addToCartSuccess(product, total, item) {
    return {
        type: "@cart/ADD_SUCCESS",
        payload: { product, total, item }
    };
}

export function contBuying() {
    return {
        type: "@cart/CONT_BUYING"
    };
}

export function openCart() {
    return {
        type: "@cart/OPEN_CART"
    };
}

export function removeFromCart(id) {
    return {
        type: "@cart/REMOVE",
        payload: { id }
    };
}
