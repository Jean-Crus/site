import { call, select, put, all, takeLatest } from "redux-saga/effects";
import { toast } from "react-toastify";
import history from "@routes/history";
import { formatPrice } from "@util/format";
import api from "@api";

import { addToCartSuccess, updateAmountSuccess } from "./actions";

function* addToCart({ payload: { id } }) {
    try {
        const productsExists = yield select(state =>
            state.cart.data.find(p => p.id === id)
        );
        if (!productsExists) {
            const response = yield call(api.get, `/teams/${id}`);
            const data = {
                ...response.data.data,
                amount: 1,
                priceFormatted:
                    response.data.data.product.sale_price > 0
                        ? formatPrice(response.data.data.product.sale_price)
                        : formatPrice(response.data.data.product.price)
            };
            const total =
                data.product.sale_price > 0
                    ? data.product.sale_price
                    : data.product.price;
            if (response.data.data.category) {
                yield put(
                    addToCartSuccess(
                        data,
                        total,
                        response.data.data.category.id
                    )
                );
            } else {
                yield put(addToCartSuccess(data, total));
            }
            toast.success(`${data.name} adicionado ao carrinho`);
        } else {
            toast.error(`Você já adicionou ${productsExists.name}!`);
        }
    } catch (err) {}
}

export default all([
    takeLatest("@cart/ADD_REQUEST", addToCart)
    // takeLatest('@cart/UPDATE_AMOUNT_REQUEST', updateAmount),
]);
