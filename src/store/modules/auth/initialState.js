const initialState = {
    token: null,
    signed: false,
    loading: false,
};

export default initialState;
