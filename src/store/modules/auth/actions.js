export function signInRequest(email, password) {
    return {
        type: "@auth/SIGN_IN_REQUEST",
        payload: { email, password }
    };
}

export function signInSuccess(token, profile) {
    return { type: "@auth/SIGN_IN_SUCCESS", payload: { token, profile } };
}

export function logoutRequest() {
    return { type: "@auth/LOGOUT_REQUEST" };
}

export function logoutSuccess() {
    return { type: "@auth/LOGOUT_SUCCESS" };
}

export function signFailure() {
    return {
        type: "@auth/SIGN_FAILURE"
    };
}

export function signUpRequest(name, lastname, email, tel, password, cpf) {
    return {
        type: "@auth/SIGN_UP_REQUEST",
        payload: { name, lastname, email, tel, password, cpf }
    };
}

export function signUpSuccess(token, profile) {
    return {
        type: "@auth/SIGN_UP_SUCCESS",
        payload: { token, profile }
    };
}

export function lostPasswordRequest(email) {
    return {
        type: "@auth/LOST_PASSWORD_REQUEST",
        payload: { email }
    };
}

export function lostPasswordSuccess() {
    return {
        type: "@auth/LOST_PASSWORD_SUCCESS"
    };
}

export function resetPasswordRequest(email, password, confirmPassword, token) {
    return {
        type: "@auth/RESET_PASSWORD_REQUEST",
        payload: { email, password, confirmPassword, token }
    };
}

export function resetPasswordSuccess() {
    return {
        type: "@auth/RESET_PASSWORD_SUCCESS",
        payload: {}
    };
}
