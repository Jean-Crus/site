import { produce } from "immer";
import history from "@routes/history";
import initialState from "./initialState";

export default function auth(state = initialState, action) {
    return produce(state, draft => {
        switch (action.type) {
            case "@auth/SIGN_UP_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@auth/SIGN_IN_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@auth/SIGN_IN_SUCCESS": {
                const { token } = action.payload;
                draft.token = token;
                draft.signed = true;
                draft.loading = false;
                break;
            }
            case "@auth/SIGN_FAILURE": {
                draft.loading = false;
                break;
            }
            case "@auth/LOGOUT_SUCCESS": {
                // draft.token = null;
                // draft.signed = false;
                return initialState;
            }
            case "@auth/SIGN_UP_SUCCESS": {
                const { token } = action.payload;
                draft.token = token;
                draft.signed = true;
                draft.loading = false;
                break;
            }
            case "@auth/RESET_PASSWORD_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@auth/LOST_PASSWORD_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@auth/RESET_PASSWORD_SUCCESS": {
                draft.loading = false;
                break;
            }
            case "@auth/LOST_PASSWORD_SUCCESS": {
                draft.loading = false;
                break;
            }
            default:
        }
    });
}
