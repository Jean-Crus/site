import { takeLatest, call, put, all, select } from "redux-saga/effects";
import history from "@routes/history";
import { toast } from "react-toastify";
import { TestaCPF } from "@util/tools";
import { newError } from "@util/error";
import api from "@api";
import {
    signInSuccess,
    signFailure,
    signUpSuccess,
    logoutSuccess,
    lostPasswordSuccess,
    resetPasswordSuccess
} from "./actions";
import { TOKEN_KEY } from "../../../services/auth";

export function* signIn({ payload }) {
    try {
        const amount = yield select(state => state.cart.amount);
        const { email, password } = payload;
        const response = yield call(api.post, "auth/login", {
            email,
            password
        });

        const { token, user } = response.data;
        yield put(signInSuccess(token, user));
        yield localStorage.setItem(TOKEN_KEY, token);
        if (amount > 0) history.push("/checkout");
        else history.push("/conta");
    } catch (error) {
        yield put(signFailure());
        yield newError(error.response);
    }
}

export function* signUp({ payload }) {
    try {
        const amount = yield select(state => state.cart.amount);
        const { name, lastname, email, password, tel, cpf } = payload;

        if (!TestaCPF(cpf)) return toast.error("CPF inválido");

        const response = yield call(api.post, "auth/register", {
            name,
            lastname,
            email,
            password,
            telephone: tel,
            cpf
        });

        const { data } = response;

        yield put(signUpSuccess(data.token, data.user));
        yield localStorage.setItem(TOKEN_KEY, data.token);
        if (amount > 0) history.push("/checkout");
        else history.push("/conta");
    } catch (error) {
        yield newError(error.response);
    } finally {
        yield put(signFailure());
    }
}

export function* logout() {
    try {
        yield put(logoutSuccess());
        yield localStorage.removeItem(TOKEN_KEY);
        history.push("/login");
    } catch (error) {}
}

export function* lostPassword({ payload: { email } }) {
    try {
        const response = yield call(api.post, "auth/password/forgot", {
            email
        });
        yield put(lostPasswordSuccess());
        toast.success(response.data.message);
    } catch (error) {
        yield newError(error.response, "card");
    }
}

export function* resetPassword({
    payload: { email, password, confirmPassword, token }
}) {
    try {
        const response = yield call(
            api.post,
            `auth/password/reset?token=${token}`,
            {
                email,
                password,
                password_confirmation: confirmPassword
            }
        );
        toast.success(response.data.message);
        yield put(resetPasswordSuccess());
        history.push("/login");
    } catch (error) {
        yield newError(error.response, "card");
    }
}

export default all([
    takeLatest("@auth/SIGN_IN_REQUEST", signIn),
    takeLatest("@auth/SIGN_UP_REQUEST", signUp),
    takeLatest("@auth/LOGOUT_REQUEST", logout),
    takeLatest("@auth/LOST_PASSWORD_REQUEST", lostPassword),
    takeLatest("@auth/RESET_PASSWORD_REQUEST", resetPassword)
]);
