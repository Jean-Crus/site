import { call, select, put, all, takeLatest } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import { formatPrice } from '@util/format';
import history from '@routes/history';
import api from '@api';

// import api from '../../../services/api';

// import { formatPrice } from '../../../util/format';

import { getCursoDetailSuccess } from './actions';

function* getCursoDetail({ payload: { id } }) {
    try {
        const response = yield call(api.get, `/teams/${id}`);
        const curso = {
            ...response.data,
            priceFormatted:
                response.data.data.product.sale_price > 0
                    ? formatPrice(response.data.data.product.sale_price)
                    : formatPrice(response.data.data.product.price),
        };
        yield put(getCursoDetailSuccess(curso));
    } catch (err) {
        history.push('/404');
    }
}

export default all([
    takeLatest('@curso/GET_CURSO_DETAIL_REQUEST', getCursoDetail),
]);
