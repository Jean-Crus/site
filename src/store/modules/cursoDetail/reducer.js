import produce from 'immer';
import initialState from './initialState';

export default function cursoDetail(state = initialState, action) {
    return produce(state, draft => {
        switch (action.type) {
            case '@curso/GET_CURSO_DETAIL_REQUEST': {
                draft.loading = true;
                break;
            }
            case '@curso/GET_CURSO_DETAIL_SUCCESS': {
                const { curso } = action.payload;
                draft.data = curso;
                draft.loading = false;
                break;
            }
            default:
                return state;
        }
    });
}
