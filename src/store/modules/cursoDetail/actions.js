export function getCursoDetailRequest(id) {
    return { type: '@curso/GET_CURSO_DETAIL_REQUEST', payload: { id } };
}

export function getCursoDetailSuccess(curso) {
    return { type: '@curso/GET_CURSO_DETAIL_SUCCESS', payload: { curso } };
}
