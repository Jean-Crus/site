export function getCursosDestaqueRequest() {
    return { type: "@curso/GET_CURSOS_DESTAQUE_REQUEST" };
}

export function getCursosDestaqueSuccess(cursos) {
    return { type: "@curso/GET_CURSOS_DESTAQUE_SUCCESS", payload: { cursos } };
}

export function getCursosPrincipalRequest(id) {
    return { type: "@curso/GET_CURSOS_PRINCIPAL_REQUEST", payload: { id } };
}

export function getCursosPrincipalSuccess(cursos, pagination) {
    return {
        type: "@curso/GET_CURSOS_PRINCIPAL_SUCCESS",
        payload: { cursos, pagination }
    };
}

export function getCursosRelacionadosRequest(id) {
    return { type: "@curso/GET_CURSOS_RELACIONADOS_REQUEST", payload: { id } };
}

export function getCursosRelacionadosSuccess(relacionados) {
    return {
        type: "@curso/GET_CURSOS_RELACIONADOS_SUCCESS",
        payload: { relacionados }
    };
}

export function getCursosCartRequest() {
    return { type: "@curso/GET_CURSOS_CART_REQUEST" };
}

export function getCursosCartSuccess(cart) {
    return {
        type: "@curso/GET_CURSOS_CART_SUCCESS",
        payload: { cart }
    };
}

export function addCursoRequest(id) {
    return { type: "@curso/ADD_CURSO_REQUEST", payload: { id } };
}

export function addCursoSuccess(curso) {
    return { type: "@curso/ADD_CURSO_SUCCESS", payload: { curso } };
}

export function getCursoDetail(id) {
    return { type: "@curso/GET_CURSO_DETAIL", payload: { id } };
}

export function removeFromCart(id) {
    return {
        type: "@curso/REMOVE",
        payload: { id }
    };
}
