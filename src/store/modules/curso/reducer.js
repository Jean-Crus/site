import produce from "immer";
import initialState from "./initialState";

export default function curso(state = initialState, action) {
    return produce(state, draft => {
        switch (action.type) {
            case "@curso/GET_CURSOS_DESTAQUE_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@filter/SEND_FILTER_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@filter/SEND_FILTER_SUCCESS": {
                draft.loading = false;
                break;
            }
            case "@filter/REMOVE_FILTER_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@filter/REMOVE_FILTER_SUCCESS": {
                draft.loading = false;
                break;
            }
            case "@filter/REMOVE_ALL_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@filter/REMOVE_ALL_SUCCESS": {
                draft.loading = false;
                break;
            }
            case "@curso/GET_CURSOS_DESTAQUE_SUCCESS": {
                const { cursos } = action.payload;
                draft.data = cursos;
                draft.loading = false;
                break;
            }
            case "@curso/GET_CURSOS_PRINCIPAL_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@curso/GET_CURSOS_PRINCIPAL_SUCCESS": {
                const { cursos, pagination } = action.payload;
                draft.data = cursos;
                draft.pagination = pagination;
                draft.loading = false;
                break;
            }
            case "@curso/GET_CURSOS_RELACIONADOS_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@curso/GET_CURSOS_RELACIONADOS_SUCCESS": {
                const { relacionados } = action.payload;
                draft.data = relacionados;
                draft.loading = false;
                break;
            }
            case "@curso/GET_CURSOS_CART_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@curso/GET_CURSOS_CART_SUCCESS": {
                const { cart } = action.payload;
                draft.data = cart;
                draft.loading = false;
                break;
            }
            case "@filter/ORDER_FILTER_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@filter/GET_NAME_FILTER_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@filter/GET_NAME_FILTER_SUCCESS": {
                draft.loading = false;
                break;
            }
            case "@filter/ORDER_FILTER_SUCCESS": {
                draft.loading = false;
                break;
            }
            default:
                return state;
        }
    });
}
