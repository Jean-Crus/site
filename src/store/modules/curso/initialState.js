const initialState = {
    loading: true,
    data: [],
    amount: 0,
    isVazio: false,
    pagination: [],
};

export default initialState;
