import { call, select, put, all, takeLatest } from "redux-saga/effects";

import { formatPrice } from "@util/format";
import { newError } from "@util/error";
import api from "@api";

import {
    getCursosDestaqueSuccess,
    getCursosPrincipalSuccess,
    getCursosRelacionadosSuccess,
    getCursosCartSuccess
} from "./actions";
import {
    sendFilterSuccess,
    removeAllSuccess,
    removeFilterSuccess,
    getFiltersRequest,
    orderFilterSuccess,
    getNameFilterSuccess
} from "../filter/actions";

function* getCursosDestaque() {
    const response = yield call(api.get, "/teams/highlights");
    const cursos = [];
    response.data.data.map(curso => {
        return cursos.push({
            ...curso,
            salePriceFormatted: formatPrice(curso.product.sale_price),
            priceFormatted: formatPrice(curso.product.price)
        });
    });
    yield put(getCursosDestaqueSuccess(cursos));
}

function* getCursosRelacionados({ payload: { id } }) {
    try {
        const response = yield call(api.get, `/teams/${id}/relateds`);
        const relacionados = [];
        response.data.data.map(curso => {
            return relacionados.push({
                ...curso,
                salePriceFormatted: formatPrice(curso.product.sale_price),
                priceFormatted: formatPrice(curso.product.price)
            });
        });
        yield put(getCursosRelacionadosSuccess(relacionados));
    } catch (error) {}
}

function* getCursosCart() {
    try {
        const search = yield select(state => state.cart.cartId);
        const response = yield call(
            api.get,
            `/teams?category-id=${search.join()}`
        );
        const cart = [];
        response.data.data.map(curso => {
            return cart.push({
                ...curso,
                salePriceFormatted: formatPrice(curso.product.sale_price),
                priceFormatted: formatPrice(curso.product.price)
            });
        });
        yield put(getCursosCartSuccess(cart));
    } catch (error) {}
}

function* getCursosPrincipal({ payload: { id = 1 } }) {
    try {
        const historyFilter = yield select(
            state => state.filterCurso.historyFilter
        );
        const response = yield call(
            api.get,
            `/teams?page=${id}&name=${historyFilter.name}&order=${historyFilter.order}&max-price=${historyFilter.maxPriceHistory}&min-price=${historyFilter.minPriceHistory}&category-id=${historyFilter.categoriaHistory}&difficulties-id=${historyFilter.nivelHistory}`
        );
        const pagination = response.data.meta;

        const cursos = response.data.data.map(item => {
            return {
                ...item,
                salePriceFormatted: formatPrice(item.product.sale_price),
                priceFormatted: formatPrice(item.product.price)
            };
        });
        yield put(getCursosPrincipalSuccess(cursos, pagination));
    } catch (error) {}
}

function* filterCursos() {
    const list = yield select(state => state.filterCurso);

    const categoria = list.categoria
        .filter(cat => cat.checked)
        .map(cat => cat.id);
    const nivel = list.nivel.filter(niv => niv.checked).map(niv => niv.id);

    const selectCategoria = list.categoria.filter(cat => cat.checked);

    const selectNivel = list.nivel.filter(niv => niv.checked);

    const { price } = list;

    const queryCategoria = categoria.join();
    const queryNivel = nivel.join();
    const response = yield call(
        api.get,
        `/teams?page=1&name=${list.historyFilter.name}&order=${list.historyFilter.order}&max-price=${price.max}&min-price=${price.min}&category-id=${queryCategoria}&difficulties-id=${queryNivel}`
    );
    const pagination = response.data.meta;
    const cursos = [];
    response.data.data.map(curso => {
        return cursos.push({
            ...curso,
            salePriceFormatted: formatPrice(curso.product.sale_price),
            priceFormatted: formatPrice(curso.product.price)
        });
    });
    yield put(getCursosPrincipalSuccess(cursos, pagination));
    yield put(
        sendFilterSuccess(
            selectCategoria,
            selectNivel,
            price,
            queryCategoria,
            queryNivel,
            price.max,
            price.min
        )
    );
}

function* removeAll() {
    try {
        const response = yield call(api.get, `/teams?page=1`);
        const pagination = response.data.meta;
        const cursos = [];
        response.data.data.map(curso => {
            return cursos.push({
                ...curso,
                salePriceFormatted: formatPrice(curso.product.sale_price),
                priceFormatted: formatPrice(curso.product.price)
            });
        });
        yield put(getCursosPrincipalSuccess(cursos, pagination));
        yield put(removeAllSuccess());
        yield put(getFiltersRequest());
    } catch (error) {
        newError(error.response);
    }
}

function* removeFilter({ payload: { filter, id } }) {
    const list = yield select(state => state.filterCurso);
    const historyFilter = yield select(
        state => state.filterCurso.historyFilter
    );
    if (filter === "categoria") {
        const categoriaRemoved = list.categoria.map(item => {
            if (item.id === id) {
                return { ...item, checked: !item.checked };
            }
            return item;
        });
        const catSelect = list.selected.categoria.filter(
            item => item.id !== id
        );
        const categoria = categoriaRemoved
            .filter(cat => cat.checked)
            .map(cat => cat.id);

        const queryCategoria = categoria.join();

        const response = yield call(
            api.get,
            `/teams?page=${1}&name=${historyFilter.name}&order=${
                historyFilter.order
            }&max-price=${historyFilter.maxPriceHistory}&min-price=${
                historyFilter.minPriceHistory
            }&category-id=${queryCategoria}&difficulties-id=${
                historyFilter.nivelHistory
            }`
        );
        const pagination = response.data.meta;
        const cursos = [];
        response.data.data.map(curso => {
            return cursos.push({
                ...curso,
                salePriceFormatted: formatPrice(curso.product.sale_price),
                priceFormatted: formatPrice(curso.product.price)
            });
        });
        yield put(getCursosPrincipalSuccess(cursos, pagination));
        yield put(
            removeFilterSuccess(
                categoriaRemoved,
                catSelect,
                queryCategoria,
                filter
            )
        );
    }
    if (filter === "nivel") {
        const nivelRemoved = list.nivel.map(item => {
            if (item.id === id) {
                return { ...item, checked: !item.checked };
            }
            return item;
        });
        const nivSelect = list.selected.nivel.filter(item => item.id !== id);
        const nivel = nivelRemoved
            .filter(cat => cat.checked)
            .map(cat => cat.id);

        const queryNivel = nivel.join();

        const response = yield call(
            api.get,
            `/teams?page=${1}&name=${historyFilter.name}&order=${
                historyFilter.order
            }&max-price=${historyFilter.maxPriceHistory}&min-price=${
                historyFilter.minPriceHistory
            }&category-id=${
                historyFilter.categoriaHistory
            }&difficulties-id=${queryNivel}`
        );
        const pagination = response.data.meta;
        const cursos = [];
        response.data.data.map(curso => {
            return cursos.push({
                ...curso,
                salePriceFormatted: formatPrice(curso.product.sale_price),
                priceFormatted: formatPrice(curso.product.price)
            });
        });
        yield put(getCursosPrincipalSuccess(cursos, pagination));
        yield put(
            removeFilterSuccess(nivelRemoved, nivSelect, queryNivel, filter)
        );
    }
    if (filter === "price") {
        const newPrice = {
            min: 0,
            max: 1000
        };
        const response = yield call(
            api.get,
            `/teams?page=${1}&name=${historyFilter.name}&order=${
                historyFilter.order
            }&max-price=${newPrice.max}&min-price=${newPrice.min}&category-id=${
                historyFilter.categoriaHistory
            }&difficulties-id=${historyFilter.nivelHistory}`
        );
        const pagination = response.data.meta;
        const cursos = [];
        response.data.data.map(curso => {
            return cursos.push({
                ...curso,
                salePriceFormatted: formatPrice(curso.product.sale_price),
                priceFormatted: formatPrice(curso.product.price)
            });
        });
        yield put(getCursosPrincipalSuccess(cursos, pagination));
        yield put(removeFilterSuccess(newPrice, newPrice, newPrice, filter));
    }
}

function* orderCursos({ payload: { order } }) {
    try {
        const historyFilter = yield select(
            state => state.filterCurso.historyFilter
        );
        const response = yield call(
            api.get,
            `/teams?page=1&name=${historyFilter.name}&order=${order}&max-price=${historyFilter.maxPriceHistory}&min-price=${historyFilter.minPriceHistory}&category-id=${historyFilter.categoriaHistory}&difficulties-id=${historyFilter.nivelHistory}`
        );
        const pagination = response.data.meta;

        const cursos = response.data.data.map(item => {
            return {
                ...item,
                salePriceFormatted: formatPrice(item.product.sale_price),
                priceFormatted: formatPrice(item.product.price)
            };
        });
        yield put(getCursosPrincipalSuccess(cursos, pagination));
        yield put(orderFilterSuccess(order));
    } catch (error) {}
}

function* filterNameCurso({ payload: { filterName } }) {
    try {
        const historyFilter = yield select(
            state => state.filterCurso.historyFilter
        );
        const response = yield call(
            api.get,
            `/teams?page=1&name=${filterName}&order=${historyFilter.order}&max-price=${historyFilter.maxPriceHistory}&min-price=${historyFilter.minPriceHistory}&category-id=${historyFilter.categoriaHistory}&difficulties-id=${historyFilter.nivelHistory}`
        );
        const pagination = response.data.meta;

        const cursos = response.data.data.map(item => {
            return {
                ...item,
                salePriceFormatted: formatPrice(item.product.sale_price),
                priceFormatted: formatPrice(item.product.price)
            };
        });
        yield put(getCursosPrincipalSuccess(cursos, pagination));
        yield put(getNameFilterSuccess(filterName));
    } catch (error) {}
}

export default all([
    takeLatest("@curso/GET_CURSOS_RELACIONADOS_REQUEST", getCursosRelacionados),
    takeLatest("@curso/GET_CURSOS_CART_REQUEST", getCursosCart),
    takeLatest("@curso/GET_CURSOS_DESTAQUE_REQUEST", getCursosDestaque),
    takeLatest("@curso/GET_CURSOS_PRINCIPAL_REQUEST", getCursosPrincipal),
    takeLatest("@filter/SEND_FILTER_REQUEST", filterCursos),
    takeLatest("@filter/REMOVE_FILTER_REQUEST", removeFilter),
    takeLatest("@filter/REMOVE_ALL_REQUEST", removeAll),
    takeLatest("@filter/ORDER_FILTER_REQUEST", orderCursos),
    takeLatest("@filter/GET_NAME_FILTER_REQUEST", filterNameCurso)
]);
