import { call, select, put, all, takeLatest } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import { formatPrice } from '@util/format';
import history from '@routes/history';
import api from '@api';
import { getOrdersSuccess } from './actions';

function* getOrders() {
    try {
        const token = yield select(state => state.auth.token);
        const response = yield call(api.get, 'users/orders', {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        const data = response.data.data.map(cat => {
            return { ...cat, active: false };
        });
        yield put(getOrdersSuccess(data));
    } catch (err) {
        history.push('/404');
    }
}

export default all([takeLatest('@order/GET_ORDERS_REQUEST', getOrders)]);
