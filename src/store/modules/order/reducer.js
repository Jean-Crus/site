import produce from 'immer';
import initialState from './initialState';

export default function order(state = initialState, action) {
    return produce(state, draft => {
        switch (action.type) {
            case '@order/GET_ORDERS_REQUEST': {
                draft.loading = true;
                break;
            }
            case '@order/GET_ORDERS_SUCCESS': {
                const { data } = action.payload;
                draft.data = data;
                draft.loading = false;
                break;
            }
            case '@order/EXPAND_TABLE': {
                const { id } = action.payload;
                const lista = draft.data.map(item => {
                    if (item.id === id) {
                        return { ...item, active: !item.active };
                    }
                    return item;
                });
                draft.data = lista;
                break;
            }
            default:
                return state;
        }
    });
}
