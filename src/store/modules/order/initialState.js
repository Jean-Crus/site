const initialState = {
    loading: false,
    data: [],
    order: 'course',
};

export default initialState;
