export function getOrdersRequest() {
    return { type: '@order/GET_ORDERS_REQUEST' };
}

export function getOrdersSuccess(data) {
    return { type: '@order/GET_ORDERS_SUCCESS', payload: { data } };
}

export function expandTable(id) {
    return { type: '@order/EXPAND_TABLE', payload: { id } };
}
