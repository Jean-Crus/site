const initialState = {
    loading: true,
    loadingCourse: true,
    ativo: 0,
    typeCourse: [
        { id: 0, name: "Ativos", checked: false },
        {
            id: 1,
            name: "Expirados",
            checked: false
        },
        {
            id: 2,
            name: "Aguardando pagamento",
            checked: false
        }
    ],
    data: [],
    courseExpired: [],
    profile: [],
    course: [],
    modules: [],
    lesson: []
};

export default initialState;
