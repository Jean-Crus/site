export function getMyCoursesRequest() {
    return {
        type: "@user/GET_MYCOURSES_REQUEST"
    };
}

export function getMyCoursesSuccess(myCourses) {
    return {
        type: "@user/GET_MYCOURSES_SUCCESS",
        payload: { myCourses }
    };
}

export function getOneCourseRequest(id) {
    return {
        type: "@user/GET_ONECOURSE_REQUEST",
        payload: { id }
    };
}

export function getOneCourseSuccess(course, modules) {
    return {
        type: "@user/GET_ONECOURSE_SUCCESS",
        payload: { course, modules }
    };
}

export function getLessonRequest(id) {
    return {
        type: "@user/GET_LESSON_REQUEST",
        payload: { id }
    };
}

export function getLessonSuccess(lesson) {
    return {
        type: "@user/GET_LESSON_SUCCESS",
        payload: { lesson }
    };
}

export function getFinishedRequest(id, curso, finished) {
    return {
        type: "@user/GET_FINISHED_REQUEST",
        payload: { id, curso, finished }
    };
}

export function getUserRequest() {
    return {
        type: "@user/GET_USER_REQUEST"
    };
}

export function getUserSuccess(user) {
    return {
        type: "@user/GET_USER_SUCCESS",
        payload: { user }
    };
}

export function expandList(id) {
    return {
        type: "@user/EXPAND_LIST",
        payload: { id }
    };
}

export function failRequest() {
    return {
        type: "@user/FAIL_REQUEST"
    };
}

export function getFinishedSuccess() {
    return {
        type: "@user/GET_FINISHED_SUCCESS"
        // payload: { id },
    };
}

export function updateUserRequest(name, email, tel, lastname, password) {
    return {
        type: "@user/UPDATE_USER_REQUEST",
        payload: { name, email, tel, lastname, password }
    };
}

export function updateUserSuccess(user) {
    return {
        type: "@user/UPDATE_USER_SUCCESS",
        payload: { user }
    };
}

export function getAtivos(idAtivo) {
    return { type: "@user/GET_ATIVOS", payload: { idAtivo } };
}

export function getExpirados() {
    return { type: "@user/GET_EXPIRADOS" };
}

export function getCourseExpiredRequest(id) {
    return { type: "@user/GET_COURSE_EXPIRED_REQUEST", payload: { id } };
}

export function getCourseExpiredSuccess(courseExpired) {
    return {
        type: "@user/GET_COURSE_EXPIRED_SUCCESS",
        payload: { courseExpired }
    };
}
