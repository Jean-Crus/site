import { produce } from "immer";
import initialState from "./initialState";

export default function user(state = initialState, action) {
    return produce(state, draft => {
        switch (action.type) {
            case "@auth/SIGN_IN_SUCCESS":
            case "@auth/SIGN_UP_SUCCESS":
                draft.profile = action.payload.profile;
                break;
            case "@user/GET_MYCOURSES_REQUEST":
                draft.loading = true;
                break;
            case "@user/GET_MYCOURSES_SUCCESS":
                draft.data = action.payload.myCourses;
                draft.loading = false;
                break;
            case "@auth/LOGOUT":
                draft.data = [];
                draft.profile = [];
                draft.loading = false;
                break;
            case "@user/GET_LESSON_REQUEST":
                draft.loading = true;
                break;
            case "@user/GET_LESSON_SUCCESS":
                draft.lesson = action.payload.lesson;
                draft.loading = false;
                break;
            case "@user/GET_ONECOURSE_REQUEST":
                draft.loadingCourse = true;
                break;
            case "@user/GET_ONECOURSE_SUCCESS":
                draft.course = action.payload.course;
                draft.modules = action.payload.modules;
                draft.loadingCourse = false;
                break;
            case "@user/EXPAND_LIST": {
                const { id } = action.payload;
                const lista = draft.modules.map(item => {
                    if (item.id === id) {
                        return { ...item, active: !item.active };
                    }
                    return item;
                });
                draft.modules = lista;
                break;
            }
            case "@user/UPDATE_USER_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@user/UPDATE_USER_SUCCESS": {
                draft.profile = action.payload.user;
                draft.loading = false;
                break;
            }
            case "@auth/LOGOUT_SUCCESS": {
                return initialState;
            }
            case "@user/FAIL_REQUEST": {
                draft.loading = false;
                break;
            }
            case "@user/GET_ATIVOS": {
                const { idAtivo } = action.payload;
                draft.ativo = idAtivo;
                break;
            }
            case "@user/GET_COURSE_EXPIRED_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@user/GET_COURSE_EXPIRED_SUCCESS": {
                draft.courseExpired = action.payload.courseExpired;
                draft.loading = false;
                break;
            }
            default:
                return state;
        }
    });
}
