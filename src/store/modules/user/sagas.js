import { takeLatest, call, put, all, select } from "redux-saga/effects";
import history from "@routes/history";
import { toast } from "react-toastify";
import { newError } from "@util/error";
import api from "@api";

import {
    getMyCoursesSuccess,
    getLessonSuccess,
    getFinishedSuccess,
    getOneCourseSuccess,
    getCourseExpiredSuccess,
    updateUserSuccess,
    failRequest
} from "./actions";

export function* getMyCourse() {
    try {
        const token = yield select(state => state.auth.token);
        const response = yield call(api.get, "users/teams", {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        yield put(getMyCoursesSuccess(response.data.data));
    } catch (error) {
        yield newError(error.response);
    }
}

export function* getLesson({ payload: { id } }) {
    try {
        if (id === "finish" || id === "0") {
            yield put(failRequest());
        } else {
            // const token = yield select(state => state.auth.token);
            const response = yield call(api.get, `lessons/${id}`, {
                // headers: {
                //     Authorization: `Bearer ${token}`
                // }
            });
            const { data } = response;
            yield put(getLessonSuccess(data.data));
        }
    } catch (error) {
        yield newError(error.response);
    }
}

export function* getFinished({ payload: { id, curso, finished } }) {
    try {
        const response = yield call(api.post, `/users/lessons/finished/${id}`);
        const { data } = response;
        yield put(getFinishedSuccess());
        if (!finished) {
            yield toast.info("Aula finalizada com sucesso!");
        }
        yield history.push(
            `/curso-andamento/${curso}/${data.data.next_lesson}`
        );
    } catch (error) {
        yield newError(error.response);
    }
}
export function* getOneCourse({ payload: { id } }) {
    try {
        // const token = yield select(state => state.auth.token);
        const response = yield call(api.get, `users/teams/${id}`, {
            // headers: {
            //     Authorization: `Bearer ${token}`
            // }
        });
        const {
            data: { data }
        } = response;
        const modules = response.data.data.modules.map(item => {
            return { ...item, active: false };
        });
        yield put(getOneCourseSuccess(data, modules));
    } catch (error) {
        yield newError(error.response, id);
        // history.push('/404');
    }
}

export function* getUser() {
    try {
        // const token = yield select(state => state.auth.token);
        const id = yield select(state => state.user.profile.id);
        const response = yield call(api.get, `users/${id}`, {
            // headers: {
            //     Authorization: `Bearer ${token}`
            // }
        });
    } catch (error) {}
}

export function* updateUser({
    payload: { name, email, tel, lastname, password }
}) {
    try {
        const id = yield select(state => state.user.profile.id);

        const response = yield call(api.put, `/users/${id}`, {
            name,
            email,
            telephone: tel,
            lastname,
            password
        });

        yield put(updateUserSuccess(response.data));
        toast.success("Dados atualizados com sucesso!");
    } catch (error) {
        yield newError(error.response);
    } finally {
        yield put(failRequest());
    }
}

export function* getCourseExpired({ payload: { id } }) {
    try {
        // const token = yield select(state => state.auth.token);
        const response = yield call(api.get, `users/teams/expired/${id}`, {
            // headers: {
            //     Authorization: `Bearer ${token}`
            // }
        });
        yield put(getCourseExpiredSuccess(response.data.message.data));
    } catch (error) {
        yield newError(error.response);
    }
}

export default all([
    takeLatest("@user/GET_MYCOURSES_REQUEST", getMyCourse),
    takeLatest("@user/GET_LESSON_REQUEST", getLesson),
    takeLatest("@user/GET_FINISHED_REQUEST", getFinished),
    takeLatest("@user/GET_ONECOURSE_REQUEST", getOneCourse),
    takeLatest("@user/UPDATE_USER_REQUEST", updateUser),
    takeLatest("@user/GET_USER_REQUEST", getUser),
    takeLatest("@user/GET_COURSE_EXPIRED_REQUEST", getCourseExpired)
]);
