const initialState = {
    loading: false,
    categoria: [],
    nivel: [],
    price: {
        min: 0,
        max: 1000
    },
    limit: [0, 1000],
    selected: [],
    historyFilter: {
        categoriaHistory: "",
        nivelHistory: "",
        maxPriceHistory: "",
        minPriceHistory: "",
        order: "",
        name: ""
    }
};

export default initialState;
