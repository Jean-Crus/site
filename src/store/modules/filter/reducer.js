import produce from "immer";
import initialState from "./initialState";

export default function filterCurso(state = initialState, action) {
    return produce(state, draft => {
        switch (action.type) {
            case "@filter/GET_FILTER_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@filter/GET_FILTER_SUCCESS": {
                const { categoria, nivel } = action.payload;
                draft.categoria = categoria;
                draft.nivel = nivel;
                draft.loading = false;
                break;
            }
            case "@filter/SELECT_FILTER_NIVEL": {
                const { name } = action.payload;
                const lista = draft.nivel.map(item => {
                    if (item.id === name) {
                        return { ...item, checked: !item.checked };
                    }
                    return item;
                });
                draft.nivel = lista;
                break;
            }
            case "@filter/SELECT_FILTER_CATEGORIA": {
                const { name } = action.payload;
                const lista = draft.categoria.map(item => {
                    if (item.id === name) {
                        return { ...item, checked: !item.checked };
                    }
                    return item;
                });
                draft.categoria = lista;
                break;
            }
            case "@filter/ORDER_FILTER_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@filter/ORDER_FILTER_SUCCESS": {
                const { order } = action.payload;
                draft.historyFilter = {
                    ...draft.historyFilter,
                    order
                };
                draft.loading = false;
                break;
            }
            case "@filter/GET_NAME_FILTER_REQUEST": {
                draft.loading = true;
                break;
            }
            case "@filter/GET_NAME_FILTER_SUCCESS": {
                const { filterName } = action.payload;
                draft.historyFilter = {
                    ...draft.historyFilter,
                    name: filterName
                };
                draft.loading = false;
                break;
            }
            case "@filter/SELECT_PRICE": {
                const { price } = action.payload;
                draft.price = price;
                break;
            }
            // case "@filter/REMOVE_ALL_REQUEST": {
            //     draft.loading = true;
            //     break;
            // }
            case "@filter/SEND_FILTER_SUCCESS": {
                const {
                    categoria,
                    nivel,
                    price,
                    categoriaHistory,
                    nivelHistory,
                    maxPriceHistory,
                    minPriceHistory
                } = action.payload;
                draft.historyFilter = {
                    ...draft.historyFilter,
                    categoriaHistory,
                    nivelHistory,
                    maxPriceHistory,
                    minPriceHistory
                };
                draft.selected = { categoria, nivel, price };
                break;
            }
            case "@filter/REMOVE_FILTER_SUCCESS": {
                const { removed, selected, query, tipo } = action.payload;
                if (tipo === "categoria") {
                    draft.selected.categoria = selected;
                    draft.categoria = removed;
                    draft.historyFilter = {
                        ...draft.historyFilter,
                        categoriaHistory: query
                    };
                }
                if (tipo === "nivel") {
                    draft.selected.nivel = selected;
                    draft.nivel = removed;
                    draft.historyFilter = {
                        ...draft.historyFilter,
                        nivelHistory: query
                    };
                }
                if (tipo === "price") {
                    draft.selected.price = selected;
                    draft.price = removed;
                    draft.historyFilter = {
                        ...draft.historyFilter,
                        maxPriceHistory: query.max,
                        minPriceHistory: query.min
                    };
                }
                break;
            }
            case "@filter/REMOVE_ALL_SUCCESS": {
                return initialState;
            }
            default:
                return state;
        }
    });
}
