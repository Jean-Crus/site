import { call, select, put, all, takeLatest } from "redux-saga/effects";

import history from "@routes/history";
import api from "@api";
import { getFiltersSuccess } from "./actions";

function* getFilters() {
    try {
        const responseNiv = yield call(api.get, `/teams/difficulties`);

        const responseCat = yield call(api.get, `/teams/categories`);

        const categoriaRedux = yield select(
            state => state.filterCurso.categoria
        );
        const nivelRedux = yield select(state => state.filterCurso.nivel);

        const categoria = responseCat.data.data.map((cat, index) => {
            if (categoriaRedux.length > 0) {
                return { ...cat, checked: categoriaRedux[index].checked };
            }
            return { ...cat, checked: false };
        });
        const nivel = responseNiv.data.data.map((niv, index) => {
            if (nivelRedux.length > 0) {
                return { ...niv, checked: nivelRedux[index].checked };
            }
            return { ...niv, checked: false };
        });
        yield put(getFiltersSuccess(categoria, nivel));
    } catch (err) {
        history.push("/404");
    }
}

export default all([takeLatest("@filter/GET_FILTER_REQUEST", getFilters)]);
