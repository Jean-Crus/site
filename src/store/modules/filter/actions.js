export function filterCursos(id) {
    return { type: "@curso/GET_CURSOS_PRINCIPAL_REQUEST", payload: { id } };
}

export function getFiltersRequest() {
    return { type: "@filter/GET_FILTER_REQUEST" };
}

export function getFiltersSuccess(categoria, nivel) {
    return {
        type: "@filter/GET_FILTER_SUCCESS",
        payload: { categoria, nivel }
    };
}

export function orderFilterRequest(order) {
    return {
        type: "@filter/ORDER_FILTER_REQUEST",
        payload: { order }
    };
}

export function orderFilterSuccess(order) {
    return {
        type: "@filter/ORDER_FILTER_SUCCESS",
        payload: { order }
    };
}

export function selectFilterNivel(name) {
    return {
        type: "@filter/SELECT_FILTER_NIVEL",
        payload: { name }
    };
}

export function selectFilterCategoria(name) {
    return {
        type: "@filter/SELECT_FILTER_CATEGORIA",
        payload: { name }
    };
}

export function selectPrice(price) {
    return {
        type: "@filter/SELECT_PRICE",
        payload: { price }
    };
}

export function sendFilterRequest(filter) {
    return { type: "@filter/SEND_FILTER_REQUEST", payload: { filter } };
}
export function sendFilterSuccess(
    categoria,
    nivel,
    price,
    categoriaHistory,
    nivelHistory,
    maxPriceHistory,
    minPriceHistory
) {
    return {
        type: "@filter/SEND_FILTER_SUCCESS",
        payload: {
            categoria,
            nivel,
            price,
            categoriaHistory,
            nivelHistory,
            maxPriceHistory,
            minPriceHistory
        }
    };
}

export function removeFilterRequest(filter, id) {
    return {
        type: "@filter/REMOVE_FILTER_REQUEST",
        payload: { filter, id }
    };
}
export function removeFilterSuccess(removed, selected, query, tipo) {
    return {
        type: "@filter/REMOVE_FILTER_SUCCESS",
        payload: { removed, selected, query, tipo }
    };
}

export function removeAllRequest() {
    return {
        type: "@filter/REMOVE_ALL_REQUEST"
    };
}

export function removeAllSuccess() {
    return {
        type: "@filter/REMOVE_ALL_SUCCESS"
    };
}

export function getNameFilterRequest(filterName) {
    return {
        type: "@filter/GET_NAME_FILTER_REQUEST",
        payload: { filterName }
    };
}

export function getNameFilterSuccess(filterName) {
    return {
        type: "@filter/GET_NAME_FILTER_SUCCESS",
        payload: { filterName }
    };
}
