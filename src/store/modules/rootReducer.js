import { combineReducers } from 'redux';
import curso from './curso/reducer';
import cart from './cart/reducer';
import cursoDetail from './cursoDetail/reducer';
import app from './app/reducer';
import auth from './auth/reducer';
import user from './user/reducer';
import checkout from './checkout/reducer';
import depoiments from './depoiments/reducer';
import filterCurso from './filter/reducer';
import order from './order/reducer';

export default combineReducers({
    curso,
    cart,
    cursoDetail,
    app,
    auth,
    checkout,
    user,
    depoiments,
    filterCurso,
    order,
});
