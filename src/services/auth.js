export const TOKEN_KEY = "@tesis-token";

const getToken = () => localStorage.getItem(TOKEN_KEY);

export default getToken;
