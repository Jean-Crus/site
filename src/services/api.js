import axios from "axios";
import getToken from "./auth";

const host =
    // "http://192.168.100.19:8888/";
    // "https://api.professorcarlosandre.com";
    "https://api-stg.tesis.app";

export const tokenJavascript = `${host}/pagseguro/javascript`;

const api = axios.create({
    baseURL: `${host}/api/`
});

api.interceptors.request.use(async config => {
    const token = getToken();

    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
});

export const checkout = axios.create({
    baseURL: `${host}/pagseguro`
});

export const checkoutIugu = axios.create({
    baseURL: `https://cors-anywhere.herokuapp.com/https://api.iugu.com/v1/`
});

export default api;
