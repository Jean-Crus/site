import React from 'react';

const CardAbout = ({content}, state) => {

    const {topTitle, title, title2, link, type, linkMidia} = content[0];

    return (
        <div className="card-about">
            <h2>{topTitle}</h2>
            <div className="card-body-about">
                <div className="row">
                    <div className="align-items-center col-sm-5 d-flex">
                        <div className="content">
                            <h3>{title}{title2 && <span>{title2}</span>}</h3>
                            <a href={link}>Ver Cursos</a>
                        </div>
                    </div>
                    <div className="col-sm-7">
                        <div className="midia">
                            {
                                type == 'video' &&
                                <video src={linkMidia} controls></video>                           
                            }
                            {
                                type == 'image' &&
                                <img src={linkMidia} alt={topTitle}/>
                            }
                            {
                                type == 'iframe' &&
                                {linkMidia}
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CardAbout;