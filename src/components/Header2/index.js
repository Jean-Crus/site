import React from 'react';
import { Link } from 'react-router-dom';
import { logo, brasil } from '@images';

const Header2 = ({history}, props, state) => {
    
    state = {
        menu:[
            {
                nome: 'Home',
                link: '/'
            },
            {
                nome: 'Cursos',
                link: '/cursos'
            },
            {
                nome: 'Eventos',
                link: '/eventos'
            },
            {
                nome: 'Sobre',
                link: '/sobre'
            },
            {
                nome: 'Contato',
                link: '/contato'
            }
        ]
    }

    return (
        <div>
            <div className="second-header">
                <header className="tr-header-nav container">
                    <div className="logo-full">
                        <img src={logo} alt="Professor Carlos André" />
                    </div>
                    <div className="itens-menu">
                        <ul className="structure">
                            {state.menu.map((item, index) => ( 
                                <li key={index} className={history.pathname == item.link ? 'itens active' : 'itens'}>
                                    <Link to={item.link}>{item.nome}</Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                    <div className="search">
                        <form action="pages/search_results.php">
                            <div className="input-group">
                                <input type="text" className="form-control" placeholder="Pesquisar" aria-label="Recipient's username"
                                    aria-describedby="basic-addon2" />
                                <div className="input-group-append">
                                    <button className="btn tr-search" type="button"></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="add-card">
                        <Link to="/carrinho">
                            <span className="icon-card"></span>
                        </Link>
                    </div>
                    <div className="login">
                        <div className="btn-group">
                            <button type="button" className="btn tr-primary small dropdown-toggle" id="login_create">
                                Entrar/Criar conta
                            </button>
                            <div className="dropdown-menu">
                                <div className="dropdown-divider op-0"></div>
                                <Link className="dropdown-item" to="/login">Entrar</Link>
                                <div className="dropdown-divider"></div>
                                <Link className="dropdown-item" to="/create-account">Criar Conta</Link>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        </div>
    );
};

export default Header2;
