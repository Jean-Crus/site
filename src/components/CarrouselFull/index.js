import React from 'react';
import Slider from "react-slick";

import {
	CardProductSimple
} from '@components';

const CarrouselFull = ({content,}, state) => {

    var settings = {
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 10000,
        speed: 800,
        arrows: false,
        slidesToShow: content.quant,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '200px'
      };

    return (
        <div id="carrousel-full">
            <h2>{content.title}</h2>
            <Slider {...settings}>
            { content.itens.map(item => 
                (
                    <div key={Math.random()}>
                        <CardProductSimple content={item} />
                    </div>
                )
            )}
            </Slider>
        </div>
    );
};

export default CarrouselFull;