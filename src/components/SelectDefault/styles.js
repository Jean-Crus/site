import styled from "styled-components";
import { TextField } from "@material-ui/core";

export const Text = styled(TextField)`
    .MuiFormLabel-root.Mui-focused {
        color: #ef7300;
    }
    .MuiFilledInput-underline:hover:before {
        border: transparent;
    }
    .MuiFilledInput-underline:after {
        border-bottom: transparent;
    }
    .MuiFormLabel-root {
        color: #ef7300;
        font-family: Montserrat;
    }
    .MuiSelect-select {
        font-weight: bold;
    }
    .MuiFilledInput-root {
        background-color: #fff;
        border: 1px solid #ccc;
        border-radius: 6px;
    }
    .MuiFilledInput-underline:before {
        border-bottom: transparent;
    }
    .MuiFilledInput-root:hover {
        background-color: #fff;
    }
    .MuiInputBase-input {
        font-family: Montserrat;
    }
    .MuiFilledInput-root.Mui-focused {
        background-color: #fff;
    }
    .MuiFormLabel-root.Mui-focused {
        color: #ef7300;
    }
    .MuiSelect-icon {
        color: #ef7300;
    }
    .MuiInputBase-root {
        font-family: Montserrat;
    }
    .MuiFormLabel-root {
        color: #ef7300;
    }
    .MuiSelect-select:focus {
        background-color: transparent;
    }
`;
