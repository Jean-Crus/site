import React, { useEffect } from "react";
import { TextField, MenuItem } from "@material-ui/core";
import { formatPrice } from "@util/format";
import { Text } from "./styles";

const SelectDefault = ({
    label,
    campos,
    onChange,
    value = "",
    textNull,
    payment,
    ...props
}) => {
    return (
        <Text
            select
            fullWidth
            label={!!label && label}
            variant="filled"
            onChange={onChange}
            value={value}
            {...props}
        >
            {!!textNull && <MenuItem value="">{textNull}</MenuItem>}
            {!!campos &&
                !payment &&
                campos.map(camp => {
                    return (
                        !camp.checked && (
                            <MenuItem key={camp.id} value={camp.id}>
                                {camp.name}
                            </MenuItem>
                        )
                    );
                })}
            {!!campos &&
                payment &&
                campos.map(camp => {
                    return (
                        <MenuItem key={camp.quantity} value={camp.quantity}>
                            {camp.quantity}x de{" "}
                            {formatPrice(camp.installmentAmount)}
                        </MenuItem>
                    );
                })}
        </Text>
    );
};

export default SelectDefault;
