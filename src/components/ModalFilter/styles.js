import styled from "styled-components";
import { Dialog } from "@material-ui/core";

export const Container = styled.div``;

export const Context = styled(Dialog)`
    .MuiDialog-paperFullScreen {
        height: 90%;
        border-radius: 12px;
        padding: 10px 30px;
        overflow-x: hidden;
    }
    .MuiPaper-elevation4 {
        box-shadow: none;
    }
    .MuiAppBar-colorPrimary {
        color: #1f1f1f;
        background: #fff;
    }
    .MuiGrid-grid-sm-3,
    .MuiGrid-grid-sm-6 {
        display: flex;
    }
    .MuiListItem-gutters {
        padding: 0;
    }
    .MuiIconButton-root {
        padding: 0;
    }
    .MuiGrid-container {
        padding: 20px 0;
    }
    .last-grid {
        justify-content: flex-end;
        button {
            border: none;
            background-color: transparent;
            font-family: Montserrat;
            font-size: 0.8rem;
            font-weight: bold;
            -webkit-text-decoration: underline;
            text-decoration: underline;
        }
    }
    .mid-grid {
        justify-content: center;
        font-family: Montserrat;
        font-weight: bold;
        font-size: 1.6rem;
    }
    .button-space {
        width: 100%;
        display: flex;
        justify-content: center;
    }
    .MuiSvgIcon-root {
        font-size: 1.2rem;
        color: #ef7300;
    }
    .text {
        font-family: Montserrat;
        font-size: 0.8rem;
    }
`;
