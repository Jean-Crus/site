import React, { useState, useEffect } from "react";

import { useSelector, useDispatch } from "react-redux";

import * as FilterActions from "@store/modules/filter/actions";

import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import { Grid } from "@material-ui/core";
import {
    SelectDefault,
    SearchDefault,
    RangeFilterDefault,
    ButtonTriade,
    TagFilter,
    Loading
} from "@components";
import { Container, Context } from "./styles";

const useStyles = makeStyles(theme => ({
    appBar: {
        position: "relative"
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1
    }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog({ open, handleClose }) {
    const classes = useStyles();
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(FilterActions.getFiltersRequest());
    }, [dispatch]);

    const nivel = useSelector(state => state.filterCurso.nivel);
    const categoria = useSelector(state => state.filterCurso.categoria);

    const loading = useSelector(state => state.curso.loading);
    const price = useSelector(state => state.filterCurso.price);
    const limit = useSelector(state => state.filterCurso.limit);

    const handlePrice = value => {
        dispatch(FilterActions.selectPrice(value));
    };

    const handleChangeNivel = event => {
        dispatch(FilterActions.selectFilterNivel(event.target.value));
    };

    const handleChangeCategoria = event => {
        dispatch(FilterActions.selectFilterCategoria(event.target.value));
    };
    const handleRemoveNivel = (filter, name) => {
        dispatch(FilterActions.selectFilterNivel(name));
    };

    const handleRemoveCategoria = (filter, name) => {
        dispatch(FilterActions.selectFilterCategoria(name));
    };

    const sendFilter = () => {
        dispatch(FilterActions.sendFilterRequest("filter"));
        handleClose();
    };
    return (
        <Container>
            <Context
                fullScreen
                open={open}
                onClose={handleClose}
                TransitionComponent={Transition}
            >
                <AppBar className={classes.appBar}>
                    <Grid container>
                        <Grid item sm={3} md={3} xs={3}>
                            <IconButton
                                color="inherit"
                                onClick={handleClose}
                                aria-label="close"
                            >
                                <CloseIcon />
                            </IconButton>
                        </Grid>
                        <Grid item sm={6} md={6} xs={6} className="mid-grid">
                            Filtro
                        </Grid>
                        <Grid item sm={3} md={3} xs={3} className="last-grid">
                            <button
                                type="button"
                                onClick={() =>
                                    dispatch(FilterActions.removeAllRequest())
                                }
                            >
                                LIMPAR
                            </button>
                        </Grid>
                    </Grid>
                </AppBar>
                {loading ? (
                    <Loading />
                ) : (
                    <List>
                        {/* <ListItem className="text">Pesquisa</ListItem>
                    <ListItem className="mb-4">
                        <SearchDefault label="O que procura?" />
                    </ListItem> */}
                        <ListItem className="text">Nível</ListItem>
                        <ListItem className="mb-4">
                            <SelectDefault
                                label="Nivel"
                                campos={nivel}
                                onChange={handleChangeNivel}
                                textNull="Selecione vários níveis"
                            />
                        </ListItem>
                        <ListItem className="mb-4">
                            <TagFilter
                                onClick={handleRemoveNivel}
                                selected={{ nivel }}
                            />
                        </ListItem>
                        <Divider className="mb-2" />
                        <ListItem className="text mb-5">Preço</ListItem>
                        <ListItem>
                            <RangeFilterDefault
                                maxValue={limit[1]}
                                minValue={limit[0]}
                                initial={price}
                                onChange={handlePrice}
                            />
                        </ListItem>
                        <Divider className="mt-5 mb-2" />
                        <ListItem className="text">Categoria</ListItem>
                        <ListItem className="mb-4">
                            <SelectDefault
                                label="Categoria"
                                campos={categoria}
                                onChange={handleChangeCategoria}
                                textNull="Selecione várias categorias"
                            />
                        </ListItem>
                        <ListItem className="mb-4">
                            <TagFilter
                                onClick={handleRemoveCategoria}
                                selected={{ categoria }}
                            />
                        </ListItem>
                        <Divider className="mb-2" />
                        <ListItem className="button-space">
                            <ButtonTriade
                                button="Modelo 02"
                                cor="Modelo 02"
                                letterColor="Modelo 02"
                                type="button"
                                text="Filtrar"
                                onClick={sendFilter}
                                readOnly
                            />
                        </ListItem>
                    </List>
                )}
            </Context>
        </Container>
    );
}
