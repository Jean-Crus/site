import React, { useState } from "react";
import "react-quill/dist/quill.bubble.css";
import { Quill } from "react-quill";
import ReactHtmlParser from "react-html-parser";
import { QuillContainer } from "./styles";

const QuillEditor = ({
    value,
    id,
    campo,
    className,
    onChange,
    readOnly,
    valueHighlight = ""
}) => {
    const [textValue, setTextValue] = useState({
        text: value,
        textValue: valueHighlight
    });
    const Font = Quill.import("formats/font");
    Font.whitelist = ["Lobster", "Montserrat", "Playfair", "Lato", "Roboto"];
    Quill.register(Font, true);

    const modules = {
        toolbar: [
            [{ header: [1, 2, 3, 4, 5, 6, false] }],

            ["bold", "italic", "underline", "strike"],
            [
                { list: "ordered" },
                { list: "bullet" },
                { indent: "-1" },
                { indent: "+1" }
            ],
            [{ align: [] }],
            [{ color: [] }, { background: [] }], // dropdown with defaults from theme
            [
                {
                    font: [
                        "Lobster",
                        "Montserrat",
                        "Playfair",
                        "Lato",
                        "Roboto"
                    ]
                }
            ],
            ["link"],
            ["clean"]
        ]
    };

    const formats = [
        "header",
        "bold",
        "italic",
        "underline",
        "strike",
        "blockquote",
        "list",
        "bullet",
        "indent",
        "link",
        "align",
        "color",
        "font",
        "background"
    ];

    return (
        <QuillContainer
            theme="bubble"
            value={textValue.text}
            modules={modules}
            formats={formats}
            className={className}
            readOnly={readOnly}
            onChange={(e, d, s, text) => {
                setTextValue({ text: e, textValue: text.getText() });
            }}
            onBlur={(e, s, b) =>
                onChange(textValue.text, campo, id, textValue.textValue)
            }
        />
    );
};

export default QuillEditor;
