import styled from "styled-components";
import ReactQuill from "react-quill";

export const QuillContainer = styled(ReactQuill)`
    .ql-font-Montserrat {
        font-family: Montserrat;
    }
    .ql-font-Lato {
        font-family: Lato;
    }
    .ql-font-Roboto {
        font-family: Roboto;
    }
    .ql-font-Playfair {
        font-family: Playfair;
    }
    .ql-font-Lobster {
        font-family: Lobster;
    }

    .ql-bubble {
        .ql-font {
            span[data-value="Lobster"] {
                &::before {
                    content: "Lobster";
                }
            }
            span[data-value="Lato"] {
                &::before {
                    content: "Lato";
                }
            }
            span[data-value="Playfair"] {
                &::before {
                    content: "Playfair";
                }
            }
            span[data-value="Roboto"] {
                &::before {
                    content: "Roboto";
                }
            }
            span[data-value="Montserrat"] {
                &::before {
                    content: "Montserrat";
                }
            }
        }
        .ql-picker-options {
            .ql-picker-item {
                padding: 0;
            }
        }
        .ql-header {
            width: 100px !important;
        }
        .ql-picker-label {
            &::before {
                padding-right: 20px;
            }
        }
        .ql-tooltip {
            z-index: 10;
            width: 508px !important;
            text-align: center;
            left: 20% !important;
        }
        .ql-editor {
            padding: 0 15px;
            h1 {
                font-size: 3rem;
            }
            h2 {
                font-size: 2.4rem;
            }
            h3 {
                font-size: 2rem;
            }
            h4 {
                font-size: 1.6rem;
            }
            h5 {
                font-size: 1.2rem;
            }
            .ql-editor {
                p,
                h1,
                h2,
                h3,
                h4,
                h5,
                h6,
                span {
                    cursor: ${props =>
                        props.readOnly ? "default" : "initial"};
                }
            }
        }
    }
    .ql-bubble {
        .ql-tooltip {
            z-index: 10;
        }
    }
`;
