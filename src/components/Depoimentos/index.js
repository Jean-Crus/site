import React from 'react';
import Slider from "react-slick";

const Depoimentos = ({content,}, state) => {

    var settings = {
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 10000,
        speed: 800,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1
      };

    return (
        <div id="depoimentos">
            <div className="container">
                <h2>O que dizem</h2>
                <Slider {...settings}>
                    { content.map(item => 
                        (
                            <div key={Math.random()}>
                                <div className="col-sm-8 offset-sm-2">
                                    <div className="depoimento-body">
                                        <div className="content">
                                            <p>{item.content}</p>
                                        </div>
                                        <div className="person-info">
                                            <strong>{item.name}</strong>
                                            <small>{item.country}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    )}
                </Slider>
            </div>
        </div>
    );
};

export default Depoimentos;