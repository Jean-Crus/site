import React from 'react';
import { Link } from 'react-router-dom';

const Pagination = () => {
    return (
        <div className="content container">
            <ul className="pagination">
                <li className="page-item previous disabled">
                    <Link className="page-link" to="#" tabindex="-1" />
                </li>
                <li className="page-item active">
                    <Link className="page-link" to="#">
                        1
                    </Link>
                </li>
                <li className="page-item ">
                    <Link className="page-link" to="#">
                        2
                    </Link>
                </li>
                <li className="page-item">
                    <Link className="page-link" to="#">
                        3
                    </Link>
                </li>
                <li className="page-item next">
                    <Link className="page-link " to="#" />
                </li>
            </ul>
        </div>
    );
};

export default Pagination;
