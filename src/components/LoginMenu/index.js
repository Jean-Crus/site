import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import { Link } from "react-router-dom";
import PersonIcon from "@material-ui/icons/Person";
import { ButtonMaterial, StyleMenu } from "./styles";

const StyledMenu = withStyles({
    paper: {
        border: "1px solid #d3d4d5",
        "& .MuiList-padding": {
            padding: "0"
        }
    }
})(props => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
        }}
        transformOrigin={{
            vertical: "top",
            horizontal: "center"
        }}
        {...props}
    />
));

const useStyle = makeStyles(theme => ({
    button: {
        textTransform: "none",
        fontFamily: "Montserrat, sans-serif",
        fontSize: "0.75rem"
    },
    icon: {
        marginLeft: theme.spacing(2)
    }
}));

export default function LoginMenu() {
    const { button, icon, text } = useStyle();
    const [anchorEl, setAnchorEl] = React.useState(null);

    function handleClick(event) {
        setAnchorEl(event.currentTarget);
    }

    function handleClose() {
        setAnchorEl(null);
    }

    return (
        <>
            <ButtonMaterial
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                onClick={handleClick}
                className={button}
            >
                Entrar/Criar conta
                <PersonIcon fontSize="small" className={icon} />
            </ButtonMaterial>
            <StyledMenu
                id="customized-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <Link to="/login">
                    <StyleMenu>
                        <ListItemText primary="Entrar" />
                    </StyleMenu>
                    <Divider />
                </Link>

                <Link to="/create-account">
                    <StyleMenu>
                        <ListItemText primary="Criar conta" />
                    </StyleMenu>
                </Link>
            </StyledMenu>
        </>
    );
}
