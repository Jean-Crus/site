import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import { MenuItem } from '@material-ui/core';

export const StyleMenu = styled(MenuItem)`
    .MuiButton-containedPrimary {
        color: black;
        text-transform: none;
        font-family: Montserrat, sans-serif;
        font-size: 0.75rem;
    }
    .MuiListItemText-root {
        padding: 0 40px;
    }
    .MuiTypography-body1 {
        color: black;
        text-transform: none;
        font-family: Montserrat, sans-serif;
        font-size: 0.75rem;
    }
    &:hover {
        background-color: transparent;
    }
    &:hover {
        span {
            color: ${props => props.theme.primary};
        }
    }
`;

export const Container = styled.div``;

export const ButtonMaterial = styled(Button)`
    background-color: ${props => props.theme.primary} !important;
    color: ${props => props.theme.letter} !important;
`;
