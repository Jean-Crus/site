import React, { useState } from 'react';
import Slim from '@slim/slim.react';
import { Container } from './styles';

const InsertImage = ({
    title,
    image,
    slimService,
    id,
    slimInit,
    campo,
    handleFunction,
}) => {
    const [openImage, setOpenImage] = useState({ active: false, id: '' });
    const handleImage = name => {
        if (openImage.id === name) {
            setOpenImage({ active: !openImage.active, id: name });
        } else setOpenImage({ active: true, id: name });
    };
    return (
        <>
            <Container>
                <div className="title">{title}</div>
                <div className="img-cabecalho">
                    <img
                        src="https://picsum.photos/id/12/700/400"
                        alt=""
                        onClick={() =>
                            handleFunction(
                                'https://picsum.photos/id/12/700/400',
                                campo,
                                id
                            )
                        }
                    />

                    <img
                        src="https://picsum.photos/id/10/700/400"
                        alt=""
                        onClick={() =>
                            handleFunction(
                                'https://picsum.photos/id/10/700/400',
                                campo,
                                id
                            )
                        }
                    />

                    <img
                        src="https://picsum.photos/id/27/700/400"
                        alt=""
                        onClick={() =>
                            handleFunction(
                                'https://picsum.photos/id/27/700/400',
                                campo,
                                id
                            )
                        }
                    />
                    <img
                        src="https://picsum.photos/id/76/700/400"
                        alt=""
                        onClick={() =>
                            handleFunction(
                                'https://picsum.photos/id/76/700/400',
                                campo,
                                id
                            )
                        }
                    />
                    <button
                        type="button"
                        className="plus"
                        onClick={() => handleImage(id)}
                    >
                        {openImage.active && openImage.id === id ? '-' : '+'}
                    </button>
                </div>
            </Container>
            {openImage.active && openImage.id === id ? (
                <Slim
                    push
                    download
                    labelLoading="Carregando imagem..."
                    instantEdit
                    buttonEditTitle="Editar"
                    buttonRemoveTitle="Remover"
                    rotateButton={false}
                    ratio="free"
                    willTransform={(e, b) => {
                        return b(e);
                    }}
                    label="Clique para adicionar sua imagem"
                    forceSize={{ width: 700, height: 400 }}
                    minSize={{ width: 700, height: 400 }}
                    service={(formdata, progress, success, failure, slim) =>
                        slimService(
                            formdata,
                            progress,
                            success,
                            failure,
                            slim,
                            id
                        )
                    }
                    didInit={slimInit}
                >
                    <input type="file" name="foo" />
                </Slim>
            ) : null}
        </>
    );
};

export default InsertImage;
