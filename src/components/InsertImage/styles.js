import styled from 'styled-components';

export const Container = styled.section`
    .title {
        color: ${props => props.theme.letter};
        font-size: 14px;
        /* font-weight: bold; */
    }
    .img-cabecalho {
        display: flex;
        flex-wrap: wrap;
        img {
            cursor: pointer;
            width: 49px;
            height: 49px;
            border-radius: 5px;
            margin-right: 5px;
        }
    }
    .plus {
        background-color: transparent;
        width: 49px;
        height: 49px;
        border-radius: 5px;
        display: flex;
        font-size: 30px;
        color: ${props => props.theme.letter};
        cursor: pointer;
        align-items: center;
        justify-content: center;
        border: ${props => '2px solid #d2d2d2'};
        border-radius: 5px;
    }
`;
