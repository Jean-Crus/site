import React from "react";

const ContatoArea = ({ content }, state) => {
  return (
    <div id="contatos-area">
      <h2>{content.title}</h2>
      <div className="container d-flex justify-content-center">
        <div className="col-sm-10">
          <div className="d-flex justify-content-between row">
            <div className="col-sm-5">
              <div className="content">
                <h3>
                  {content.subTitle} <span>{content.subTitle}</span>
                </h3>
                <p>{content.description}</p>
                <img
                  src={content.image}
                  alt={content.title + " " + content.subTitle}
                />
              </div>
            </div>
            <div className="col-sm-5">
              <form action="">
                <input type="text" name="teste" id="teste" placeholder="Nome" />
                <input
                  type="email"
                  name="teste"
                  id="teste"
                  placeholder="Email"
                />
                <input
                  type="text"
                  name="teste"
                  id="teste"
                  placeholder="Telefone"
                />
                <input
                  type="textarea"
                  name="teste"
                  id="teste"
                  placeholder="Mensagem"
                />
                <button>Enviar</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContatoArea;
