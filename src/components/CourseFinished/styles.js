import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 256px;
    font-family: Montserrat;
    img {
        width: 89px;
        height: 77px;
    }
    div {
        margin-left: 20px;
        color: ${props => props.theme.letter2};
        font-weight: 300;
        font-size: 14px;
        &:first-child {
            color: #34b16f;
            font-size: 60px;
            font-weight: bold;
            line-height: 55px;
        }
        .frase {
            line-height: 20px;
            padding: 5px;
            text-align: center;
            span {
                color: #34b16f;
                font-weight: bold;
            }
            strong {
                font-weight: bold;
            }
        }
    }
    @media only screen and (max-width: 999px) {
        border: 1px solid #efefef;
        flex-direction: column;
        align-items: center;
        padding: 40px;
        height: auto;
        border-radius: 5px;
        background-color: #ffffff;
        div {
            text-align: center;
            margin: 0;
            &:first-child {
                color: #2bb26d;
                font-size: 32px;
            }
        }
    }
`;
