import React from "react";
import { useSelector } from "react-redux";
import { Container } from "./styles";

const CourseFinished = () => {
    const user = useSelector(state => state.user);
    return (
        <Container>
            <img
                src="https://image.flaticon.com/icons/svg/411/411779.svg"
                alt=""
            />
            <div>
                <div>
                    Parabéns, {user.profile.name} {user.profile.lastname}!
                </div>
                <div className="frase">
                    Você concluiu o curso <strong>{user.course.name}</strong>.
                    Seu aproveitamento foi de{" "}
                    <span>{user.course.user_progress}%.</span>
                </div>
            </div>
        </Container>
    );
};

export default CourseFinished;
