import styled from "styled-components";

import { currentHours } from "@images";

export const FixedTimer = styled.div`
    position: sticky;
    top: 90px;
    width: 80px;
    height: 100px;
    border-radius: 5px;
    background-color: ${props => {
        if (props.minutes === 1) {
            if (props.seconds === 0) {
                return "#ce2b2b";
            }
        }
        if (props.minutes < 1 && props.seconds <= 30) {
            if (props.seconds % 2 === 0) {
                return "#ce2b2b";
            }
            return props.theme.primary;
        }
        if (props.seconds % 3 === 0) return "#ce2b2b";
        return props.theme.primary;
    }};
    left: 90%;

    .content {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        width: 100%;
        height: 100%;

        .hours {
            display: block;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: center center;
            background-image: url(${currentHours});
            width: 40px;
            height: 50px;
        }
        .current-time {
            margin-bottom: 0px;
            font-size: 1rem;
            font-weight: bold;
            font-style: normal;
            font-stretch: 1.67;
            letter-spacing: normal;
            text-align: center;
            color: ${props => props.theme.letter};
        }
    }
    @media only screen and (max-width: 1280px) {
        display: none;
    }
`;

export const TimerMobile = styled.div`
    display: flex;
    /* margin-top: 10px; */
    padding: 10px;
    justify-content: center;
    align-items: center;
    background-color: ${props => props.theme.primary};
    font-family: Montserrat;
    font-size: 10px;
    color: ${props => props.theme.letter};
    background-color: ${props => {
        if (props.minutes === 1) {
            if (props.seconds === 0) {
                return "#ce2b2b";
            }
        }
        if (props.minutes < 1 && props.seconds <= 30) {
            if (props.seconds % 2 === 0) {
                return "#ce2b2b";
            }
            return props.theme.primary;
        }
        if (props.seconds % 3 === 0) return "#ce2b2b";
        return props.theme.primary;
    }} !important;
    @media only screen and (min-width: 1281px) {
        display: none;
    }
`;
