import React from "react";
import Countdown from "react-countdown-now";
import { Icon } from "@material-ui/core";
import { FixedTimer, TimerMobile } from "./styles";

const TimerPay = ({ mobile }) => {
    const renderer = ({ days, hours, minutes, seconds, completed }) => {
        if (completed) {
            // Render a completed state
            return (
                <FixedTimer
                    days={days}
                    hours={hours}
                    minutes={minutes}
                    seconds={seconds}
                    completed={completed}
                >
                    <div className="content">
                        <span className="hours" />
                        <p className="current-time">Completo</p>
                    </div>
                </FixedTimer>
            );
        }
        // Render a countdown
        return (
            <FixedTimer
                days={days}
                hours={hours}
                minutes={minutes}
                seconds={seconds}
                completed={completed}
            >
                <div className="content">
                    <span className="hours" />
                    <p className="current-time">
                        {minutes}:{seconds}
                    </p>
                </div>
            </FixedTimer>
        );
    };
    const rendererMobile = ({ days, hours, minutes, seconds, completed }) => {
        if (completed) {
            // Render a completed state
            return (
                <TimerMobile
                    days={days}
                    hours={hours}
                    minutes={minutes}
                    seconds={seconds}
                    completed={completed}
                    className="encerrar-sessao"
                >
                    <span>ENCERRADO</span>
                </TimerMobile>
            );
        }
        // Render a countdown
        return (
            <TimerMobile
                days={days}
                hours={hours}
                minutes={minutes}
                seconds={seconds}
                completed={completed}
                className="encerrar-sessao"
            >
                <Icon className="far fa-clock" style={{ color: "#fff" }} />
                <span className="ml-2">
                    {minutes}:{seconds} para encerrar a sessão
                </span>
            </TimerMobile>
        );
    };
    return (
        <Countdown
            date={Date.now() + 60000 * 5}
            renderer={mobile ? rendererMobile : renderer}
            intervalDelay={0}
            precision={3}
        />
    );
};

export default TimerPay;
