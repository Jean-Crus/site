import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import {
    DrawerContainer,
    SearchBar,
    IconSearch,
    IconClose,
    InputText,
    ButtonSubmit,
    Login,
    LinkItem,
    IconMenu
} from "./styles";

const useStyles = makeStyles({
    list: {
        width: "100%"
    },
    fullList: {
        width: "auto"
    }
});

export default function TemporaryDrawer({ history }) {
    const classes = useStyles();
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false
    });

    const toggleDrawer = (side, open) => event => {
        if (
            event.type === "keydown" &&
            (event.key === "Tab" || event.key === "Shift")
        ) {
            return;
        }

        setState({ ...state, [side]: open });
    };

    const handleSubmit = value => {
        alert(JSON.stringify(value));
    };

    const sideList = side => (
        <div className={classes.list} role="presentation">
            {/* <List>
                <ListItem>
                    <SearchBar onSubmit={value => handleSubmit(value)}>
                        <InputText
                            type="text"
                            name="searchfield"
                            id=""
                            placeholder="Pesquisa"
                        />
                        <ButtonSubmit type="submit">
                            <IconSearch />
                        </ButtonSubmit>
                    </SearchBar>
                </ListItem>
            </List> */}
            <Divider />
            <List>
                {[
                    { name: "Home", link: "" },
                    {
                        name: "Cursos",
                        link: "cursos"
                    },
                    {
                        name: "Sobre",
                        link: "sobre"
                    },
                    // { name: "Depoimentos", link: "depoimentos" },
                    { name: "Contato", link: "contato" }
                ].map(menu => (
                    <LinkItem
                        key={menu.name}
                        to={`/${menu.link}`}
                        active={`/${menu.link}` === `${history.pathname}`}
                    >
                        <ListItem
                            onClick={toggleDrawer(side, false)}
                            onKeyDown={toggleDrawer(side, false)}
                        >
                            <ListItemText primary={menu.name} />
                        </ListItem>
                    </LinkItem>
                ))}
            </List>
            <List>
                <ListItem
                    onClick={toggleDrawer(side, false)}
                    onKeyDown={toggleDrawer(side, false)}
                >
                    <Login>
                        <LinkItem to="/login">
                            <span>Entrar/criar conta</span>
                        </LinkItem>
                    </Login>
                </ListItem>
            </List>
            <Divider />
            <List
                onClick={toggleDrawer(side, false)}
                onKeyDown={toggleDrawer(side, false)}
            >
                <ListItem button>
                    <IconClose fontSize="large" />
                </ListItem>
            </List>
        </div>
    );

    return (
        <div>
            <IconMenu fontSize="large" onClick={toggleDrawer("left", true)} />
            <DrawerContainer
                open={state.left}
                onClose={toggleDrawer("left", false)}
            >
                {sideList("left")}
            </DrawerContainer>
        </div>
    );
}
