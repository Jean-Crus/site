import styled from "styled-components";
import Drawer from "@material-ui/core/Drawer";
import SearchIcon from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close";
import { Link } from "react-router-dom";
import MenuIcon from "@material-ui/icons/Menu";

export const IconMenu = styled(MenuIcon)`
    cursor: pointer;
`;

export const DrawerContainer = styled(Drawer)`
    .MuiDrawer-paper {
        width: 100%;
    }
    .MuiPaper-root {
        background: linear-gradient(0deg, #012a50 0%, #0058a8 100%);
        color: ${props => props.theme.letter};
    }
    .MuiTypography-body1 {
        font-family: Montserrat, sans-serif;
        font-size: 13px;
    }
    .MuiDivider-root {
        background-color: ${props => props.theme.letter};
    }
    .MuiListItem-root {
        justify-content: center;
        text-align: center;
    }
`;

export const SearchBar = styled.form`
    width: 100%;
    max-width: 200px;
    height: 37px;
    display: flex;
    border: ${props => `1px solid ${props.theme.primary}`};
    border-radius: 5px;
    align-items: center;
    justify-content: space-between;
    .MuiSvgIcon-root {
        width: 37px;
        padding-left: 2px;
        height: 37px;
        border-radius: 5px;
        background-color: ${props => props.theme.primary};
        text-align: center;
    }
`;

export const InputText = styled.input`
    /* font-family: Montserrat, sans-serif; */
    color: #fff;
    background-color: transparent;
    padding-left: 10px;
    border: none;
    width: 100%;
    &:focus {
        outline: none;
    }
`;

export const IconSearch = styled(SearchIcon)`
    color: #fff;
`;

export const IconClose = styled(CloseIcon)`
    color: #fff;
`;

export const ButtonSubmit = styled.button`
    background-color: transparent;
    border: none;
    padding: 0;
`;

export const LinkItem = styled(Link)`
    color: ${props => props.theme.letter};
    &:hover {
        color: ${props => props.theme.primary};
    }
    &:focus {
        outline: none;
    }
    .MuiTypography-body1 {
        border-bottom: ${props =>
            props.active ? `2px solid ${props.theme.primary}` : "none"};
        color: ${props => (props.active ? props.theme.letter : "none")};
        font-weight: ${props => (props.active ? "bold" : "none")};
    }
`;

export const Login = styled.div`
    width: 100%;
    max-width: 131px;
    height: 40px;
    border-radius: 5px;
    background-color: transparent;
    border: ${props => `1px solid ${props.theme.primary}`};
    display: flex;
    justify-content: center;
    align-items: center;
    span {
        color: ${props => props.theme.letter};
        font-family: MontSerrat;
        font-size: 10px;
        font-weight: bold;
    }
`;
