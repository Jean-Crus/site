import styled from "styled-components";
import { Button } from "@material-ui/core";

export const ButtonWhite = styled(Button)`
    width: 200px;
    width: ${props => props.tamanho};
    margin: ${props => props.space} !important;
    height: 56px;
    text-transform: initial !important;
    padding: 18px 0 !important;
    .fas {
        color: ${props => {
            if (props.lettercolor === "Modelo 01") return "#486AAB";
            if (props.lettercolor === "Modelo 02") return "#fff";
            if (props.lettercolor === "Modelo 03") return "#5074B7";
            return props.lettercolor;
        }};
    }
    .fab {
        color: ${props => {
            if (props.lettercolor === "Modelo 01") return "#486AAB";
            if (props.lettercolor === "Modelo 02") return "#fff";
            if (props.lettercolor === "Modelo 03") return "#5074B7";
            return props.lettercolor;
        }};
    }
    .quill {
        border: ${props => (props.readOnly ? `none` : "")} !important;
        border-radius: 5px;
    }
    box-shadow: ${props =>
        props.cor === "Modelo 02" ? "none" : ""} !important;
    &:hover {
        box-shadow: ${props =>
            props.cor === "Modelo 02" ? "none" : ""}!important;
    }
    background: ${props => {
        if (props.cor === "Modelo 01")
            return `linear-gradient(0deg, #fff 0%, #fff 100%)`;
        if (props.cor === "Modelo 02")
            return `linear-gradient(0deg, #ef9300 0%, #ef7300 100%)`;
        if (props.cor === "Modelo 03")
            return `linear-gradient(0deg, transparent 0%, transparent 100%)`;
        if (props.cor === "whatsapp")
            return `linear-gradient(0deg, #01e675 0%, #01e675 100%)`;
        return `linear-gradient(0deg, ${props.cor} 0%, ${props.cor} 100%)`;
    }} !important;
    .ql-editor {
        text-align: center;
        p {
            cursor: pointer;
            font-family: Roboto;
            font-size: 13px;
            font-weight: bold;

            color: ${props => {
                if (props.lettercolor === "Modelo 01") return "#486AAB";
                if (props.lettercolor === "Modelo 02") return "#fff";
                if (props.lettercolor === "Modelo 02") return "#5074B7";
                return props.lettercolor;
            }};
        }
    }
`;
