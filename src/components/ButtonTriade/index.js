import React from "react";
import { Icon } from "@material-ui/core";
import { QuillEditor } from "@components";
import { ButtonWhite } from "./styles";

const ButtonTesis = ({
    className,
    text,
    icon,
    type,
    letterColor,
    style,
    onClick,
    button,
    readOnly = true,
    size,
    space,
    disabled
}) => {
    return (
        <ButtonWhite
            variant="contained"
            type={type}
            className={`${className}`}
            cor={button}
            lettercolor={letterColor}
            style={style && style}
            onClick={onClick}
            readOnly={readOnly}
            tamanho={size}
            disabled={disabled}
            space={space}
        >
            {icon && <Icon className={icon} />}
            {text ? (
                <QuillEditor
                    //   theme="bubble"
                    value={text}
                    // className="title"
                    // campo="title"
                    // id={slide.id}
                    readOnly={readOnly}
                    onChange={() => null}
                />
            ) : (
                ""
            )}
        </ButtonWhite>
    );
};

export default ButtonTesis;
