import React, { useEffect } from "react";
import * as CursosActions from "@store/modules/curso/actions";
import * as CartActions from "@store/modules/cart/actions";
import * as AppActions from "@store/modules/app/actions";

import { useDispatch, useSelector } from "react-redux";
import Slider from "react-slick";
import { CursosDestaque, Loading, QuillEditor } from "@components";
import { ArrowBack, ArrowForward, Container } from "./styles";

import { SliderWhite } from "../Parts/Home/SobreCurso/SobreCursoWhite/styles";

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <ArrowForward
            className={className}
            onClick={onClick}
            fontSize="large"
        />
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <ArrowBack className={className} onClick={onClick} fontSize="large" />
    );
}

export default function ListaCurso({
    relacionados,
    principal,
    cartCursos,
    destaque,
    title,
    readOnly
}) {
    const dispatch = useDispatch();

    useEffect(() => {
        if (cartCursos) {
            dispatch(CursosActions.getCursosCartRequest());
        }
        if (relacionados) {
            dispatch(CursosActions.getCursosRelacionadosRequest(relacionados));
        }
        if (destaque) {
            dispatch(CursosActions.getCursosDestaqueRequest());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch]);
    const curso = useSelector(state => state.curso.data);
    const loading = useSelector(state => state.curso.loading);

    const addCurso = id => {
        dispatch(CartActions.addToCartRequest(id));
    };

    const handleText = (event, campo, id, text) => {
        // dispatch(AppActions.changeTextHome1(event, campo, id, text));
    };

    const settings = {
        infinite: true,
        speed: 1000,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 3000,
        centerPadding: "10px",
        className: "center",
        centerMode: true,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        responsive: [
            {
                breakpoint: 9999,
                settings: {
                    slidesToShow: curso.length > 3 ? 3 : curso.length,
                    infinite: true,
                    slidesToScroll: 3,
                    centerPadding: "40px",
                    className: "center",
                    centerMode: true
                }
            },
            {
                breakpoint: 1215,
                settings: {
                    slidesToShow: curso.length > 3 ? 3 : curso.length,
                    infinite: true,
                    slidesToScroll: 3,
                    centerPadding: "50px",
                    className: "center",
                    centerMode: true
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: curso.length > 3 ? 3 : curso.length,
                    infinite: true,
                    slidesToScroll: 3,
                    centerPadding: "50px",
                    className: "center",
                    centerMode: true,
                    arrows: false
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: curso.length > 2 ? 2 : curso.length,
                    infinite: true,
                    slidesToScroll: 2,
                    centerPadding: "20px",
                    className: "center",
                    centerMode: true,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: curso.length > 1 ? 1 : curso.length,
                    slidesToScroll: 1,
                    centerPadding: "80px",
                    className: "center",
                    centerMode: true,
                    arrows: false
                }
            },
            {
                breakpoint: 508,
                settings: {
                    slidesToShow: curso.length > 1 ? 1 : curso.length,
                    slidesToScroll: 1,
                    centerPadding: "60px",
                    className: "center",
                    centerMode: true,
                    arrows: false
                }
            },
            {
                breakpoint: 430,
                settings: {
                    slidesToShow: curso.length > 1 ? 1 : curso.length,
                    slidesToScroll: 1,
                    centerPadding: "45px",
                    className: "center",
                    centerMode: true,
                    arrows: false
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: curso.length > 1 ? 1 : curso.length,
                    slidesToScroll: 1,
                    centerPadding: "30px",
                    className: "center",
                    centerMode: true,
                    arrows: false
                    // dots: true,
                }
            },
            {
                breakpoint: 370,
                settings: {
                    slidesToShow: curso.length > 1 ? 1 : curso.length,
                    slidesToScroll: 1,
                    centerPadding: "20px",
                    className: "center",
                    centerMode: true,
                    arrows: false
                    // dots: true,
                }
            },
            {
                breakpoint: 340,
                settings: {
                    slidesToShow: curso.length > 1 ? 1 : curso.length,
                    slidesToScroll: 1,
                    centerPadding: "10px",
                    className: "center",
                    centerMode: true,
                    arrows: false
                    // dots: true,
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: curso.length > 1 ? 1 : curso.length,
                    slidesToScroll: 1,
                    centerPadding: "7px",
                    className: "center",
                    centerMode: true,
                    arrows: false
                    // dots: true,
                }
            }
        ]
    };
    return loading ? (
        <Loading />
    ) : (
        <Container className="destaque" id="cursos-destaque">
            <div className="tr-cursos-destaque container pb-5">
                <div className="title-1 size-50 wh-600 black m-5">
                    {!!curso.length && (
                        <QuillEditor
                            theme="bubble"
                            value={title}
                            className="title"
                            campo="title"
                            id={1}
                            readOnly={readOnly}
                            onChange={handleText}
                        />
                    )}
                </div>

                <Slider {...settings}>
                    {curso.map(item => {
                        return (
                            <CursosDestaque
                                onClick={() => addCurso(item.id)}
                                link={item.link}
                                image={item.cover}
                                priceFormatted={item.priceFormatted}
                                price={item.product.price}
                                salePrice={item.product.sale_price}
                                category={!!item.category && item.category}
                                introduction={item.introduction}
                                description={item.description}
                                salePriceFormatted={item.salePriceFormatted}
                                title={item.name}
                                key={item.id}
                                id={item.id}
                                label={item.label}
                                hours={item.learning_time}
                                modules={item.modules}
                            />
                        );
                    })}
                </Slider>
            </div>
        </Container>
    );
}
