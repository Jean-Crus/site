import styled from "styled-components";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";

export const ArrowBack = styled(ArrowBackIosIcon)`
    color: black !important;
    opacity: 0.5;
    &:hover {
        opacity: 1;
    }
`;

export const ArrowForward = styled(ArrowForwardIosIcon)`
    color: black !important;
    opacity: 0.5;
    &:hover {
        opacity: 1;
    }
`;

export const Container = styled.div`
    .slick-list {
        margin: 0 30px;
        .slick-slide {
            display: flex;
            justify-content: center;
        }
    }
    .slick-next {
        right: 0;
    }
    .slick-prev {
        left: 0;
    }
    @media only screen and (max-width: 1201px) {
        .slick-list {
            margin: 0 10px;
        }
    }
`;
