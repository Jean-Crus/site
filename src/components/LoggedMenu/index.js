import React from "react";
import { useDispatch } from "react-redux";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import { Link } from "react-router-dom";
import PersonIcon from "@material-ui/icons/Person";
import * as AuthActions from "@store/modules/auth/actions";
import { Container, StyleMenu } from "./styles";

const StyledMenu = withStyles({
    paper: {
        border: "1px solid #d3d4d5",
        "& .MuiList-padding": {
            padding: "0"
        }
    }
})(props => (
    <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
        }}
        transformOrigin={{
            vertical: "top",
            horizontal: "center"
        }}
        {...props}
    />
));

const useStyle = makeStyles(theme => ({
    icon: {
        marginLeft: theme.spacing(2)
    }
}));

export default function LoggedMenu() {
    const dispatch = useDispatch();
    const { icon } = useStyle();
    const [anchorEl, setAnchorEl] = React.useState(null);

    function handleClick(event) {
        setAnchorEl(event.currentTarget);
    }

    function handleClose() {
        setAnchorEl(null);
    }

    return (
        <Container>
            <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                color="primary"
                onClick={handleClick}
            >
                Minha conta
                <PersonIcon fontSize="small" className={icon} />
            </Button>
            <StyledMenu
                id="customized-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <Link to="/conta">
                    <StyleMenu>
                        <ListItemText primary="Meus cursos" />
                    </StyleMenu>
                    <Divider />
                </Link>
                <Link to="/conta/meus-pedidos">
                    <StyleMenu>
                        <ListItemText primary="Meus pedidos" />
                    </StyleMenu>
                    <Divider />
                </Link>
                <Link to="/conta/configurar-conta">
                    <StyleMenu>
                        <ListItemText primary="Configurar conta" />
                    </StyleMenu>
                    <Divider />
                </Link>
                {/* <Link to="/conta/tutorial">
                    <StyleMenu>
                        <ListItemText primary="Tutorial" />
                    </StyleMenu>
                    <Divider />
                </Link> */}
                <StyleMenu
                    onClick={() => dispatch(AuthActions.logoutRequest())}
                >
                    <ListItemText primary="Sair" />
                </StyleMenu>
            </StyledMenu>
        </Container>
    );
}
