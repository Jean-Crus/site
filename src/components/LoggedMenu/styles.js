import styled from "styled-components";
import { MenuItem } from "@material-ui/core";

export const StyleMenu = styled(MenuItem)`
    .MuiButton-containedPrimary {
        color: black;
        text-transform: none;
        font-family: Montserrat, sans-serif;
        font-size: 0.75rem;
    }
    .MuiListItemText-root {
        padding: 0 4px;
    }
    .MuiTypography-body1 {
        color: black;
        text-transform: none;
        font-family: Montserrat, sans-serif;
        font-size: 0.75rem;
    }
    &:hover {
        background-color: transparent;
    }
    &:hover {
        span {
            color: ${props => props.theme.primary};
        }
    }
`;

export const Container = styled.div`
    button {
        background-color: #fff;
        border: ${props => `2px solid ${props.theme.primary}`};
        text-transform: none;
        font-family: Montserrat, sans-serif;
        font-size: 0.75rem;
    }
    .MuiButton-containedPrimary {
        color: #002a50;
    }
    .MuiButton-containedPrimary:hover {
        background-color: ${props => props.theme.primary};
    }
`;
