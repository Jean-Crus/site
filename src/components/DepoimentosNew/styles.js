import styled from "styled-components";
import Slider from "react-slick";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { Modal, Grid } from "@material-ui/core";
import ReactPlayer from "react-player";
import { darken } from "polished";
import Coverflow from "react-coverflow";

export const Container = styled.div`
    font-family: Montserrat;
    .size-50 {
        font-size: 30px !important;
    }
`;

export const FirstTab = styled.header`
    width: 100%;
    display: flex;
    position: relative;
    justify-content: center;
    padding: 40px 0;
    background-color: ${props => props.theme.background};
    .background {
        display: flex;
        position: relative;
        width: 100%;
        padding: 60px;
        max-width: 1200px;
        .text {
            display: flex;
            position: absolute;
            flex-direction: column;
            height: 400px;
            justify-content: center;
            max-width: 600px;
            line-height: 40px;
            .depoimentos {
                color: ${props => props.theme.letter};
            }
            .depoimentos-highlight {
                color: ${props => props.theme.letter};
                font-size: 40px;
                font-weight: bold;
            }
        }
        .img {
            width: 100%;
            display: flex;
            justify-content: flex-end;
            border-radius: 5px;
            img {
                box-shadow: -2px 2px 33px -2px rgba(0, 0, 0, 0.75);
                border-radius: 5px;
                height: 400px;
                width: 100%;
                max-width: 700px;
            }
        }
    }
    @media only screen and (max-width: 1201px) {
        display: none;
    }
`;

export const FirstTabMob = styled.header`
    display: none;
    @media only screen and (max-width: 1201px) {
        margin-top: 10px;
        width: 100%;
        display: flex;
        position: relative;
        justify-content: center;
        padding: 20px 0;
        background-color: ${props => props.theme.background};
        color: ${props => props.theme.letter};
        font-size: 30px;
        font-weight: bold;
    }
    @media only screen and (max-width: 427px) {
        font-size: 16px;
    }
`;

export const MenuCircle = styled.div`
    display: flex;
    justify-content: center;
    position: absolute;
    top: 95%;
    left: 50%;
    transform: translateX(-50%);
    align-items: center;
    height: 67px;
    width: 67px;
    border-radius: 67px;
    background-color: ${props => props.theme.primary};
    box-shadow: 0 10px 30px 0 rgba(0, 0, 0, 0.2);
`;

export const Arrow1 = styled.div`
    position: absolute;
    top: 30%;
    height: 12px;
    width: 12px;
    border: ${props => `solid ${props.theme.letter}`};
    border-width: 0 3px 3px 0;
    opacity: 0.5;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
`;

export const Arrow2 = styled.div`
    position: absolute;
    top: 50%;
    height: 12px;
    width: 12px;
    border: ${props => `solid ${props.theme.letter}`};
    border-width: 0 3px 3px 0;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
`;

export const SecondTab = styled.main`
    background-color: ${props => props.theme.letter};
    padding: 100px 60px;
    display: flex;
    width: 100%;
    justify-content: center;
    .container {
        width: 100%;
        max-width: 1200px;
        .title {
            color: ${props => props.theme.background};
            font-size: 30px;
            font-weight: bold;
            text-align: center;
        }
    }
    @media only screen and (max-width: 1201px) {
        padding: 40px 0;
    }

    @media only screen and (max-width: 427px) {
        .container {
            .title {
                font-size: 16px;
            }
        }
    }
`;

export const ArrowBack = styled(ArrowBackIosIcon)`
    margin-left: 5.8px;
    color: ${props => props.theme.letter};
    font-size: 15px !important;
`;

export const ArrowForward = styled(ArrowForwardIosIcon)`
    color: ${props => props.theme.letter};
    font-size: 15px !important;
`;

export const FirstSlider = styled(Slider)`
    justify-content: center;
    align-items: center;
    display: flex !important;
    .slick-list {
        max-width: 900px;
        margin: 0 30px;
        .slick-slide {
            display: flex;
            justify-content: center;
        }
    }
    .slick-next {
        right: 0;
    }
    .slick-prev {
        left: 0;
    }
    .slick-dots {
        display: flex !important;
        justify-content: center;
        align-items: center;
        li {
            display: flex !important;
            justify-content: center;
            align-items: center;
        }
        .slick-active {
            border-radius: 50%;
            border: 2px solid ${props => props.theme.primary};
        }
    }
    .dots {
        display: flex;
        justify-content: center;
        align-items: center;
        .circle {
            width: 4px;
            height: 4px;
            border-radius: 20px;
            background-color: ${props => props.theme.letter2};
        }
    }
    @media only screen and (max-width: 1201px) {
        .slick-list {
            max-height: 410px;
            padding: 0px !important;
        }
        .slick-track {
            .slick-slide {
                opacity: 0.5;
            }
            .slick-active {
                opacity: 1;
                background-color: #fff;
            }
            .slick-cloned {
                &:nth-child(1) {
                    max-width: 700px !important;
                }
            }
        }
    }
`;

export const CardSlide = styled.div`
    display: flex !important;
    width: 100%;
    padding: 30px;
    .text {
        display: flex;
        flex-direction: column;
        justify-content: center;
        max-width: 400px;
        padding: 0 20px;
        .text-title {
            font-size: 14px;
            color: ${props => props.theme.background};
            span {
                font-weight: 600;
            }
        }
        .text-phrase {
            color: ${props => props.theme.background};
            font-size: 22px;
            line-height: 22px;
        }
    }
    .img {
        position: relative;
        display: flex;
        justify-content: center;
        align-items: center;
        cursor: pointer;
        &:hover {
            .botao-play {
                opacity: 0.5;
            }
        }
        img {
            width: 100%;
            max-width: 580px;
            box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.3);
            border-radius: 5px;
            height: 346px;
        }
    }
    .text-title2 {
        display: none;
    }
    @media only screen and (max-width: 1201px) {
        flex-direction: column;
        .text {
            display: none;
        }
        .text-title2 {
            display: flex;
            justify-content: center;
            margin: 10px;
            font-size: 14px;
            color: ${props => props.theme.background};
            span {
                font-weight: 600;
            }
        }
    }
    @media only screen and (max-width: 787px) {
        img {
            height: 240px !important;
        }
    }
    @media only screen and (max-width: 597px) {
        img {
            width: 280px !important;
            height: 180px !important;
        }
        .text-title2 {
            font-size: 8px;
        }
    }
    @media only screen and (max-width: 497px) {
        img {
            width: 190px !important;
            height: 140px !important;
        }
        .text-title2 {
            font-size: 8px;
        }
    }
`;

export const IconCircle = styled.div`
    display: flex;
    justify-content: center;
    position: absolute;
    align-items: center;
    height: 50px;
    width: 50px;
    border-radius: 67px;
    background-color: ${props => props.theme.primary};
    box-shadow: 0 10px 30px 0 rgba(0, 0, 0, 0.2);
`;

export const CircleSlide = styled.div`
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 50px;
    width: 50px;
    border-radius: 50%;
    background-color: ${props => props.theme.primary};
    box-shadow: 0 10px 30px 0 rgba(0, 0, 0, 0.2);
    &:hover {
        opacity: 0.5;
    }
`;

export const ArrowPlay = styled(PlayArrowIcon)`
    color: ${props => props.theme.letter};
`;

export const ModalPlayer = styled(Modal)`
    display: flex;
    align-items: center;
    justify-content: center;
    margin: auto;
`;

export const Player = styled(ReactPlayer)``;

export const ThirdTab = styled(Grid)`
    background-color: ${props => props.theme.background};
    width: 100%;
    display: flex;
    padding-bottom: 40px;
    .text {
        padding: 0 20px;
        width: 100%;
        max-width: 550px;
        height: 270px;
        display: flex;
        flex-direction: column;
        justify-content: center;

        .text-sub {
            color: ${props => props.theme.letter2};
            font-size: 24px;
            text-align: center;
        }
        .highlight {
            font-size: 60px;
            text-align: center;
            font-weight: bold;
            line-height: 62px;
            color: ${props => props.theme.letter};
            span {
                color: ${props => props.theme.primary};
            }
        }
    }
    .coverflow__container__1P-xE {
        background-color: transparent;
    }
    .coverflow__text__39hqd {
        background-color: transparent;
    }
    @media only screen and (max-width: 800px) {
        padding-bottom: 40px;
        .text {
            padding: 20px;
            height: auto;
            display: flex;
            flex-direction: column;
            justify-content: center;

            .text-sub {
                color: ${props => props.theme.letter2};
                font-size: 16px;
                text-align: center;
            }
            .highlight {
                font-size: 40px;
                text-align: center;
                font-weight: bold;
                line-height: 42px;
            }
        }
    }
`;

export const SecondCard = styled.div`
    background-color: ${props => props.theme.letter};
    display: flex !important;
    flex-direction: column;
    padding: 20px;
    color: ${props => props.theme.background};
    border-radius: 5px;
    max-height: 350px;
    .text-card {
        text-align: center;
        font-size: 16px;
        margin-bottom: 20px;
        span {
            font-weight: bold;
        }
    }
    .perfil {
        width: 100%;
        display: flex;
        align-items: center;
        img {
            width: 74px;
            height: 74px;
            border-radius: 50%;
            margin-right: 10px;
        }
        .perfil-text {
            display: flex;
            flex-direction: column;
            font-size: 16px;
            .perfil-title {
                font-weight: bold;
            }
        }
    }
    @media only screen and (max-width: 1210px) {
        .text-card {
            font-size: 12px;
            margin-bottom: 10px;
            span {
                font-weight: bold;
            }
        }
        .perfil {
            img {
                width: 38px;
                height: 38px;
            }
            .perfil-text {
                font-size: 12px;
                .perfil-title {
                }
            }
        }
    }
    @media only screen and (max-width: 550px) {
        .text-card {
            font-size: 10px;
            margin-bottom: 10px;
            span {
                font-weight: bold;
            }
        }
        .perfil {
            img {
                width: 32px;
                height: 32px;
            }
            .perfil-text {
                font-size: 10px;
                .perfil-title {
                }
            }
        }
    }
    @media only screen and (max-width: 500px) {
        padding: 5px;
        .text-card {
            font-size: 8px;
            margin-bottom: 5px;
            span {
                font-weight: bold;
            }
        }
        .perfil {
            flex-direction: column;
            img {
                width: 32px;
                height: 32px;
                margin: 0;
                margin-bottom: 5px;
            }
            .perfil-text {
                text-align: center;
                font-size: 8px;
                .perfil-title {
                }
            }
        }
    }
`;

export const CircleFirst = styled.div`
    cursor: pointer;
    position: absolute;
    top: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 50px;
    width: 50px;
    border-radius: 50%;
    background-color: ${props => props.theme.primary};
    box-shadow: 0 10px 30px 0 rgba(0, 0, 0, 0.2);
    &:hover {
        opacity: 0.5;
    }
    @media only screen and (max-width: 1280px) {
        top: 35%;
        right: 0;
    }
`;

export const CircleSecond = styled.div`
    cursor: pointer;
    position: absolute;
    top: 30%;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 50px;
    width: 50px;
    border-radius: 50%;
    background-color: ${props => props.theme.primary};
    box-shadow: 0 10px 30px 0 rgba(0, 0, 0, 0.2);
    &:hover {
        opacity: 0.5;
    }
    @media only screen and (max-width: 1280px) {
        top: 35%;
    }
`;

export const ReactCoverFullSize = styled(Coverflow)``;

export const ReactCoverMobSize = styled(Coverflow)``;
