import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import YouTubePlayer from 'react-player/lib/players/YouTube';
import {
    Container,
    FirstTab,
    MenuCircle,
    Arrow1,
    Arrow2,
    SecondTab,
    ArrowForward,
    ArrowBack,
    CardSlide,
    IconCircle,
    ArrowPlay,
    CircleSlide,
    ModalPlayer,
    ThirdTab,
    FirstSlider,
    FirstTabMob,
    SecondCard,
    ReactCoverFullSize,
    ReactCoverMobSize,
} from './styles';

import { ListaCurso, Newsletter } from '@components';
import { News } from '../../App/Pages/Home/styles';

function SampleNextArrow(props) {
    const { onClick } = props;
    return (
        <CircleSlide onClick={onClick}>
            <ArrowForward />
        </CircleSlide>
    );
}

function SamplePrevArrow(props) {
    const { onClick } = props;
    return (
        <CircleSlide onClick={onClick}>
            <ArrowBack />
        </CircleSlide>
    );
}

const DepoNew = () => {
    const [state, setState] = useState([
        {
            id: 1,
            video: 'https://www.youtube.com/watch?v=ZRx4mTDMmzA',
            imagem:
                'https://images.pexels.com/photos/3163927/pexels-photo-3163927.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        },
        {
            id: 2,
            video: 'https://www.youtube.com/watch?v=npCMuKdtDKI',
            imagem:
                'https://images.pexels.com/photos/3158665/pexels-photo-3158665.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        },
        {
            id: 3,
            video: 'https://www.youtube.com/watch?v=eVFs6GnJTZs',
            imagem:
                'https://images.pexels.com/photos/3170413/pexels-photo-3170413.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        },
        {
            id: 4,
            video: 'https://www.youtube.com/watch?v=_f9sK9KMxRI',
            imagem:
                'https://images.pexels.com/photos/3164112/pexels-photo-3164112.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        },
        {
            id: 5,
            video: 'https://www.youtube.com/watch?v=zjojsT9zo8M',
            imagem:
                'https://images.pexels.com/photos/934718/pexels-photo-934718.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
        },
    ]);
    const [open, setOpen] = useState(false);
    const [active, setActive] = useState({ url: '', id: '' });
    const handleOpen = (link, id) => {
        setOpen(!open);
        setActive({ url: link, id });
    };
    const settings = {
        customPaging() {
            return (
                <div className="dots">
                    <div className="circle" />
                </div>
            );
        },
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        className: 'slider center variable-width',
        dots: true,
        fade: true,
        autoplay: false,
        autoplaySpeed: 3000,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: state.length > 1 ? 1 : state.length,
                    infinite: true,
                    slidesToScroll: 1,
                    centerPadding: '100px',
                    fade: false,
                    centerMode: true,
                    arrows: false,
                },
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: state.length > 1 ? 1 : state.length,
                    infinite: true,
                    slidesToScroll: 1,
                    centerPadding: '50px',
                    fade: false,
                    centerMode: true,
                    arrows: false,
                },
            },
            {
                breakpoint: 397,
                settings: {
                    slidesToShow: state.length > 1 ? 1 : state.length,
                    slidesToScroll: 1,
                    fade: false,
                    centerPadding: '30px',
                    centerMode: true,
                    arrows: false,
                    dots: true,
                },
            },
            {
                breakpoint: 345,
                settings: {
                    slidesToShow: state.length > 1 ? 1 : state.length,
                    slidesToScroll: 1,
                    fade: false,
                    centerPadding: '20px',
                    centerMode: true,
                    arrows: false,
                    dots: true,
                },
            },
            {
                breakpoint: 325,
                settings: {
                    slidesToShow: state.length > 1 ? 1 : state.length,
                    slidesToScroll: 1,
                    fade: false,
                    centerPadding: '10px',
                    centerMode: true,
                    arrows: false,
                    dots: true,
                },
            },
        ],
    };

    return (
        <Container>
            <FirstTab>
                <div className="background">
                    <div className="text">
                        <div className="depoimentos">Depoimentos</div>
                        <div className="depoimentos-highlight">
                            "Aprendo muito em qualquer lugar que eu estiver."
                        </div>
                    </div>
                    <div className="img">
                        <img
                            src="https://images.pexels.com/photos/2324423/pexels-photo-2324423.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                            alt=""
                        />
                    </div>
                </div>
                <MenuCircle>
                    <Arrow1 />
                    <Arrow2 />
                </MenuCircle>
            </FirstTab>
            <FirstTabMob>Depoimentos</FirstTabMob>
            <SecondTab>
                <div className="container">
                    <div className="title">Depoimentos em vídeo</div>
                    <FirstSlider {...settings}>
                        {state.map(depoimentos => {
                            return (
                                <CardSlide key={depoimentos.id}>
                                    <div className="text">
                                        <div className="text-title">
                                            <span>Tristan Olsen</span> | São
                                            Paulo (BR)
                                        </div>
                                        <div className="text-phrase">
                                            Suspendisse vitae justo non erat
                                            maximus blandit sagittis eu dui.
                                            lorem ipsum dolor sit amet,
                                            consectetur adipiscing elit.
                                        </div>
                                    </div>
                                    <div
                                        className="img"
                                        onClick={() =>
                                            handleOpen(
                                                depoimentos.video,
                                                depoimentos.id
                                            )
                                        }
                                    >
                                        <img src={depoimentos.imagem} alt="" />
                                        {active.id === depoimentos.id ? null : (
                                            <IconCircle className="botao-play">
                                                <ArrowPlay />
                                            </IconCircle>
                                        )}
                                    </div>
                                    <div className="text-title2">
                                        <span>Tristan Olsen</span> | São Paulo
                                        (BR)
                                    </div>
                                </CardSlide>
                            );
                        })}
                    </FirstSlider>
                    <ModalPlayer open={open} onClose={handleOpen}>
                        <YouTubePlayer url={active.url} playing controls />
                    </ModalPlayer>
                </div>
            </SecondTab>
            <ThirdTab container justify="center">
                <Grid item xs={12} sm={12} md={12} lg={12} className="text">
                    <div className="text-sub">Nossos melhores alunos</div>
                    <div className="highlight">
                        Quem já fez <span>indica</span>.
                    </div>
                </Grid>

                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <ReactCoverFullSize
                        displayQuantityOfSide={2}
                        navigation
                        // enableHeading
                        enableScroll
                        clickable
                        infiniteScroll
                        active={0}
                        currentFigureScale={1.2}
                        otherFigureScale={0.7}
                        media={{
                            '@media (max-width: 2000px)': {
                                width: '100%',
                                height: '400px',
                            },
                            '@media (max-width: 1210px)': {
                                width: '100%',
                                height: '300px',
                            },
                            '@media (max-width: 900px)': {
                                display: 'none',
                                width: '100%',
                                height: '300px',
                            },
                        }}
                    >
                        {state.map(depoimentos => {
                            return (
                                <SecondCard
                                    className="second-card"
                                    key={depoimentos.id}
                                >
                                    <div className="text-card">
                                        “Suspendisse vitae justo non erat
                                        maximus blandit sagittis eu dui. lorem
                                        ipsum dolor sit amet, consectetur
                                        adipiscing elit. Suspendisse vitae justo
                                        non erat maximus.”
                                    </div>
                                    <div className="perfil">
                                        <img
                                            src=" http://placeimg.com/100/100/people"
                                            alt=""
                                        />
                                        <div className="perfil-text">
                                            <div className="perfil-title">
                                                Eduardo Silva
                                            </div>
                                            <div>São Paulo (BR)</div>
                                        </div>
                                    </div>
                                </SecondCard>
                            );
                        })}
                    </ReactCoverFullSize>
                    <ReactCoverMobSize
                        displayQuantityOfSide={1}
                        // navigation
                        // enableHeading
                        // enableScroll
                        clickable
                        infiniteScroll
                        active={0}
                        currentFigureScale={1.3}
                        otherFigureScale={0.8}
                        media={{
                            '@media (max-width: 2000px)': {
                                width: '100%',
                                height: '400px',
                            },
                            '@media (max-width: 1210px)': {
                                width: '100%',
                                height: '300px',
                            },
                            '@media (min-width: 900px)': {
                                display: 'none',
                                width: '100%',
                                height: '300px',
                            },
                        }}
                    >
                        {state.map(depoimentos => {
                            return (
                                <SecondCard
                                    className="second-card"
                                    key={depoimentos.id}
                                >
                                    <div className="text-card">
                                        “Suspendisse vitae justo non erat
                                        maximus blandit sagittis eu dui. lorem
                                        ipsum dolor sit amet, consectetur
                                        adipiscing elit. Suspendisse vitae justo
                                        non erat maximus.”
                                    </div>
                                    <div className="perfil">
                                        <img
                                            src=" http://placeimg.com/100/100/people"
                                            alt=""
                                        />
                                        <div className="perfil-text">
                                            <div className="perfil-title">
                                                Eduardo Silva
                                            </div>
                                            <div>São Paulo (BR)</div>
                                        </div>
                                    </div>
                                </SecondCard>
                            );
                        })}
                    </ReactCoverMobSize>
                </Grid>
            </ThirdTab>
            <ListaCurso title="Cursos destaque" destaque />
            <News className="tr-newsletter purple mt-4">
                <Newsletter />
            </News>
        </Container>
    );
};

export default DepoNew;
