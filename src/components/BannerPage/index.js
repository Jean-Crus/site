import React from 'react';

const BannerPage = (props) => {

    const style = {
        backgroundImage: "url('" + props.banner + "')"
    }

	return(
        <div className="header-course" style={style}>
            <div className="title-1 size-50">
                {props.title}
            </div>
        </div>
	);
};

export default BannerPage;
