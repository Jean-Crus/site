import styled from "styled-components";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import InstagramIcon from "@material-ui/icons/Instagram";
import FacebookIcon from "@material-ui/icons/Facebook";

export const Foot = styled.footer`
    background: ${props =>
        `linear-gradient(0deg, #0058a8 0%, #0058a8 100%)`} !important;

    .socials {
        a {
            color: ${props => props.theme.letter} !important;
        }
    }
    .title-3 {
        color: ${props => props.theme.letter} !important;
    }

    .description-3 {
        max-width: 500px !important;
        color: ${props => props.theme.letter} !important;
    }

    .navigation,
    .about {
        a {
            color: ${props => props.theme.letter} !important;
        }
    }
    .information {
        .logo {
            img {
                width: 100%;
                height: 100%;
                max-height: 50px;
                max-width: 50px;
            }
        }
    }
    @media only screen and (max-width: 768px) {
        padding-top: 50px;
        height: auto !important;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        .content {
            width: 100%;
            padding-top: 0 !important;
            flex-basis: unset !important;
            flex-direction: column;
            justify-content: center !important;
            align-items: center !important;

            .information {
                justify-content: center !important;
                align-items: center !important;

                .description {
                    .description-3 {
                        text-align: center;
                    }
                }
                .socials-desktop {
                    display: none !important;
                }
            }
            .about {
                display: flex;
                justify-content: center !important;
                align-items: center !important;
                flex-direction: column;
                margin-bottom: 30px;
                .title-3 {
                    margin-top: 30px;
                    margin-bottom: 0px !important;
                }
            }
        }
        .navigation {
            display: none;
        }
    }
`;

export const ContainerSocial = styled.div`
    display: none;
    @media only screen and (max-width: 768px) {
        display: flex;
        margin-bottom: 50px;
        a {
            padding-right: 50px;
            color: #fff;
            &:hover {
                color: #fff;
            }
            &:last-child {
                padding-right: 0;
            }
        }
    }
`;

export const SubFooter = styled.div`
    background-color: ${props => props.theme.backgroundHeader} !important;
    .description-2 {
        color: ${props => props.theme.letter} !important;
        font-size: 10px !important;
        text-align: center !important;
    }
`;
