import React from "react";
import { Link } from "react-router-dom";
import { header, footer } from "@theme";
import logo from "@images/logo.png";
import { Foot, ContainerSocial, INSTA, LN, FB, SubFooter } from "./styles";

const Footer = () => {
    return (
        <>
            <Foot className="tr-footer">
                <div className="container content">
                    <div className="information">
                        <div className="logo">
                            <img src={logo} alt="Professor Carlos André" />
                        </div>
                        <div className="description">
                            <div className="description-3">{footer.text}</div>
                        </div>
                        <div className="socials socials-desktop">
                            <a
                                href="https://www.youtube.com/user/cursoaprovacaoo"
                                rel="noopener noreferrer"
                                target="_blank"
                            >
                                <i className="fab fa-youtube" />
                            </a>
                            <a
                                href="https://www.instagram.com/mastercoachcarlosandre/?hl=pt-br"
                                rel="noopener noreferrer"
                                target="_blank"
                            >
                                <i className="fab fa-instagram" />
                            </a>
                            <a
                                href="https://www.facebook.com/profcarlosandre2"
                                rel="noopener noreferrer"
                                target="_blank"
                            >
                                <i className="fab fa-facebook-f" />
                            </a>
                        </div>
                    </div>
                    <div className="navigation">
                        <div className="title-3 size-50">Navegação</div>
                        <ul className="navigation">
                            <li className="itens">
                                <Link to="/">Home</Link>
                            </li>
                            <li className="itens">
                                <Link to="/sobre">Quem somos</Link>
                            </li>
                            <li className="itens">
                                <Link to="/cursos">Cursos</Link>
                            </li>
                            <li className="itens">
                                <Link to="/contato">Contato</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="about">
                        <div className="title-3 size-50">Fale conosco</div>
                        <ul className="about">
                            <li className="itens">
                                <Link to="/">Curitiba</Link>
                            </li>
                            {/* <li className="itens">
                                <Link to="/">contato@tesis.digital</Link>
                            </li> */}
                            <li className="itens">
                                <Link to="/" />
                            </li>
                        </ul>
                    </div>
                    <ContainerSocial>
                        <a href="#" rel="noopener noreferrer" target="_blank">
                            <i className="fab fa-youtube" />
                        </a>
                        <a href="#" rel="noopener noreferrer" target="_blank">
                            <i className="fab fa-instagram" />
                        </a>
                        <a href="#" rel="noopener noreferrer" target="_blank">
                            <i className="fab fa-facebook-f" />
                        </a>
                    </ContainerSocial>
                </div>
            </Foot>
            <SubFooter className="tr-subfooter">
                <div className="content container">
                    <div className="description-2">
                        © Tesis Digital 2020 - Todos os direitos reservados.
                        {/* <br />
                        <Link to="/">Termos de uso</Link> e{" "}
                        <Link to="/">política de privacidade</Link>. */}
                    </div>
                </div>
            </SubFooter>
        </>
    );
};

export default Footer;
