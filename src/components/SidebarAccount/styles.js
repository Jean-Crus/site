import styled from 'styled-components';

export const Container = styled.div`
    
    ul {
        li {
            a {
                &:hover {
                    background-color: ${props =>
                        props.theme.primary} !important;
                }
            }
            &.logout-sidebar {
                margin-top: 0px !important;
                a:hover{
                    color: white !important;
                    text-decoration: none !important;
                }
            }
        }
    }
`;
