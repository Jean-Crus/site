import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import * as AuthActions from "@store/modules/auth/actions";
import * as UserActions from "@store/modules/user/actions";
import { Spinner } from "react-bootstrap";
import { Grid } from "@material-ui/core";
import { Container } from "./styles";

export default function SidebarAccount({ history }, props) {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.user.loading);

    useEffect(() => {
        dispatch(UserActions.getMyCoursesRequest());
    }, [dispatch]);

    const myCourses = useSelector(state => state.user.data.length);
    const menu = [
        {
            nome: "Meus cursos",
            link: "/conta",
            count: myCourses
        },
        // {
        //     nome: 'Meus eventos',
        //     link: '/conta/meus-eventos',
        //     count_alert: 4,
        // },
        // {
        //     nome: 'Fórum',
        //     link: '/conta/forum',
        // },
        // {
        //     nome: 'Falar com Tutor',
        //     link: '/conta/falar-com-tutor',
        // },
        // {
        //     nome: 'Certificado',
        //     link: '/conta/certificado',
        // },
        // {
        //     nome: 'Biblioteca',
        //     link: '/conta/biblioteca',
        // },
        // {
        //     nome: 'Conquistas',
        //     link: '/conta/conquistas',
        // },
        {
            nome: "Meus pedidos",
            link: "/conta/meus-pedidos"
        },
        {
            nome: "Configurar Conta",
            link: "/conta/configurar-conta"
        }
        // {
        //     nome: "Tutorial",
        //     link: "/conta/tutorial"
        // }
        // {
        //     nome: 'Suporte da Plataforma',
        //     link: '/conta/suporte-da-plataforma',
        // },
    ];

    return loading ? (
        <Grid container justify="center" className="mt-4 mb-4">
            <Spinner animation="border" role="status" />
        </Grid>
    ) : (
        <Container className="sidebar-style">
            <ul>
                {menu.map(item => (
                    <li
                        key={item.nome}
                        className={
                            history.pathname === item.link && "active"
                                ? "true"
                                : undefined
                        }
                    >
                        <Link to={item.link}>
                            {item.nome}
                            {item.count ? (
                                <span className="count-alert">
                                    ({item.count})
                                </span>
                            ) : (
                                ""
                            )}
                        </Link>
                    </li>
                ))}
                <li className="logout-sidebar">
                    <Link
                        to="#"
                        onClick={() => dispatch(AuthActions.logoutRequest())}
                    >
                        Sair
                    </Link>
                </li>
            </ul>
        </Container>
    );
}
