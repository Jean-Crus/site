import React from "react";
import ReactHtmlParser from "react-html-parser";
import "suneditor/dist/css/suneditor.min.css";
import { Container } from "./styles";

const RenderSunEditor = ({ renderComp }) => (
    <body className="sun-editor-editable">{ReactHtmlParser(renderComp)}</body>
);

export default RenderSunEditor;
