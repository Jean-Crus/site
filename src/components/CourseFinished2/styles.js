import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    width: 100%;
    justify-content: flex-start;
    align-items: center;
    /* flex-direction: column; */
    img {
        height: 256px;
    }
    font-family: Montserrat;
    background-image: ${props => `url(${props.url})`};
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
    padding-left: 20px;
    div {
        margin-left: 20px;
        color: ${props => props.theme.letter2};
        font-weight: 300;
        font-size: 14px;
        &:first-child {
            color: #34b16f;
            font-size: 60px;
            font-weight: bold;
            line-height: 55px;
        }
        .frase {
            line-height: 20px;
            max-width: 300px;
            padding: 5px;
            span {
                color: #34b16f;
                font-weight: bold;
            }
            strong {
                font-weight: bold;
            }
        }
    }
    @media only screen and (max-width: 1336px) {
        flex-direction: column;
        align-items: center;
        padding-top: 40px;
        div {
            text-align: center;
            margin: 0;
            &:first-child {
                color: #2bb26d;
                font-size: 32px;
            }
        }
    }
`;
