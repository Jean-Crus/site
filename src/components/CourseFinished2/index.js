import React from "react";
import { useSelector } from "react-redux";
import { Container } from "./styles";

const CourseFinished2 = ({ url }) => {
    const user = useSelector(state => state.user);
    return (
        <Container className="row">
            <div className="col-lg-12 d-flex justify-content-center">
                <img src={url} alt="" />
            </div>
            <div className="col-lg-12 d-flex justify-content-center flex-column mt-4 align-items-center">
                <div>
                    Parabéns, {user.profile.name} {user.profile.lastname}!
                </div>
                <div className="frase">
                    Você concluiu o curso <strong>{user.course.name}</strong> .
                    Seu aproveitamento foi de{" "}
                    <span>{user.course.user_progress}%.</span>
                </div>
            </div>
        </Container>
    );
};

export default CourseFinished2;
