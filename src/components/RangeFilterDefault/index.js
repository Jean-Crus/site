import React, { useState, useEffect } from "react";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { Container } from "./styles";

const RangeFilterDefault = ({ onChange, maxValue, minValue, initial }) => {
    const [state, setState] = useState({ min: initial.min, max: initial.max });
    useEffect(() => {
        setState({ min: initial.min, max: initial.max });
    }, [initial]);
    return (
        <Container>
            <InputRange
                maxValue={maxValue}
                minValue={minValue}
                value={state}
                onChange={value => setState(value)}
                onChangeComplete={onChange}
            />
        </Container>
    );
};

export default RangeFilterDefault;
