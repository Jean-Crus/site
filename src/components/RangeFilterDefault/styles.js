import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    width: 100%;
    .input-range__track--active,
    .input-range__slider {
        background: ${props => props.theme.primary};
        border-color: ${props => props.theme.primary};
    }
    .input-range__label {
        font-family: Montserrat;
        color: ${props => props.theme.letter};
        font-size: 12px;
        font-weight: bold;
    }
    .input-range__label--value {
        margin: -9px 0;
    }
    .input-range__label-container {
        padding: 2px 5px;
        position: relative;
        border-radius: 5px;
        background-color: ${props => props.theme.primary};

        &::after {
            position: absolute;
            width: 6px;
            height: 6px;
            border-top: ${props => `0px solid ${props.theme.primary}`};
            border-right: ${props => `2px solid ${props.theme.primary}`};
            border-bottom: ${props => `2px solid ${props.theme.primary}`};
            border-left: ${props => `0px solid ${props.theme.primary}`};
            top: 87%;
            left: 50%;
            margin-left: -3px;
            content: "";
            transform: rotate(45deg);
            background: ${props => props.theme.primary};
        }
    }
    .input-range__label--min,
    .input-range__label--max {
        display: none !important;
        .input-range__label-container {
            padding: 2px 5px;
            background-color: ${props => props.theme.letter};
            color: ${props => props.theme.primary};
        }
    }
`;
