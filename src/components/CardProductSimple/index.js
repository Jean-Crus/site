import React from "react";
var classNames = require("classnames");

const rating = rating => {
  var li = [];
  for (let i = 1; i <= 5; i++) {
    li.push(
      <li
        className={classNames({
          star: true,
          fill: rating >= i
        })}
      >
        <i class="fas fa-star" />
      </li>
    );
  }
  return li;
};

const CardProductSimple = ({ content }, state) => {
  return (
    <div className="card-product-simple">
      <div className="picture">
        <ul>
          {content.categories.map(item => (
            <li key={Math.random()}>
              <a href={item.url}>{item.name}</a>
            </li>
          ))}
        </ul>
        <a
          class="image-link"
          href="#"
          style={
            content.image ? { backgroundImage: `url('${content.image}')` } : {}
          }
        >
          <span className="sr-only">{content.title}</span>
        </a>
      </div>
      <div className="rating">
        {content.rating > 0 && (
          <div>
            <ul className="stars">{rating(content.rating)}</ul>
            <span className="note">{content.rating}</span>
          </div>
        )}
      </div>
      <div className="title">
        <h3>
          <a href="#">{content.title}</a>
        </h3>
      </div>
      <div className="price">
        <div className="product-class">
          <span>{content.productClass}</span>
        </div>
        <div className="product-price">
          <strong>
            R$ <span>{content.price}</span>
          </strong>
        </div>
      </div>
    </div>
  );
};

export default CardProductSimple;
