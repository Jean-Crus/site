import React, { useState, useEffect } from 'react';
import { FormGroup, FormControlLabel, Switch } from '@material-ui/core';
import { Container } from './styles';

const CheckEdit = ({ title = 'Texto padrão', handleFunction }) => {
    const [state, setState] = React.useState({
        checkedA: true,
    });

    const handleChange = name => event => {
        setState({ [name]: event.target.checked });
    };

    useEffect(() => {
        handleFunction(state);
    }, [handleFunction, state]);

    return (
        <Container>
            <div className="title">{title}</div>
            <FormGroup row>
                <FormControlLabel
                    control={
                        <Switch
                            checked={state.checkedA}
                            onChange={handleChange('checkedA')}
                            value="checkedA"
                        />
                    }
                    label="Desativar/ativar subtítulo card"
                />
            </FormGroup>
        </Container>
    );
};

export default CheckEdit;
