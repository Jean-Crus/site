import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    .MuiTypography-body1 {
        color: ${props => props.theme.letter};
        font-family: Montserrat;
        font-size: 0.7rem;
    }
    .MuiSwitch-colorSecondary {
        color: ${props => props.theme.primary} !important;
    }
    .Mui-checked {
        + .MuiSwitch-track {
            background-color: ${props => props.theme.primary} !important;
        }
    }
`;
