import React from "react";
import { Grid } from "@material-ui/core";
import box from "@images/box.svg";
import { Container } from "./styled";

const NoResult = () => {
    return (
        <Container container>
            <Grid
                item
                xs={12}
                sm={12}
                xl={3}
                md={3}
                lg={3}
                container
                alignItems="center"
                justify="center"
            >
                <img src={box} alt="" />
            </Grid>
            <Grid item xs={12} xl={8} sm={12} md={8} lg={8} container>
                <Grid
                    item
                    xs={12}
                    xl={12}
                    sm={12}
                    md={12}
                    lg={12}
                    className="mb-3"
                >
                    Sem resultados
                </Grid>
                <Grid item xs={12} xl={12} sm={12} md={12} lg={12}>
                    Não conseguimos achar nenhum resultado para sua pesquisa.
                    Que tal pesquisar de novo?
                </Grid>
            </Grid>
        </Container>
    );
};

export default NoResult;
