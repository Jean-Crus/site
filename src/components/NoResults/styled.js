import styled from "styled-components";
import { Grid } from "@material-ui/core";

export const Container = styled(Grid)`
    border: 1px solid #efefef;
    border-radius: 5px;
    background-color: #ffffff;
    padding: 60px 30px;
    font-family: Montserrat;
    font-size: 1rem;
    color: #6c6c6c;
    font-weight: 300;
    text-align: left;
    b {
        font-weight: bold;
    }
    img {
        width: 103px;
        height: 98px;
    }

    .mb-3 {
        font-weight: bold;
        font-size: 2.5rem;
        color: #1f1f1f;
        line-height: 2.5rem;
    }
    @media only screen and (max-width: 999px) {
        text-align: center;
        font-size: 0.9rem;
        padding: 30px;
        padding-bottom: 40px;
        .mb-3 {
            font-size: 1.6rem;
            margin-top: 0.6rem;
            margin-bottom: 0.6rem !important;
            line-height: 1.6rem;
        }
    }
`;
