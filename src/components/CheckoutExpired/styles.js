import styled from 'styled-components';

export const Container = styled.div`
    background-color: ${props => props.theme.letter};
    min-height: 50vh;
    padding: 80px 0;
    font-family: Montserrat;
    .button-group {
        display: flex;
        justify-content: flex-end;
        margin-top: 20px;
        div {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 220px;
        }
        .button-text {
            display: flex;
            align-items: center;
            justify-content: center;
            color: ${props => props.theme.background};
            font-weight: bold;
            font-size: 10px;
        }
    }
    .return {
        font-size: 11px;
        color: ${props => props.theme.primary};
        font-weight: bold;
        margin-bottom: 10px;
        text-decoration: underline;
    }
    @media only screen and (max-width: 999px) {
        .button-group {
            justify-content: center;
            div {
                width: auto;
                button {
                    display: none;
                }
            }
            button {
                height: 45px;
                width: 100%;
                max-width: 150px;
            }
        }
    }
    @media only screen and (min-width: 999px) {
        .return {
            display: none;
        }
        .button-group {
            .button-text {
                display: none;
            }
        }
    }
`;

export const Board = styled.section`
    border-radius: 14px;
    border: 1px solid #d1cdcd;
    padding: 100px;
    display: flex;

    .MuiTextField-root {
        background-color: ${props => props.theme.letter};
        border-radius: 5px;
    }
    .Mui-focused {
        color: ${props => props.theme.primary} !important;
    }
    .MuiFilledInput-underline:after {
        border-bottom: ${props => `2px solid ${props.theme.primary}`};
    }
    .MuiFilledInput-underline:before {
        border-bottom: transparent;
    }
    img {
        width: 100%;
        max-width: 108px;
        max-height: 112px;
        margin-right: 60px;
    }
    main {
        display: flex;
        flex-direction: column;
        header {
            font-size: 60px;
            color: ${props => props.theme.primary};
            font-weight: bold;
            line-height: 60px;
            margin-bottom: 20px;
        }
        .course-text {
            font-size: 22px;
            line-height: 24px;
            color: ${props => props.theme.letter2};
            font-weight: 300;
            margin-bottom: 60px;

            span {
                font-weight: bold;
            }
        }
        section {
            div {
                line-height: 18px;
                color: ${props => props.theme.letter2};
                font-weight: 300;
                font-size: 16px;
            }
        }
    }
    .links {
        display: flex;
        justify-content: flex-end;
        a {
            margin-left: 20px;
            font-weight: 300;
            font-size: 12px;
            color: ${props => props.theme.secondary};
            text-decoration: underline;
        }
    }

    @media only screen and (max-width: 999px) {
        flex-direction: column;
        align-items: center;
        padding: 20px;
        img {
            margin: 0;
        }
        main {
            align-items: center;
            header {
                text-align: center;
            }
            .course-text {
                text-align: center;
            }
            section {
                width: 100%;
            }
        }
    }
    @media only screen and (max-width: 787px) {
        img {
        }
        main {
            header {
                font-size: 32px;
            }
            .course-text {
                font-size: 12px;
                margin-bottom: 40px;
            }
            section {
                div {
                    font-size: 12px;
                }
            }
            .MuiFormLabel-root {
                font-size: 12px;
            }
            .links {
                font-size: 10px;
            }
        }
    }
`;
