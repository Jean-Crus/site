import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { TextField, MenuItem } from '@material-ui/core';
import image from '@images/expiredImg.png';
import { Link } from 'react-router-dom';
import * as UserActions from '@store/modules/user/actions';
import Moment from 'react-moment';
import history from '@routes/history';
import * as CartActions from '@store/modules/cart/actions';

import { Container, Board } from './styles';
import { ButtonTriade, Loading } from '@components';

const CheckoutExpired = ({
    match: {
        params: { id },
    },
}) => {
    const loading = useSelector(state => state.user.loading);
    const courseExpired = useSelector(state => state.user.courseExpired);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(UserActions.getCourseExpiredRequest(id));
    }, [dispatch, id]);
    const [check, setCheck] = useState('');
    const handleChange = event => {
        setCheck(event.target.value);
    };

    const addCurso = () => {
        dispatch(CartActions.addToCartRequest(parseInt(id, 10)));
    };

    return loading ? (
        <Loading />
    ) : (
        <Container className="container">
            <div className="return" onClick={() => history.goBack()}>
                Voltar
            </div>
            <Board>
                <img src={image} alt="" />
                <main>
                    <header>Curso expirado</header>
                    <div className="course-text">
                        O <span>{courseExpired.name}</span> expirou no dia{' '}
                        <Moment format="DD/MM/YYYY">
                            {courseExpired.access_date}
                        </Moment>
                        . Para continuar você precisa renovar sua matrícula.
                    </div>
                    <section>
                        <div>Como deseja continuar o curso?</div>
                        <div>
                            <TextField
                                label="Começar o curso do inicio (20% OFF)"
                                fullWidth
                                select
                                margin="normal"
                                value={check}
                                variant="filled"
                                onChange={handleChange}
                            >
                                <MenuItem value="Teste">Teste</MenuItem>
                                <MenuItem value="Teste2">Teste 2</MenuItem>
                                <MenuItem value="Teste3">Teste 3</MenuItem>
                            </TextField>
                        </div>
                        <div className="links">
                            <Link to="/">Entre em contato</Link>
                            <Link to="/">Termos de aceite</Link>
                        </div>
                    </section>
                </main>
            </Board>
            <div className="button-group">
                <div className="mr-3">
                    <ButtonTriade
                        text="Não quero continuar"
                        onClick={() => history.push('/conta')}
                        button="primary"
                    />
                    <div
                        className="button-text"
                        onClick={() => history.push('/conta')}
                    >
                        Não quero continuar
                    </div>
                </div>
                <ButtonTriade
                    text="Renovar matrícula"
                    onClick={addCurso}
                    button="secondary"
                />
            </div>
        </Container>
    );
};

export default CheckoutExpired;
