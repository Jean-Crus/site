import styled from "styled-components";

export const Container = styled.form`
    position: relative;
    width: 100%;
    .MuiFormLabel-root.Mui-focused {
        color: #ef7300;
        background-color: #fff;
    }
    .MuiFilledInput-underline:after {
        background-color: transparent;
    }
    .MuiFilledInput-underline.Mui-error:after {
        background-color: transparent;
    }
    .MuiFilledInput-underline:hover:before {
        border: transparent;
    }
    .MuiFilledInput-underline:after {
        border-bottom: transparent;
    }
    .MuiFormLabel-root {
        color: #ef7300;
        font-family: Montserrat;
    }
    .MuiFormHelperText-root.Mui-error {
        color: ${props => props.errorColor};

        &:after {
            content: "*";
            color: red;
        }
    }
    .MuiFilledInput-root {
        background-color: #fff;
        border: 1px solid #ccc;
        border-radius: 6px;
    }
    .MuiFilledInput-root.Mui-focused {
        background-color: #fff;
        border: 2px solid #ef7300;
    }
    .MuiFilledInput-underline:before {
        border-bottom: transparent;
    }
    .MuiFilledInput-root:hover {
        background-color: #fff;
        border: 2px solid #ef7300;
    }
    .MuiFilledInput-root:focus {
        background-color: #fff;
        border: 2px solid #ef7300;
    }
    .MuiInputBase-input {
        font-family: Montserrat;
        font-weight: bold;
    }

    .fas {
        position: absolute;
        display: flex;
        justify-content: center;
        align-items: center;
        top: 2px;
        right: 2px;
        background: white;
        width: 59px;
        height: 53px;
        border-radius: 6px;
        color: #ef7300;
        z-index: 2;
    }
`;
