import React from "react";
import { TextField, Icon } from "@material-ui/core";
import { Container } from "./styles";

const SearchDefault = ({ label, onSubmit }) => {
    const [state, setState] = React.useState("");
    const handleChange = event => {
        setState(event.target.value);
    };
    return (
        <Container
            onSubmit={e => {
                e.preventDefault();
                onSubmit(state);
            }}
        >
            <TextField
                fullWidth
                label={label}
                type="text"
                variant="filled"
                onChange={handleChange}
                onBlur={() => {}}
                value={state}
            />
            <Icon className="fas fa-search" onClick={() => onSubmit(state)} />
        </Container>
    );
};

export default SearchDefault;
