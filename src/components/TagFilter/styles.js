import styled from "styled-components";

export const FilterTagContainer = styled.div`
    display: flex;
    width: 100%;
    main {
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        justify-content: center;
    }
`;
