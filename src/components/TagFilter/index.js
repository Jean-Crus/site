import React from "react";
import { FilterTagg } from "@components";
import { FilterTagContainer } from "./styles";

const TagFilter = ({ onClick, selected }) => {
    return (
        <FilterTagContainer>
            <main>
                {!!selected.price &&
                    (selected.price.min > 0 || selected.price.max < 1000) && (
                        <FilterTagg
                            priceTag
                            priceMin={selected.price.min}
                            priceMax={selected.price.max}
                            onClick={() => onClick("price")}
                        />
                    )}
                {selected.categoria &&
                    selected.categoria.map(
                        item =>
                            item.checked && (
                                <FilterTagg
                                    valor={item.name}
                                    onClick={() =>
                                        onClick("categoria", item.id)
                                    }
                                />
                            )
                    )}
                {selected.nivel &&
                    selected.nivel.map(
                        item =>
                            item.checked && (
                                <FilterTagg
                                    valor={item.name}
                                    onClick={() => onClick("nivel", item.id)}
                                />
                            )
                    )}
            </main>
        </FilterTagContainer>
    );
};

export default TagFilter;
