import React from 'react';
import { Header, Header2, HeaderConta, HeaderCheckout } from '@components';

export default function HeaderType({ history, signed }) {
    // "Header", "HeaderConta", "HeaderCheckout",
    return (
        <>
            {signed === 'Header' && <Header history={history.location} />}
            {signed === 'Header2' && <Header2 history={history.location} />}
            {signed === 'HeaderConta' && (
                <HeaderConta history={history.location} />
            )}
            {signed === 'HeaderCheckout' && (
                <HeaderCheckout history={history.location} />
            )}
        </>
    );
}
