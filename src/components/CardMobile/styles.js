import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 250px;
    border: 1px solid #efefef;
    border-radius: 5px;
    background-color: #ffffff;
    font-family: Montserrat;
    .image {
        position: relative;
        display: flex;
        align-items: flex-end;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        background-image: ${props => `url(${props.url})`};
        width: 100%;
        height: 250px;

        background-position: center center;

        background-repeat: no-repeat;
        background-size: cover;
        background-color: #464646;
    }

    .title {
        text-align: initial;
        font-size: 15px;
        font-weight: 600;
        min-height: 50px;
        color: ${props => props.theme.background};
        margin-top: 20px;
        margin-left: 15px;
        margin-right: 15px;
        margin-bottom: 10px;
    }

    .tags {
        display: flex;
        align-items: center;
        color: ${props => props.theme.letter2};
        font-size: 11px;
        margin-left: 15px;
        margin-right: 15px;
        margin-bottom: 10px;
        div {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-right: 15px;
            span {
                margin-right: 2px;
            }
        }
    }

    .valor {
        display: flex;
        justify-content: space-between;
        align-items: center;
        flex-direction: column;
        margin-left: 15px;
        margin-right: 15px;
        margin-bottom: 10px;
        min-height: 58px;
        .promo {
            color: #ccc;
            font-size: 0.7rem;
            text-decoration: line-through;
        }
        div {
            font-size: 16px;
            font-weight: bold;
            color: ${props => props.theme.background};
            &:first-child {
                font-size: 12px;
                font-weight: 300;
                color: ${props => props.theme.background};
            }
        }
    }

    @media only screen and (min-width: 701px) {
        display: none;
    }
`;

export const Tag = styled.div`
    position: absolute;
    bottom: -12.5px;
    left: 10px;
    background-color: ${props => (props.color ? props.color : "violet")};
    color: ${props => props.theme.letter};
    font-family: Montserrat, sans-serif;
    font-size: 12px;
    text-align: center;
    padding: 4px;
    border-radius: 3px;
`;

export const Divider = styled.div`
    width: 100%;
    height: 1px;
    background-color: #f1f1f1;
    margin-bottom: 10px;
`;
