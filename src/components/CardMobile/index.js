import React from "react";
import ScheduleIcon from "@material-ui/icons/Schedule";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { Link } from "react-router-dom";
import Icon from "@material-ui/core/Icon";
import { Container, Tag, Divider } from "./styles";

const CardMobile = ({
    label,
    id,
    image,
    title,
    category,
    colorCategory,
    difficulty,
    closed,
    price,
    priceFormatted,
    salePriceFormatted,
    salePrice,
    hours,
    modules,
    description,
    onClick,
    cursos
}) => {
    return (
        <Link to={`/curso-aberto/${id}`} className="mr-2 mb-4">
            <Container url={image}>
                <div className="image">
                    <Tag color={colorCategory || undefined}>{category}</Tag>
                </div>
                <div className="title">{title}</div>
                {/* <div className="tags"> */}
                {/* <div> */}
                {/* <StarBorderIcon /> */}
                {/* <Icon className="fa fa-plus-circle" /> */}
                {/* {difficulty} */}
                {/* </div> */}
                {/* <div> */}
                {/* <Icon className="fa fa-plus-circle" /> */}
                {/* {hours < 2 ? `${hours} Hora` : `${hours} Horas`} */}
                {/* </div> */}
                {/* </div> */}
                <Divider />
                <div className="valor">
                    <div>Investimento de:</div>

                    {salePrice > 0 ? (
                        <div className="promo">{priceFormatted}</div>
                    ) : null}

                    <div>
                        {salePrice > 0 ? salePriceFormatted : priceFormatted}
                    </div>
                </div>
            </Container>
        </Link>
    );
};

export default CardMobile;
