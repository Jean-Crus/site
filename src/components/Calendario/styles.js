import styled from 'styled-components';
import { lighten } from 'polished';

export const Container = styled.div`
    width: 100%;
    display: flex;
    position: relative;
    button {
        position: absolute;
        border: transparent;
        top: 55%;
        left: 37%;
        height: 120px;
        width: 120px;
        background-color: transparent;
    }
    img {
        position: relative;
        width: 100%;
        max-width: 1200px;
        margin-bottom: 40px;
    }
`;

export const OpenCalendar = styled.div`
    font-family: Montserrat;
    width: 165px;
    background-color: #fd9b5b;
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    color: #ffffff;
    padding: 10px;
    .title-curso {
        font-size: 12px;
        font-weight: bold;
        max-width: 100px;
        margin-bottom: 15px;
    }

    .icon {
        display: flex;
        flex-direction: column;
        margin-bottom: 15px;
    }

    .tags {
        display: flex;
        font-size: 11px;
        align-items: center;
        margin-bottom: 5px;
        &:last-child {
            margin-bottom: 0;
        }
        span {
            text-align: center;
            margin-right: 8px;
        }
    }
    a {
        color: #9e5423;
        font-size: 10px;
        font-weight: bold;
        text-decoration: underline;
        &:hover {
            color: ${props => lighten(0.1, `#9e5423`)};
        }
    }
`;
