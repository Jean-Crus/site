import React, { useState } from 'react';
import image from '@images/calendario.png';
import Popover from '@material-ui/core/Popover';
import { Link } from 'react-router-dom';
import { Icon } from '@material-ui/core';
import { Container, OpenCalendar } from './styles';

const Calendario = () => {
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <Container>
            <img src={image} alt="" />

            <button
                type="button"
                className="open-calendar"
                onClick={handleClick}
            ></button>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'center',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
            >
                <OpenCalendar>
                    <div className="title-curso">
                        Treinamento Ceramill Mind + Match2 Básico
                    </div>
                    <div className="icon">
                        <div className="tags">
                            <Icon className="fas fa-medal" /> Avançado
                        </div>
                        <div className="tags">
                            <Icon className="fas fa-clipboard-list" /> Possuir
                            Kit Strauman ou CoDiagnostiX
                        </div>
                        <div className="tags">
                            <Icon className="far fa-clock" /> 20 horas
                        </div>
                    </div>
                    <Link to="/curso-aberto/1f1fb25f-51c0-41dd-88ef-690a48d02f2c">
                        Ver Mais
                    </Link>
                </OpenCalendar>
            </Popover>
        </Container>
    );
};

export default Calendario;
