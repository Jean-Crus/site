import React from "react";

const Benefits = ({ content }, state) => {
  return (
    <div
      id="benefits"
      style={
        content.background
          ? { backgroundImage: `url('${content.background}')` }
          : {}
      }
    >
      <div className="container">
        <h2>{content.title}</h2>
        <div className="row">
          {content.itens.map(item => (
            <div key={Math.random()} className="col-sm-4">
              <div className="benefit-item">
                <img src={item.image} alt={item.title} />
                <h3>{item.title}</h3>
                <p>{item.description}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Benefits;
