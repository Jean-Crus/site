import styled from "styled-components";
import Drawer from "@material-ui/core/Drawer";
import SearchIcon from "@material-ui/icons/Search";
import ListItem from "@material-ui/core/ListItem";
import CloseIcon from "@material-ui/icons/Close";
import { Link } from "react-router-dom";
import MenuIcon from "@material-ui/icons/Menu";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

export const IconMenu = styled(MenuIcon)`
    cursor: pointer;
`;

export const ListaItem = styled(ListItem)`
    background-color: ${props =>
        props.open ? `${props.theme.primary}` : "transparent"} !important;
`;

export const DrawerContainer = styled(Drawer)`
    .MuiDrawer-paper {
        width: 100%;
    }
    .MuiPaper-root {
        background: linear-gradient(0deg, #012a50 0%, #0058a8 100%);
        color: ${props => props.theme.letter};
    }
    .MuiTypography-body1 {
        font-family: Montserrat, sans-serif;
        font-size: 13px;
    }
    .MuiDivider-root {
        background-color: ${props => props.theme.letter};
    }
    .MuiListItem-root {
        justify-content: center;
        text-align: center;
    }
    .logout {
        margin: 20px 0;
        cursor: pointer;
        &:hover {
            color: #fff;
        }
    }
`;

export const SearchBar = styled.form`
    width: 100%;
    max-width: 200px;
    height: 37px;
    display: flex;
    border: ${props => `1px solid ${props.theme.primary}`};
    border-radius: 5px;
    align-items: center;
    justify-content: space-between;
    .MuiSvgIcon-root {
        width: 37px;
        padding-left: 2px;
        height: 37px;
        border-radius: 5px;
        background-color: ${props => props.theme.primary};
        text-align: center;
    }
`;

export const InputText = styled.input`
    /* font-family: Montserrat, sans-serif; */
    color: #fff;
    background-color: transparent;
    padding-left: 10px;
    border: none;
    width: 100%;
    &:focus {
        outline: none;
    }
`;

export const IconExpandMore = styled(ExpandMoreIcon)``;

export const IconExpandLess = styled(ExpandLessIcon)`
    color: ${props => (props.open ? "#fff" : "#6c6c6c")};
`;

export const IconSearch = styled(SearchIcon)`
    color: #fff;
`;

export const IconClose = styled(CloseIcon)`
    color: #fff;
`;

export const ButtonSubmit = styled.button`
    background-color: transparent;
    border: none;
    padding: 0;
`;

export const LinkItem = styled(Link)`
    color: ${props => props.theme.letter};
    &:hover {
        color: ${props => props.theme.primary};
    }
    &:focus {
        outline: none;
    }
    .MuiTypography-body1 {
        border-bottom: ${props =>
            props.active ? `2px solid ${props.theme.primary}` : "none"};
        color: ${props => (props.active ? props.theme.letter : "none")};
        font-weight: ${props => (props.active ? "bold" : "none")};
    }
`;

export const Login = styled.div`
    cursor: pointer;
    width: 100%;
    background-color: transparent;
    display: flex;
    justify-content: center;
    align-items: center;
    img {
        width: 29px;
        height: 29px;
        border: 1px solid #979797;
        border-radius: 50%;
    }
    span {
        padding-left: 10px;
        color: ${props => (props.open ? "#fff" : "#fff")};
        font-weight: ${props => (props.open ? "bold" : "")};
        font-family: MontSerrat;
        font-size: 14px;
    }
`;
