import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import * as AuthActions from "@store/modules/auth/actions";
import List from "@material-ui/core/List";
import { Divider, ListItem, ListItemText, Collapse } from "@material-ui/core";
import {
    DrawerContainer,
    SearchBar,
    IconSearch,
    IconClose,
    InputText,
    ButtonSubmit,
    Login,
    LinkItem,
    IconMenu,
    IconExpandLess,
    IconExpandMore,
    ListaItem
} from "./styles";

const useStyles = makeStyles({
    list: {
        width: "100%"
    },
    fullList: {
        width: "auto"
    }
});

export default function TemporaryDrawer({ history }) {
    const dispatch = useDispatch();
    const classes = useStyles();
    const [state, setState] = useState({
        top: false,
        left: false,
        bottom: false,
        right: false
    });
    const [open, setOpen] = useState(false);

    const handleClick = () => {
        setOpen(!open);
    };

    const toggleDrawer = (side, open) => event => {
        if (
            event.type === "keydown" &&
            (event.key === "Tab" || event.key === "Shift")
        ) {
            return;
        }

        setState({ ...state, [side]: open });
    };
    const handleLogout = () => {
        toggleDrawer("left", false);
        dispatch(AuthActions.logoutRequest());
    };
    const handleSubmit = value => {
        alert(JSON.stringify(value));
    };

    const sideList = side => (
        <div className={classes.list} role="presentation">
            <List style={{ padding: "0" }}>
                <ListaItem button onClick={handleClick} open={open}>
                    <Login open={open}>
                        <div>
                            {/* <img
                                src="https://images.pexels.com/photos/2921424/pexels-photo-2921424.jpeg?cs=srgb&dl=adulto-atraente-beleza-2921424.jpg&fm=jpg"
                                alt="Avatar usuário"
                            /> */}
                            <span>Minha conta</span>
                        </div>
                        {open ? (
                            <IconExpandLess open={open} />
                        ) : (
                            <IconExpandMore />
                        )}
                    </Login>
                </ListaItem>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        {[
                            { name: "Meus Cursos", link: "conta" },
                            {
                                name: "Meus Pedidos",
                                link: "conta/meus-pedidos"
                            },
                            {
                                name: "Configurar Conta",
                                link: "conta/configurar-conta"
                            }
                            // { name: "Tutorial", link: "conta/tutorial" }
                        ].map(menu => (
                            <LinkItem
                                active={
                                    `/${menu.link}` === `${history.pathname}`
                                        ? "true"
                                        : undefined
                                }
                                key={menu.name}
                                to={`/${menu.link}`}
                            >
                                <ListItem
                                    onClick={toggleDrawer(side, false)}
                                    onKeyDown={toggleDrawer(side, false)}
                                >
                                    <ListItemText primary={menu.name} />
                                </ListItem>
                            </LinkItem>
                        ))}
                        <ListItem
                            className="logout"
                            onClick={handleLogout}
                            onKeyDown={toggleDrawer(side, false)}
                        >
                            <ListItemText primary="Sair" />
                        </ListItem>
                    </List>
                </Collapse>
            </List>
            <Divider />
            {/* <List>
                <ListItem>
                    <SearchBar onSubmit={value => handleSubmit(value)}>
                        <InputText
                            type="text"
                            name="searchfield"
                            id=""
                            placeholder="Pesquisa"
                        />
                        <ButtonSubmit type="submit">
                            <IconSearch />
                        </ButtonSubmit>
                    </SearchBar>
                </ListItem>
            </List> */}
            <List>
                {["Home", "Cursos", "Sobre", "Contato"].map(text => (
                    <LinkItem
                        key={text}
                        to={`/${text.toLocaleLowerCase()}`}
                        active={
                            `/${text.toLocaleLowerCase()}` ===
                            `${history.pathname}`
                                ? "true"
                                : undefined
                        }
                    >
                        <ListItem
                            onClick={toggleDrawer(side, false)}
                            onKeyDown={toggleDrawer(side, false)}
                        >
                            <ListItemText primary={text} />
                        </ListItem>
                    </LinkItem>
                ))}
            </List>
            <Divider />
            <List
                onClick={toggleDrawer(side, false)}
                onKeyDown={toggleDrawer(side, false)}
            >
                <ListItem button>
                    <IconClose fontSize="large" />
                </ListItem>
            </List>
        </div>
    );

    return (
        <div>
            <IconMenu fontSize="large" onClick={toggleDrawer("left", true)} />
            <DrawerContainer
                open={state.left}
                onClose={toggleDrawer("left", false)}
            >
                {sideList("left")}
            </DrawerContainer>
        </div>
    );
}
