import React from 'react';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty';
import LoyaltyOutlinedIcon from '@material-ui/icons/LoyaltyOutlined';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import LocalShippingOutlinedIcon from '@material-ui/icons/LocalShippingOutlined';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';
import SyncProblemIcon from '@material-ui/icons/SyncProblem';
import DoneAllOutlinedIcon from '@material-ui/icons/DoneAllOutlined';

const IconAlert = ({ status }) => {
    switch (status) {
        case 'pending':
            return <HourglassEmptyIcon fontSize="small" />;
        case 'packing':
            return <LoyaltyOutlinedIcon fontSize="small" />;
        case 'failed':
            return <ErrorOutlineIcon fontSize="small" />;
        case 'delivered':
            return <DoneAllOutlinedIcon fontSize="small" />;
        case 'shipping':
            return <LocalShippingOutlinedIcon fontSize="small" />;
        case 'canceled':
            return <CancelOutlinedIcon fontSize="small" />;
        case 'actionRequired':
            return <SyncProblemIcon fontSize="small" />;
        case 'done':
            return <DoneOutlineIcon fontSize="small" />;
        default:
            return null;
    }
};

export default IconAlert;
