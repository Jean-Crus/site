import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Document } from '@react-pdf/renderer';
import { formatPrice } from '@util/format';

import {
    PagePDF,
    HeaderPDF,
    TextHeader,
    TableItem,
    TableContainer,
    TableHeader,
    ProdItem,
    TitlePlaceholder,
    Name,
    Row,
    Column,
    Row20,
    Row30,
    Row70,
    NascTitle,
    RowLast,
    Row40,
    CityTitle,
    Row55,
    Row45,
    CpfTitle,
    TableTotal,
    SubItem,
    TotalValor,
    BusinessTitle,
    Row45WB,
    NameEmpresa,
    NameEnd,
    SubItemLast,
    TotalValorLast,
    Prod,
    Item,
    ItemChave,
    ProdChave,
    SubItemChave,
} from './styles';

const MyDocument = ({ order }) => {
    return (
        <Document>
            <PagePDF>
                <HeaderPDF>
                    <TextHeader>Detalhes do pedido</TextHeader>
                </HeaderPDF>
                <Column>
                    <Row>
                        <Row45WB>
                            <BusinessTitle>Vend.:</BusinessTitle>
                            <NameEmpresa>
                                EMPRESA EMPRESA COM NOME BEM EMPRESA
                            </NameEmpresa>
                        </Row45WB>
                        <Row30>
                            <NascTitle>CNPJ:</NascTitle>
                            <Name>121414214214214-1</Name>
                        </Row30>
                        <Row20>
                            <NascTitle>Data da compra:</NascTitle>
                            <Name>26/06/2019</Name>
                        </Row20>
                    </Row>

                    <RowLast>
                        <Row70>
                            <TitlePlaceholder>End.:</TitlePlaceholder>
                            <Name>
                                Rua cafundo do juda cafundo do juda cafundo do
                                juda cafundo do juda Rua cafundo do
                            </Name>
                        </Row70>
                    </RowLast>
                </Column>
                <Column>
                    <Row>
                        <Row70>
                            <TitlePlaceholder>Comp.:</TitlePlaceholder>
                            <Name>José João Maria Almeida</Name>
                        </Row70>
                        <Row30>
                            <NascTitle>Data nasc.:</NascTitle>
                            <Name>26/06/1998</Name>
                        </Row30>
                    </Row>

                    <Row>
                        <Row70>
                            <TitlePlaceholder>End:</TitlePlaceholder>
                            <NameEnd>
                                Rua cafundo do juda cafundo do, 447
                            </NameEnd>
                        </Row70>
                        <Row30>
                            <NascTitle>Pedido:</NascTitle>
                            <Name>#1212131</Name>
                        </Row30>
                    </Row>
                    <Row>
                        <Row40>
                            <CityTitle>Cidade:</CityTitle>
                            <Name>Curitiba</Name>
                        </Row40>
                        <Row20>
                            <NascTitle>Estado:</NascTitle>
                            <Name>PR</Name>
                        </Row20>
                        <Row20>
                            <NascTitle>Cep:</NascTitle>
                            <Name>81999-999</Name>
                        </Row20>
                        <Row20>
                            <NascTitle>Telefone:</NascTitle>
                            <Name>4199999999</Name>
                        </Row20>
                    </Row>
                    <RowLast>
                        <Row55>
                            <CpfTitle>CPF CNPJ Nº:</CpfTitle>
                            <Name>042141241444</Name>
                        </Row55>
                        <Row45>
                            <NascTitle>RG/INSC Nº:</NascTitle>
                            <Name>32837283728</Name>
                        </Row45>
                    </RowLast>
                </Column>
                <TableContainer>
                    <TableHeader>
                        <Item first>Item</Item>
                        <Prod>Produto</Prod>
                        <SubItem>Quantidade</SubItem>
                        <SubItem>Valor Unitario</SubItem>
                        <SubItem>Valor total</SubItem>
                    </TableHeader>
                    {order.items.map((prod, index) => {
                        return (
                            <TableTotal key={prod.id_item} index={index}>
                                <Item>{index + 1}</Item>
                                <ProdItem>{prod.name}</ProdItem>
                                <SubItem>{prod.quantity}</SubItem>
                                <SubItem>{formatPrice(prod.price)}</SubItem>
                                <TotalValor>
                                    {formatPrice(prod.price * prod.quantity)}
                                </TotalValor>
                            </TableTotal>
                        );
                    })}
                    <TableTotal>
                        <ItemChave />
                        <ProdChave />
                        <SubItemChave />
                        <SubItem total>SUB-TOTAL</SubItem>
                        <TotalValor total>R$ 11.750,50</TotalValor>
                    </TableTotal>
                    <TableItem>
                        <ItemChave />
                        <ProdChave>Chave de segurança: aa32322515a24</ProdChave>
                        <SubItemChave />
                        <SubItem total>FRETE</SubItem>
                        <TotalValor total>R$ 587,50</TotalValor>
                    </TableItem>
                    <TableItem>
                        <ItemChave />
                        <ProdChave />
                        <SubItemChave />
                        <SubItemLast total>TOTAL</SubItemLast>
                        <TotalValorLast total>R$ 13.537,50</TotalValorLast>
                    </TableItem>
                </TableContainer>
            </PagePDF>
        </Document>
    );
};

export default MyDocument;
