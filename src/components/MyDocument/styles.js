import styled from '@react-pdf/styled-components';

export const PagePDF = styled.Page`
    width: 100%;
    height: 100%;
    padding: 0 20px;
    flex-direction: column;
`;

export const HeaderPDF = styled.View`
    margin-top: 10px;
    flex-direction: row;
    justify-content: center;
`;

export const TextHeader = styled.Text`
    color: #31394d;
    font-size: 10px;
    font-weight: bold;
`;

export const RowLast = styled.View`
    flex-direction: row;
    width: 100%;
    border-bottom: ${props => `1pt solid #1f1f1f`};
    border-radius: 5px;
`;

export const RowFirst = styled.View`
    flex-direction: row;
    width: 100%;
`;

export const Row = styled.View`
    flex-direction: row;
    width: 100%;
    border-bottom: ${props => `1pt solid #1f1f1f`};
`;

export const Column = styled.View`
    margin-top: 10px;
    width: 100%;
    flex-direction: column;
    border: ${props => `1pt solid #1f1f1f`};
    border-bottom: transparent;
    border-radius: 5pt;
`;

export const Row60 = styled.View`
    flex-basis: 60%;
    flex-direction: row;
    align-items: center;
    padding: 5px 0;
`;

export const Row70 = styled.View`
    flex-basis: 70%;
    flex-direction: row;
    align-items: center;
    padding: 5px 0;
`;

export const Row55 = styled.View`
    flex-basis: 55%;
    flex-direction: row;
    align-items: center;
    padding: 5px 0;
`;

export const Row45 = styled.View`
    border-left: ${props => `1pt solid #1f1f1f`};
    flex-basis: 45%;
    flex-direction: row;
    align-items: center;
    padding: 5px 0;
`;

export const Row45WB = styled.View`
    flex-basis: 45%;
    flex-direction: row;
    align-items: center;
    padding: 5px 0;
`;

export const Row30 = styled.View`
    padding: 5px 0;
    flex-basis: 30%;
    flex-direction: row;
    align-items: center;
    border-left: ${props => `1pt solid #1f1f1f`};
    height: 100%;
`;

export const Row40 = styled.View`
    padding: 5px 0;
    flex-basis: 40%;
    flex-direction: row;
    align-items: center;
    height: 100%;
`;

export const Row10 = styled.View`
    padding: 5px 0;
    flex-basis: 10%;
    flex-direction: row;
    align-items: center;
    border-left: ${props => `1pt solid #1f1f1f`};
    height: 100%;
`;

export const Row20 = styled.View`
    padding: 5px 0;
    flex-basis: 20%;
    flex-direction: row;
    align-items: center;
    border-left: ${props => `1pt solid #1f1f1f`};
    height: 100%;
`;

export const NascTitle = styled.Text`
    padding: 5px;
    margin-left: 10px;
    font-size: 6px;
`;

export const CityTitle = styled.Text`
    flex-basis: 23%;
    padding: 5px;
    font-size: 6px;
    text-transform: uppercase;
`;

export const CpfTitle = styled.Text`
    flex-basis: 23%;
    padding: 5px;
    font-size: 6px;
    text-transform: uppercase;
`;

export const BusinessTitle = styled.Text`
    flex-basis: 20%;
    padding: 5px;
    font-size: 6px;
    text-transform: uppercase;
`;

export const TitlePlaceholder = styled.Text`
    flex-basis: 13%;
    padding: 5px;
    font-size: 6px;
    text-transform: uppercase;
`;

export const Name = styled.Text`
    padding: 5px 0;
    font-size: 10px;
`;

export const NameEnd = styled.Text`
    padding: 5px 0;
    font-size: 10px;
    max-width: 300pt;
`;

export const NameEmpresa = styled.Text`
    padding: 5px 0;
    font-size: 10px;
    max-width: 200pt;
`;

export const TableContainer = styled.View`
    margin-top: 20px;
    width: 100%;
    flex-direction: column;
    border-radius: 5pt;
    border: ${props => `1pt solid #1f1f1f`};
`;

export const TableHeader = styled.View`
    flex-direction: row;
    background-color: #f8f8f8;
    border-radius: 5pt;
`;

export const TableItem = styled.View`
    flex-direction: row;
`;

export const TableTotal = styled.View`
    flex-direction: row;
    border-top: ${props => `1pt solid #1f1f1f`};
    background-color: ${props => (props.index % 2 === 0 ? '#c3c3c3' : '')};
`;

export const ProdItem = styled.Text`
    flex-basis: 50%;
    flex-direction: row;
    color: #2e2e2e;
    font-size: 8px;
    align-items: center;
    text-align: left;
    border-left: ${props => `1pt solid #1f1f1f`};
    padding: 5px;
`;

export const Item = styled.Text`
    flex-basis: 5%;
    flex-direction: row;
    color: #2e2e2e;
    font-size: 8px;
    align-items: center;
    text-align: center;
    padding: 5px;
`;

export const Prod = styled.Text`
    flex-basis: 50%;
    flex-direction: row;
    color: #2e2e2e;
    font-size: 8px;
    align-items: center;
    text-align: center;
    border-left: ${props => `1pt solid #1f1f1f`};
    padding: 5px;
`;

export const SubItem = styled.Text`
    flex-basis: 15%;
    flex-direction: row;
    color: #2e2e2e;
    font-size: 8px;
    align-items: center;
    text-align: center;
    border-left: ${props => `1pt solid #1f1f1f`};
    border-bottom: ${props => (props.total ? `1pt solid #1f1f1f` : 'none')};
    padding: 5px;
`;

export const ItemChave = styled.Text`
    flex-basis: 5%;
    flex-direction: row;
    color: #2e2e2e;
    font-size: 8px;
    align-items: center;
    text-align: center;
    padding: 5px;
`;

export const ProdChave = styled.Text`
    flex-basis: 50%;
    flex-direction: row;
    color: #2e2e2e;
    font-size: 8px;
    align-items: center;
    text-align: center;
    padding: 5px;
`;

export const SubItemChave = styled.Text`
    flex-basis: 15%;
    flex-direction: row;
    color: #2e2e2e;
    font-size: 8px;
    align-items: center;
    text-align: center;
    padding: 5px;
`;

export const TotalValor = styled.Text`
    flex-basis: 15%;
    text-align: center;
    padding: 5px;
    flex-direction: row;
    color: #2e2e2e;
    font-size: 8px;
    font-weight: 600;
    text-align: center;
    align-items: center;
    justify-content: center;
    border-left: ${props => `1pt solid #1f1f1f`};
    border-bottom: ${props => (props.total ? `1pt solid #1f1f1f` : 'none')};
`;

export const SubItemLast = styled.Text`
    flex-basis: 15%;
    flex-direction: row;
    color: #2e2e2e;
    font-size: 8px;
    align-items: center;
    text-align: center;
    border: ${props => `1pt solid #1f1f1f`};
    border-top: none;
    border-right: none;
    border-bottom: none;
    padding: 5px;
`;

export const TotalValorLast = styled.Text`
    flex-basis: 15%;
    text-align: center;
    padding: 5px;
    flex-direction: row;
    color: #2e2e2e;
    font-size: 8px;
    font-weight: 600;
    text-align: center;
    align-items: center;
    justify-content: center;
    border-left: ${props => `1pt solid #1f1f1f`};
`;
