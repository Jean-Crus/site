import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Grid from '@material-ui/core/Grid';
import { logo } from '@images';

const useStyles = makeStyles(theme => ({
    appBar: {
        position: 'relative',
        backgroundColor: '#131313',
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog({ open, handleClose }) {
    const classes = useStyles();

    return (
        <div>
            <Dialog
                fullScreen
                open={open}
                onClose={handleClose}
                TransitionComponent={Transition}
            >
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <Grid
                            container
                            style={{ height: '100%' }}
                            alignItems="center"
                        >
                            <div style={{ width: '125px' }}>
                                <img src={logo} alt="Professor Carlos André" />
                            </div>
                        </Grid>
                        <Grid container justify="center" alignItems="center">
                            Chat Tesis
                        </Grid>
                        <Grid container justify="flex-end">
                            <IconButton
                                edge="start"
                                color="primary"
                                onClick={handleClose}
                                aria-label="close"
                            >
                                <CloseIcon />
                            </IconButton>
                        </Grid>
                    </Toolbar>
                </AppBar>
                <Grid container>
                    <iframe
                        style={{
                            width: '100%',
                            height: '800px',
                            border: 'none',
                        }}
                        title="chat"
                        src="https://www.tidio.com/talk/95c9pfnrz3ocgcolrlrube01ckbqrqeh"
                    />
                </Grid>
            </Dialog>
        </div>
    );
}
