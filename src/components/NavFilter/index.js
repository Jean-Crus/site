import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Checkbox, FormGroup } from "@material-ui/core";
import * as FilterActions from "@store/modules/filter/actions";
import { ButtonTriade, RangeFilterDefault } from "@components";
import {
    Container,
    HeaderSub,
    List,
    BodyNivel,
    FormLabel,
    Box
} from "./styles";

const NavFilter = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(FilterActions.getFiltersRequest());
    }, [dispatch]);

    const [expand, setExpand] = useState([
        { id: 0, active: false, title: "Preço" },
        { id: 1, active: false, title: "Nível" },
        { id: 2, active: false, title: "Categoria" }
    ]);

    const nivel = useSelector(state => state.filterCurso.nivel);
    const categoria = useSelector(state => state.filterCurso.categoria);

    useEffect(() => {}, [categoria]);
    const loading = useSelector(state => state.filterCurso.loading);
    const price = useSelector(state => state.filterCurso.price);
    const limit = useSelector(state => state.filterCurso.limit);

    const handleExpand = id => {
        const lista = expand.map(item => {
            if (item.id === id) {
                return { ...item, active: !item.active };
            }
            return item;
        });
        setExpand(lista);
    };

    const handlePrice = value => {
        dispatch(FilterActions.selectPrice(value));
    };

    const handleMax = (value, n) => {
        if (
            (value === "max" && price.max >= limit[1] && n === 1) ||
            (value === "min" && price.min <= limit[0] && n === -1) ||
            (value === "max" && price.max === price.min && n === -1) ||
            (value === "min" && price.max === price.min && n === 1)
        ) {
            return "";
        }
        return value === "max"
            ? dispatch(
                  FilterActions.selectPrice({
                      ...price,
                      max: price.max + n
                  })
              )
            : dispatch(
                  FilterActions.selectPrice({
                      ...price,
                      min: price.min + n
                  })
              );
    };

    const handleChangeNivel = name => () => {
        dispatch(FilterActions.selectFilterNivel(name));
    };

    const handleChangeCategoria = name => () => {
        dispatch(FilterActions.selectFilterCategoria(name));
    };

    const sendFilter = () => {
        dispatch(FilterActions.sendFilterRequest("filter"));
    };

    return (
        <Container>
            <Box>
                <div className="title">
                    <span>Filtrar por</span>
                    <button
                        type="button"
                        onClick={() => {
                            window.scrollTo(0, 0);
                            dispatch(FilterActions.removeAllRequest());
                        }}
                    >
                        {loading ? "Carregando..." : "Limpar"}
                    </button>
                </div>
                <main>
                    <List active={expand[0].active ? 0 : undefined}>
                        <HeaderSub
                            className="filter-title"
                            onClick={() => handleExpand(expand[0].id)}
                        >
                            <span>{expand[0].title}</span>
                            {expand[0].active ? (
                                <ExpandMoreIcon />
                            ) : (
                                <ExpandLessIcon />
                            )}
                        </HeaderSub>
                        <div className="filter-content">
                            <div className="range-div">
                                <RangeFilterDefault
                                    maxValue={limit[1]}
                                    minValue={limit[0]}
                                    initial={price}
                                    onChange={handlePrice}
                                />
                            </div>
                            <div className="range-button-container">
                                <div className="range-button">
                                    <button
                                        type="button"
                                        onClick={() => handleMax("min", -1)}
                                    >
                                        -
                                    </button>
                                    <input
                                        type="number"
                                        value={price.min}
                                        readOnly
                                    />
                                    <button
                                        type="button"
                                        onClick={() => handleMax("min", 1)}
                                    >
                                        +
                                    </button>
                                </div>
                                <div className="range-button">
                                    <button
                                        type="button"
                                        onClick={() => handleMax("max", -1)}
                                    >
                                        -
                                    </button>
                                    <input
                                        type="number"
                                        value={price.max}
                                        readOnly
                                    />
                                    <button
                                        type="button"
                                        onClick={() => handleMax("max", 1)}
                                    >
                                        +
                                    </button>
                                </div>
                            </div>
                        </div>
                    </List>
                    <List active={expand[1].active ? 1 : undefined}>
                        <HeaderSub
                            className="filter-title"
                            onClick={() => handleExpand(expand[1].id)}
                        >
                            <span>{expand[1].title}</span>
                            {expand[1].active ? (
                                <ExpandMoreIcon />
                            ) : (
                                <ExpandLessIcon />
                            )}
                        </HeaderSub>
                        <BodyNivel className="check-nivel">
                            <FormGroup row>
                                {nivel &&
                                    nivel.map(niv => {
                                        return (
                                            <FormLabel
                                                key={niv.id}
                                                checked={
                                                    niv.checked
                                                        ? true
                                                        : undefined
                                                }
                                                control={
                                                    <Checkbox
                                                        checked={niv.checked}
                                                        onChange={handleChangeNivel(
                                                            niv.id
                                                        )}
                                                        value={niv.checked}
                                                        color="primary"
                                                    />
                                                }
                                                label={niv.name}
                                            />
                                        );
                                    })}
                            </FormGroup>
                        </BodyNivel>
                    </List>
                    <List active={expand[2].active ? 2 : undefined}>
                        <HeaderSub
                            className="filter-title"
                            onClick={() => handleExpand(expand[2].id)}
                        >
                            <span>{expand[2].title}</span>
                            {expand[2].active ? (
                                <ExpandMoreIcon />
                            ) : (
                                <ExpandLessIcon />
                            )}
                        </HeaderSub>
                        <BodyNivel className="categoria">
                            <FormGroup row>
                                {categoria &&
                                    categoria.map(item => {
                                        return (
                                            <FormLabel
                                                key={item.id}
                                                checked={
                                                    item.checked
                                                        ? true
                                                        : undefined
                                                }
                                                control={
                                                    <Checkbox
                                                        checked={item.checked}
                                                        onChange={handleChangeCategoria(
                                                            item.id
                                                        )}
                                                        value={item.id}
                                                        color="primary"
                                                    />
                                                }
                                                label={item.name}
                                            />
                                        );
                                    })}
                            </FormGroup>
                        </BodyNivel>
                    </List>
                    <List>
                        <ButtonTriade
                            onClick={sendFilter}
                            readOnly
                            type="button"
                            text="Filtrar"
                            letterColor="Modelo 02"
                            button="Modelo 02"
                            cor="Modelo 02"
                        />
                    </List>
                </main>
            </Box>
        </Container>
    );
};

export default NavFilter;
