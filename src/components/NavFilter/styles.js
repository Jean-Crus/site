import styled from "styled-components";
import { FormControlLabel } from "@material-ui/core";
import { lighten } from "polished";

export const Container = styled.nav`
    width: 260px;
    display: flex;
    flex-direction: column;
    font-family: Montserrat;
    margin-right: 10px;
    .title {
        display: flex;
        justify-content: space-between;
        align-items: center;
        border-bottom: 1px solid #ececec;
        span {
            font-size: 18px;
            font-weight: bold;
            color: ${props => props.theme.background};
        }
        button {
            border: none;
            background-color: transparent;
            &:hover {
                color: ${props => lighten(0.1, `${props.theme.primary}`)};
            }
            color: ${props => props.theme.primary};
            font-size: 10px;
            font-weight: 600;
        }
    }

    main {
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        width: 100%;
        .input-range__track--active,
        .input-range__slider {
            background: ${props => props.theme.primary};
            border-color: ${props => props.theme.primary};
        }
        .input-range__label {
            font-family: Montserrat;
            color: ${props => props.theme.letter};
            font-size: 12px;
            font-weight: bold;
        }
        .input-range__label--value {
            margin: -9px 0;
        }
        .input-range__label-container {
            padding: 2px 5px;
            position: relative;
            border-radius: 5px;
            background-color: ${props => props.theme.primary};

            &::after {
                position: absolute;
                width: 6px;
                height: 6px;
                border-top: ${props => `0px solid ${props.theme.primary}`};
                border-right: ${props => `2px solid ${props.theme.primary}`};
                border-bottom: ${props => `2px solid ${props.theme.primary}`};
                border-left: ${props => `0px solid ${props.theme.primary}`};
                top: 87%;
                left: 50%;
                margin-left: -3px;
                content: "";
                transform: rotate(45deg);
                background: ${props => props.theme.primary};
            }
        }
        .input-range__label--min,
        .input-range__label--max {
            display: none !important;
            .input-range__label-container {
                padding: 2px 5px;
                background-color: ${props => props.theme.letter};
                color: ${props => props.theme.primary};
            }
        }
    }
    @media only screen and (max-width: 768px) {
        display: none;
    }
`;

export const Box = styled.div`
    position: sticky;
    top: 120px;
`;

export const HeaderSub = styled.div`
    cursor: pointer;
    svg {
        color: ${props => props.theme.primary};
    }
`;

export const List = styled.li`
    .MuiFormGroup-root {
        padding: 0 10px;
    }
    margin: 10px 0;
    display: flex;
    list-style: none;
    flex-direction: column;
    border-bottom: 1px solid #ececec;
    &:last-child {
        border-bottom: none;
    }
    button {
        height: 50px;
    }
    div {
        display: flex;
        justify-content: flex-start;
        button {
            width: 150px;
            margin: 0 !important;
            height: 45px;
            background-color: ${props => props.theme.primary};
        }
    }

    .filter-title {
        display: flex;
        justify-content: space-between;
        width: 100%;
        margin: 10px 0;
        span {
            color: ${props => props.theme.background};
            font-size: 14px;
            font-weight: bold;
        }
    }
    .filter-content {
        width: 100%;
        display: ${props => (props.active === 0 ? "flex" : "none")};
        flex-direction: column;
        .input-range {
            margin: 20px 0;
        }
        .range-div {
            margin: 0 10px;
        }
        .range-button-container {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-bottom: 20px;
            .range-button {
                border: 1px solid #dcdcdc;
                background-color: #ffffff;
                border-radius: 3px;
                display: flex;
                align-items: center;
                &:last-child {
                    margin-left: 10px;
                }
                button {
                    width: 25px;
                    height: 30px;
                    font-size: 18px;
                    font-weight: bold;
                    color: ${props => props.theme.primary};
                    text-align: center;
                    border: none;
                    background-color: transparent;
                }
                input[type="number"] {
                    border: none;
                    text-align: center;
                    border-left: 1px solid #dcdcdc;
                    border-right: 1px solid #dcdcdc;
                    width: 40px;
                    height: 30px;
                    font-weight: 600;
                    font-size: 14px;
                    color: ${props => props.theme.background};
                    &::-webkit-inner-spin-button,
                    &::-webkit-outer-spin-button {
                        -webkit-appearance: none;
                        margin: 0;
                    }
                }
            }
        }
    }
    .check-nivel {
        width: 100%;
        display: ${props => (props.active === 1 ? "flex" : "none")};
        flex-direction: column;
    }
    .categoria {
        width: 100%;
        display: ${props => (props.active === 2 ? "flex" : "none")};
        flex-direction: column;
    }
`;

export const BodyNivel = styled.div`
    max-height: 200px;
    overflow: auto;

    &::-webkit-scrollbar {
        width: 8px;
        background-color: #f5f5f5;
    }
    &::-webkit-scrollbar-thumb {
        background: linear-gradient(0deg, #ef9300 0%, #ef7300 100%);
        border: 1px solid transparent;
        border-radius: 9px;
        background-clip: content-box;
    }
    label {
        margin-bottom: 0;
    }
    .Mui-checked {
        color: ${props => props.theme.primary} !important;
    }
    .MuiIconButton-colorSecondary {
        color: ${props => props.theme.primary} !important;
    }
`;

export const FormLabel = styled(FormControlLabel)`
    .MuiTypography-body1 {
        font-family: Montserrat !important;
        color: ${props =>
            props.checked ? props.theme.background : "#6c6c6c"} !important;
        font-size: 14px !important;
        font-weight: ${props => (props.checked ? "bold" : "none")} !important;
    }
`;
