import styled from 'styled-components';
import { TwitterPicker, SliderPicker } from 'react-color';
import { darken } from 'polished';

export const Container = styled.main`
    display: flex;
    .button-template {
        max-width: 210px;
        max-height: 49px;
    }
`;

export const ColorPicker1 = styled(TwitterPicker)`
    margin: 12px 0;

    > div {
        &:nth-child(1) {
            left: initial !important;
            right: 21px !important;
        }
        &:nth-child(2) {
            left: initial !important;
            right: 21px !important;
        }
    }
`;
export const ColorPicker2 = styled(SliderPicker)``;
export const ColorComp = styled.div`
    display: ${props => (props.fechado ? 'flex' : 'none')};
    flex-direction: column;
`;

export const Cores = styled.button`
    cursor: pointer;
    width: 49px;
    height: 49px;
    border: transparent;
    border-radius: 5px;
    background: ${props =>
        `linear-gradient(0deg, ${props.backColor.colorOne} 0%, ${props.backColor.colorTwo} 100%)`};
    margin-right: 5px;
`;
