import React, { useState } from "react";
import { ButtonTriade } from "@components";
import {
    ColorComp,
    Cores,
    ColorPicker1,
    ColorPicker2,
    Container
} from "./styles";

const ButtonTemplate = ({
    handleClick,
    handleFunction,
    cor,
    id,
    modelo,
    readOnly,
    className
}) => {
    const [buttonClose, setButtonClose] = useState({ active: false, id: "" });
    const handleButton = name => {
        if (buttonClose.id === name) {
            setButtonClose({ active: !buttonClose.active, id: name });
        } else setButtonClose({ active: true, id: name });
    };
    return (
        <>
            <Container>
                <ButtonTriade
                    text={modelo}
                    button={modelo}
                    type="button"
                    letterColor={modelo}
                    className={`${className} button-template`}
                    readOnly={readOnly}
                    onClick={
                        () => handleFunction(modelo, id)
                        // dispatch(AppActions.changeButtonHome1('Modelo 01', slide.id))
                    }
                />
                <button
                    type="button"
                    className="plus"
                    onClick={() => handleButton(id)}
                >
                    {buttonClose.active && buttonClose.id === id ? "-" : "+"}
                </button>
            </Container>
            <ColorComp
                fechado={buttonClose.active && buttonClose.id === id}
                style={{ marginBottom: "10px" }}
            >
                <ColorPicker1
                    color={cor}
                    onChange={
                        c => handleFunction(c, id)
                        // dispatch(AppActions.changeButtonHome1(c, slide.id))
                    }
                />
                <ColorPicker2
                    color={cor}
                    onChange={
                        c => handleFunction(c, id)
                        // dispatch(AppActions.changeButtonHome1(c, slide.id))
                    }
                />
            </ColorComp>
        </>
    );
};

export default ButtonTemplate;
