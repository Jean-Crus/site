import styled from "styled-components";

export const Container = styled.div`
    position: relative;
    width: 100%;
    .MuiFormLabel-root.Mui-focused {
        color: #ef7300;
        background-color: #fff;
    }
    .MuiFilledInput-underline:after {
        background-color: transparent;
    }
    .MuiFilledInput-underline.Mui-error:after {
        background-color: transparent;
    }
    .MuiFilledInput-underline:hover:before {
        border: transparent;
    }
    .MuiFilledInput-underline:after {
        border-bottom: transparent;
    }
    .MuiFormLabel-root {
        color: #ef7300;
        font-family: Montserrat;
    }
    .MuiFormHelperText-root.Mui-error {
        color: ${props => props.errorColor};

        &:after {
            content: "*";
            color: red;
        }
    }
    .MuiFilledInput-root {
        background-color: #fff;
        border: 1px solid #ccc;
        border-radius: 6px;
    }
    .MuiFilledInput-root.Mui-focused {
        background-color: #fff;
        border: 2px solid #ef7300;
    }
    .MuiFilledInput-underline:before {
        border-bottom: transparent;
    }
    .MuiFilledInput-root:hover {
        background-color: #fff;
        border: 2px solid #ef7300;
    }
    .MuiFilledInput-root:focus {
        background-color: #fff;
        border: 2px solid #ef7300;
    }
    .MuiInputBase-input {
        font-family: Montserrat;
        font-weight: bold;
    }
`;
