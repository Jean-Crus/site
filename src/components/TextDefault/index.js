import React from "react";
import { TextField } from "@material-ui/core";
import { Container } from "./styles";

const TextDefault = ({
    placeholder,
    name,
    type,
    onChange,
    onBlur,
    value,
    helperText,
    error,
    className,
    errorColor,
    ...props
}) => {
    return (
        <Container errorColor={errorColor}>
            <TextField
                className={className}
                helperText={helperText}
                error={error}
                name={name}
                fullWidth
                label={placeholder}
                type={type}
                variant="filled"
                onChange={onChange}
                onBlur={onBlur}
                value={value}
                {...props}
            />
        </Container>
    );
};

export default TextDefault;
