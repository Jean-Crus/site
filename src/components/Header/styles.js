import styled from "styled-components";
import { Badge } from "@material-ui/core";

export const Search = styled.div`
    input {
        border: ${props => `1px solid ${props.theme.primary}`} !important;
    }
    button {
        &:hover {
            background-color: ${props => props.theme.primary} !important;
        }
    }
    .input-group {
        &:hover {
            .tr-search {
                background-color: ${props => props.theme.primary} !important;
            }
        }
    }
`;

export const HeaderContainer = styled.div`
    .language {
        p {
            color: #fff !important;
        }
    }
    .tr-header-nav {
        align-items: initial;
        .add-card {
            .show {
                .icon-button {
                    background-color: ${props => props.theme.primary};
                }
            }
            .group-card {
                .dropdown-menu {
                    border-top: ${props =>
                        `4px solid ${props.theme.primary} `}!important;
                    .card-view {
                        .status a {
                            color: ${props => props.theme.primary};
                        }
                    }
                }
            }
        }
    }
    .logo-full {
        display: flex;
        align-items: center;
        img {
            max-height: 50px;
            width: 100px !important;
        }
    }
    .background-header {
        background-color: ${props => props.theme.backgroundHeader} !important;
        .tr-header-nav {
            .itens-menu {
                .structure {
                    .itens {
                        &:hover {
                            &::before {
                                background-color: ${props =>
                                    props.theme.primary};
                            }
                        }
                        a {
                            color: #fff;
                        }
                    }
                }
            }
        }
    }
    .active {
        &::before {
            background-color: ${props => props.theme.primary} !important;
        }
        a {
            color: ${props => props.theme.letter} !important;
        }
    }

    @media only screen and (max-width: 1201px) {
        display: none;
    }
`;

export const CartNumber = styled(Badge)`
    .MuiBadge-colorPrimary {
        color: ${props => props.theme.letter};
        background-color: ${props => props.theme.primary};
    }
`;

export const CartToggle = styled.div`
    .item {
        ul {
            li {
                flex-wrap: nowrap !important;
            }
        }
    }
    .button-buy {
        height: auto !important;
        padding: 0px;
        margin: 0px;
        button {
            width: 100% !important;
        }
    }
    .purple {
        background-color: ${props => props.theme.primary} !important;
        color: ${props => props.theme.letter};
    }
    .tr-secondary {
        background-color: ${props => props.theme.secondary} !important;
        color: ${props => props.theme.letter};
    }
    .btn {
        width: 45% !important;
        height: 48px !important;
    }
    .description {
        flex-direction: row !important;
        .price {
            text-align: left !important;
            color: #ef7300 !important;
        }
    }
    .scrollbar {
        height: 50px !important;
        overflow: auto;
    }
    .scrollbar::-webkit-scrollbar {
        width: 8px;
        background-color: #f5f5f5;
    }
    .scrollbar::-webkit-scrollbar-thumb {
        background: linear-gradient(0deg, #ef9300 0%, #ef7300 100%);
        border: 1px solid transparent;
        border-radius: 9px;
        background-clip: content-box;
    }
`;
