import styled from "styled-components";

export const HeaderMobile = styled.div`
    position: fixed;
    padding: 0 20px;
    top: 0;
    z-index: 1000;
    display: flex;
    justify-content: space-between;
    border-bottom: ${props => `1px solid ${props.theme.letter}`};
    align-items: center;
    width: 100%;
    height: 50px;
    background: linear-gradient(0deg, #012a50 0%, #0058a8 100%);
    img {
        width: 100px;
        height: 45px;
    }
    .MuiSvgIcon-root {
        color: #fff;
    }

    @media only screen and (min-width: 1201px) {
        display: none;
    }
`;
