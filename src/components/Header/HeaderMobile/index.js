import React, { useRef, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { IconButton, Tooltip } from "@material-ui/core";
import * as CartActions from "@store/modules/cart/actions";
import { LoginMenu, Drawer } from "@components";
import { header } from "@theme";
import { CartNumber, CartToggle } from "../styles";
import { HeaderMobile } from "./styles";

const HeaderMob = ({ history }) => {
    const cart = useSelector(state => state.cart);
    const openCart = useSelector(state => state.cart.openCart);
    const dispatch = useDispatch();
    function useOutsideAlerter(ref) {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event) {
            if (
                ref.current &&
                !ref.current.contains(event.target) &&
                openCart
            ) {
                dispatch(CartActions.openCart());
            }
        }

        useEffect(() => {
            // Bind the event listener
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener("mousedown", handleClickOutside);
            };
        });
    }
    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef);

    const handleBuy = () => {
        dispatch(CartActions.contBuying());
    };

    const removeProd = id => {
        dispatch(CartActions.removeFromCart(id));
    };

    const handleCart = () => {
        dispatch(CartActions.openCart());
    };
    return (
        <HeaderMobile>
            <Drawer history={history} />
            <Link to="/">
                <img src={header.logo} alt="logo" />
            </Link>
            <Link to="/carrinho">
                <Tooltip title="Módulos">
                    <IconButton size="small" className="icon-button">
                        <CartNumber
                            color="primary"
                            badgeContent={cart.amount}
                            showZero
                        >
                            <ShoppingCartIcon style={{ color: "#fff" }} />
                        </CartNumber>
                    </IconButton>
                </Tooltip>
            </Link>
        </HeaderMobile>
    );
};

export default HeaderMob;
