import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Icon } from "@material-ui/core";
import * as AppActions from "@store/modules/app/actions";
import { SectionColorBack, ButtonTemplate, InsertImage } from "@components";
import { FirstHome } from "./styles";

const HomeOne = () => {
    const dispatch = useDispatch();
    const [keyProp, setKeyProp] = useState(0);
    const appTheme = useSelector(state => state.app.themeHome1);
    const [toggle, setToggle] = useState({
        active: true,
        id: appTheme.slides.length
    });

    const handleCor = (color, event, name) => {
        // setCor(color.hex);
        dispatch(AppActions.changeGeralHome1(color, name));
    };

    const handleChangeComplete = (color, name) => {
        dispatch(AppActions.changeGeralHome1(color, name));
    };

    const handleToggle = id => {
        if (toggle.id === id) {
            setToggle({ active: !toggle.active, id });
        } else setToggle({ active: true, id });
    };

    const slimInit = (data, slim) => {
        // slim instance reference
        // current slim data object and slim reference
    };

    // called when upload button is pressed or automatically if push is enabled
    const slimService = (formdata, progress, success, failure, slim, id) => {
        success(setKeyProp(keyProp + 1));
        let res;
        // Display the values
        for (const value of formdata.values()) {
            res = value;
        }
        const blobNew = JSON.parse(res);
        dispatch(AppActions.changeImageHome1(blobNew.output.image, "img", id));

        // call these methods to handle upload state
    };

    return (
        <FirstHome>
            <div
                className="title-slide"
                onClick={() => handleToggle(appTheme.slides.length)}
            >
                Geral
                {toggle.active && toggle.id === appTheme.slides.length ? (
                    <Icon className="fas fa-minus" fontSize="small" />
                ) : (
                    <Icon className="fas fa-plus" fontSize="small" />
                )}
            </div>

            {toggle.active && toggle.id === appTheme.slides.length ? (
                <>
                    <SectionColorBack
                        color={appTheme.background.colorOne}
                        title="Cor tela de fundo"
                        campo="background"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />
                    <SectionColorBack
                        color={appTheme.dotsTitle.colorOne}
                        title="Cor título destaque"
                        campo="dotsTitle"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />
                    <SectionColorBack
                        color={appTheme.dotsSub.colorOne}
                        title="Cor subtitulo destaque"
                        campo="dotsSub"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />
                    <SectionColorBack
                        color={appTheme.circle.colorOne}
                        title="Cor circulo"
                        campo="circle"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />
                    <SectionColorBack
                        color={appTheme.circleColor.colorOne}
                        title="Cor flechas circulo"
                        campo="circleColor"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />

                    <SectionColorBack
                        color={appTheme.slideArrow.colorOne}
                        title="Cor flechas slide"
                        campo="slideArrow"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />

                    <SectionColorBack
                        color={appTheme.dotsProgress.colorOne}
                        title="Cor progresso destaque"
                        campo="dotsProgress"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />
                </>
            ) : null}

            {appTheme.slides.map((slide, index) => {
                return (
                    <div key={slide.id}>
                        <div
                            className="title-slide"
                            onClick={() => handleToggle(slide.id)}
                        >
                            Slide {slide.id + 1}
                            {toggle.active && toggle.id === slide.id ? (
                                <Icon
                                    className="fas fa-minus"
                                    fontSize="small"
                                />
                            ) : (
                                <Icon
                                    className="fas fa-plus"
                                    fontSize="small"
                                />
                            )}
                        </div>
                        {toggle.active && toggle.id === slide.id ? (
                            <>
                                <InsertImage
                                    id={slide.id}
                                    campo="img"
                                    slimService={slimService}
                                    slimInit={slimInit}
                                    title={`Imagem slide ${index + 1}`}
                                    handleFunction={(src, campo, id) => {
                                        return dispatch(
                                            AppActions.changeImageHome1(
                                                src,
                                                campo,
                                                id
                                            )
                                        );
                                    }}
                                />
                                <section>
                                    <div className="title">
                                        Modelos de botões
                                    </div>
                                    <ButtonTemplate
                                        modelo="Modelo 01"
                                        className="mb-2 mr-1"
                                        cor={slide.button.cor}
                                        id={slide.id}
                                        readOnly
                                        handleFunction={(cor, id) =>
                                            dispatch(
                                                AppActions.changeButtonHome1(
                                                    cor,
                                                    id
                                                )
                                            )
                                        }
                                    />
                                    <ButtonTemplate
                                        modelo="Modelo 02"
                                        className="mb-2 mr-1"
                                        cor={slide.button.cor}
                                        id={slide.id}
                                        readOnly
                                        handleFunction={(cor, id) =>
                                            dispatch(
                                                AppActions.changeButtonHome1(
                                                    cor,
                                                    id
                                                )
                                            )
                                        }
                                    />

                                    <ButtonTemplate
                                        modelo="Modelo 02"
                                        className="mr-1"
                                        cor={slide.button.cor}
                                        id={slide.id}
                                        readOnly
                                        handleFunction={(cor, id) =>
                                            dispatch(
                                                AppActions.changeButtonHome1(
                                                    cor,
                                                    id
                                                )
                                            )
                                        }
                                    />
                                </section>
                            </>
                        ) : null}
                    </div>
                );
            })}
        </FirstHome>
    );
};

export default HomeOne;
