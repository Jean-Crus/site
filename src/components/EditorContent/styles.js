import styled from "styled-components";

export const Container = styled.main`
    display: ${props => (props.open ? "flex" : "none")};
    font-family: Montserrat;
    min-width: 360px;
    margin-top: 10px;
    position: relative;
    &:before {
        content: '';
        position: absolute;
        top: -20px;
        right: 10px;
        transform: rotate(90deg);
        border: solid 10px transparent;
        /* border-right-color: ${props => props.theme.background}; */
        border-right-color: #ccc;

        z-index: 1;
    }
    border-radius: 5px;
    padding: 30px;
    min-height: 150px;
    max-height: 514px;
    overflow: auto;
    background-color: #3e3d3d;
    /* background-color: ${props => props.theme.background}; */

    margin-right: 40px;
    &::-webkit-scrollbar {
        width: 8px;
        background-color: #f5f5f5;
    }
    &::-webkit-scrollbar-thumb {
        background: linear-gradient(0deg, #ef9300 0%, #ef7300 100%);
        border: 1px solid transparent;
        border-radius: 9px;
        background-clip: content-box;
    }
`;
