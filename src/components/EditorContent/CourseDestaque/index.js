import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Icon } from "@material-ui/core";
import * as AppActions from "@store/modules/app/actions";
import {
    SectionColorBack,
    ButtonTemplate,
    InsertImage,
    CheckEdit
} from "@components";
import { FirstHome } from "./styles";

const CourseDestaque = () => {
    const dispatch = useDispatch();
    const appTheme = useSelector(state => state.app.themeCourseDestaque);
    const [toggle, setToggle] = useState({
        active: true,
        id: 1
    });
    const handleCor = (color, event, name) => {
        // setCor(color.hex);
        dispatch(AppActions.changeGeralDestaque(color, name));
    };

    const handleChangeComplete = (color, name) => {
        dispatch(AppActions.changeGeralDestaque(color, name));
    };

    const handleToggle = id => {
        if (toggle.id === id) {
            setToggle({ active: !toggle.active, id });
        } else setToggle({ active: true, id });
    };

    return (
        <FirstHome>
            <div className="title-slide" onClick={() => handleToggle(1)}>
                Geral
                {toggle.active && toggle.id === 1 ? (
                    <Icon className="fas fa-minus" fontSize="small" />
                ) : (
                    <Icon className="fas fa-plus" fontSize="small" />
                )}
            </div>
            {toggle.active && toggle.id === 1 ? (
                <>
                    <SectionColorBack
                        color={appTheme.background.colorOne}
                        title="Cor tela de fundo"
                        campo="background"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />
                    <SectionColorBack
                        color={appTheme.titleCardColor.colorOne}
                        title="Cor título card"
                        campo="titleCardColor"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />
                    <SectionColorBack
                        color={appTheme.cardBackground.colorOne}
                        title="Card cor background"
                        campo="cardBackground"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />
                    <SectionColorBack
                        color={appTheme.textColor.colorOne}
                        title="Cor descrição card"
                        campo="textColor"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />

                    <SectionColorBack
                        color={appTheme.priceOfferColor.colorOne}
                        title="Cor letra oferta card"
                        campo="priceOfferColor"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />

                    <SectionColorBack
                        color={appTheme.priceHighlightColor.colorOne}
                        title="Cor letra preço card"
                        campo="priceHighlightColor"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />

                    <SectionColorBack
                        color={appTheme.tagsColor.colorOne}
                        title="Cor letra preço card"
                        campo="tagsColor"
                        handleCor={handleCor}
                        handleClick={handleChangeComplete}
                    />
                    <CheckEdit
                        title="Campos subtítulo"
                        handleFunction={checked => {}}
                    />
                    <section>
                        <div className="title">Botão número 1</div>
                        <ButtonTemplate
                            modelo="Modelo 01"
                            className="mb-2 mr-1"
                            // cor={slide.button.cor}
                            id="buttonOne"
                            readOnly
                            handleFunction={(cor, id) =>
                                dispatch(
                                    AppActions.changeButtonDestaque(cor, id)
                                )
                            }
                        />
                        <ButtonTemplate
                            modelo="Modelo 02"
                            className="mb-2 mr-1"
                            // cor={slide.button.cor}
                            id="buttonOne"
                            readOnly
                            handleFunction={(cor, id) =>
                                dispatch(
                                    AppActions.changeButtonDestaque(cor, id)
                                )
                            }
                        />

                        <ButtonTemplate
                            modelo="Modelo 02"
                            className="mr-1"
                            // cor={slide.button.cor}
                            id="buttonOne"
                            readOnly
                            handleFunction={(cor, id) =>
                                dispatch(
                                    AppActions.changeButtonDestaque(cor, id)
                                )
                            }
                        />
                    </section>
                    <section>
                        <div className="title">Botão número 2</div>
                        <ButtonTemplate
                            modelo="Modelo 01"
                            className="mb-2 mr-1"
                            // cor={slide.button.cor}
                            id="buttonTwo"
                            readOnly
                            handleFunction={(cor, id) =>
                                dispatch(
                                    AppActions.changeButtonDestaque(cor, id)
                                )
                            }
                        />
                        <ButtonTemplate
                            modelo="Modelo 02"
                            className="mb-2 mr-1"
                            // cor={slide.button.cor}
                            id="buttonTwo"
                            readOnly
                            handleFunction={(cor, id) =>
                                dispatch(
                                    AppActions.changeButtonDestaque(cor, id)
                                )
                            }
                        />

                        <ButtonTemplate
                            modelo="Modelo 02"
                            className="mr-1"
                            // cor={slide.button.cor}
                            id="buttonTwo"
                            readOnly
                            handleFunction={(cor, id) =>
                                dispatch(
                                    AppActions.changeButtonDestaque(cor, id)
                                )
                            }
                        />
                    </section>
                </>
            ) : null}
        </FirstHome>
    );
};

export default CourseDestaque;
