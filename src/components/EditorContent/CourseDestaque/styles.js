import styled from 'styled-components';
import { TwitterPicker, SliderPicker } from 'react-color';
import { darken } from 'polished';

export const ColorPicker1 = styled(TwitterPicker)`
    margin: 12px 0;

    > div {
        &:nth-child(1) {
            left: initial !important;
            right: 21px !important;
        }
        &:nth-child(2) {
            left: initial !important;
            right: 21px !important;
        }
    }
`;
export const ColorPicker2 = styled(SliderPicker)``;

export const Container = styled.div``;

export const ColorComp = styled.div`
    display: ${props => (props.fechado ? 'flex' : 'none')};
    flex-direction: column;
`;

export const FirstHome = styled.section`
    display: flex;
    flex-direction: column;
    width: 100%;

    .slim {
        margin-bottom: 30px;
        border-radius: 5px;
    }

    .title-slide {
        cursor: pointer;

        display: flex;
        justify-content: space-between;
        color: ${props => props.theme.letter};
        font-size: 18px;
        font-weight: bold;

        &:hover {
            color: ${darken(0.2, '#ffffff')};
            .MuiIcon-root {
                color: ${darken(0.2, '#ffffff')};
            }
        }
        .MuiIcon-fontSizeSmall {
            font-size: 14px;
        }
    }

    .title {
        color: ${props => props.theme.letter};
        font-size: 14px;
        /* font-weight: bold; */
    }
    .plus {
        background-color: transparent;
        width: 49px;
        height: 49px;
        border-radius: 5px;
        display: flex;
        font-size: 30px;
        color: ${props => props.theme.letter};
        cursor: pointer;
        align-items: center;
        justify-content: center;
        border: ${props => '2px solid #d2d2d2'};
        border-radius: 5px;
    }
    section {
        margin-bottom: 30px;
        display: flex;
        flex-direction: column;
        .img-cabecalho {
            display: flex;
            flex-wrap: wrap;
            img {
                cursor: pointer;
                width: 49px;
                height: 49px;
                border-radius: 5px;
                margin-right: 5px;
            }
        }

        .sobre-cores {
            display: flex;
            flex-wrap: wrap;
            textarea {
                border-radius: 5px;
            }
        }

        .blur-img {
            display: flex;
            justify-content: space-between;
            align-items: center;

            .input-range {
                max-width: 75%;
                .input-range__label-container {
                    display: none;
                }
            }
            .perc {
                display: flex;
                align-items: center;
                justify-content: center;
                font-size: 12px;
                color: ${props => props.theme.letter};
                border: ${props => '2px solid #d2d2d2'};
                width: 49px;
                padding: 2px 0;
                border-radius: 5px;
            }
        }

        .align-text {
            display: flex;
            div {
                cursor: pointer;
                width: 49px;
                height: 49px;
                display: flex;
                align-items: center;
                justify-content: center;
                background-color: ${props => '#1f1f1f4f'};
                margin-right: 10px;
                border-radius: 5px;
                &:last-child {
                    margin-right: 0;
                }
            }
        }
    }
`;

export const Cores = styled.button`
    cursor: pointer;
    width: 49px;
    height: 49px;
    border: transparent;
    border-radius: 5px;
    background: ${props =>
        `linear-gradient(0deg, ${props.backColor.colorOne} 0%, ${props.backColor.colorTwo} 100%)`};
    margin-right: 5px;
`;
