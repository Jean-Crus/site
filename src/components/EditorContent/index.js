import React, { useState } from "react";
import { Icon } from "@material-ui/core";
import { Container } from "./styles";
import "react-input-range/lib/css/index.css";
import HomeOne from "./HomeOne";
import CourseDestaque from "./CourseDestaque";

const EditorContent = ({ edit, open, close }) => {
    const [components, setComponents] = useState({
        homeOne: HomeOne,
        courseDestaque: CourseDestaque
    });

    const TagName = components[edit] || components.homeOne;

    return (
        <Container ref={close} open={open}>
            <TagName />
        </Container>
    );
};

export default EditorContent;
