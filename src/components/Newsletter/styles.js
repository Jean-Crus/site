import styled from "styled-components";
import { Grid } from "@material-ui/core";

export const Container = styled(Grid)`
    .content {
        .title-2 {
            text-align: center;
        }
    }
    .MuiTextField-root {
        background-color: ${props => props.theme.letter};
        border-radius: 5px;
    }
    .Mui-focused {
        color: ${props => props.theme.primary} !important;
    }
    .MuiFilledInput-underline:after {
        border-bottom: ${props => `2px solid ${props.theme.primary}`};
    }
    .MuiFilledInput-underline:before {
        border-bottom: transparent;
    }

    form {
        display: flex;
        width: 100%;
        align-items: center;
        justify-content: center;

        button {
            margin-top: 8px;
        }
        .MuiGrid-root {
            width: 100%;
        }
    }
`;
