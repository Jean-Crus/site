import React from "react";
import { TextField, MenuItem, Grid } from "@material-ui/core";
import { Formik } from "formik";
import { ButtonTriade } from "@components";
import { newsletter } from "@theme";
import { Container } from "./styles";

const Newsletter = () => {
    const initialValues = {
        duvida: "",
        email: ""
    };
    return (
        <Container container className="container the-news">
            <Formik
                initialValues={initialValues}
                enableReinitialize
                // validationSchema={schema}
                // validate={props => handleBrand(props.numberCard)}
            >
                {({
                    handleBlur,
                    handleChange,
                    handleSubmit,
                    values,
                    errors,
                    touched
                }) => {
                    return (
                        <form>
                            <Grid
                                item
                                xs={12}
                                sm={12}
                                md={12}
                                lg={12}
                                justify="center"
                                alignItems="center"
                                container
                                spacing={3}
                            >
                                <Grid
                                    item
                                    md={12}
                                    lg={3}
                                    container
                                    justify="center"
                                    className="content"
                                >
                                    <div className="title-2">
                                        <b>{newsletter.highlight}</b>
                                        {newsletter.sub}
                                    </div>
                                </Grid>
                                <Grid item xs={12} sm={12} md={7} lg={3}>
                                    <TextField
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        label={newsletter.doubtPlaceholder}
                                        margin="normal"
                                        fullWidth
                                        name="duvida"
                                        value={values.duvida}
                                        select
                                        variant="filled"
                                    >
                                        <MenuItem
                                            value={newsletter.doubtValue1}
                                        >
                                            {newsletter.doubtValue1}
                                        </MenuItem>
                                        <MenuItem
                                            value={newsletter.doubtValue2}
                                        >
                                            {newsletter.doubtValue2}
                                        </MenuItem>
                                        <MenuItem
                                            value={newsletter.doubtValue3}
                                        >
                                            {newsletter.doubtValue3}
                                        </MenuItem>
                                        <MenuItem
                                            value={newsletter.doubtValue4}
                                        >
                                            {newsletter.doubtValue4}
                                        </MenuItem>
                                    </TextField>
                                </Grid>
                                <Grid item xs={12} sm={12} md={7} lg={4}>
                                    <TextField
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        label={newsletter.email}
                                        name="email"
                                        value={values.email}
                                        margin="normal"
                                        fullWidth
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid
                                    item
                                    xs={12}
                                    sm={12}
                                    md={12}
                                    lg={2}
                                    container
                                    justify="center"
                                    alignItems="center"
                                >
                                    <ButtonTriade
                                        text={newsletter.buttonTitle}
                                        button="#fff"
                                        letterColor="#ef7300"
                                        cor="#fff"
                                        type="button"
                                    />
                                </Grid>
                            </Grid>
                        </form>
                    );
                }}
            </Formik>
        </Container>
    );
};

export default Newsletter;
