import React, { useRef, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import * as CartActions from "@store/modules/cart/actions";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import historylink from "@routes/history";

import { brasil } from "@images";
import logo from "@images/logo.png";

import { LoggedMenu, ButtonTriade } from "@components";
import { CartNumber, CartToggle, HeaderContainer, Search } from "./styles";
import HeaderMob from "./HeaderMobileConta";

const HeaderConta = ({ history }) => {
    const openCart = useSelector(state => state.cart.openCart);
    const dispatch = useDispatch();
    function useOutsideAlerter(ref) {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event) {
            if (
                ref.current &&
                !ref.current.contains(event.target) &&
                openCart
            ) {
                dispatch(CartActions.openCart());
            }
        }

        useEffect(() => {
            // Bind the event listener
            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener("mousedown", handleClickOutside);
            };
        });
    }
    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef);

    const handleBuy = () => {
        dispatch(CartActions.contBuying());
    };

    const removeProd = id => {
        dispatch(CartActions.removeFromCart(id));
    };

    const handleCart = () => {
        dispatch(CartActions.openCart());
    };

    const cart = useSelector(state => state.cart);
    const menu = [
        {
            nome: "Home",
            link: "/"
        },
        {
            nome: "Cursos",
            link: "/cursos"
        },
        // {
        //     nome: 'Eventos',
        //     link: '/eventos'
        // },
        // {
        //     nome: 'Produtos',
        //     link: '/produtos'
        // },
        // {
        //     nome: 'Artigos',
        //     link: '/artigos'
        // },
        {
            nome: "Sobre",
            link: "/sobre"
        },
        // {
        //     nome: "Depoimentos",
        //     link: "/depoimentos"
        // },
        {
            nome: "Contato",
            link: "/contato"
        }
    ];

    return (
        <>
            <HeaderMob history={history} />
            <HeaderContainer>
                <div className="background-header">
                    <header className="tr-header-nav container">
                        <Link to="/">
                            <div className="logo-full">
                                <img src={logo} alt="Professor Carlos André" />
                            </div>
                        </Link>
                        <div className="itens-menu">
                            <ul className="structure">
                                {menu.map((item, index) => (
                                    <li
                                        key={item.nome}
                                        className={
                                            history.pathname === item.link
                                                ? "itens active"
                                                : "itens"
                                        }
                                    >
                                        <Link to={item.link}>{item.nome}</Link>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        {/* <Search className="search">
                            <form action="pages/search_results.php">
                                <div className="input-group">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Pesquisar"
                                        aria-label="Recipient's username"
                                        aria-describedby="basic-addon2"
                                    />
                                    <div className="input-group-append">
                                        <button
                                            className="btn tr-search"
                                            type="button"
                                        />
                                    </div>
                                </div>
                            </form>
                        </Search> */}
                        <div className="add-card">
                            <div className="group-card">
                                <div className="cart" onClick={handleCart}>
                                    <Tooltip
                                        title="Carrinho de compras"
                                        placement="bottom"
                                    >
                                        <IconButton
                                            size="small"
                                            className="icon-button"
                                        >
                                            <CartNumber
                                                badgeContent={cart.amount}
                                                showZero
                                                color="primary"
                                            >
                                                <ShoppingCartIcon
                                                    style={{ color: "#fff" }}
                                                />
                                            </CartNumber>
                                        </IconButton>
                                    </Tooltip>
                                </div>

                                <CartToggle
                                    ref={wrapperRef}
                                    className={`dropdown-menu ${
                                        openCart ? "show" : ""
                                    }`}
                                    aria-labelledby="dropdownMenuLink"
                                >
                                    <div className="card-view scrollbar">
                                        <div className="status">
                                            <div className="quantity">
                                                {cart.amount
                                                    ? `Carrinho (${cart.amount})`
                                                    : "Carrinho vazio"}
                                            </div>
                                            <Link to="/carrinho">
                                                Ver carrinho
                                            </Link>
                                        </div>
                                        <div className="item">
                                            <ul>
                                                {cart.empty
                                                    ? "O seu carrinho não possui nenhum item"
                                                    : cart.data.map(items => {
                                                          return (
                                                              <li
                                                                  key={items.id}
                                                              >
                                                                  <Link
                                                                      to={`/curso-aberto/${items.id}`}
                                                                  >
                                                                      <div
                                                                          className="image"
                                                                          style={{
                                                                              backgroundImage: `url(${items.cover})`
                                                                          }}
                                                                      />
                                                                      <div className="description">
                                                                          <div className="title">
                                                                              {
                                                                                  items.name
                                                                              }
                                                                          </div>
                                                                          <div className="price">
                                                                              {
                                                                                  items.priceFormatted
                                                                              }
                                                                          </div>
                                                                      </div>
                                                                  </Link>
                                                                  <div className="remove">
                                                                      <Tooltip title="Remover do carrinho">
                                                                          <IconButton
                                                                              size="small"
                                                                              onClick={() =>
                                                                                  removeProd(
                                                                                      items.id
                                                                                  )
                                                                              }
                                                                          >
                                                                              <HighlightOffIcon color="error" />
                                                                          </IconButton>
                                                                      </Tooltip>
                                                                  </div>
                                                              </li>
                                                          );
                                                      })}
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="button-buy row">
                                        <div className="col-lg-12">
                                            <ButtonTriade
                                                text="Continuar comprando"
                                                button="Modelo 02"
                                                className="mt-2"
                                                letterColor="Modelo 02"
                                                cor="Modelo 02"
                                                type="button"
                                                onClick={handleBuy}
                                            />
                                        </div>
                                        {!cart.empty && (
                                            <div className="col-lg-12">
                                                <ButtonTriade
                                                    text="Finalizar compra"
                                                    button="Modelo 02"
                                                    className="mt-2"
                                                    letterColor="Modelo 02"
                                                    cor="Modelo 02"
                                                    onClick={() => {
                                                        dispatch(
                                                            CartActions.contBuying()
                                                        );
                                                        historylink.push(
                                                            "/carrinho"
                                                        );
                                                    }}
                                                />
                                            </div>
                                        )}
                                    </div>
                                </CartToggle>
                            </div>
                        </div>

                        <div className="login logon">
                            <div className="btn-group">
                                <LoggedMenu />
                            </div>
                        </div>
                        <div className="language">
                            <div className="icon-lg">
                                <img src={brasil} alt="brasil" />
                            </div>
                            <p>PT</p>
                        </div>
                    </header>
                </div>
            </HeaderContainer>
        </>
    );
};

export default HeaderConta;
