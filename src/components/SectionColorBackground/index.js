import React, { useState } from 'react';
import {
    ColorPicker1,
    ColorComp,
    ColorPicker2,
    Cores,
    Container,
} from './styles';

const SectionColorBack = ({ handleClick, handleCor, color, campo, title }) => {
    const [fechado, setFechado] = useState({ active: false, id: '' });
    const handleFechado = number => {
        if (fechado.id === number) {
            setFechado({ active: !fechado.active, id: number });
        } else setFechado({ active: true, id: number });
    };
    return (
        <Container>
            <div className="title">{title}</div>
            <div className="sobre-cores">
                <Cores
                    backColor={{
                        colorOne: '#EEB4C1',
                        colorTwo: '#FFDFE6',
                    }}
                    onClick={() =>
                        handleClick(
                            {
                                colorOne: '#EEB4C1',
                                colorTwo: '#FFDFE6',
                            },
                            campo
                        )
                    }
                />
                <Cores
                    backColor={{
                        colorOne: '#524F4B',
                        colorTwo: '#35322F',
                    }}
                    onClick={() =>
                        handleClick(
                            {
                                colorOne: '#524F4B',
                                colorTwo: '#35322F',
                            },
                            campo
                        )
                    }
                />
                <Cores
                    backColor={{
                        colorOne: '#F2F1F6',
                        colorTwo: '#D7D5DE',
                    }}
                    onClick={() =>
                        handleClick(
                            {
                                colorOne: '#F2F1F6',
                                colorTwo: '#D7D5DE',
                            },
                            campo
                        )
                    }
                />
                <Cores
                    backColor={{
                        colorOne: '#7674FF',
                        colorTwo: '#4F53E6',
                    }}
                    onClick={() =>
                        handleClick(
                            {
                                colorOne: '#7674FF',
                                colorTwo: '#4F53E6',
                            },
                            campo
                        )
                    }
                />
                <button
                    type="button"
                    className="plus"
                    onClick={() => handleFechado(1)}
                >
                    {fechado.active && fechado.id === 1 ? '-' : '+'}
                </button>
                <ColorComp fechado={fechado.active && fechado.id === 1}>
                    <ColorPicker1
                        color={color}
                        onChange={c => handleCor(c, '', campo)}
                    />
                    <ColorPicker2
                        color={color}
                        onChange={c => handleCor(c, '', campo)}
                    />
                </ColorComp>
            </div>
        </Container>
    );
};
export default SectionColorBack;
