import React from 'react';
import Slider from "react-slick";

const BannerFull = ({content,}, state) => {

    var settings = {
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 800,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1
      };

    return (
        <div id="banner-full">
            <Slider {...settings}>
                { content.map(item => 
                    (
                        <div key={Math.random()}>
                            <div className="image" style={{
                                backgroundImage: `url('${item.image}')`
                            }}></div>
                            <div className="container">
                                <div className="col-sm-5">
                                    <h2>{item.title}{item.title2 && <span>{item.title2}</span>}</h2>
                                    <a href={item.link}>Cursos</a>
                                </div>
                            </div>
                        </div>
                    )
                )}
            </Slider>
        </div>
    );
};

export default BannerFull;