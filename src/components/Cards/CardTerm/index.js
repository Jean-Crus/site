import React from 'react';
import { Link } from 'react-router-dom';

class CardTerm extends React.Component {
    
    constructor(props){
        super(props);
    }
    
    render(){

        return(
            <div className="tr-card-term-use">
                <div className="image-terms">
                    <span className="file-image"></span>
                    <div className="title-1 black size-50">Termos de uso</div>
                </div>
                <div className="readme">
                    <div className="description-2 black">
                        Recomendamos que você leia com atenção
                        Termos de Uso do “Cursinho”, que contém
                        os seus direitos e deveres com relação ao curso.
                    </div>
                </div>
                <div className="read-term">
                    <div className="buttons">
                        <Link to="#" className="btn tr-third small">Li e aceito os termos de Uso </Link>
                    </div>
                    <div className="terms-archive">
                        <Link to="#" className="tr-link"> Ver os termos de uso</Link>
                    </div>
                </div>
            </div>
        );
    }
};

export default CardTerm;
