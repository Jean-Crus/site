import React from 'react';
import { ButtonTriade } from '@components';

class CardSuccess extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { title, description } = this.props;

        return (
            <div className="tr-card-sucess">
                <div className="image-sucess">
                    <span className="file-image"></span>
                    <div className="title-1 black size-25">
                        {title && title}
                    </div>
                </div>
                <div className="wait-list">
                    <div className="description-2 black">
                        {description && description}
                    </div>
                </div>
                <div className="closed">
                    <div className="buttons">
                        <ButtonTriade text="Ok" button="primary" />
                    </div>
                </div>
            </div>
        );
    }
}

export default CardSuccess;
