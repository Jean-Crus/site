import React from 'react';
import { Link } from 'react-router-dom';

class CardProduct extends React.Component {
    
    constructor(props){
        super(props);
    } 
    
    render(){

        const { title, price, salePrice, discount, newProd, image } = this.props;

        const style = {
            image: {
                backgroundImage: "url('" + image + "')",
            }
        }

        return(
            <div className="tr-card-product">
                <Link to="#" className="tr-addFavorite add">
                    <i className="far fa-heart"></i>
                </Link>
                {discount && (
                    <span className="product-desc discount">
                        <b>{discount && discount}</b>
                        OFF
                    </span>
                )}

                {newProd && (
                    <span className="product-desc new">
                        <b>NOVO</b>
                    </span>
                )}
                
                <div className="image">
                    <Link to="#">
                        <span className="image-full" style={style.image}></span>
                    </Link>
                </div>
                <div className="description">
                    <Link to="#" className="title-1 black ff-16 wh-600">
                        {title && title}
                    </Link>
                </div>
                <div className="price">
                    <del>R$ {price && price}</del>
                    <ins>R$ {salePrice && salePrice}</ins>
                </div>
            </div>
        );
    }
};

export default CardProduct;
