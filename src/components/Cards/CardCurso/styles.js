import styled from "styled-components";

export const Container = styled.div`
    overflow: initial !important;
    margin-bottom: ${props => (props.readOnly ? "" : "220px")} !important;
    .MuiSvgIcon-colorDisabled {
        color: ${props => props.themeCard.tagsColor.colorOne};
    }
    .title-5.red {
        color: ${props => props.themeCard.titleCardColor.colorOne};
    }
    .title-5.black {
        color: ${props => props.themeCard.priceHighlightColor.colorOne};
    }
    .description-2 {
        color: ${props => props.themeCard.priceOfferColor.colorOne};
        font-size: 10px;
    }

    .description-2.text-descript {
        color: ${props => props.themeCard.textColor.colorOne};
    }
    .name,
    .footer,
    .hover-card {
        /* border: none !important; */
        background: ${props =>
            `linear-gradient(0deg, ${props.themeCard.cardBackground.colorOne} 0%, ${props.themeCard.cardBackground.colorTwo} 100%)`};

        .buttons.buttons-tesis {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            flex: 1 1 10% !important;
        }
        .header.header-styled {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            flex: 1 1 20% !important;
        }
    }
    .footer,
    .footer-active {
        padding: 0 !important;
        padding-right: 5px !important;
    }
    @media only screen and (max-width: 700px) {
        display: none !important;
    }
    button {
        height: 50px;
        p {
            text-align: center;
        }
    }
    .hover-card {
        .header {
            .box-1 {
                height: 20%;
                display: flex;
                flex-direction: column;
            }
            .box-2 {
                height: 80%;

                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
            }
            .information {
                .description-3 {
                    color: ${props =>
                        props.themeCard.tagsColor.colorOne} !important;
                    display: flex;
                    flex-direction: column;
                    align-items: center;
                }
            }
        }
        .footer-active {
            padding: 0 !important;
            padding-right: 5px !important;
        }
    }

    .price-desc,
    .price {
        padding: 0 !important;
    }
`;
