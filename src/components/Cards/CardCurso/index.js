import React from "react";
import { useSelector } from "react-redux";
import ScheduleIcon from "@material-ui/icons/Schedule";
import AssignmentIcon from "@material-ui/icons/Assignment";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { convertText } from "@util/tools";
import history from "@routes/history";
import { ButtonTriade, QuillEditor } from "@components";
import cardcurso from "@images/cardcurso.jpg";
import { Container } from "./styles";

function CardCurso({
    image,
    link,
    title,
    category,
    colorCategory,
    closed,
    price,
    salePrice,
    salePriceFormatted,
    priceFormatted,
    hours,
    modules,
    description,
    label,
    onClick,
    id,
    cursos,
    readOnly
}) {
    const marginStyle = cursos
        ? { margin: 0, marginRight: "40px", marginBottom: "40px" }
        : null;

    const style = {
        image: {
            backgroundImage: `url('${image}')`
        }
    };

    const theme = useSelector(state => state.app.themeCourseDestaque);

    const handleClick = () => {
        history.push(`/curso-aberto/${id}`);
    };

    return (
        <Container
            className="tr-card"
            style={marginStyle}
            readOnly
            themeCard={theme}
        >
            <div className="image-full">
                <span className="file-image" style={style.image} />

                <span
                    className="badge"
                    style={{
                        backgroundColor: `${colorCategory}`,
                        color: "#ffffffff"
                    }}
                >
                    {category}
                </span>
            </div>
            <div className="name">
                <div className="title-5 red">{convertText(title, 50)}</div>
            </div>
            {closed ? (
                <div className="footer">
                    <div className="inscriptions-closed">
                        <div className="title-6">Inscrições Encerradas</div>
                    </div>
                </div>
            ) : (
                <div className="footer">
                    <div className="price-desc">
                        <div className="description-3">
                            <QuillEditor
                                theme="bubble"
                                value={theme.textOffer}
                                className="title"
                                campo="title"
                                id={id}
                                readOnly={readOnly}
                                onChange={() => {}}
                            />
                        </div>
                    </div>
                    {salePrice > 0 ? (
                        <div className="old-price">
                            <del className="description-2">
                                {priceFormatted}
                            </del>
                        </div>
                    ) : null}
                    {salePrice > 0 ? (
                        <div className="price">
                            <b className="title-5 black">
                                {salePriceFormatted}
                            </b>
                        </div>
                    ) : (
                        <div className="price">
                            <b className="title-5 black">{priceFormatted}</b>
                        </div>
                    )}
                </div>
            )}
            <div className="hover-card">
                <div className="header header-styled">
                    <div className="box-1">
                        <span
                            className="badge"
                            style={{
                                color: "#fff",
                                backgroundColor: `${colorCategory}`
                            }}
                        >
                            {category}
                        </span>
                        <div className="title-5 red">{title}</div>
                    </div>
                    <div className="box-2">
                        <ButtonTriade
                            text={theme.buttonOne.title}
                            className="mb-2"
                            button={theme.buttonOne.cor}
                            type="button"
                            onClick={onClick}
                            letterColor={theme.buttonOne.letterColor}
                            readOnly
                        />
                        <ButtonTriade
                            className="mb-2"
                            text={theme.buttonTwo.title}
                            button={theme.buttonTwo.cor}
                            type="button"
                            letterColor={theme.buttonTwo.letterColor}
                            onClick={() => handleClick(id)}
                            readOnly
                        />
                    </div>
                    {/* {theme.subtitleCheck && (
                        <div className="information">
                            <div className="description-3">
                                <ScheduleIcon color="disabled" />
                                {hours < 2 ? `${hours} Hora` : `${hours} Horas`}
                            </div>
                            <div className=" description-3">
                                <AssignmentIcon color="disabled" />
                                {modules < 2
                                    ? `${modules} Módulo`
                                    : `${modules} Módulos`}
                            </div>
                            <div className="description-3">
                                <StarBorderIcon color="disabled" />
                                {convertText(label, 6) ||
                                    convertText("Texto grande", 6)}
                            </div>
                        </div>
                    )} */}
                </div>
                {/* <div className="content">
                    <p className="description-2 text-descript">
                        {convertText(description, 144)}
                    </p>
                </div> */}
                {/* <div className="buttons buttons-tesis">
                    {closed ? (
                        <div className="inscriptions-closed">
                            <a
                                href="#"
                                className="btn tr-third small"
                                data-toggle="modal"
                                data-target="#listadeesperacurso"
                            >
                                Entrar na lista de espera
                            </a>
                        </div>
                    ) : null
                    <div className="add-card">
                    <ButtonTriade
                        text={theme.buttonOne.title}
                        className="mb-2"
                        button={theme.buttonOne.cor}
                        type="button"
                        onClick={onClick}
                        letterColor={theme.buttonOne.letterColor}
                        readOnly
                    />

                    <ButtonTriade
                        text="Adicionar ao carrinho"
                        button="primary"
                        size="small"

                        readOnly
                    />
                    }
                    <ButtonTriade
                        className="mb-2"
                        text={theme.buttonTwo.title}
                        button={theme.buttonTwo.cor}
                        type="button"
                        letterColor={theme.buttonTwo.letterColor}
                        onClick={() => handleClick(id)}
                        readOnly
                    />
                    <ButtonTriade
                        text="Saiba mais"
                        button="secondary"
                        className="mt-2 mb-2"
                        size="small"
                        readOnly

                    />
                </div> */}
                {closed ? (
                    <div className="footer-active">
                        <div className="inscriptions-closed">
                            <div className="title-6">Inscrições Encerradas</div>
                        </div>
                    </div>
                ) : (
                    <div className="footer-active">
                        <div className="price-desc">
                            <div className="description-3">
                                <QuillEditor
                                    theme="bubble"
                                    value={theme.textOffer}
                                    className="title"
                                    campo="title"
                                    id={id}
                                    // readOnly
                                    onChange={() => {}}
                                />
                            </div>
                        </div>
                        {salePrice > 0 ? (
                            <div className="old-price">
                                <del className="description-2">
                                    {priceFormatted}
                                </del>
                            </div>
                        ) : null}
                        {salePrice > 0 ? (
                            <div className="price">
                                <b className="title-5 black">
                                    {salePriceFormatted}
                                </b>
                            </div>
                        ) : (
                            <div className="price">
                                <b className="title-5 black">
                                    {priceFormatted}
                                </b>
                            </div>
                        )}
                    </div>
                )}
            </div>
        </Container>
    );
}

export default CardCurso;
