import React from 'react';
import { ButtonTriade } from '@components';

class CardWait extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="tr-card-wait-list">
                <div className="wait-description">
                    <div className="title-1 black size-25">
                        Curso de <br /> Lorem Ipsum Avançado
                    </div>
                </div>
                <div className="form-wait">
                    <form action="" className="tr-form">
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nome"
                                value="Matheus"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="e-mail"
                                className="form-control"
                                value="mattheus"
                                placeholder="E-mail"
                                pattern="^((?!mattheus).)*$"
                            />
                            <small className="error">Email inválido</small>
                        </div>
                        <div className="form-group">
                            <input
                                type="number"
                                className="form-control"
                                placeholder="Telefone"
                            />
                        </div>
                        <ButtonTriade
                            button="primary"
                            size="small"
                            text="Me coloque na lista de espera"
                        />
                    </form>
                </div>
            </div>
        );
    }
}

export default CardWait;
