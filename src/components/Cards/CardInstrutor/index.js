import React from "react";
import { Grid, Icon } from "@material-ui/core";
import compress from "@images/sobreProfessor.png";
import { convertText } from "@util/tools";
import { Container } from "./styles";

const CardInstrutor = ({ instrutor, onClick }) => {
    const style = {
        compress: {
            backgroundImage: `url('${compress}')`
        }
    };

    return (
        <Container
            container
            className="card-instrutor mb-4 mt-4"
            onClick={onClick}
        >
            <Grid
                md={2}
                lg={2}
                xl={2}
                xs={12}
                sm={2}
                item
                className="content"
                container
                justify="center"
            >
                <Grid
                    md={12}
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    item
                    container
                    justify="center"
                    alignItems="center"
                >
                    <div
                        className="picture"
                        style={{
                            backgroundImage: `url(${instrutor.url_photo})`
                        }}
                    />
                </Grid>
                <Grid
                    item
                    md={12}
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    className="mt-2 mb-2"
                >
                    {/* <div className="description-3 line-center">
                        <span>Conecte-se</span>
                    </div> */}
                    <ul>
                        {instrutor.social_network > 0 &&
                            instrutor.social_network.map((item, index) => {
                                return (
                                    index <= 3 && (
                                        <li key={item.id}>
                                            <a
                                                href={item.link}
                                                target="_blank"
                                                rel="noopener noreferrer"
                                            >
                                                <Icon className={item.icon} />
                                            </a>
                                        </li>
                                    )
                                );
                            })}
                    </ul>
                </Grid>
            </Grid>
            <Grid
                md={10}
                lg={10}
                xl={10}
                xs={12}
                sm={10}
                item
                className="nome-profile"
                container
                alignItems="center"
                justify="flex-start"
            >
                <Grid
                    item
                    md={12}
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    className="nome-instrutor"
                >
                    <div className="instrutor-name">
                        {convertText(instrutor.name, 85)}
                    </div>
                    {/* <div className="profession">
                        {convertText("Professor", 85)}
                    </div> */}
                </Grid>
                <Grid
                    item
                    md={12}
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    className="mt-3"
                >
                    <p className="description">
                        {convertText(instrutor.description, 250)}
                    </p>
                </Grid>
            </Grid>
        </Container>
    );
};

export default CardInstrutor;
