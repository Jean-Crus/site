import styled from "styled-components";
import { Grid } from "@material-ui/core";

export const Container = styled(Grid)`
    font-family: Montserrat;
    padding: 40px !important;
    cursor: pointer;
    .picture {
        width: 73px !important;
        height: 73px !important;
        margin: 0 !important;
    }
    .description {
        color: #6c6c6c;
        font-weight: 300;
        font-size: 1rem;
        text-align: initial;
    }
    .nome-instrutor {
        .instrutor-name {
            font-size: 1.1rem;
            color: #1f1f1f;
            font-weight: 600;
        }
        .profession {
            color: #6c6c6c;
            font-size: 0.8rem;
            font-weight: 300;
        }
    }
    .content {
        .MuiGrid-root {
            ul {
                li {
                    a {
                        color: ${props => props.theme.primary} !important;
                    }
                }
            }
        }
    }
    .MuiIcon-root {
        overflow: initial;
    }
    @media only screen and (max-width: 780px) {
        flex-direction: column;
        align-items: center;
        .instrutor-name,
        .profession,
        .description {
            text-align: center;
        }
    }
`;
