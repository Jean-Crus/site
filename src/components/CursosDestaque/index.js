import React from "react";
import { CardCurso, CardMobile } from "@components";

const CursosDestaque = ({
    image,
    link,
    title,
    category,
    closed,
    price,
    salePrice,
    hours,
    modules,
    description,
    onClick,
    priceFormatted,
    salePriceFormatted,
    id,
    label,
    cursos,
    readOnly
}) => {
    return (
        <>
            <CardCurso
                label={label}
                id={id}
                image={image}
                title={title}
                category={!!category && category.name}
                colorCategory={!!category && category.color}
                difficulty="Avançado"
                closed={false}
                price={price}
                priceFormatted={priceFormatted}
                salePriceFormatted={salePriceFormatted}
                salePrice={salePrice}
                hours={hours}
                modules={modules}
                description={description}
                onClick={onClick}
                cursos={cursos}
                readOnly={readOnly}
            />
            <CardMobile
                label={label}
                id={id}
                image={image}
                title={title}
                category={category && category.name}
                colorCategory={category && category.color}
                difficulty="Avançado"
                closed={false}
                price={price}
                priceFormatted={priceFormatted}
                salePriceFormatted={salePriceFormatted}
                salePrice={salePrice}
                hours={hours}
                modules={modules}
                description={description}
                onClick={onClick}
                cursos={cursos}
            />
        </>
    );
};

export default CursosDestaque;
