import React from 'react';

class SelectMult extends React.Component {

    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="select_multinivel">
                <button className="tr-multiSelect">
                    <div className="select">Selecione</div>
                    <ul>
                        {
                            this.props.selectItems.map((item, index) => (
                                <li key={index} className={item.subItems && 'subcategory'}>
                                    {item.name}

                                    {item.subItems && (
                                        <ul>
                                            {
                                                item.subItems.map((subItems, index) => (
                                                    <li key={index}>{subItems.name}</li>
                                                ))
                                            }
                                        </ul>
                                    )}

                                </li>
                            ))
                        }
                    </ul>
                </button>
            </div>
        );
    }
};

export default SelectMult;
