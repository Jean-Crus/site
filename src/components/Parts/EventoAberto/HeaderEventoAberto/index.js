import React from 'react';
import { ButtonTriade } from '@components';

const HeaderEventoAberto = props => {
    return (
        <div className="content container">
            <div className="row">
                <div className="col-lg-5 tr-flex-center">
                    <div className="information-course">
                        <div className="title-1 size-50">
                            Curso Lorem ipsum Avançado
                        </div>
                        <div className="information-description">
                            <span className="badge badge-purple">
                                12 de Dezembro
                            </span>
                            <div className="hours description-3">
                                {' '}
                                18 as 20h
                            </div>
                            <div className="capacity description-3">
                                6 Môdulos
                            </div>
                            <div className="location description-3">
                                Av.manoel ribas 183
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-4 tr-flex-center">
                    <div className="information-event">
                        <div className="content">
                            <small>Investimento</small>
                            <div className="price">R$ 250,00</div>
                            <small>Em até 8x sem juros.</small>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 tr-flex-center justify-content-end p-0">
                    <div className="button-buy">
                        <ButtonTriade
                            to="/checkout"
                            button="secondary"
                            text="Inscreva-se"
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HeaderEventoAberto;
