import React from 'react';

const ConteudoEvento = (props) => {
	
	return(
		<div className="content">
			<div className="title-1 size-50 black mb-4">
				Sobre o curso
			</div>
			<ul>
				<div className="title">Organização de arquivos</div>
				<li>Resumo do tópico 1</li>
				<li>Resumo do tópico 2</li>
				<li>Resumo do tópico 3</li>
				<li>Resumo do tópico 4</li>
			</ul>
			<ul>
				<div className="title">Organização de arquivos</div>
				<li>Resumo do tópico 1</li>
				<li>Resumo do tópico 2</li>
				<li>Resumo do tópico 3</li>
				<li>Resumo do tópico 4</li>
			</ul>
			<ul>
				<div className="title">Organização de arquivos</div>
				<li>Resumo do tópico 1</li>
				<li>Resumo do tópico 2</li>
				<li>Resumo do tópico 3</li>
				<li>Resumo do tópico 4</li>
			</ul>
			<ul>
				<div className="title">Organização de arquivos</div>
				<li className='before-none'>
					Overview
					Lorem ipsum dolor has been the industry's standard dummy text ever since the 1500s, when an unknown printer
					took a galley of type. Has been the industry's standard dummy text ever since the 1500s, when an unknown
					printer took a galley of type.
				</li>
			</ul>
		</div>
	);	
}

export default ConteudoEvento;