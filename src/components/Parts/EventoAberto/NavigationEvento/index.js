import React from 'react';
import { ButtonTriade } from '@components';

const NavigationEvento = props => {
    return (
        <nav className="navigation">
            <div className="buttons">
                <div className="btn-group-vertical" role="group">
                    <button type="button" className="btn tr-tercinary active">
                        Sobre o evento
                    </button>
                    <button type="button" className="btn tr-tercinary">
                        Conteúdo
                    </button>
                    <button type="button" className="btn tr-tercinary">
                        Palestrantes
                    </button>
                    <button type="button" className="btn tr-tercinary">
                        Localização
                    </button>
                    <button type="button" className="btn tr-tercinary counter">
                        <div className="time-count-down">
                            <ul>
                                <li>
                                    <p>mes</p>
                                    <span id="seila">01</span>
                                </li>
                                <li>
                                    <p>dias</p>
                                    <span id="days">0</span>
                                </li>
                                <li>
                                    <p>horas</p>
                                    <span id="hours">0</span>
                                </li>
                                <li>
                                    <p>min</p>
                                    <span id="minutes">0</span>
                                </li>
                                <li>
                                    <p>seg</p>
                                    <span id="seconds">0</span>
                                </li>
                            </ul>
                        </div>
                    </button>
                </div>
            </div>
            <div className="information-course">
                <div className="content">
                    <small>Investimento</small>
                    <div className="price">R$ 250,00</div>
                    <small>Em até 8x sem juros.</small>
                    <div className="button-buy">
                        <ButtonTriade
                            button="secondary"
                            size="small"
                            text="Inscreva-se"
                        />
                    </div>
                </div>
            </div>
        </nav>
    );
};

export default NavigationEvento;
