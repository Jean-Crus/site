import React from 'react';
import { bitmap } from '@images';
import { Link } from 'react-router-dom';

const SobreEvento = (props) => {
	
	return(
		<div className="sobre">
			<div className="content mb-3">
				<div className="title-1 size-50 black mb-3">
					Sobre o curso
				</div>
				<div className="description-1">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus velit eros, vehicula a viverra a,
					condimentum id ligula. Maecenas hendrerit cursus massa
				</div>
				<div className="card-information">
					<div className="date">
						<div className="date-number">
							<p>07</p>
							<b>jan</b>
						</div>
					</div>
					<div className="information">
						<div className="content">
							<Link to="#" className="title-2 size-50 black">
								Evento Lorem Ipsum
							</Link>
							<div className="local">
								<div className="hours description-3"> 9h -10h </div>
								<div className="type-event description-3 local">R: Flávio Dallegrave 2139, Boa vista</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="video mb-4">
				<div className="video">
					<img src={bitmap} />
				</div>
			</div>
			<div className="description-1">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus velit eros, vehicula a viverra a,
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus velit eros, vehicula a viverra a,
				condimentum id ligula. Maecenas hendrerit cursus massa
			</div>
		</div>
	);	
}

export default SobreEvento;