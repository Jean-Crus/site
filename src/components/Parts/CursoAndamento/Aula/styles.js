import styled from "styled-components";
import { lighten } from "polished";

export const Container = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    border: 1px solid #ccc;
    border-radius: 6px;
    font-family: Montserrat;
    .title-1 {
        text-align: center !important;
    }
    .html {
        display: flex;
        justify-content: center;
        margin: 20px 0;
    }
    iframe {
        width: 100%;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
        height: 454px;
    }
    .arquivo {
        width: 100%;
        padding: 0 20px;
        main {
            width: 100%;
            display: flex;
            flex-direction: column;
            padding: 10px 20px;
            background-color: #ececec;
            margin-bottom: 20px;

            justify-content: space-between;
            a {
                &:hover {
                    color: ${props => lighten(0.2, "#5433f1")};
                }
                display: flex;
                align-items: center;
                color: #5433f1;
                text-decoration: underline;
                font-weight: bold;
                opacity: 0.8;
                margin-bottom: 10px;
                &:last-child {
                    margin-bottom: 0;
                }
            }
        }
    }
`;
