import React from "react";
import ReactPlayer from "react-player";
import { Icon } from "@material-ui/core";
import ReactHtmlParser from "react-html-parser";
import { Container } from "./styles";

const Aula = ({ introduction, cursoandamento }) => {
    return (
        <Container className="sobre">
            {!!introduction.url_video && (
                <ReactPlayer
                    width="100%"
                    url={introduction.url_video}
                    controls
                    height="454px"
                    youtubeConfig={{ playerVars: { showinfo: 0, rel: 0 } }}
                />
            )}
            <div className="html">
                {ReactHtmlParser(introduction.description)}
            </div>

            {!!introduction.file_link && (
                <div className="arquivo">
                    <main>
                        <a
                            href={introduction.file_link}
                            download
                            target="_blank"
                            rel="noreferrer noopener"
                        >
                            <Icon className="fas fa-file" />
                            Acesse aqui o conteúdo da aula
                        </a>
                    </main>
                </div>
            )}
        </Container>
    );
};

export default Aula;
