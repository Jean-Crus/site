import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
    Checked,
    Container,
    UnChecked,
    CurrentClass,
    NavStyled
} from "./styles";

const NagivationAndamento = ({ curso, id }) => {
    const modules = useSelector(state => state.user.course.modules);
    const check = (finished, active) => {
        const number = parseInt(id, 10);
        if (finished) {
            return <Checked />;
        }
        if (number === active) {
            return <CurrentClass />;
        }
        return <UnChecked color="disabled" />;
    };
    return (
        <>
            <NavStyled className="navigation">
                <div className="buttons">
                    <div className="btn-group-vertical" role="group">
                        {modules.map((modulo, index) => (
                            <>
                                <button
                                    type="button"
                                    className="btn tr-tercinary"
                                    data-toggle="collapse"
                                    href={`#sobre-${modulo.id}`}
                                    aria-controls="sobreocurso"
                                    key={modulo.id}
                                >
                                    Módulo {index + 1}:{" "}
                                    <small>{modulo.name}</small>
                                </button>
                                <div
                                    className="collapse show"
                                    id={`sobre-${modulo.id}`}
                                >
                                    <ul className="module-lessons">
                                        {modulo.lessons.map(licoes => (
                                            <li key={licoes.id}>
                                                <Link
                                                    to={`/curso-andamento/${curso}/${licoes.id}`}
                                                >
                                                    <Container
                                                        current={
                                                            id === licoes.id
                                                        }
                                                    >
                                                        {check(
                                                            licoes.finished,
                                                            licoes.id
                                                        )}

                                                        <span>
                                                            {licoes.name}
                                                        </span>
                                                    </Container>
                                                </Link>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </>
                        ))}
                    </div>
                </div>
            </NavStyled>

            {/* <nav className="navigation mt-3 bonus">
                <div className="buttons">
                    <div className="btn-group-vertical" role="group">
                        <button
                            type="button"
                            className="btn tr-tercinary collapsed red"
                            data-toggle="collapse"
                            href="#bonuscurso"
                            role="button"
                            aria-controls="bonuscurso"
                        >
                            Sobre o módulo 1<small>Nome do módulo</small>
                        </button>
                        <div className="collapse" id="bonuscurso">
                            <ul className="module-lessons">
                                <li>
                                    <Link to="#">
                                        <input
                                            type="checkbox"
                                            checked="checked"
                                            name="manager"
                                            id="asss"
                                        />
                                        <label
                                            htmlFor="asss"
                                            className="checkbox red"
                                        >
                                            Aula 1 Lorem ipsum
                                        </label>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="#">
                                        <input
                                            type="checkbox"
                                            name="manager"
                                            id="asasd"
                                        />
                                        <label
                                            htmlFor="asasd"
                                            className="checkbox red"
                                        >
                                            Avaliação módulo 1
                                        </label>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav> */}
        </>
    );
};

export default NagivationAndamento;
