import styled from "styled-components";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";

export const CurrentClass = styled(RadioButtonCheckedIcon)`
    color: ${props => props.theme.primary};
`;

export const Checked = styled(CheckCircleIcon)`
    color: #34b16f !important;
`;

export const UnChecked = styled(RadioButtonUncheckedIcon)``;

export const NavStyled = styled.nav`
    .tr-tercinary {
        background: linear-gradient(0deg, #ef9300 0%, #ef7300 100%) !important;
    }
    ul.module-lessons {
        padding-left: 0px !important;
    }
    &.navigation .buttons .btn-group-vertical .tr-tercinary:before {
        border: solid #1f1f1f !important;
        border-width: 0 3px 3px 0 !important;
    }
`;

export const Container = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    span {
        color: ${props => props.theme.background};
        font-family: Montserrat, sans-serif;
        font-weight: ${props => (props.current ? "bold" : "")};
        font-size: 14px;
        padding-left: 5px;
    }
`;
