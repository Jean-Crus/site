/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from "react";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import { Grid } from "@material-ui/core";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import {
    Container,
    Text,
    TextTitle,
    TextLesson,
    Box,
    TextAula
} from "./styles";

const ModulosCursoDetail = ({ modules, curso }) => {
    const [active, setActive] = useState({
        clicked: null
    });

    const handleDropDown = id => {
        return active.clicked === id
            ? setActive({ clicked: null })
            : setActive({ clicked: id });
    };
    return (
        <Box className="tr-modulos-cursando">
            <div className="content">
                <div className="modules-dropdown">
                    <ul className="list-modules">
                        {modules &&
                            modules.map((element, index) => {
                                return (
                                    <li className="mb-4" key={element.id}>
                                        <div
                                            className={
                                                active.clicked === element.id
                                                    ? `title`
                                                    : `title open`
                                            }
                                            onClick={() =>
                                                handleDropDown(element.id)
                                            }
                                        >
                                            <Grid container>
                                                <Grid item md={12} lg={12}>
                                                    <TextTitle>
                                                        <strong>
                                                            Módulo {index + 1}
                                                            <TextTitle className="name-module">
                                                                {" "}
                                                                - {element.name}
                                                            </TextTitle>
                                                        </strong>
                                                    </TextTitle>
                                                    <div className="badge badge-purple">
                                                        {element.lessons.length}{" "}
                                                        {element.lessons
                                                            .length <= 1
                                                            ? "aula"
                                                            : "aulas"}
                                                    </div>
                                                </Grid>
                                            </Grid>

                                            <Container item md={2} lg={2}>
                                                <TextLesson className="total-lessons">
                                                    (
                                                    {`${element.lessons_finished}/${element.lessons.length}`}
                                                    )
                                                </TextLesson>
                                            </Container>
                                        </div>
                                        {active.clicked === element.id ? (
                                            <ul className="module-lessons">
                                                {element.lessons.length ? (
                                                    element.lessons.map(
                                                        licoes => (
                                                            <li
                                                                key={licoes.id}
                                                                className="lessons-list"
                                                            >
                                                                <Text
                                                                    to={`/curso-andamento/${curso}/${licoes.id}`}
                                                                    finished={
                                                                        licoes.finished
                                                                    }
                                                                >
                                                                    <Grid
                                                                        container
                                                                        alignItems="center"
                                                                    >
                                                                        <Grid
                                                                            md={
                                                                                8
                                                                            }
                                                                            lg={
                                                                                8
                                                                            }
                                                                            item
                                                                        >
                                                                            {
                                                                                licoes.name
                                                                            }
                                                                        </Grid>
                                                                        <Grid
                                                                            lg={
                                                                                3
                                                                            }
                                                                            md={
                                                                                3
                                                                            }
                                                                            item
                                                                            container
                                                                            justify="flex-end"
                                                                        >
                                                                            <Grid className="lesson-watched">
                                                                                <TextAula
                                                                                    finished={
                                                                                        licoes.finished
                                                                                    }
                                                                                >
                                                                                    Aula
                                                                                    assistida
                                                                                </TextAula>
                                                                                {licoes.finished ===
                                                                                true ? (
                                                                                    <CheckBoxIcon
                                                                                        style={{
                                                                                            color:
                                                                                                "#34b16f"
                                                                                        }}
                                                                                    />
                                                                                ) : (
                                                                                    <CheckBoxOutlineBlankIcon color="disabled" />
                                                                                )}
                                                                            </Grid>
                                                                        </Grid>
                                                                    </Grid>
                                                                </Text>
                                                            </li>
                                                        )
                                                    )
                                                ) : (
                                                    <li>
                                                        <div className="description-1">
                                                            Sem descrição
                                                        </div>
                                                    </li>
                                                )}
                                            </ul>
                                        ) : null}
                                    </li>
                                );
                            })}
                    </ul>
                </div>
            </div>
        </Box>
    );
};

export default ModulosCursoDetail;
