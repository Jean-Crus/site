import styled from "styled-components";
import { Grid } from "@material-ui/core";
import { Link } from "react-router-dom";

export const Box = styled.div`
    .badge-purple {
        background-color: #ef7300 !important;
    }
    .module-lessons {
        pointer-events: all !important;
        li {
            pointer-events: all !important;
        }
    }
    .lesson-watched {
        display: flex !important;
        justify-content: center;
    }
`;

export const Container = styled(Grid)`
    display: flex;
    justify-content: space-evenly;
`;

export const Text = styled(Link)`
    font-family: Montserrat;
    font-size: 12px;
    color: ${props => props.theme.background};
    font-weight: ${props => (props.finished ? "" : "bold")};
    cursor: pointer;
    &:hover {
        color: ${props => props.theme.background};
    }
`;

export const TextAula = styled.div`
    display: flex;
    align-items: center;
    font-family: Montserrat;
    color: ${props => (props.finished ? "rgba(0, 0, 0, 0.26)" : "#1f1f1f")};
    font-size: 12px;
    font-weight: ${props => (props.finished ? "bold" : "")};
    margin-right: 5px;
`;

export const TextTitle = styled.span`
    color: ${props => props.theme.letter} !important;
    font-family: Montserrat;
`;

export const TextLesson = styled.span`
    color: ${props => props.theme.letter} !important;
    font-family: Montserrat;
    font-size: 14px !important;
`;
