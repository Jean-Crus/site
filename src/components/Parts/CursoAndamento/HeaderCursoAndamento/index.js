import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { LinearProgress, Grid } from "@material-ui/core";
import history from "@routes/history";
import Moment from "react-moment";
import { ButtonTriade, QuillEditor } from "@components";
import { Button, Container } from "./styles";

const HeaderCursoAndamento = ({ back, forward, title }) => {
    const course = useSelector(state => state.user.course);
    return (
        <Container className="content container">
            <div className="row mx-0">
                <div className="col-lg-8 tr-flex-center">
                    <Link to={back}>
                        <Button className="btn tr-circle-primary" />
                    </Link>
                    <div className="information-course">
                        <div className="title-1 size-50">{course.name}</div>
                        <div className="information-description">
                            {/* {!!course.registration_date &&
                                <div className="initial description-3">
                                    Matrícula em:{' '}
                                    <Moment format="DD/MM/YYYY">
                                        {course.registration_date}
                                    </Moment>
                                </div>
                            }
                            {course.access_date_type !== "eternal" && (
                                <div className="final description-3">
                                    Expira em:{' '}
                                    <Moment format="DD/MM/YYYY">
                                        {course.access_date}
                                    </Moment>
                                </div>
                            )} */}
                        </div>
                        <div className="mt-2 ml-1">
                            <div 
                                className="size-15"
                                style={{
                                    color: "white",
                                    fontWeight: "bold"
                                }}>Progresso:</div>
                            <LinearProgress
                                color="primary"
                                variant="determinate"
                                value={course.user_progress}
                            />
                            <Grid container justify="center">
                                <span
                                    className="description-3 mt-1 font-weight-bolder"
                                    style={{ color: "#fff" }}
                                >
                                    {course.user_progress}%
                                </span>
                            </Grid>
                        </div>
                    </div>
                </div>
                {course.launch_open ? (
                    <div className="col-lg-4 tr-flex-center px-0 mt-2">
                        <QuillEditor
                            theme="bubble"
                            value={course.launch_message}
                            className="sem-aula"
                            campo="title"
                            // id={slide.id}
                            readOnly
                            onChange={() => null}
                        />
                    </div>
                ) : (
                    <div className="col-lg-4 tr-flex-center px-0 mt-2">
                        {forward ? (
                            <div className="button-continue">
                                <ButtonTriade
                                    className="mr-4"
                                    button="Modelo 02"
                                    letterColor="Modelo 02"
                                    cor="Modelo 02"
                                    text="Continuar"
                                    onClick={() => history.push(forward)}
                                />
                            </div>
                        ) : // <div className="button-tutor">
                        //     <ButtonTriade button="primary" text="Forum" />
                        //     <ButtonTriade
                        //         button="secondary"
                        //         text="Falar com o tutor"
                        //     />
                        // </div>
                        null}
                    </div>
                )}
            </div>
        </Container>
    );
};

export default HeaderCursoAndamento;
