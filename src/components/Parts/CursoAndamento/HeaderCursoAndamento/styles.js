import styled from "styled-components";

export const Container = styled.div`
    .button-tutor {
        display: flex;
        width: 100%;
        justify-content: space-between;
        button {
            width: 200px;
        }
    }

    .sem-aula {
        p {
            font-family: Montserrat;
            font-size: 20px;
            color: #f13333;
            font-weight: 500;
        }
    }
`;

export const Button = styled.div`
    cursor: pointer;
    background: ${props =>
        `linear-gradient(0deg, #ef9300 0%, #ef7300 100%)`} !important;
`;
