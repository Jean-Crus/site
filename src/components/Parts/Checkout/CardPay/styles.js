import styled from "styled-components";

export const Container = styled.div`
    @media only screen and (max-width: 1280px) {
        .card-sample {
            display: flex;
            width: 100%;
            justify-content: center;
        }
    }
    @media only screen and (max-width: 700px) {
        .form-pay {
            padding: 10px !important;
        }
        .card-sample {
            display: none;
        }
    }
`;
