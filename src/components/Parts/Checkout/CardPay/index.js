import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Formik } from "formik";
import * as Yup from "yup";
import { Grid, MenuItem, TextField } from "@material-ui/core";
import Script from "react-load-script";
import * as CheckoutActions from "@store/modules/checkout/actions";
import { Spinner } from "react-bootstrap";
import InputMask from "react-input-mask";
import { tokenJavascript } from "@api";
import { ButtonTriade, TextDefault, SelectDefault } from "@components";
import { Container } from "./styles";

// const initialValues = {
//     numberCard: "",
//     nomeTitular: "",
//     tel: "",
//     validade: "",
//     ccv: "",
//     cpf: "",
//     birthDate: "",
//     opcaoPagamento: "1"
// };

const initialValues = {
    numberCard: "",
    nomeTitular: "",
    tel: "",
    validade: "",
    ccv: "",
    cpf: "",
    birthDate: "",
    opcaoPagamento: 1
};

const schema = Yup.object().shape({
    nomeTitular: Yup.string().required("Nome do titular obrigatório"),
    numberCard: Yup.string().required("O número do cartão é obrigatório"),
    cpf: Yup.string().required("O cpf é obrigatório"),
    birthDate: Yup.string().required(
        'Data de nascimento obrigatória Ex: "12/12/2012'
    ),
    tel: Yup.string()
        .matches(
            /^\([0-9]{2}\)[0-9]?[0-9]{4}-[0-9]{4}$/,
            "Telefone inválido. Ex: (xx)xxxxx-xxxx"
        )
        .required("Número de telefone celular obrigatório"),
    validade: Yup.string().required("Validade obrigatória Ex: 12/2012"),
    ccv: Yup.string().required("CCV é obrigatório"),
    opcaoPagamento: Yup.number().required("Selecione o valor do pagamento")
});

const CardPay = () => {
    const [cardBrand, setCardBrand] = useState(null);
    const loading = useSelector(state => state.checkout.loading);

    const totalFormatted = useSelector(state => state.cart.totalFormatted);
    const totalInstallments = useSelector(state => state.cart.installment);

    const dispatch = useDispatch();
    // useEffect(() => {
    //     dispatch(CheckoutActions.getSessionIuguCardRequest());
    // }, [dispatch]);
    const handleLoad = () => {
        dispatch(CheckoutActions.getSessionCardRequest());
    };
    const handleBrand = numberCard => {
        window.PagSeguroDirectPayment.getBrand({
            cardBin: numberCard,
            success(response) {
                setCardBrand(response.brand.name);
            },
            error(err) {},
            complete(response) {}
        });
    };
    return (
        <>
            <Formik
                initialValues={initialValues}
                enableReinitialize
                validationSchema={schema}
                className="tr-form"
                validate={props => handleBrand(props.numberCard)}
                onSubmit={({
                    nomeTitular,
                    numberCard,
                    cpf,
                    birthDate,
                    validade,
                    ccv,
                    opcaoPagamento,
                    tel
                }) => {
                    // dispatch(
                    //     CheckoutActions.getSessionIuguCardRequest(
                    //         nomeTitular,
                    //         numberCard,
                    //         cpf,
                    //         birthDate,
                    //         validade,
                    //         ccv,
                    //         opcaoPagamento,
                    //         cardBrand,
                    //         tel
                    //     )
                    // );
                    dispatch(
                        CheckoutActions.getCheckoutRequest(
                            nomeTitular,
                            numberCard,
                            cpf,
                            birthDate,
                            validade,
                            ccv,
                            opcaoPagamento,
                            cardBrand,
                            tel
                        )
                    );
                }}
            >
                {({
                    handleBlur,
                    handleChange,
                    handleSubmit,
                    values,
                    errors,
                    touched
                }) => {
                    return (
                        <>
                            <Script url={tokenJavascript} onLoad={handleLoad} />
                            {loading ? (
                                <Grid
                                    container
                                    justify="center"
                                    style={{ margin: "60px 0" }}
                                >
                                    <Spinner animation="border" role="status" />
                                </Grid>
                            ) : (
                                <form onSubmit={handleSubmit}>
                                    <div className="card" id="one-card">
                                        <Container className="content align-items-end">
                                            <div className="form-pay">
                                                <div className="title-1 size-25 black text-left mb-3">
                                                    Dados do titular do Cartão
                                                </div>
                                                <Grid container>
                                                    <TextDefault
                                                        name="nomeTitular"
                                                        error={
                                                            touched.nomeTitular &&
                                                            errors.nomeTitular
                                                        }
                                                        helperText={
                                                            touched.nomeTitular &&
                                                            errors.nomeTitular
                                                        }
                                                        onChange={handleChange}
                                                        value={
                                                            values.nomeTitular
                                                        }
                                                        label="Nome do titular"
                                                        fullWidth
                                                        margin="normal"
                                                        onBlur={handleBlur}
                                                    />
                                                    <InputMask
                                                        mask="(99)99999-9999"
                                                        maskChar=" "
                                                        fullWidth
                                                        helperText={
                                                            touched.tel &&
                                                            errors.tel
                                                        }
                                                        error={
                                                            touched.tel &&
                                                            errors.tel
                                                        }
                                                        label="Telefone celular*"
                                                        margin="normal"
                                                        name="tel"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.tel}
                                                    >
                                                        {props => (
                                                            <TextDefault
                                                                {...props}
                                                            />
                                                        )}
                                                    </InputMask>
                                                    <Grid container>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={12}
                                                            md={12}
                                                            lg={12}
                                                        >
                                                            <InputMask
                                                                mask="999.999.999-99"
                                                                maskChar=" "
                                                                fullWidth
                                                                label="CPF do Titular"
                                                                name="cpf"
                                                                margin="normal"
                                                                error={
                                                                    touched.cpf &&
                                                                    errors.cpf
                                                                }
                                                                helperText={
                                                                    touched.cpf &&
                                                                    errors.cpf
                                                                }
                                                                onChange={
                                                                    handleChange
                                                                }
                                                                onBlur={
                                                                    handleBlur
                                                                }
                                                                value={
                                                                    values.cpf
                                                                }
                                                            >
                                                                {props => (
                                                                    <TextDefault
                                                                        {...props}
                                                                    />
                                                                )}
                                                            </InputMask>
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={12}
                                                            md={12}
                                                            lg={12}
                                                        >
                                                            <InputMask
                                                                mask="99/99/9999"
                                                                maskChar=" "
                                                                fullWidth
                                                                type="text"
                                                                label="Data de nascimento"
                                                                name="birthDate"
                                                                margin="normal"
                                                                error={
                                                                    touched.birthDate &&
                                                                    errors.birthDate
                                                                }
                                                                helperText={
                                                                    touched.birthDate &&
                                                                    errors.birthDate
                                                                }
                                                                onChange={
                                                                    handleChange
                                                                }
                                                                onBlur={
                                                                    handleBlur
                                                                }
                                                                value={
                                                                    values.birthDate
                                                                }
                                                            >
                                                                {props => (
                                                                    <TextDefault
                                                                        {...props}
                                                                    />
                                                                )}
                                                            </InputMask>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <div className="title-1 size-25 black text-left mb-3 mt-4">
                                                    Dados do Cartão
                                                </div>
                                                <Grid container>
                                                    <InputMask
                                                        mask="9999999999999999"
                                                        maskChar=" "
                                                        name="numberCard"
                                                        error={
                                                            touched.numberCard &&
                                                            errors.numberCard
                                                        }
                                                        helperText={
                                                            touched.numberCard &&
                                                            errors.numberCard
                                                        }
                                                        onChange={handleChange}
                                                        value={
                                                            values.numberCard
                                                        }
                                                        label="Número do cartão"
                                                        fullWidth
                                                        margin="normal"
                                                        onBlur={handleBlur}
                                                    >
                                                        {props => (
                                                            <TextDefault
                                                                {...props}
                                                            />
                                                        )}
                                                    </InputMask>

                                                    <Grid container>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={12}
                                                            md={12}
                                                            lg={12}
                                                        >
                                                            <InputMask
                                                                mask="99/9999"
                                                                fullWidth
                                                                type="text"
                                                                label="Validade"
                                                                name="validade"
                                                                error={
                                                                    touched.validade &&
                                                                    errors.validade
                                                                }
                                                                helperText={
                                                                    touched.validade &&
                                                                    errors.validade
                                                                }
                                                                margin="normal"
                                                                onChange={
                                                                    handleChange
                                                                }
                                                                onBlur={
                                                                    handleBlur
                                                                }
                                                                value={
                                                                    values.validade
                                                                }
                                                            >
                                                                {props => (
                                                                    <TextDefault
                                                                        {...props}
                                                                    />
                                                                )}
                                                            </InputMask>
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            xs={12}
                                                            sm={12}
                                                            md={12}
                                                            lg={12}
                                                        >
                                                            <InputMask
                                                                mask="9999"
                                                                maskChar=" "
                                                                fullWidth
                                                                type="text"
                                                                label="CCV"
                                                                name="ccv"
                                                                error={
                                                                    touched.ccv &&
                                                                    errors.ccv
                                                                }
                                                                helperText={
                                                                    touched.ccv &&
                                                                    errors.ccv
                                                                }
                                                                margin="normal"
                                                                onChange={
                                                                    handleChange
                                                                }
                                                                onBlur={
                                                                    handleBlur
                                                                }
                                                                value={
                                                                    values.ccv
                                                                }
                                                            >
                                                                {props => (
                                                                    <TextDefault
                                                                        {...props}
                                                                    />
                                                                )}
                                                            </InputMask>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid>
                                                    <div className="title-1 size-25 black text-left mb-3">
                                                        Opções de pagamento
                                                    </div>
                                                </Grid>
                                                <div>
                                                    <SelectDefault
                                                        select
                                                        error={
                                                            touched.opcaoPagamento &&
                                                            errors.opcaoPagamento
                                                        }
                                                        helperText={
                                                            touched.opcaoPagamento &&
                                                            errors.opcaoPagamento
                                                        }
                                                        fullWidth
                                                        name="opcaoPagamento"
                                                        label="Opção de pagamento"
                                                        margin="normal"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={
                                                            values.opcaoPagamento
                                                        }
                                                        campos={
                                                            totalInstallments
                                                        }
                                                        payment
                                                    />
                                                </div>
                                            </div>
                                            <div className="card-sample">
                                                {/* <Link to="#" className="title-2 alter-card-option">
                            Pagar com dois cartões
                        </Link> */}

                                                <div className="credit-card">
                                                    <div
                                                        className="logocard"
                                                        style={{
                                                            backgroundImage: `url(https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/${cardBrand}.png)`
                                                        }}
                                                    />
                                                    {/* <img
                                                            src="https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/mastercard.png"
                                                            className="logocard"
                                                            alt="logo-card"
                                                        /> */}
                                                    <div id="card-numer">
                                                        <span>
                                                            Numero do Cartão
                                                        </span>
                                                        <p>
                                                            {values.numberCard}
                                                        </p>
                                                    </div>
                                                    <div id="name">
                                                        <span>
                                                            Nome do Titular
                                                        </span>
                                                        <p>
                                                            {values.nomeTitular}
                                                        </p>
                                                    </div>
                                                    <div className="group">
                                                        <div id="validation">
                                                            <span>
                                                                Validade
                                                            </span>
                                                            <p>
                                                                {
                                                                    values.validade
                                                                }
                                                            </p>
                                                        </div>
                                                        <div id="ccv">
                                                            <span>CCV</span>
                                                            <p>{values.ccv}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Container>
                                    </div>
                                    <Grid container justify="flex-end">
                                        <ButtonTriade
                                            type="submit"
                                            button="Modelo 02"
                                            letterColor="Modelo 02"
                                            className="mt-2"
                                            cor="Modelo 02"
                                            text={
                                                loading
                                                    ? "Carregando..."
                                                    : "Efetuar pagamento"
                                            }
                                        />
                                    </Grid>
                                </form>
                            )}
                        </>
                    );
                }}
            </Formik>

            {/* <div className="card d-none" id="two-card">
                <div className="content">
                    <div className="form-pay two-card">
                        <div className="options">
                            <div className="title-1 size-25 black text-left mb-3">
                                Dados do Cartão
                            </div>
                            <Link to="#" className="title-2 alter-card-option">
                                Pagar com um cartão
                            </Link>
                        </div>
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="card">
                                    <div className="description-3 text-left mb-3">
                                        Dados do Cartão
                                    </div>
                                    <form action="" className="tr-form">
                                        <div className="form-group label-animate">
                                            <TextDefault
                                                type="number"
                                                className="form-control"
                                                placeholder="Valor"
                                            />
                                            <label htmlFor="">valor</label>
                                        </div>
                                        <div className="form-group label-animate">
                                            <TextDefault
                                                type="number"
                                                className="form-control"
                                                placeholder="Numero do Cartão"
                                            />
                                            <label htmlFor="">
                                                Numero do Cartão
                                            </label>
                                        </div>
                                        <div className="form-group label-animate">
                                            <TextDefault
                                                type="text"
                                                className="form-control"
                                                placeholder="Nome do titular"
                                            />
                                            <label htmlFor="">
                                                Nome do titular
                                            </label>
                                        </div>
                                        <div className="form-group TextDefault-group mb-4">
                                            <div className="label-animate">
                                                <TextDefault
                                                    type="number"
                                                    className="form-control mr-3"
                                                    placeholder="Validade"
                                                />
                                                <label> Validade</label>
                                            </div>
                                            <div className="label-animate">
                                                <TextDefault
                                                    type="number"
                                                    className="form-control"
                                                    placeholder="CCV"
                                                />
                                                <label>CCV</label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="card">
                                    <div className="description-3 text-left mb-3">
                                        Dados do Cartão
                                    </div>
                                    <form action="" className="tr-form">
                                        <div className="form-group label-animate">
                                            <TextDefault
                                                type="number"
                                                className="form-control"
                                                placeholder="Valor"
                                            />
                                            <label htmlFor="">Valor</label>
                                        </div>
                                        <div className="form-group label-animate">
                                            <TextDefault
                                                type="number"
                                                className="form-control"
                                                placeholder="Numero do Cartão"
                                            />
                                            <label htmlFor="">
                                                Número do Cartão
                                            </label>
                                        </div>
                                        <div className="form-group label-animate">
                                            <TextDefault
                                                type="text"
                                                className="form-control"
                                                placeholder="Nome do titular"
                                            />
                                            <label htmlFor="">
                                                Nome do titular
                                            </label>
                                        </div>
                                        <div className="form-group TextDefault-group mb-4">
                                            <div className="label-animate">
                                                <TextDefault
                                                    type="number"
                                                    className="form-control mr-3"
                                                    placeholder="Validade"
                                                />
                                                <label> Validade</label>
                                            </div>
                                            <div className="label-animate">
                                                <TextDefault
                                                    type="number"
                                                    className="form-control"
                                                    placeholder="CCV"
                                                />
                                                <label>CCV</label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> */}
        </>
    );
};

export default CardPay;
