import styled from "styled-components";
import { Grid } from "@material-ui/core";

export const Container = styled(Grid)`
    border-radius: 5px;
    background-color: #ffffff;
    border: 1px solid rgba(0, 0, 0, 0.125);
    font-family: Montserrat;
    .title {
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 1.4rem;
        font-weight: bold;
        color: #1f1f1f;
    }

    .boleto-msg {
        .title-text {
            font-weight: 300;
            color: #6c6c6c;
            font-size: 0.9rem;
        }
        .numbers {
            .circle-text {
                display: flex;
                align-items: center;
                color: #7d7474;
                font-size: 0.8rem;
                span {
                    font-weight: bold;
                }
                text-align: initial;
            }
            .circle {
                display: flex;
                justify-content: center;
                align-items: center;

                div {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    border-radius: 50%;
                    background-color: #f8f7fe;
                    font-size: 1.2rem;
                    color: #ef7300;
                    height: 48px;
                    width: 48px;
                }
            }
        }
    }
    .total-tab {
        border-radius: 5px;
        border: 1px solid rgba(0, 0, 0, 0.125);
        padding: 40px;
        .tot {
            display: flex;
            font-weight: bold;
            font-size: 1.2rem;
        }
        .mb-4 {
            display: flex;
            font-weight: bold;
            font-size: 2rem;
            color: #ef7300;
            line-height: 2rem;
        }
        button {
            width: 100%;
        }
    }

    @media only screen and (max-width: 940px) {
        .total-tab {
            margin-top: 40px;
            padding: 0;
            button {
                display: none;
            }
            .tot {
                margin-top: 24px;
                justify-content: center;
            }
            .mb-4 {
                justify-content: center;
            }
        }
        .title-text {
            margin-bottom: 20px;
        }
    }
    @media only screen and (min-width: 941px) {
        .button-mobile {
            display: none;
        }
    }
    @media only screen and (max-width: 622px) {
        .boleto-msg {
            .numbers {
                .circle {
                    div {
                        font-size: 1rem;
                    }
                }
                .circle-text {
                    font-size: 0.6rem;
                    /* text-align: end; */
                }
                .title-text {
                    font-size: 0.7rem;
                }
            }
        }
    }
`;
