import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Grid } from "@material-ui/core";
import { tokenJavascript } from "@api";
import Script from "react-load-script";
import { ButtonTriade, Loading } from "@components";
import * as CheckoutActions from "@store/modules/checkout/actions";
import { Container } from "./styles";

const BoletoPay = () => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.checkout.loading);
    const loadingBoleto = useSelector(state => state.checkout.loadingBoleto);

    const totalCart = useSelector(state => state.cart.totalFormatted);
    const handleLoad = () => {
        dispatch(CheckoutActions.getSessionCardRequest());
    };
    return (
        <>
            <Script url={tokenJavascript} onLoad={handleLoad} />
            {loading ? (
                <Loading />
            ) : (
                <Container container>
                    <Grid
                        className="title mt-5 mb-5"
                        item
                        sm={12}
                        lg={12}
                        xs={12}
                        md={12}
                        xl={12}
                    >
                        Boleto Bancário
                    </Grid>

                    <Grid container justify="center" className="mb-5">
                        <Grid
                            className="boleto-msg"
                            item
                            sm={10}
                            lg={6}
                            xs={10}
                            md={6}
                            xl={6}
                            container
                        >
                            <Grid
                                item
                                className="title-text"
                                sm={12}
                                lg={12}
                                xs={12}
                                md={12}
                                xl={12}
                            >
                                O boleto pode ser pago em qualquer agência
                                bancária antes do vencimento. Confira os dados
                                antes de pagá-lo.
                            </Grid>

                            <Grid
                                className="numbers mb-3"
                                item
                                sm={12}
                                lg={12}
                                xs={12}
                                md={12}
                                xl={12}
                                container
                            >
                                <Grid
                                    className="circle"
                                    item
                                    sm={2}
                                    lg={2}
                                    xs={3}
                                    md={2}
                                    xl={2}
                                >
                                    <div>1.</div>
                                </Grid>
                                <Grid
                                    className="circle-text"
                                    item
                                    sm={10}
                                    lg={10}
                                    xs={9}
                                    md={10}
                                    xl={10}
                                >
                                    <div>
                                        Imprima o boleto e{" "}
                                        <span>pague no banco</span> ou{" "}
                                        <span>pague pela internet </span>
                                        utilizando o código de barras.
                                    </div>
                                </Grid>
                            </Grid>
                            <Grid
                                className="numbers mb-3"
                                item
                                sm={12}
                                lg={12}
                                xs={12}
                                md={12}
                                xl={12}
                                container
                            >
                                <Grid
                                    className="circle"
                                    item
                                    sm={2}
                                    lg={2}
                                    xs={3}
                                    md={2}
                                    xl={2}
                                >
                                    <div>2.</div>
                                </Grid>
                                <Grid
                                    className="circle-text"
                                    item
                                    sm={10}
                                    lg={10}
                                    xs={9}
                                    md={10}
                                    xl={10}
                                >
                                    <div>
                                        O prazo de validade do boleto é de{" "}
                                        <span>2 dias úteis</span>.
                                    </div>
                                </Grid>
                            </Grid>
                            <Grid
                                className="numbers"
                                item
                                sm={12}
                                lg={12}
                                xs={12}
                                md={12}
                                xl={12}
                                container
                            >
                                <Grid
                                    className="circle"
                                    item
                                    sm={2}
                                    lg={2}
                                    xs={3}
                                    md={2}
                                    xl={2}
                                >
                                    <div>3.</div>
                                </Grid>
                                <Grid
                                    className="circle-text"
                                    item
                                    sm={10}
                                    lg={10}
                                    xs={9}
                                    md={10}
                                    xl={10}
                                >
                                    <div>
                                        Seu curso só será liberado após a{" "}
                                        <span>compensação do boleto</span>.
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid
                            item
                            sm={10}
                            lg={4}
                            xs={10}
                            md={4}
                            xl={4}
                            container
                            className="total-tab"
                        >
                            <Grid item sm={12} lg={12} xs={12} md={12} xl={12}>
                                <div className="tot">Total</div>
                                <div className="mb-4">{totalCart}</div>
                            </Grid>
                            <Grid item sm={12} lg={12} xs={12} md={12} xl={12}>
                                <ButtonTriade
                                    button="Modelo 02"
                                    letterColor="Modelo 02"
                                    cor="Modelo 02"
                                    type="button"
                                    key={loadingBoleto}
                                    text={
                                        loadingBoleto
                                            ? "Preparando boleto, aguarde..."
                                            : "Pagar com boleto"
                                    }
                                    onClick={() =>
                                        dispatch(
                                            CheckoutActions.boletoPaymentRequest()
                                        )
                                    }
                                />
                            </Grid>
                        </Grid>
                        <Grid
                            className="button-mobile mt-4"
                            item
                            sm={12}
                            lg={12}
                            xs={12}
                            md={12}
                            xl={12}
                        >
                            <ButtonTriade
                                button="Modelo 02"
                                letterColor="Modelo 02"
                                cor="Modelo 02"
                                type="button"
                                text="Pagar com boleto"
                                onClick={() =>
                                    dispatch(
                                        CheckoutActions.boletoPaymentRequest()
                                    )
                                }
                            />
                        </Grid>
                    </Grid>
                </Container>
            )}
        </>
    );
};

export default BoletoPay;
