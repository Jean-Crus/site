import styled from "styled-components";

export const Container = styled.div`
    background: linear-gradient(0deg, #012a50 0%, #0058a8 100%);
    .description-2 {
        color: #fff;
    }
    @media only screen and (max-width: 1201px) {
        display: none;
    }

    border-bottom: none !important;
`;
