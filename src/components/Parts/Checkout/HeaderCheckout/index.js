import React from "react";
import { Link } from "react-router-dom";
import logo from "@images/logo.png";
import { HeaderMobileConta } from "@components";
import { Container } from "./styles";

const HeaderCheckout = ({ history }) => {
    return (
        <>
            <Container className="background-header">
                <header className="tr-header-nav-checkout container">
                    <Link to="/">
                        <div className="logo-full">
                            <img src={logo} alt="Professor Carlos André" />
                        </div>
                    </Link>
                    <div className="description-2 end">
                        <p>
                            Site protegido sua <br />
                            compra 100% segura
                        </p>
                    </div>
                    <div className="description-2">
                        <p>
                            Atendimento ao cliente
                            <br />
                            (41) 99969-2129
                        </p>
                    </div>
                </header>
            </Container>
            <HeaderMobileConta history={history} />
        </>
    );
};

export default HeaderCheckout;
