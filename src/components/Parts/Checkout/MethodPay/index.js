import React from "react";
import { useSelector, useDispatch } from "react-redux";
import * as CheckoutActions from "@store/modules/checkout/actions";
import { Container } from "./styles";

const MethodPay = props => {
    const dispatch = useDispatch();
    const checkoutMethod = useSelector(state => state.checkout.payMethod);
    return (
        <Container className="content">
            <div className="row">
                <div className="col-lg-6 col-6 col-sm-6 col-md-6 col-xl-6">
                    <button
                        type="button"
                        onClick={() =>
                            dispatch(
                                CheckoutActions.changePaymentMethod(
                                    "credit-card"
                                )
                            )
                        }
                        className={`card-payment ${checkoutMethod ===
                            "credit-card" && "active"}`}
                    >
                        <span className="card_pay " />
                        <p>Cartão de crédito</p>
                    </button>
                </div>
                <div className="col-lg-6 col-6 col-sm-6 col-md-6 col-xl-6">
                    <button
                        type="button"
                        onClick={() =>
                            dispatch(
                                CheckoutActions.changePaymentMethod("boleto")
                            )
                        }
                        className={`card-payment ${checkoutMethod ===
                            "boleto" && "active"}`}
                    >
                        <span className="ticket_pay" />
                        <p>Boleto</p>
                    </button>
                </div>
                {/* <div className="col-lg-4">
                    <button
                        type="button"
                        className={`card-payment ${checkoutMethod ===
                            "credit-card" && "active"}`}
                    >
                        <span className="pal_pay" />
                        <p>PayPal</p>
                    </button>
                </div> */}
            </div>
        </Container>
    );
};

export default MethodPay;
