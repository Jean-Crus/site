import styled from "styled-components";

export const Container = styled.div`
    .card-payment {
        padding: 0 !important;
    }
    .card-payment.active {
        box-shadow: 0px 0px 0px 4px #ef7300 !important;
    }

    @media only screen and (max-width: 998px) {
        button {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .card-payment {
            height: 144px !important;
            span {
                margin: 0 !important;
            }
        }
    }
    @media only screen and (max-width: 640px) {
        .card-payment {
            p {
                font-size: 0.8rem !important;
            }
        }
    }
`;
