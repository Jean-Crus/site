import styled from "styled-components";

export const Container = styled.div`
    width: 100%;
    border-radius: 6px;
    font-family: Montserrat;

    .player-wrapper {
        position: relative;
        padding-top: 56.25%; /* Player ratio: 100 / (1280 / 720) */
    }

    .react-player {
        position: absolute;
        top: 0;
        left: 0;
    }
    .title-1 {
        text-align: center !important;
    }

    main {
        width: 100%;
        max-width: 800px;
        display: flex;
        flex-direction: column;
        padding: 10px 20px;
        background-color: #ececec;
        margin-bottom: 20px;
        justify-content: space-between;
        a {
            display: flex;
            align-items: center;
            color: #5433f1;
            text-decoration: underline;
            font-weight: bold;
            opacity: 0.8;
            margin-bottom: 10px;
            &:last-child {
                margin-bottom: 0;
            }
        }
    }
`;
