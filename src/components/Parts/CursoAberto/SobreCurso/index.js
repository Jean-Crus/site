import React from "react";
import ReactPlayer from "react-player";
import { Icon } from "@material-ui/core";
import ReactHtmlParser from "react-html-parser";
import { Container } from "./styles";
import RenderSunEditor from "../../../RenderSunEditor";

const SobreCurso = ({ introduction, cursoandamento, title, address }) => {
    return (
        <Container className="sobre">
            {!!introduction.introduction && (
                <div
                    className="title-1 size-50 black mb-3"
                    style={{ fontWeight: 600 }}
                >
                    {title}
                </div>
            )}
            {introduction.url_video && (
                <div className="player-wrapper">
                    <ReactPlayer
                        className="react-player"
                        width="100%"
                        height="100%"
                        url={introduction.url_video}
                    />
                </div>
            )}
            {introduction.introduction && (
                <RenderSunEditor renderComp={introduction.introduction} />
            )}

            {address && <RenderSunEditor renderComp={address} />}
            {/* <div className="description-1">
                    O Máquina da 1ª Fase é um MÉTODO TESTADO e APROVADO que
                    ajuda pessoas ATUAREM EM ALTA PERFORMANCE na 1ª FASE de
                    QUALQUER concurso de Cartório!
                </div>
            </div>
            <div className="video">
                <Grid container justify="center">
                    <ReactPlayer url="https://youtu.be/1f8jjIFWqhE" controls />
                </Grid> */}
        </Container>
    );
};

export default SobreCurso;
