import styled from "styled-components";
import ScheduleIcon from "@material-ui/icons/Schedule";
import AssignmentIcon from "@material-ui/icons/Assignment";
import StarBorderIcon from "@material-ui/icons/StarBorder";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 30px 40px;
    background: linear-gradient(0deg, #0058a8 0%, #0058a8 100%);

    .title {
        color: ${props => props.theme.letter};
        font-family: Montserrat, sans-serif;
        font-size: 20px;
        font-weight: bold;
    }
    .tag {
        margin: 20px 0;
        span {
            background-color: violet;
            color: ${props => props.theme.letter};
            font-family: Montserrat, sans-serif;
            font-size: 12px;
            text-align: center;
            padding: 4px;
            border-radius: 3px;
        }
    }

    .multi-tag {
        display: flex;
        justify-content: space-between;
        align-items: center;
        flex-wrap: wrap;
        width: 80%;
        div {
            span {
                margin-left: 5px;
                color: ${props => props.theme.letter};
                font-family: Montserrat;
                font-size: 11px;
            }
        }
    }

    .button-box {
        padding: 50px 0px 40px 0px;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    @media only screen and (min-width: 999px) {
        display: none;
    }
`;

export const IconSchedule = styled(ScheduleIcon)`
    color: #fff;
`;

export const IconAssignment = styled(AssignmentIcon)`
    color: #fff;
`;

export const IconStarBorder = styled(StarBorderIcon)`
    color: #fff;
`;

export const TimerBox = styled.div`
    margin: 30px 0 0 0;
    .time-count-down {
        width: 100%;
        height: 80px;
        border-radius: 5px;
        border: 2px solid rgba(175, 175, 175, 0.3);
        padding: 10px;
        position: relative;
        .title-3 {
            margin-top: 10px;
        }
        ul {
            display: flex;
            justify-content: space-around;
            align-items: center;
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            list-style-type: none;
            li {
                p {
                    font-family: Montserrat, sans-serif;
                    font-size: 0.6875rem;
                    font-weight: 500;
                    font-style: normal;
                    line-height: 1.54;
                    margin-bottom: 10px !important;
                    letter-spacing: normal;
                    text-align: center;
                    color: #afafaf;
                }

                span {
                    font-family: Montserrat, sans-serif;
                    font-size: 1.875rem;
                    font-weight: 100;
                    line-height: 0.73;
                    display: block;
                    color: ${props => props.theme.letter};
                    text-align: center;
                }
            }
            .separator {
                font-family: Montserrat, sans-serif;
                font-size: 1.875rem;
            }
        }
    }
`;
