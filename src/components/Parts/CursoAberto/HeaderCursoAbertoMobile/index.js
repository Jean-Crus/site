import React from "react";
import { useDispatch } from "react-redux";
import * as CartActions from "@store/modules/cart/actions";
import { ButtonTriade } from "@components";
import Room from "@material-ui/icons/Room";
import {
    Container,
    IconStarBorder,
    IconAssignment,
    IconSchedule,
    TimerBox
} from "./styles";
import Timer from "../HeaderCursosAberto/Timer";

const HeaderCursoMobile = ({
    title,
    category,
    hours,
    isAberto,
    modules,
    difficulty,
    lessonAddress,
    id
}) => {
    const dispatch = useDispatch();
    const addCurso = course => {
        dispatch(CartActions.addToCartRequest(course));
    };
    return (
        <Container>
            <div>{title && <span className="title">{title}</span>}</div>
            {!!category && (
                <div className="tag">
                    <span> {category.name}</span>
                </div>
            )}
            <div className="multi-tag mt-5">
                {!!difficulty && (
                    <div>
                        <IconStarBorder />
                        <span>{difficulty.name}</span>
                    </div>
                )}
                {!!modules && (
                    <div>
                        <IconAssignment />
                        <span>
                            {modules.length < 2
                                ? `${modules.length} Módulo`
                                : `${modules.length} Módulos`}
                        </span>
                    </div>
                )}
                {!!hours && (
                    <div>
                        <IconSchedule />
                        <span>
                            {hours < 2 ? `${hours} Hora` : `${hours} Horas`}
                        </span>
                    </div>
                )}
            </div>
            {!!lessonAddress && (
                <div className="multi-tag mt-5">
                    <div>
                        <Room style={{ color: "#fff" }} />

                        <span>Endereço da aula: {lessonAddress}</span>
                    </div>
                </div>
            )}
            {/* <TimerBox>
                <Timer />
            </TimerBox> */}

            <div className="button-box">
                <ButtonTriade
                    readOnly
                    text="Comprar"
                    letterColor="Modelo 02"
                    type="button"
                    cor="Modelo 02"
                    button="Modelo 02"
                    onClick={() => addCurso(id)}
                />
            </div>
        </Container>
    );
};
export default HeaderCursoMobile;
