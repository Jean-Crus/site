import styled from "styled-components";

export const Modulos = styled.div`
    .title-1 {
        text-align: center !important;
    }
    .badge-purple {
        background-color: #ef7300 !important;
        max-width: 140px;
    }
    .modules-dropdown ul.list-modules > li .title:before {
        display: none !important;
    }
    .modules-dropdown ul.list-modules > li span {
        margin: 0;
    }
    .modules-dropdown {
        .list-modules {
            .title {
                .fas {
                    color: #ef7300 !important;
                }
                flex-direction: row;
                /* align-items: center !important; */
                @media only screen and (max-width: 580px) {
                    padding: 15px 0 5px 0 !important;
                }
            }
        }
    }
`;
