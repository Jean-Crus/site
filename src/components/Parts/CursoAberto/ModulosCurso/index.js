/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Grid, Icon } from "@material-ui/core";
import { Modulos } from "./styles";

const ModulosCurso = () => {
    const modulos = useSelector(state => state.cursoDetail.data.data.modules);

    const [active, setActive] = useState({
        clicked: null
    });

    const handleDropDown = id => {
        return active.clicked === id
            ? setActive({ clicked: null })
            : setActive({ clicked: id });
    };
    return (
        <Modulos className="content ">
            {!!modulos.length && (
                <div className="title-1 size-50 black mt-5 mb-3">Módulos</div>
            )}
            <div className="modules-dropdown">
                <ul className="list-modules">
                    {modulos.map((element, index) => {
                        return (
                            <li className="" key={element.id}>
                                <Grid
                                    container
                                    className="title"
                                    onClick={() => handleDropDown(element.id)}
                                >
                                    <Grid
                                        item
                                        xl={1}
                                        lg={1}
                                        sm={1}
                                        md={1}
                                        xs={1}
                                    >
                                        <span>
                                            <strong>{index + 1}.</strong>
                                        </span>
                                    </Grid>
                                    <Grid
                                        xl={10}
                                        lg={10}
                                        sm={10}
                                        md={10}
                                        xs={10}
                                        container
                                        alignItems="center"
                                    >
                                        <span>{element.name}</span>
                                        <div className="badge badge-purple ml-4">
                                            {element.lessons.length}{" "}
                                            {element.lessons.length <= 1
                                                ? "aula"
                                                : "aulas"}
                                        </div>
                                    </Grid>

                                    <Grid
                                        xl={1}
                                        lg={1}
                                        sm={1}
                                        md={1}
                                        xs={1}
                                        container
                                        justify="flex-end"
                                    >
                                        {active.clicked === element.id ? (
                                            <Icon className="fas fa-chevron-down" />
                                        ) : (
                                            <Icon className="fas fa-chevron-up" />
                                        )}
                                    </Grid>
                                </Grid>
                                {active.clicked === element.id ? (
                                    <ul className="module-lessons">
                                        {element.lessons.length ? (
                                            element.lessons.map(licoes => (
                                                <li key={licoes.id}>
                                                    <div className="description-1">
                                                        {licoes.name}
                                                    </div>
                                                </li>
                                            ))
                                        ) : (
                                            <li>
                                                <div className="description-1">
                                                    Sem descrição
                                                </div>
                                            </li>
                                        )}
                                    </ul>
                                ) : null}
                            </li>
                        );
                    })}
                </ul>
            </div>
        </Modulos>
    );
};

export default ModulosCurso;
