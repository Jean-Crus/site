import React from 'react';
import Countdown from 'react-countdown-now';

export default function Timer({ launch_date, end_date }) {
    const renderer = ({ days, hours, minutes, seconds, completed }) => {
        if (completed) {
            // Render a completed state
            return <h1>Completo!!</h1>;
        }
        // Render a countdown
        return (
            <div className="time-count-down">
                <ul>
                    <li>
                        <p>Dias</p>
                        <span id="days">{days}</span>
                    </li>
                    <li className="separator">:</li>
                    <li>
                        <p>Horas</p>
                        <span id="hours">{hours}</span>
                    </li>
                    <li className="separator">:</li>
                    <li>
                        <p>Min</p>
                        <span id="minutes">{minutes}</span>
                    </li>
                    <li className="separator">:</li>
                    <li>
                        <p>Seg</p>
                        <span id="seconds">{seconds}</span>
                    </li>
                </ul>
                <div className="title-3 size-50 count-description">
                    para o encerramento das inscrições
                </div>
            </div>
        );
    };
    return <Countdown date={Date.now() + 1000000000} renderer={renderer} />;
}
