import React from "react";
import { useDispatch } from "react-redux";
import { Grid } from "@material-ui/core";
import * as CartActions from "@store/modules/cart/actions";
import ScheduleIcon from "@material-ui/icons/Schedule";
import AssignmentIcon from "@material-ui/icons/Assignment";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import Room from "@material-ui/icons/Room";

import { ButtonTriade } from "@components";
import Timer from "./Timer";
import { Container } from "./styles";

const HeaderCursosAberto = ({
    title,
    category,
    hours,
    isAberto,
    modules,
    difficulty,
    id,
    lessonAddress
}) => {
    const dispatch = useDispatch();
    const addCurso = course => {
        dispatch(CartActions.addToCartRequest(course));
    };
    return (
        <Container className="content container">
            <div className="row">
                <div className="col-lg-5 tr-flex-center">
                    <div className="information-course">
                        <div className="information-description">
                            <span
                                className="badge"
                                style={{
                                    backgroundColor: `${!!category &&
                                        category.color}`
                                }}
                            >
                                {!!category && category.name}
                            </span>
                        </div>
                        <div className="title-1 size-50">{title}</div>

                        <div className="information-description">
                            {!!hours && (
                                <div className="description-3">
                                    <ScheduleIcon />
                                    {hours < 2
                                        ? `${hours} Hora`
                                        : `${hours} Horas`}
                                </div>
                            )}
                            {!!modules && (
                                <div className="description-3">
                                    <AssignmentIcon />
                                    {modules.length < 2
                                        ? `${modules.length} Módulo`
                                        : `${modules.length} Módulos`}
                                </div>
                            )}
                            {!!difficulty && (
                                <div className="description-3">
                                    <StarBorderIcon />{" "}
                                    {!!difficulty && difficulty.name}
                                </div>
                            )}
                        </div>
                        {lessonAddress && (
                            <div className="information-description">
                                <div className="description-3">
                                    <Room />
                                    Endereço da aula: {lessonAddress}
                                </div>
                            </div>
                        )}
                    </div>
                </div>
                {isAberto ? (
                    <>
                        <div className="col-lg-4 tr-flex-center">
                            {/* <Timer /> */}
                        </div>
                        <div className="col-lg-3 tr-flex-center pr-0 justify-content-end">
                            <div className="button-buy">
                                <ButtonTriade
                                    onClick={() => addCurso(id)}
                                    button="Modelo 02"
                                    text="Comprar"
                                    letterColor="Modelo 02"
                                    cor="Modelo 02"
                                    type="button"
                                />
                            </div>
                        </div>
                    </>
                ) : (
                    <>
                        <div className="col-lg-4 tr-flex-center d-flex justify-content-end pr-5">
                            <div className="title-1 size-15 red wh-300 alert-vagas-encerradas">
                                Inscrições encerradas
                            </div>
                        </div>
                        <div className="col-lg-3 tr-flex-center pr-0 justify-content-end">
                            <div className="button-buy">
                                <a
                                    href="#"
                                    className="btn tr-course-close"
                                    data-toggle="modal"
                                    data-target="#listadeesperacurso"
                                >
                                    Lista de espera
                                </a>
                            </div>
                        </div>
                    </>
                )}
            </div>
        </Container>
    );
};

export default HeaderCursosAberto;
