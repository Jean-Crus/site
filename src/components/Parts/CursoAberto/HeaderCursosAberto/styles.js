import styled from "styled-components";

export const Container = styled.div`
    @media only screen and (max-width: 999px) {
        display: none;
    }
`;
