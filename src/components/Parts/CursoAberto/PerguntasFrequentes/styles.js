import styled from "styled-components";

export const Container = styled.div`
    .title-1 {
        text-align: center !important;
    }
`;

export const ModulesDropdown = styled.ul`
    padding: 0;

    li .title::before {
        border-color: solid #ef7300 !important;
    }
`;
export const DropDown = styled.li`
    span {
        margin: 0 !important;
    }
    .title {
        cursor: pointer;
        flex-direction: row !important;
        &::before {
            display: none !important;
        }
        .fas {
            color: #ef7300 !important;
        }
    }
    ul {
        height: ${props => (props.active ? "initial" : "")} !important;
        overflow: ${props => (props.active ? "initial" : "")} !important;
        visibility: ${props => (props.active ? "initial" : "")} !important;
        opacity: ${props => (props.active ? "initial" : "")} !important;
        background-color: #f7f7f7 !important;
    }
`;
