import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Grid, Icon } from "@material-ui/core";
import { Container, DropDown, ModulesDropdown } from "./styles";

const PerguntasFrequentes = ({ perguntas }) => {
    const [active, setActive] = useState({
        clicked: null
    });
    const modulos = useSelector(state => state.cursoDetail.data.data.modules);
    const handleDropDown = id => {
        return active.clicked === id
            ? setActive({ clicked: null })
            : setActive({ clicked: id });
    };
    return (
        <Container className="content ">
            {!!perguntas && (
                <div className="title-1 size-50 black mt-5 mb-3">
                    Perguntas Frequentes
                </div>
            )}
            <ModulesDropdown className="modules-dropdown">
                <ul className="list-modules">
                    {!!perguntas &&
                        perguntas.map((element, index) => {
                            return (
                                <DropDown
                                    key={element.id}
                                    active={
                                        active.clicked === index
                                            ? true
                                            : undefined
                                    }
                                >
                                    <Grid
                                        container
                                        className="title"
                                        onClick={() => handleDropDown(index)}
                                    >
                                        <Grid
                                            item
                                            xl={1}
                                            lg={1}
                                            sm={1}
                                            md={1}
                                            xs={1}
                                        >
                                            <span>
                                                <strong>{index + 1}.</strong>
                                            </span>
                                        </Grid>
                                        <Grid
                                            item
                                            xl={10}
                                            lg={10}
                                            sm={10}
                                            md={10}
                                            xs={10}
                                        >
                                            <span>{element.name}</span>
                                        </Grid>
                                        <Grid
                                            item
                                            xl={1}
                                            lg={1}
                                            sm={1}
                                            md={1}
                                            xs={1}
                                            container
                                            justify="flex-end"
                                        >
                                            {active.clicked === index ? (
                                                <Icon className="fas fa-chevron-down" />
                                            ) : (
                                                <Icon className="fas fa-chevron-up" />
                                            )}
                                        </Grid>
                                    </Grid>
                                    <ul className="module-lessons">
                                        <li>{element.description}</li>
                                    </ul>
                                </DropDown>
                            );
                        })}
                </ul>
            </ModulesDropdown>
        </Container>
    );
};

export default PerguntasFrequentes;
