import React from 'react';

const BonusCurso = (props) => {
	
	return(
		<div className="content">
			<div className="title-1 size-50 black mt-5 mb-3">
				Bônus
			</div>
			<div className="bonus-dropdown">
				<ul className="list-bonus"> 
					<li className="">
						<div className="title before-none">
							<span><strong>1.</strong>Curso de Programação</span>
							<div className="badge badge-red">
								12 aulas
							</div>
						</div>
						<ul className="bonus-lessons">
							<li>- Introdução </li>
							<li>- Unidade 1 - Formação do condutor </li>
							<li>- Unidade 2 - Exigências para categorias de habilitação em relação a veículo conduzido </li>
							<li>- Unidade 3 - Sinalização viária </li>
							<li>- Unidade 4 - Normas de circulação e conduta </li>
							<li>- Unidade 5 - Infrações e penalidades referentes à documentação do condutor e do veículo, estacionamento, parada e circulação </li>
							<li>- Unidade 6 - Penalidades e crimes de trânsito </li>
							<li>- Unidade 7 - Direitos e deveres do cidadão </li>
							<li>- Unidade 8 - Segurança e atitudes do condutor, passageiro, pedestre e demais atores do processo de circulação. </li>
							<li>- Unidade 9 - Meio ambiente </li>
							<li>- Prova do módulo I </li>
						</ul>
					</li>
					<li className="">
						<div className="title before-none">
							<span><strong>2.</strong>Legislação da Programação</span>
							<div className="badge badge-red">
								4 aulas
							</div>
						</div>
						<ul className="bonus-lessons">
							<li>- Introdução </li>
							<li>- Unidade 1 - Formação do condutor </li>
							<li>- Unidade 2 - Exigências para categorias de habilitação em relação a veículo conduzido </li>
							<li>- Unidade 3 - Sinalização viária </li>
							<li>- Unidade 4 - Normas de circulação e conduta </li>
							<li>- Unidade 5 - Infrações e penalidades referentes à documentação do condutor e do veículo, estacionamento, parada e circulação </li>
							<li>- Unidade 6 - Penalidades e crimes de trânsito </li>
							<li>- Unidade 7 - Direitos e deveres do cidadão </li>
							<li>- Unidade 8 - Segurança e atitudes do condutor, passageiro, pedestre e demais atores do processo de circulação. </li>
							<li>- Unidade 9 - Meio ambiente </li>
							<li>- Prova do módulo I </li>
						</ul>
					</li>
					<li className="">
						<div className="title before-none">
							<span><strong>2.</strong>O que é POG?</span>
							<div className="badge badge-red">
								3 aulas
							</div>
						</div>
						<ul className="bonus-lessons">
							<li>- Introdução </li>
							<li>- Unidade 1 - Formação do condutor </li>
							<li>- Unidade 2 - Exigências para categorias de habilitação em relação a veículo conduzido </li>
							<li>- Unidade 3 - Sinalização viária </li>
							<li>- Unidade 4 - Normas de circulação e conduta </li>
							<li>- Unidade 5 - Infrações e penalidades referentes à documentação do condutor e do veículo, estacionamento, parada e circulação </li>
							<li>- Unidade 6 - Penalidades e crimes de trânsito </li>
							<li>- Unidade 7 - Direitos e deveres do cidadão </li>
							<li>- Unidade 8 - Segurança e atitudes do condutor, passageiro, pedestre e demais atores do processo de circulação. </li>
							<li>- Unidade 9 - Meio ambiente </li>
							<li>- Prova do módulo I </li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	);	
}

export default BonusCurso;