import React from "react";
import { useDispatch } from "react-redux";
import * as CartActions from "@store/modules/cart/actions";
import { ButtonTriade } from "@components";
import {
    Link,
    Element,
    Events,
    animateScroll as scroll,
    scrollSpy,
    scroller
} from "react-scroll";
import { NavBar } from "./styles";

const NagivationCourse = ({ isAberto, price, id, cursoDetail }) => {
    const dispatch = useDispatch();
    const addCurso = course => {
        dispatch(CartActions.addToCartRequest(course));
    };
    return (
        <NavBar className="navigation">
            <div className="buttons">
                <div className="btn-group-vertical" role="group">
                    {!!cursoDetail.description && (
                        <Link
                            activeClass="active"
                            to="test1"
                            spy
                            smooth
                            offset={-100}
                            duration={500}
                        >
                            Sobre o curso
                        </Link>
                    )}
                    {!!cursoDetail.benefits && (
                        <Link
                            activeClass="active"
                            to="test2"
                            spy
                            smooth
                            offset={-100}
                            duration={500}
                        >
                            Vantagens
                        </Link>
                    )}
                    {!!cursoDetail.modules.length && (
                        <Link
                            activeClass="active"
                            to="test3"
                            spy
                            smooth
                            offset={-100}
                            duration={500}
                        >
                            Módulos
                        </Link>
                    )}
                    {!!cursoDetail.responsable && (
                        <Link
                            activeClass="active"
                            to="test4"
                            spy
                            smooth
                            offset={-100}
                            duration={500}
                        >
                            Sobre o professor
                        </Link>
                    )}
                    {!!cursoDetail.frequent_questions && (
                        <Link
                            activeClass="active"
                            to="test5"
                            spy
                            smooth
                            offset={-100}
                            duration={500}
                        >
                            Perguntas frequentes
                        </Link>
                    )}
                    {!!cursoDetail.bonus && (
                        <Link
                            activeClass="active"
                            to="test6"
                            spy
                            smooth
                            offset={-100}
                            duration={500}
                        >
                            Bônus
                        </Link>
                    )}
                </div>
            </div>
            {isAberto ? (
                <div className="information-course">
                    <div className="content">
                        <small className="small-title mb-3">
                            O melhor Curso para Câmara de Vereadores de Curitiba
                        </small>
                        <small>Investimento de</small>
                        <div className="price">{price}</div>
                        <ButtonTriade
                            onClick={() => addCurso(id)}
                            className="mt-2 mb-2"
                            button="Modelo 02"
                            size="small"
                            letterColor="Modelo 02"
                            cor="Modelo 02"
                            text="Comprar"
                        />
                    </div>
                </div>
            ) : (
                <div className="information-course">
                    <div className="content">
                        <div className="title-1 alert-vagas-encerradas size-25">
                            Inscrições
                            <br />
                            encerradas
                        </div>
                        <div className="button-buy">
                            <a
                                href="#"
                                className="btn tr-course-close small"
                                data-toggle="modal"
                                data-target="#listadeesperacurso"
                            >
                                Lista de espera
                            </a>
                        </div>
                    </div>
                </div>
            )}
        </NavBar>
    );
};

export default NagivationCourse;
