import styled from "styled-components";

export const NavBar = styled.nav`
    background: linear-gradient(0deg, #012a50 0%, #0058a8 100%);

    .content {
        display: flex;
        width: 100%;
        min-height: 220px !important;
        flex-direction: column;
        align-items: center;
        padding: 0 !important;
        small {
            color: #fff !important;
        }
        .small-title {
            padding: 0 10px;
            color: #fff !important;

            font-weight: bold !important;
            text-align: center;
        }
    }
    button {
        width: 180px !important;
    }
    .active {
        color: #fff !important;
        font-weight: bold;
        &:before {
            content: "";
            position: absolute;
            height: 100%;
            width: 6px;
            background: linear-gradient(
                0deg,
                #5074b7 0%,
                #31528a 100%
            ) !important;
            left: 0px;
        }
    }
    a {
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: transparent;
        color: #afafaf !important;
        min-height: 50px;
        text-align: left;
        outline: none;
        box-shadow: none;
        border-bottom: 1px solid rgba(175, 175, 175, 0.1);
        font-family: "Montserrat", sans-serif;
        font-size: 0.875rem;
        font-weight: 500;
        font-style: normal;
        font-stretch: normal;
        line-height: normal;
        letter-spacing: normal;
        position: relative;
        overflow: hidden;
    }
    .information-course {
        align-items: center !important;
        width: 100% !important;
        min-height: 150px !important;
        display: flex !important;
        height: 100% !important;
    }
    @media only screen and (max-width: 1201px) {
        width: 95% !important;
    }
    @media only screen and (max-width: 999px) {
        display: none;
    }
`;
