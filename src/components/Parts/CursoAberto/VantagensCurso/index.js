import React from "react";
import { Icon } from "@material-ui/core";
import { Vantagens } from "./styles";

const VantagensCurso = ({ vantagens, title }) => {
    return (
        <Vantagens className="vantagens">
            <div className="content">
                {!!vantagens && (
                    <div className="title-1 size-50 black mt-5 mb-3">
                        {title}
                    </div>
                )}

                <div className="blocks">
                    <div className="row">
                        {!!vantagens &&
                            vantagens.map(item => {
                                return (
                                    <div className="col-lg-4">
                                        <div className="itens">
                                            <Icon
                                                className={item.icon}
                                                fontSize="large"
                                                color="primary"
                                            />
                                            <div className="content">
                                                <div className="title-1 size-25 black">
                                                    {item.name}
                                                </div>
                                                <div className="description">
                                                    <div className="description-2">
                                                        {item.description}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            })}
                    </div>
                </div>
            </div>
        </Vantagens>
    );
};

export default VantagensCurso;
