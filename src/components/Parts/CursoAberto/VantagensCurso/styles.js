import styled from "styled-components";

export const Vantagens = styled.div`
    .MuiIcon-root {
        overflow: initial;
    }
    .MuiIcon-colorPrimary {
        color: ${props => props.theme.primary};
    }
    .title-1 {
        text-align: center !important;
    }
    .content {
        .itens {
            display: flex !important;
            justify-content: center !important;
            align-items: center !important;
            .content {
                text-align: center !important;
            }
        }
    }
`;
