import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { CardInstrutor } from "@components";
import { Container } from "./styles";

const InstrutorCurso = ({ onClick }) => {
    const instructor = useSelector(
        state => state.cursoDetail.data.data.responsable
    );
    return (
        instructor.length > 0 && (
            <Container className="content ">
                {
                    <div className="title-1 size-50 black ">
                        Sobre o professor
                    </div>
                }
                {!!instructor &&
                    instructor.map(instrutor => {
                        return (
                            <CardInstrutor
                                instrutor={instrutor}
                                key={instrutor.id}
                                onClick={onClick}
                            />
                        );
                    })}
            </Container>
        )
    );
};

export default InstrutorCurso;
