import React from 'react';
import { Link } from 'react-router-dom';

const FiltroEventos = () => {

	return(
        <div className="content container">
            <div className="filter-button">
                <div className="filter-1 "> 
                    <div className="button">
                        <Link to="#" className="btn tr-quaternary active">Próximos eventos</Link>
                    </div>
                </div>
                <div className="filter-2">
                    <div className="button">
                        <Link to="#" className="btn tr-quaternary">Eventos realizados</Link>
                    </div>
                </div>
            </div>
            <div className="filter-3">
                <div className="selected">
                    <select className="selectpicker">
                        <option selected>Presencial</option>
                        <option value="1">A Distância</option>
                    </select>
                </div>
            </div>
        </div>
	);
};

export default FiltroEventos;