import React from 'react';
import { Link } from 'react-router-dom';
import { curso2 } from '@images';

const style = {
    curso2:{
        backgroundImage: "url('" + curso2 + "')",
    }
}

const SingleEventos = () => {

	return(
        <div className="card-event">
            <Link to="evento_aberto.php" className="event">
                <div className="banner" style={style.curso2}></div>
            </Link>
            <div className="tr-card-event-flat">
                <Link to="evento_aberto.php" className="event">
                    <div className="date">
                        <div className="date-number">
                            <p>12</p>
                            <b>DEZ</b>
                        </div>
                    </div>
                </Link>
                <div className="information">
                    <div className="content">
                        <Link to="" className="title-2 size-50 black">
                            Workshop Lorem Ipsum
                        </Link>
                        <div className="local">
                            <div className="hours description-3"> 9h -10h </div>
                            <div className="type-event description-3 local">Centro de Convenções</div>
                        </div>
                        <div className="description">
                            <div className="description-2">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales magna pus semper
                                lorem consectetur adipiscing elit. Etiam sodales magna pus semper lorem..
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	);
};

export default SingleEventos;