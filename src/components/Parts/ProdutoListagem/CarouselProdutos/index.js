import React from 'react';
import { curso1 } from '@images';
import { CardCurso } from '@components';

const CarouselProdutos = (props, state) => {

    state = {
        produtos: [
            {},{},{},{},{},{},{},{},{},{},
        ]
    }
	
	return(
		<div className="container">
            <div className="carousel-produtos">
                <h2 className="title-1 size-50 black text-center mb-3"><strong>Mais Vendidos</strong></h2>
                <div className="owl-carousel owl-theme">
                    {
                        state.produtos.map((item, index) => (
                        <CardCurso
                            image={curso1}
                            title="Curso de Lorem Ipsum Avançado"
                            category="Design"
                            colorCategory="purple"
                            difficulty="Avançado"
                            closed={false}
                            price="250,00"
                            salePrice="99,00"
                            hours="18"
                            modules="6"
                            description="“Today Web UI Kit is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been."
                        />
                        ))
                    }
                </div>
            </div>
        </div>
	);	
}

export default CarouselProdutos;