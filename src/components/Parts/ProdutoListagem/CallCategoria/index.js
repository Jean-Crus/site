import React from 'react';
import { Link } from 'react-router-dom';
import { lab, rightGreen } from '@images';

const CallCategoria = props => {
    return (
        <div className="container">
            <div className="call-categoria">
                <div className="row">
                    <div className="col-sm-6">
                        <div className="categoria-nome">
                            <img src={lab} alt="" />
                            <h3 className="title-1">
                                Banner com
                                <br /> destaque
                            </h3>
                        </div>
                    </div>
                    <div className="align-items-center col-sm-6 d-flex justify-content-end">
                        <Link to="/" className="see-categoria">
                            Confira
                            <img src={rightGreen} alt="" />
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CallCategoria;
