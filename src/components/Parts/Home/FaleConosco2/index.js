import React, { useState } from "react";
import { Formik } from "formik";
import { useSelector, useDispatch } from "react-redux";
import { Grid, TextField } from "@material-ui/core";
import { faleConosco } from "@theme";
import { ButtonTriade } from "@components";
import { Container } from "./styles";

const style = {
    curso2: {
        backgroundImage: `url('${faleConosco.img}')`
    }
};

const FaleConosco = () => {
    const initialValues = {
        duvida: "",
        email: ""
    };
    const [open, setOpen] = useState(false);
    const handleOpen = () => {
        setOpen(!open);
    };

    return (
        <Container className="container fale-conosco">
            <div className="content">
                <div className="information">
                    <div className="image-full">
                        <span className="image" style={style.curso2} />
                    </div>
                    <div className="title">
                        <div className="title-1">{faleConosco.title}</div>
                        <div className="title-2">{faleConosco.subTitle}</div>
                    </div>
                    <div className="description">
                        <div className="description-1">
                            {faleConosco.phrase}
                        </div>
                    </div>
                </div>
            </div>
            <Formik
                initialValues={initialValues}
                enableReinitialize
                // validationSchema={schema}
                className="tr-form"
                // validate={props => handleBrand(props.numberCard)}
            >
                {({
                    handleBlur,
                    handleChange,
                    handleSubmit,
                    values,
                    errors,
                    touched
                }) => {
                    return (
                        <form>
                            <Grid
                                container
                                alignItems="center"
                                justify="center"
                            >
                                <Grid item xs={12} sm={8} md={8} lg={8}>
                                    <TextField
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        label="Nome"
                                        name="name"
                                        value={values.name}
                                        margin="normal"
                                        fullWidth
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid item xs={12} sm={8} md={8} lg={8}>
                                    <TextField
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        label="E-mail"
                                        name="email"
                                        value={values.email}
                                        margin="normal"
                                        fullWidth
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid item xs={12} sm={8} md={8} lg={8}>
                                    <TextField
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        label="Telefone"
                                        name="tel"
                                        value={values.tel}
                                        margin="normal"
                                        fullWidth
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid
                                    item
                                    xs={12}
                                    sm={8}
                                    md={8}
                                    lg={8}
                                    className="button-group mt-4"
                                >
                                    {/* <div className="chat-text">
                                        Se preferir, fale conosco{' '}
                                        <strong>ATRAVÉS DO CHAT</strong>
                                    </div> */}
                                    <ButtonTriade
                                        button="Modelo 02"
                                        type="button"
                                        letterColor="Modelo 02"
                                        cor="Modelo 02"
                                        size="small"
                                        text="Enviar"
                                    />
                                </Grid>
                            </Grid>
                        </form>
                    );
                }}
            </Formik>
        </Container>
    );
};

export default FaleConosco;
