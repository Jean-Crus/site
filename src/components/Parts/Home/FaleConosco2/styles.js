import styled from "styled-components";

export const Container = styled.div`
    margin-top: 40px;
    padding: 40px;
    display: flex;
    background: ${props =>
        `linear-gradient(0deg, ${props.backOne} 0%, ${props.backTwo} 100%)`} !important;
    align-items: center;
    flex-direction: row;

    .content {
        justify-content: flex-start !important;
        .information {
            width: 100%;
        }
    }

    .container-img {
        max-width: 600px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        img {
            margin: 40px;
            width: 100%;
            max-height: 300px;
        }
        .description-1 {
            text-align: center;
        }
    }

    form {
        width: 100%;
        height: 550px;
        padding-top: 36px;
        font-family: Montserrat !important;
        .MuiFormLabel-root {
            color: ${props => props.theme.letter2} !important;
        }
        .MuiTextField-root {
            background-color: ${props => props.theme.letter};
            border-radius: 5px;
        }
        .Mui-focused {
            color: ${props => props.theme.primary} !important;
        }
        .MuiFilledInput-underline:after {
            border-bottom: ${props => `2px solid ${props.theme.primary}`};
        }
        .MuiFilledInput-underline:before {
            border-bottom: transparent;
        }
        .button-group {
            display: flex;
            justify-content: center;
            width: 100%;
            .chat-text {
                max-width: 150px;
                color: ${props => props.theme.letter};
                font-size: 10px;
                strong {
                    text-decoration: underline;
                }
            }
        }
    }

    @media only screen and (max-width: 999px) {
        flex-direction: column;
        .content {
            max-height: 350px;
            .information {
                padding-left: initial !important;
                padding-top: initial !important;
                display: flex;
                flex-direction: column;
                align-items: center;
            }
        }
        form {
            max-height: 350px;
        }
    }

    @media only screen and (min-width: 980px) {
        display: none;
    }

    @media only screen and (max-width: 850px) {
        width: 100% !important;
        max-width: 100% !important;
    }

    @media only screen and (max-width: 481px) {
        .content {
            .information {
                .title {
                    margin-top: -20px !important;
                    .title-1 {
                        text-align: center;
                        font-size: 30px !important;
                    }
                    .title-2 {
                        font-size: 18px !important;
                    }
                }
            }
        }
        form {
            padding-top: 0;
        }
    }
`;
