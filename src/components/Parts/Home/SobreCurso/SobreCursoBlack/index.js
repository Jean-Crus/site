import React from 'react';
import { Link } from 'react-router-dom';

import { bitmap, compress } from '@images';
import {
    Container,
    SliderDepo,
    IconCircleLeft,
    IconCircleRight,
    ArrowRight,
    ArrowLeft,
} from './styles';

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <IconCircleLeft
            // className={className}
            onClick={onClick}
            // style={{ ...style, color: '#FFFFFF' }}
        >
            <ArrowLeft />
        </IconCircleLeft>
    );
}

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <IconCircleRight
            // className={className}
            onClick={onClick}
            // style={{ ...style, color: '#FFFFFF' }}
        >
            <ArrowRight />
        </IconCircleRight>
    );
}

const settings = {
    // fade: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    autoplaySpeed: 3000,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
};

export default function BlackSobre() {
    return (
        <>
            <Container>
                <div className="division" />
                <div className="testimonial">
                    <div className="title-1 size-50">O Que dizem</div>
                    <SliderDepo
                        {...settings}
                        className="testimonial-carrousel"
                        id="testimonial"
                    >
                        <div className="itens">
                            <div className="content">
                                <div className="description-2 italic">
                                    “Today Web UI Kit is simply dummy text of
                                    the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy
                                    text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled
                                    it to make a type specimen book. It has
                                    survived not only five centuries”
                                </div>
                                <div className="see-more">
                                    <Link to="/" className="tr-link">
                                        veja mais
                                    </Link>
                                </div>
                                <div className="author">
                                    <div className="avatar">
                                        <img src={compress} alt="compress" />
                                    </div>
                                    <div className="name">
                                        Matheus kindrazki
                                    </div>
                                    <div className="city">Curitiba (BR)</div>
                                </div>
                            </div>
                        </div>
                        <div className="itens">
                            <div className="content">
                                <div className="description-2 italic">
                                    “Today Web UI Kit is simply dummy text of
                                    the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy
                                    text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled
                                    it to make a type specimen book. It has
                                    survived not only five centuries”
                                </div>
                                <div className="see-more">
                                    <Link to="/" className="tr-link">
                                        veja mais
                                    </Link>
                                </div>
                                <div className="author">
                                    <div className="avatar" />
                                    <div className="name">
                                        Matheus kindrazki
                                    </div>
                                    <div className="city">Curitiba (BR)</div>
                                </div>
                            </div>
                        </div>
                    </SliderDepo>
                </div>
            </Container>
        </>
    );
}
