import styled from 'styled-components';
import Slider from 'react-slick';

export const Container = styled.div`
    .testimonial {
        max-height: 100% !important;
    }
    .itens {
        max-width: 50% !important;
        margin: 30px 0 !important;
    }
`;

export const SliderDepo = styled(Slider)`
    .content {
        width: 100% !important;
    }
`;

export const IconCircleLeft = styled.div`
    display: flex;
    top: 39%;
    left: 0;
    justify-content: center;
    position: absolute;
    align-items: center;
    z-index: 1000;
    height: 67px;
    width: 67px;
    border-radius: 67px;
    background-color: ${props => props.theme.primary};
    &:hover {
        cursor: pointer;
        div {
            opacity: 1;
        }
    }
`;

export const IconCircleRight = styled.div`
    display: flex;
    top: 39%;
    right: 0px;
    justify-content: center;
    position: absolute;
    align-items: center;
    height: 67px;
    width: 67px;
    border-radius: 67px;
    background-color: ${props => props.theme.primary};

    &:hover {
        cursor: pointer;
        div {
            opacity: 1;
        }
    }
`;

export const ArrowRight = styled.div`
    height: 12px;
    width: 12px;
    border: ${props => `solid ${props.theme.letter}`};
    border-width: 0 3px 3px 0;
    transform: rotate(-45deg);
    -webkit-transform: rotate(-45deg);
    opacity: 0.5;
`;

export const ArrowLeft = styled.div`
    height: 12px;
    width: 12px;
    border: ${props => `solid ${props.theme.letter}`};
    border-width: 0 3px 3px 0;
    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
    opacity: 0.5;
`;
