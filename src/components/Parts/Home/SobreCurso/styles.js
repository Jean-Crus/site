import styled from "styled-components";

export const Container = styled.div`
    padding: 40px;
    .img-content {
        display: flex;
        position: relative;
        img {
            width: 100%;
            height: 100%;
            max-width: 568px;
            max-height: 400px;
            border-radius: 5px;
        }
        @media only screen and (max-width: 996px) {
            justify-content: center;
            flex-direction: column;
            align-items: center;
            padding: 0;
            margin-top: 0 !important;
        }
    }

    .text-content {
        display: flex;
        flex-direction: column;
        position: absolute;
        top: 20px;
        right: 0;
        align-items: flex-end;
        @media only screen and (max-width: 1201px) {
            .title-1 {
                font-size: 40px !important;
            }
            .title-2 {
                font-size: 30px !important;
            }
        }
        @media only screen and (max-width: 996px) {
            position: static;
            align-items: center;
        }
        @media only screen and (max-width: 568px) {
            .title-1 {
                font-size: 1.6rem !important;
            }
            .title-2 {
                font-size: 1.3rem !important;
            }
        }
    }
`;
