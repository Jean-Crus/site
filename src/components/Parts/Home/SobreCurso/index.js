import React from "react";

import { Link } from "react-router-dom";
import { ButtonTriade } from "@components";
import { sobreCurso } from "@theme";
import history from "@routes/history";
import SobreBlack from "./SobreCursoBlack";
import WhiteCurso from "./SobreCursoWhite";
import { Container } from "./styles";

const SobreCurso = ({ name, className }) => {
    return (
        <>
            <Container className={`about container ${className}`} name={name}>
                <div className="title-1 size-50 wh-600">{sobreCurso.title}</div>
                <div className="img-content">
                    <img src={sobreCurso.img} alt="" />
                    <div className="text-content">
                        <div className="title-1">{sobreCurso.subTitle}</div>
                        <div className="title-2">{sobreCurso.phrase}</div>
                        <ButtonTriade
                            text="Conheça"
                            button="Modelo 02"
                            letterColor="Modelo 02"
                            cor="Modelo 02"
                            type="button"
                            onClick={() => history.push("/sobre")}
                            className="mt-5"
                        />
                    </div>
                </div>
                {/* <SobreBlack /> */}
            </Container>
            {/* <WhiteCurso /> */}
        </>
    );
};

export default SobreCurso;
