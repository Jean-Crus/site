import React, { useState } from 'react';

import {
    Container,
    CardVideo,
    ArrowPlay,
    IconCircle,
    SliderWhite,
    LeftArrow,
    RightArrow,
    ModalPlayer,
    Player,
    Box,
    VejaMais,
} from './styles';

function SampleNextArrow(props) {
    const { className, onClick } = props;
    return (
        <RightArrow className={className} onClick={onClick} fontSize="large" />
    );
}

function SamplePrevArrow(props) {
    const { className, onClick } = props;
    return (
        <LeftArrow className={className} onClick={onClick} fontSize="large" />
    );
}

const settings = {
    // fade: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    initialSlide: 0,
    responsive: [
        {
            breakpoint: 1266,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
            },
        },
        {
            breakpoint: 748,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            },
        },
        {
            breakpoint: 548,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                fade: true,
            },
        },
    ],
};

export default function WhiteCurso({ depoimentos }) {
    const [state, setState] = useState([
        {
            id: 1,
            video: 'https://www.youtube.com/watch?v=ZRx4mTDMmzA',
            imagem:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
        },
        {
            id: 2,
            video: 'https://www.youtube.com/watch?v=npCMuKdtDKI',
            imagem:
                'https://images.pexels.com/photos/774909/pexels-photo-774909.jpeg?cs=srgb&dl=alegria-atraente-bonita-774909.jpg&fm=jpg',
        },
        {
            id: 3,
            video: 'https://www.youtube.com/watch?v=eVFs6GnJTZs',
            imagem:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
        },
        {
            id: 4,
            video: 'https://www.youtube.com/watch?v=_f9sK9KMxRI',
            imagem:
                'https://images.pexels.com/photos/774909/pexels-photo-774909.jpeg?cs=srgb&dl=alegria-atraente-bonita-774909.jpg&fm=jpg',
        },
        {
            id: 5,
            video: 'https://www.youtube.com/watch?v=zjojsT9zo8M',
            imagem:
                'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?cs=srgb&dl=adulto-alegria-cabelo-220453.jpg&fm=jpg',
        },
    ]);
    const [open, setOpen] = useState(false);
    const [active, setActive] = useState({ url: '', id: '' });
    const handleOpen = (link, id) => {
        setOpen(!open);
        setActive({ url: link, id });
    };
    return (
        <Container>
            <div className="title-1 size-50-wh-600">Depoimentos</div>
            <SliderWhite {...settings}>
                {state.map(depoimentos => {
                    return (
                        <CardVideo key={depoimentos.id}>
                            <div className="video-card">
                                <div
                                    className="video"
                                    onClick={() =>
                                        handleOpen(
                                            depoimentos.video,
                                            depoimentos.id
                                        )
                                    }
                                >
                                    {active.id === depoimentos.id ? null : (
                                        <IconCircle className="play-button">
                                            <ArrowPlay fontSize="small" />
                                        </IconCircle>
                                    )}
                                    <img src={depoimentos.imagem} />
                                </div>
                                <div className="text-video">
                                    <span>Tristan Olsen</span>
                                    <small>São Paulo(BR)</small>
                                </div>
                            </div>
                        </CardVideo>
                    );
                })}
            </SliderWhite>
            <Box container justify="center">
                <VejaMais to="/depoimentos">Veja mais depoimentos</VejaMais>
            </Box>
            <ModalPlayer open={open} onClose={handleOpen}>
                <Player url={active.url} />
            </ModalPlayer>
        </Container>
    );
}
