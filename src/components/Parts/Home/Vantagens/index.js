import React from 'react';
import { Link } from 'react-router-dom';
import { material, star, time } from '@images';
import { vantagens } from '@theme';
import { Container, ReactSlider, CardVantagens } from './styles';

const Vantagens = () => {
    const settings = {
        customPaging() {
            return (
                <div className="dots">
                    <div className="circle" />
                </div>
            );
        },
        arrows: false,
        dots: true,
        // fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        // autoplay: true,
        // autoplaySpeed: 3000,
    };
    return (
        <Container className="container vantagens">
            <div className="title-1 size-50 black wh-600">
                {vantagens.titleVantagem}
            </div>
            <div className="blocks">
                <div className="itens">
                    <div className="icon">
                        <img src={vantagens.img1} alt="material" />
                    </div>
                    <div className="content">
                        <div className="title-1 size-25 black">
                            {vantagens.title1}
                        </div>
                        <div className="description">
                            <div className="description-2">
                                {vantagens.phrase1}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="itens">
                    <div className="icon">
                        <img src={vantagens.img2} alt="star" />
                    </div>
                    <div className="content">
                        <div className="title-1 size-25 black">
                            {vantagens.title2}
                        </div>
                        <div className="description">
                            <div className="description-2">
                                {vantagens.phrase2}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="itens">
                    <div className="icon">
                        <img src={vantagens.img3} alt="time" />
                    </div>
                    <div className="content">
                        <div className="title-1 size-25 black">
                            {vantagens.title3}
                        </div>
                        <div className="description">
                            <div className="description-2">
                                {vantagens.phrase3}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="itens">
                    <div className="icon">
                        <img src={vantagens.img4} alt="time" />
                    </div>
                    <div className="content">
                        <div className="title-1 size-25 black">
                            {vantagens.title4}
                        </div>
                        <div className="description">
                            <div className="description-2">
                                {vantagens.phrase4}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="itens">
                    <div className="icon">
                        <img src={vantagens.img5} alt="time" />
                    </div>
                    <div className="content">
                        <div className="title-1 size-25 black">
                            {vantagens.title5}
                        </div>
                        <div className="description">
                            <div className="description-2">
                                {vantagens.phrase5}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="itens">
                    <div className="icon">
                        <img src={vantagens.img6} alt="time" />
                    </div>
                    <div className="content">
                        <div className="title-1 size-25 black">
                            {vantagens.title6}
                        </div>
                        <div className="description">
                            <div className="description-2">
                                {vantagens.phrase6}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ReactSlider {...settings}>
                <CardVantagens>
                    <div>
                        <img
                            src="https://images.pexels.com/photos/735277/pexels-photo-735277.jpeg?cs=srgb&dl=balcao-colorido-design-735277.jpg&fm=jpg"
                            alt=""
                        />
                    </div>
                    <div>
                        <span>Qualidade comprovada</span>
                        <small>
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Ducimus molestias.
                        </small>
                    </div>
                </CardVantagens>
                <CardVantagens>
                    <div>
                        <img
                            src="https://images.pexels.com/photos/735277/pexels-photo-735277.jpeg?cs=srgb&dl=balcao-colorido-design-735277.jpg&fm=jpg"
                            alt=""
                        />
                    </div>
                    <div>
                        <span>Qualidade comprovada</span>
                        <small>
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Ducimus molestias.
                        </small>
                    </div>
                </CardVantagens>
                <CardVantagens>
                    <div>
                        <img
                            src="https://images.pexels.com/photos/735277/pexels-photo-735277.jpeg?cs=srgb&dl=balcao-colorido-design-735277.jpg&fm=jpg"
                            alt=""
                        />
                    </div>
                    <div>
                        <span>Qualidade comprovada</span>
                        <small>
                            Lorem ipsum dolor sit amet consectetur adipisicing
                            elit. Ducimus molestias.
                        </small>
                    </div>
                </CardVantagens>
            </ReactSlider>
        </Container>
    );
};

export default Vantagens;
