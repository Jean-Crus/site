import styled from 'styled-components';
import Slider from 'react-slick';

export const Container = styled.div`
    padding: 20px 0;
    div {
        .itens {
            justify-content: initial !important;
        }
    }
    .icon {
        img {
            width: 100%;
            height: 100%;
            max-width: 50px;
            max-height: 50px;
        }
    }
    .blocks {
        @media only screen and (max-width: 999px) {
            display: none !important;
        }
    }
`;

export const ReactSlider = styled(Slider)`
    width: 100%;
    max-width: 1200px;
    margin-bottom: 40px;
    .slick-list {
        margin: 0 30px;
        .slick-slide {
            display: flex;
            justify-content: center;
        }
    }
    .slick-next {
        right: 0;
    }
    .slick-prev {
        left: 0;
    }
    .slick-dots {
        display: flex !important;
        justify-content: center;
        align-items: center;
        li {
            display: flex !important;
            justify-content: center;
            align-items: center;
        }
        .slick-active {
            border-radius: 50%;
            border: 2px solid ${props => props.theme.primary};
        }
    }
    .dots {
        display: flex;
        justify-content: center;
        align-items: center;
        .circle {
            width: 4px;
            height: 4px;
            border-radius: 20px;
            background-color: ${props => props.theme.letter2};
        }
    }
    @media only screen and (min-width: 1000px) {
        display: none !important;
    }
`;

export const CardVantagens = styled.div`
    width: 216px !important;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    img {
        height: 48px;
        width: 50px;
        margin-bottom: 10px;
    }
    div {
        padding-top: 10px;
        display: flex;
        flex-direction: column;
        align-items: center;
        span {
            color: ${props => props.theme.background};
            font-family: Montserrat;
            font-size: 14px;
            font-weight: bold;
            text-align: center;
        }
        small {
            font-family: Montserrat;
            color: ${props => props.theme.letter2};
            font-weight: 200;
            text-align: center;
            font-size: 12px;
        }
    }
`;
