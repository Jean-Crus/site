import styled from "styled-components";
import Slider from "react-slick";
import { Grid } from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";

export const ReactSlider = styled(Slider)`
    user-select: ${props => (props.readOnly ? "none" : "auto")} !important;
    ul {
        display: flex !important;
    }
    svg {
        color: ${props => props.slidearrow} !important;
    }
    margin-bottom: ${props => (props.readOnly ? "" : "100px")};

    .slick-track,
    .slick-slider .slick-list {
        margin-bottom: ${props => (props.readOnly ? "" : "100px")};
    }

    .slick-dots {
        display: flex;
        justify-content: space-evenly;
        width: 100%;
        li {
            display: flex;
            opacity: 0.2;
            max-width: 280px;
            width: 100%;
            .dots {
                display: flex;
                flex-direction: column;
                align-items: flex-start;
                width: 100%;
                height: 68px;
                .progress {
                    height: 2px;
                    width: 100%;
                    border-radius: 1px;
                    background: ${props =>
                        `linear-gradient(0deg, ${props.dotsprogress} 0%, ${props.dotsprogress} 100%)`};
                    box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.5);
                }
            }
            .text-progress {
                display: flex;
                flex-direction: column;
                align-items: flex-start;
                padding-top: 14px;
                span {
                    font-family: Montserrat, sans-serif;
                    color: ${props => props.dotstitle};
                    font-weight: bold;
                    font-size: 18px;
                }
                small {
                    color: ${props => props.dotssub};
                    font-family: Montserrat, sans-serif;
                    font-size: 12px;
                }
            }
        }
        .slick-active {
            opacity: 0.8;
        }
    }

    .slick-slide {
        > div > div {
            outline: none !important;
        }
    }

    .slick-list {
        margin-right: 45px;
        margin-left: 30px;
    }
    .slick-next {
        right: 0;
    }
    .slick-prev {
        left: 0;
    }
`;

export const Container = styled(Grid)`
    width: 100%;
    height: 400px;
    > div {
        span {
            font-family: Montserrat;
            color: #fff;
        }
    }

    .ql-font-Montserrat {
        font-family: Montserrat;
    }
    .ql-font-Lato {
        font-family: Lato;
    }
    .ql-font-Roboto {
        font-family: Roboto;
    }
    .ql-font-Playfair {
        font-family: Playfair;
    }
    .ql-font-Lobster {
        font-family: Lobster;
    }

    .ql-bubble {
        .ql-font {
            span[data-value="Lobster"] {
                &::before {
                    content: "Lobster";
                }
            }
            span[data-value="Lato"] {
                &::before {
                    content: "Lato";
                }
            }
            span[data-value="Playfair"] {
                &::before {
                    content: "Playfair";
                }
            }
            span[data-value="Roboto"] {
                &::before {
                    content: "Roboto";
                }
            }
            span[data-value="Montserrat"] {
                &::before {
                    content: "Montserrat";
                }
            }
        }
        .ql-picker-options {
            .ql-picker-item {
                padding: 0;
            }
        }
        .ql-header {
            width: 100px !important;
        }
        .ql-picker-label {
            &::before {
                padding-right: 20px;
            }
        }
        .ql-tooltip {
            z-index: 10;
            width: 508px !important;
            text-align: center;
            left: 20% !important;
        }
        .ql-editor {
            h1 {
                font-size: 3rem;
            }
            h2 {
                font-size: 2.4rem;
            }
            h3 {
                font-size: 2rem;
            }
            h4 {
                font-size: 1.6rem;
            }
            h5 {
                font-size: 1.2rem;
            }
        }
    }
    .title {
        color: ${props => props.lettercolortitle};
    }
    .subtitle {
        color: ${props => props.lettercolorsubtitle};
    }

    img {
        border-radius: 5px;
        width: 100%;
        max-width: 700px;
        max-height: 400px;
    }
`;

export const ArrowBack = styled(ArrowBackIosIcon)`
    opacity: 0.5;
    &:hover {
        opacity: 1;
    }
`;

export const ArrowForward = styled(ArrowForwardIosIcon)`
    opacity: 0.5;
    &:hover {
        opacity: 1;
    }
`;
