import React from "react";
import { useSelector, useDispatch } from "react-redux";
import * as AppActions from "@store/modules/app/actions";
import history from "@routes/history";

import { convertText } from "@util/tools";
import { Grid } from "@material-ui/core";
import { ButtonTriade, QuillEditor } from "@components";
import { Container, ReactSlider, ArrowBack, ArrowForward } from "./styles";

const CarrouselOne = ({ readOnly }) => {
    const dispatch = useDispatch();
    const appTheme = useSelector(state => state.app.themeHome1);
    function SampleNextArrow(props) {
        const { className, onClick } = props;
        return (
            <ArrowForward
                className={className}
                onClick={onClick}
                fontSize="large"
            />
        );
    }

    function SamplePrevArrow(props) {
        const { className, onClick } = props;
        return (
            <ArrowBack
                className={className}
                onClick={onClick}
                fontSize="large"
            />
        );
    }

    const settings = {
        customPaging(i) {
            return (
                <div className="dots">
                    <div className="progress" />
                    <div className="text-progress">
                        <div>
                            <span>
                                {convertText(
                                    appTheme.slides[i].titleHighlight,
                                    24
                                )}
                            </span>
                        </div>
                        <div>
                            <small>
                                {convertText(
                                    appTheme.slides[i].subtitleHighlight,
                                    40
                                )}
                            </small>
                        </div>
                    </div>
                </div>
            );
        },
        dots: true,
        // fade: true,
        swipeToSlide: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        swipe: readOnly,
        draggable: readOnly,
        accessibility: readOnly,
        autoplay: readOnly,
        autoplaySpeed: 3000,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />
    };

    const handleText = (event, campo, id, text) => {
        dispatch(AppActions.changeTextHome1(event, campo, id, text));
    };

    return (
        <ReactSlider
            {...settings}
            dotsprogress={appTheme.dotsProgress.colorOne}
            dotstitle={appTheme.dotsTitle.colorOne}
            dotssub={appTheme.dotsSub.colorOne}
            slidearrow={appTheme.slideArrow.colorOne}
            readOnly={readOnly}
        >
            {appTheme.slides.map(slide => {
                return (
                    <div key={slide.id}>
                        <Container
                            container
                            lettercolortitle={appTheme.titleColor.colorOne}
                            fonttitle={appTheme.fontTitle.colorOne}
                            lettercolorsubtitle={
                                appTheme.subtitleColor.colorOne
                            }
                            fontsubtitle={appTheme.fontSubtitle.colorOne}
                            justify="flex-end"
                            style={{ position: "relative" }}
                            readOnly={readOnly}
                        >
                            <div style={{ position: "absolute", left: 0 }}>
                                <QuillEditor
                                    theme="bubble"
                                    value={slide.title}
                                    className="title"
                                    valueHighlight={slide.titleHighlight}
                                    campo="title"
                                    id={slide.id}
                                    readOnly={readOnly}
                                    onChange={handleText}
                                />
                                <QuillEditor
                                    className="subtitle"
                                    theme="bubble"
                                    value={slide.subtitle}
                                    campo="subtitle"
                                    valueHighlight={slide.subtitleHighlight}
                                    id={slide.id}
                                    readOnly={readOnly}
                                    onChange={handleText}
                                />
                                <ButtonTriade
                                    text={slide.button.title}
                                    button={slide.button.cor}
                                    type="button"
                                    letterColor={slide.button.letterColor}
                                    readOnly={readOnly}
                                    onClick={() => history.push("/cursos")}
                                />
                            </div>
                            <Grid item lg={6}>
                                <img src={slide.img} alt="carrousel1" />
                            </Grid>
                        </Container>
                    </div>
                );
            })}
        </ReactSlider>
    );
};

export default CarrouselOne;
