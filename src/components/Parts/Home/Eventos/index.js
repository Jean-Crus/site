import React from 'react';
import { Link } from 'react-router-dom';
import { curso1, curso2, curso3 } from '@images';

const style = {
    curso1: {
        backgroundImage: "url('" + curso1 + "')",
    },
    curso2: {
        backgroundImage: "url('" + curso2 + "')",
    },
    curso3: {
        backgroundImage: "url('" + curso3 + "')",
    }
};

const Eventos = () => {

	return(
        <div className="container event">
            <div className="title-1 size-50 black wh-600">Eventos</div>    
            <div className="content-card">
                <div className="tr-card-event-flat">
                    <div className="date">
                        <div className="date-number">
                            <p>12</p>
                            <b>DEZ</b>
                        </div>
                    </div>
                    <div className="information">
                        <div className="content">
                            <Link to="/" className="title-2 size-50 black">
                                Workshop Lorem Ipsum
                            </Link>
                            <div className="local">
                                <div className="hours description-3"> 9h -10h </div>
                                <div className="type-event description-3 local">Centro de Convenções</div>
                            </div>
                            <div className="description">
                                <div className="description-2">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales magna pus semper  lorem..
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="image-full">
                        <span className="image" style={style.curso1}></span>
                    </div>
                </div>
                <div className="tr-card-event-flat">
                    <div className="date">
                        <div className="date-number">
                            <p>07</p>
                            <b>jan</b>
                        </div>
                    </div>
                    <div className="information">
                        <div className="content">
                            <Link to="/" className="title-2 size-50 black">
                                Evento Lorem Ipsum
                            </Link>
                            <div className="local">
                                <div className="hours description-3"> 9h -10h </div>
                                <div className="type-event description-3 online">Evento Online</div>
                            </div>
                            <div className="description">
                                <div className="description-2">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales magna pus semper  lorem..
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="image-full">
                        <span className="image" style={style.curso2}></span>
                    </div>
                </div>
                <div className="tr-card-event-flat border-none">
                    <div className="date">
                        <div className="date-number">
                            <p>26</p>
                            <b>out</b>
                        </div>
                    </div>
                    <div className="information">
                        <div className="content">
                            <Link to="/" className="title-2 size-50 black">
                                Workshop Lorem Ipsum
                            </Link>
                            <div className="local">
                                <div className="hours description-3"> 9h -10h </div>
                                <div className="type-event description-3 local">Centro de Convenções</div>
                            </div>
                            <div className="description">
                                <div className="description-2">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales magna pus semper  lorem..
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="image-full">
                        <span className="image" style={style.curso3}></span>
                    </div>
                </div>
            </div>
        </div>
	);
};

export default Eventos;