import styled from "styled-components";
import { Form, Field, ErrorMessage } from "formik";

export const Container = styled.div`
    margin-top: 40px;
    padding: 40px;
    display: flex;
    background: linear-gradient(0deg, #012a50 0%, #0058a8 100%);
    align-items: center;
    flex-direction: column;
    .container-img {
        max-width: 600px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        img {
            margin: 40px;
            width: 100%;
            max-height: 300px;
        }
        .description-1 {
            text-align: center;
        }
    }
    @media only screen and (min-width: 980px) {
        display: none;
    }
`;

export const TextInput = styled(Field)`
    ::placeholder {
        font-family: "Montserrat, sans-serif";
    }
    background-color: transparent;
    width: 100%;
    max-width: 500px;
    padding-left: 10px;
    height: 45px;
    display: flex;
    border: ${props => `1px solid ${props.theme.primary}`};
    border-radius: 5px;
    align-items: center;
    color: #fff;
    margin-top: 10px;
    &:focus {
        outline: none;
    }
`;
export const ErrorMsg = styled(ErrorMessage)`
    color: #ff0000;
    font-size: 12px;
`;
export const FormField = styled(Form)`
    margin-top: 30px;
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    button {
        margin-top: 10px;
        width: 300px;
    }
`;
