import React, { useState } from "react";
import * as Yup from "yup";
import InputMask from "react-input-mask";
import { Formik } from "formik";
import { ButtonTriade } from "@components";
import { Container, TextInput, FormField, ErrorMsg } from "./styles";

// const initialValues = {
//     name: "",
//     email: "",
//     tel: "",
//     mensagem: ""
// };

// const schema = Yup.object().shape({
//     name: Yup.string().required("Nome obrigatório*"),
//     email: Yup.string()
//         .email("Insira um e-mail válido")
//         .required("E-mail obrigatório"),
//     tel: Yup.string()
//         .matches(
//             /^\([0-9]{2}\)[0-9]?[0-9]{4}-[0-9]{4}$/,
//             "Telefone inválido. Ex: (xx)xxxxx-xxxx"
//         )
//         .required("Número de telefone celular obrigatório")
// });

const FaleConoscoMobile = () => {
    return (
        <Container>
            <div className="title-1 size-50 wh-600">Fale Conosco</div>
            <Formik
                // initialValues={initialValues}
                // enableReinitialize
                // validationSchema={schema}
                className="tr-form"
                onSubmit={({ name, email, tel, password, cpf }) => {}}
            >
                {({
                    handleBlur,
                    handleChange,
                    handleSubmit,
                    values,
                    errors,
                    touched,
                    validateField
                }) => {
                    return (
                        <FormField onSubmit={handleSubmit}>
                            {/* <TextInput
                                placeholder="Nome"
                                type="text"
                                name="name"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.name}
                            />
                            <ErrorMsg name="name" component="div" />

                            <TextInput
                                placeholder="E-mail*"
                                type="text"
                                name="email"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.email}
                            />
                            <ErrorMsg name="email" component="div" />

                            <InputMask
                                mask="(99)99999-9999"
                                maskChar=" "
                                placeholder="Telefone"
                                name="tel"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.tel}
                            >
                                {props => <TextInput {...props} />}
                            </InputMask>
                            <ErrorMsg name="tel" component="div" />

                            <TextInput
                                placeholder="Mensagem"
                                type="text"
                                name="mensagem"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.mensagem}
                            />
                            <ErrorMsg name="mensagem" component="div" /> */}
                            <a
                                href="https://api.whatsapp.com/send?phone=554199692129"
                                rel="noopener noreferrer"
                                target="_blank"
                            >
                                <ButtonTriade
                                    button="whatsapp"
                                    icon="fab fa-whatsapp"
                                    letterColor="#fff"
                                    type="button"
                                    cor="whatsapp"
                                    className="mt-5"
                                    text="Fale conosco pelo WhatsApp"
                                    // onClick={handleOpen}
                                />
                            </a>
                            {/* <ButtonTriade
                                type="submit"
                                button="secondary"
                                text="Enviar"
                                style={{ height: "50px" }}
                            /> */}
                        </FormField>
                    );
                }}
            </Formik>
        </Container>
    );
};

export default FaleConoscoMobile;
