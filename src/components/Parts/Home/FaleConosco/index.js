import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as AppActions from "@store/modules/app/actions";
import { Modal, Grid } from "@material-ui/core";
import curso2 from "@images/faleConosco.svg";
import { ButtonTriade, FullScreenDialog } from "@components";
import { Container } from "./styles";

const style = {
    curso2: {
        backgroundImage: `url('${curso2}')`
    }
};

const FaleConosco = () => {
    const [open, setOpen] = useState(false);
    const handleOpen = () => {
        setOpen(!open);
    };

    return (
        <Container className="container fale-conosco">
            <div className="content">
                <div className="information">
                    <div className="image-full">
                        <span className="image" style={style.curso2} />
                    </div>
                    <div className="title">
                        <div className="title-1 mt-3">
                            Tire suas dúvidas aqui
                        </div>
                        {/* <div className="title-2">Lorem ipsum</div> */}
                    </div>
                    <div className="description">
                        {/* <div className="description-1">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. sodales magna purus, semper malesuada mi
                            condimentum vitae utito tempo.
                        </div> */}
                        <div className="form-group ">
                            <a
                                href="#"
                                rel="noopener noreferrer"
                                target="_blank"
                            >
                                <ButtonTriade
                                    button="whatsapp"
                                    icon="fab fa-whatsapp"
                                    letterColor="#fff"
                                    type="button"
                                    cor="whatsapp"
                                    className="mt-5"
                                    text="Fale conosco pelo WhatsApp"
                                    // onClick={handleOpen}
                                />
                            </a>
                        </div>
                    </div>
                </div>
                {/* <FullScreenDialog open={open} handleClose={handleOpen} /> */}
            </div>
        </Container>
    );
};

export default FaleConosco;
