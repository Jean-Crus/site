import styled from "styled-components";

export const Container = styled.div`
    width: 60% !important;
    height: 100% !important;
    .information {
        display: flex;
        padding: 0 !important;
        height: 100%;
        width: 100%;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        .title {
            text-align: center;
        }
    }
    background-color: transparent !important;
    background: ${props =>
        `linear-gradient(0deg, #012a50 0%, #0058a8 100%)`} !important;

    .title-1,
    .title-2 {
        color: ${props => props.theme.letter} !important;
    }
    .title {
        margin-top: 0 !important;
    }
    .description-1 {
        color: ${props => props.theme.letter2} !important;
    }
    .form-group {
        button {
            width: 300px;
        }
    }
    @media only screen and (min-width: 992px) {
        max-width: none !important;
    }
    @media only screen and (max-width: 768px) {
        max-width: none !important;
        width: 100% !important;
    }

    @media only screen and (min-width: 576px) {
        max-width: none !important;
    }
`;
