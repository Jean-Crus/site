import styled from "styled-components";
import Slider from "react-slick";

export const Box = styled.div`
    width: 100%;
    height: 100%;
    min-height: 700px;
    margin-bottom: 40px;
    background: linear-gradient(0deg, #012a50 0%, #0058a8 100%);

    position: relative;
    @media only screen and (min-width: 1201px) {
        display: none;
    }
`;

export const ReactSlider = styled(Slider)`
    ul {
        display: flex !important;
    }
    .slick-dots {
        display: flex;
        justify-content: center;
        width: 100%;
        li {
            display: flex;
            opacity: 0.2;
            .dots {
                display: flex;
                flex-direction: column;
                align-items: flex-start;
                width: 100%;
                height: 68px;
                .progress {
                    height: 2px;
                    width: 100%;
                    border-radius: 1px;
                    background-color: #ffffff;
                    box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.5);
                }
            }
        }
        .slick-active {
            opacity: 0.8;
        }
    }
`;

export const Container = styled.div`
    position: relative;
    .button-div {
        display: flex;
        margin: 60px 0 40px 0;
        width: 100%;
        justify-content: center;
    }
    .img-carousel {
        margin-top: 10px;
        position: relative;
        display: flex;
        width: 100%;
        height: 460px;
        img {
            position: relative;
            width: 100%;
            height: 100%;
            max-height: 400px;
        }
        div {
            width: 100%;
            display: flex;
            align-items: center;
            flex-direction: column;
            position: absolute;
            text-align: center;
            top: 370px;
            h1 {
                font-family: Montserrat, sans-serif;
                color: ${props => props.theme.letter};
                font-weight: bold;
                font-size: 50px;
                line-height: 50px;
                margin: 0;
            }
            span {
                font-family: Montserrat, sans-serif;
                color: ${props => props.theme.letter};
                font-size: 30px;
                font-weight: 300;
            }
        }
    }
    @media only screen and (max-width: 712px) {
        .button-div {
            margin: 130px 0 40px 0;
        }
        .img-carousel {
            height: 420px;
            div {
                top: 389px;
                h1 {
                    font-size: 30px;
                    line-height: 25px;
                }
                span {
                    font-family: Montserrat, sans-serif;
                    color: ${props => props.theme.letter};
                    font-size: 15px;
                    line-height: 30px;
                    font-weight: 300;
                }
            }
        }
    }
    @media only screen and (max-width: 578px) {
        .button-div {
            margin: 120px 0 40px 0;
        }
        .img-carousel {
            div {
                top: 383px;
                h1 {
                    font-size: 36px;
                    line-height: 32px;
                    padding-bottom: 10px;
                }
                span {
                    font-size: 22px;
                    line-height: 20px;
                }
            }
        }
    }
    @media only screen and (max-width: 368px) {
        .img-carousel {
            div {
                top: 387px;
                h1 {
                    font-size: 30px;
                    line-height: 26px;
                }
                span {
                    font-size: 18px;
                    line-height: 18px;
                }
            }
        }
    }
`;

export const IconCircle = styled.div`
    display: flex;
    justify-content: center;
    position: absolute;
    bottom: -30px;
    left: 0;
    right: 0;
    margin-left: auto;
    margin-right: auto;
    align-items: center;
    height: 67px;
    width: 67px;
    border-radius: 67px;
    background-color: ${props => props.theme.primary};
    box-shadow: 0 10px 30px 0 rgba(0, 0, 0, 0.2);
`;

export const Arrow1 = styled.div`
    position: absolute;
    top: 30%;
    height: 12px;
    width: 12px;
    border: ${props => `solid ${props.theme.letter}`};
    border-width: 0 3px 3px 0;
    opacity: 0.5;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
`;

export const Arrow2 = styled.div`
    position: absolute;
    top: 50%;
    height: 12px;
    width: 12px;
    border: ${props => `solid ${props.theme.letter}`};
    border-width: 0 3px 3px 0;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
`;
