import React from "react";
import { useSelector } from "react-redux";
import history from "@routes/history";
import { ButtonTriade } from "@components";
import {
    Container,
    ReactSlider,
    Box,
    IconCircle,
    Arrow1,
    Arrow2
} from "./styles";

const settings = {
    customPaging() {
        return <div className="dots" />;
    },
    arrows: false,
    dots: true,
    // fade: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
    // autoplay: true,
    // autoplaySpeed: 3000,
};

const CarouselTwo = () => {
    const themeHome1 = useSelector(state => state.app.themeHome1);
    return (
        <Box>
            <ReactSlider {...settings}>
                {themeHome1.slides.map(slide => {
                    return (
                        <Container key={slide.id}>
                            <div className="img-carousel" alt="carrousel1">
                                <img src={slide.img} alt="" />
                                <div>
                                    <h1>{slide.titleHighlight}</h1>
                                    <span>{slide.subtitleHighlight}</span>
                                </div>
                            </div>

                            <div className="button-div">
                                <ButtonTriade
                                    text={slide.button.title}
                                    button="Modelo 02"
                                    className="mt-4"
                                    letterColor="Modelo 02"
                                    cor="Modelo 02"
                                    onClick={() => history.push("/cursos")}
                                />
                            </div>
                        </Container>
                    );
                })}
            </ReactSlider>
            <IconCircle>
                <Arrow1 />
                <Arrow2 />
            </IconCircle>
        </Box>
    );
};

export default CarouselTwo;
