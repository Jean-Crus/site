import React from 'react';
import { Link } from 'react-router-dom';

const ModalListaEspera = () => {

	return(
        <div className="modal fade" id="listadeesperacurso" tabindex="-1" role="dialog" aria-labelledby="listadeesperacurso" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered d-flex justify-content-center align-items-center" role="document">
                <div className="modal-content">
                    <div className="tr-card-wait-list mt-5">
                        <div className="wait-description">
                            <div className="title-1 black size-25">
                                Curso de <br /> Lorem Ipsum Avançado
                            </div>
                        </div>
                        <div className="form-wait">
                            <form action="" className="tr-form">
                                <div className="form-group">
                                    <input type="text" className="form-control"  placeholder="Nome" value="Matheus" />
                                </div>
                                <div className="form-group">
                                    <input type="e-mail" className="form-control" value="mattheus" placeholder="E-mail" pattern="^((?!mattheus).)*$" />
                                    <small className="error">Email inválido</small>
                                </div>
                                <div className="form-group">
                                    <input type="number" className="form-control" placeholder="Telefone" />
                                </div>
                                <a data-toggle="modal" href="#" onclick="event.preventDefault();" id="listadeesperafinal" className="btn tr-primary small">Me coloque na lista de espera</a>
                            </form>
                        </div>
                    </div>
                    <div className="tr-card-sucess mt-5">
                        <div className="image-sucess">
                            <span className="file-image"></span>
                            <div className="title-1 black size-25">Obrigado, Raul</div>
                        </div>
                        <div className="wait-list">
                            <div className="description-2 black">
                                Agora, você está na lista de espera do Curso de Lorem ipsum Avançado
                            </div>
                        </div>
                        <div className="closed">
                            <div className="buttons">
                                <Link to="#" className="btn tr-primary">Ok </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	);
};

export default ModalListaEspera;
