import React from 'react';
import { Link } from 'react-router-dom';

const FiltroCursosSelected = () => {

	return(
        <div className="selected container">
            <div className="item-selected">
                <div className="content">
                    <div className="description-2">
                        Desenvolvimento
                        <span className="count">10</span>
                    </div>
                    <Link to="#" className="exit">
                        <i className="fas fa-times"></i>
                    </Link>
                </div>
            </div>
            <div className="item-selected">
                <div className="content">
                    <div className="description-2">
                        Até R$ 300
                        <span className="count">10</span>
                    </div>
                    <Link to="#" className="exit">
                        <i className="fas fa-times"></i>
                    </Link>
                </div>
            </div>
        </div>
	);
};

export default FiltroCursosSelected;
