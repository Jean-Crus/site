import React from 'react';
import { Link } from 'react-router-dom';
import { curso1, curso2, curso3 } from '@images';
import { CardCurso } from '@components';

const ListaCards = () => {

	return(
        <div className="content container">
            <div className="row d-flex justify-content-center">
                <div className="col-lg-4">
                    <CardCurso
                        image={curso1}
                        title="Curso de Lorem Ipsum Avançado"
                        category="Desenvolvimento"
                        colorCategory="red"
                        difficulty="Avançado"
                        closed={true}
                        price="250,00"
                        salePrice="99,00"
                        hours="8"
                        modules="6"
                        description="“Today Web UI Kit is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been."
                    />
                </div>
                <div className="col-lg-4">
                    <CardCurso
                        image={curso2}
                        title="Curso de Lorem Ipsum Avançado"
                        category="Design"
                        colorCategory="purple"
                        difficulty="Avançado"
                        closed={false}
                        price="250,00"
                        salePrice="99,00"
                        hours="8"
                        modules="6"
                        description="“Today Web UI Kit is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been."
                    />
                </div>
                <div className="col-lg-4">
                    <CardCurso
                        image={curso1}
                        title="Curso de Lorem Ipsum Avançado"
                        category="Design"
                        colorCategory="purple"
                        difficulty="Avançado"
                        closed={false}
                        price="250,00"
                        salePrice="99,00"
                        hours="8"
                        modules="6"
                        description="“Today Web UI Kit is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been."
                    />
                </div>
                <div className="col-lg-4">
                    <CardCurso
                        image={curso3}
                        title="Curso de Lorem Ipsum Avançado"
                        category="Design"
                        colorCategory="yellow"
                        difficulty="Avançado"
                        closed={false}
                        price="250,00"
                        salePrice="99,00"
                        hours="8"
                        modules="6"
                        description="“Today Web UI Kit is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been."
                    />
                </div>
                <div className="col-lg-4">

                    <CardCurso
                        image={curso3}
                        title="Curso de Lorem Ipsum Avançado"
                        category="Design"
                        colorCategory="green"
                        difficulty="Avançado"
                        closed={false}
                        price="250,00"
                        salePrice="99,00"
                        hours="8"
                        modules="6"
                        description="“Today Web UI Kit is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been."
                    />

                </div>
            </div>
        </div>
	);
};

export default ListaCards;
