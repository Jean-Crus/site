import React from 'react';

const FiltroCursos = () => {
    return (
        <div className="content container">
            <div className="filter-1 ">
                <button className="tr-multiSelect">
                    <div className="select">Filtros</div>
                    <ul>
                        <li className="subcategory">
                            Categorias
                            <ul>
                                <li>Arquitetura</li>
                                <li>Design</li>
                                <li>Desenvolvimento</li>
                                <li>Health</li>
                                <li>Marketing</li>
                            </ul>
                        </li>
                        <li>Nivel</li>
                        <li>Duração</li>
                        <li>Preço</li>
                    </ul>
                </button>
            </div>
            <div className="filter-2">
                <form>
                    <div className="form-row align-items-center">
                        <label className="sr-only" htmlFor="inlineFormInput">
                            pesquise
                        </label>
                        <input
                            type="text"
                            className="form-control"
                            id="pesquisa_cursos"
                            placeholder="Pesquise por um curso aqui"
                        />
                        <button type="submit">
                            <i className="fas fa-search" />
                        </button>
                    </div>
                </form>
            </div>
            <div className="filter-3">
                <div className="selected">
                    <select className="selectpicker">
                        <option selected>Qual seu interesse?</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
            <div className="filter-4">
                <div className="selected">
                    <select className="selectpicker">
                        <option selected>Qual seu interesse?</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
        </div>
    );
};

export default FiltroCursos;
