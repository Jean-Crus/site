import React from 'react';
import { IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { FilterTag } from './styles';

const FilterTagg = ({ priceTag, priceMin, priceMax, valor, onClick }) => {
    return (
        <FilterTag title="Excluir categoria" onClick={onClick}>
            <IconButton size="small">
                {priceTag ? `R$ ${priceMin} a R$ ${priceMax}` : valor}
                <CloseIcon />
            </IconButton>
        </FilterTag>
    );
};
export default FilterTagg;
