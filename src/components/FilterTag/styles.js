import styled from 'styled-components';
import { Tooltip } from '@material-ui/core';
import { lighten } from 'polished';

export const FilterTag = styled(Tooltip)`
    &:hover {
        background-color: ${props =>
            lighten(0.01, `${props.theme.primary}`)} !important;
        color: #fff !important;
    }
    font-family: Montserrat !important;
    font-size: 12px !important;
    font-weight: 400;
    display: flex !important;
    justify-content: space-between !important;
    align-items: center !important;
    border: ${props => `1px solid ${props.theme.primary}`} !important;
    color: ${props => props.theme.primary} !important;
    background-color: #ffffff !important;
    padding: 4px 15px !important;
    border-radius: 30px !important;
    margin-bottom: 5px !important;
    margin-right: 10px !important;
    svg {
        margin-left: 10px;
    }
    .MuiSvgIcon-root {
        font-size: 14px !important;
    }
`;
