import React from "react";

const NewsletterPurple = ({ content }, state) => {
  return (
    <div
      id="newsletter-purple"
      style={
        content.background
          ? { backgroundImage: `url('${content.background}')` }
          : {}
      }
    >
      <div className="container">
        <div className="row align-items-center container d-flex justify-content-between">
          <div className="col-sm-6">
            <h4>
              {content.title}
              <span>{content.subTitle}</span>
            </h4>
          </div>
          <div className="col-sm-6">
            <form action="">
              <input
                type="text"
                name="teste2"
                id="teste2"
                placeholder="E-mail"
              />
              <button>
                <span className="sr-only">Enviar</span>
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewsletterPurple;
