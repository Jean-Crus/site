import React from 'react';
import { Container } from './styles';

const FilterComponentConfig = ({ edit }) => {
    const components = {
        homeFirst: Container,
        lista: 'BarComponent',
    };
    const EditComp = components[edit || 'homeFirst'];
    return <EditComp>teste</EditComp>;
};

export default FilterComponentConfig;
