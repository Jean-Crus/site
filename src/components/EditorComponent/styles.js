import styled from 'styled-components';

export const Box = styled.div``;

export const Container = styled.div`
    position: relative;
    background: ${props =>
        `linear-gradient(0deg, ${props.background.colorOne} 0%, ${props.background.colorTwo} 100%)`} !important;
    .box {
        position: absolute;
        width: 400px;
        top: 0px;
        right: 0px;
        z-index: 10;
        /* background-color: red; */
        display: none;
        flex-direction: column;
        align-items: flex-end;
    }
    &:hover {
        .box {
            display: ${props => (props.readOnly ? 'none' : 'flex')};
        }
        .quill {
            border: ${props =>
                props.readOnly ? 'none' : `1px solid ${props.theme.primary}`};
            border-radius: 5px;
            &:hover {
                border: ${props =>
                    props.readOnly
                        ? 'none'
                        : `1px solid ${props.theme.primary}`};
                border-radius: 5px;
            }
        }
    }

    .MuiIcon-root {
        color: ${props => props.color};
    }
    .MuiIconButton-root {
        &:hover {
            background-color: ${props => props.iconColor} !important;
        }
    }
`;
