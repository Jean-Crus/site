import React, { useState, useEffect, useRef } from 'react';
import { Tooltip, Icon, IconButton } from '@material-ui/core';
import { Box, Container, BoxEditor } from './styles';
import { EditorContent } from '@components';

const EditorConfig = ({
    children,
    edit,
    color,
    iconColor,
    readOnly,
    background,
}) => {
    const [open, setOpen] = useState(false);

    const handleClick = () => {
        setOpen(!open);
    };

    function useOutsideAlerter(ref) {
        /**
         * Alert if clicked on outside of element
         */
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target) && open) {
                setOpen(!open);
            }
        }

        useEffect(() => {
            // Bind the event listener
            document.addEventListener('mousedown', handleClickOutside);
            return () => {
                // Unbind the event listener on clean up
                document.removeEventListener('mousedown', handleClickOutside);
            };
        });
    }
    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef);

    return (
        <Container
            color={color}
            iconColor={iconColor}
            background={background}
            readOnly={readOnly}
        >
            <div className="box">
                <div>
                    <Tooltip title="Editar">
                        <IconButton onClick={handleClick}>
                            <Icon className="fas fa-cog" />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Excluir">
                        <IconButton>
                            <Icon className="fas fa-trash" />
                        </IconButton>
                    </Tooltip>
                </div>
                <EditorContent edit={edit} close={wrapperRef} open={open} />
            </div>
            {children}
        </Container>
    );
};

export default EditorConfig;
