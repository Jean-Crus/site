import React from "react";

const EventosDate = ({ content }, state) => {
  return (
    <div id="eventos-date">
      <h2>{content.title}</h2>
      <div>
        {content.itens.map(item => (
          <div key={Math.random()} className="eventos-date-item">
            <div className="date">
              <strong>{item.day}</strong>
              <span>{item.month}</span>
            </div>
            <div className="content">
              <h3>
                <a href="#">{item.title}</a>
              </h3>
              <p>{item.description}</p>
            </div>
            <div className="image">
              <a
                href="#"
                style={
                  item.image ? { backgroundImage: `url('${item.image}')` } : {}
                }
              >
                <span className="sr-only">{item.title}</span>
              </a>
            </div>
          </div>
        ))}
      </div>
      <div className="plus-item">
        <button>
          <span className="sr-only">Adicionar Evento</span>
        </button>
      </div>
    </div>
  );
};

export default EventosDate;
