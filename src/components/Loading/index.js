import React from 'react';
import { Grid } from '@material-ui/core';
import { Spinner } from 'react-bootstrap';

const Loading = () => (
    <Grid container justify="center" className="mt-4 mb-4">
        <Spinner animation="border" role="status" variant="dark" />
    </Grid>
);

export default Loading;
