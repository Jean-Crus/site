export { default as Header } from "./Header";
export { default as Footer } from "./Footer";
export { default as HeaderConta } from "./HeaderConta";
export { default as HeaderMobileConta } from "./HeaderConta/HeaderMobileConta";

export { default as BannerPage } from "./BannerPage";
export { default as Pagination } from "./Pagination";
export { default as Newsletter } from "./Newsletter";
export { default as CursosDestaque } from "./CursosDestaque";
export { default as ButtonTriade } from "./ButtonTriade";
export { default as SelectMult } from "./SelectMult";
export { default as SelectDefault } from "./SelectDefault";
export { default as TextDefault } from "./TextDefault";

export { default as SearchDefault } from "./SearchDefault";
export { default as RangeFilterDefault } from "./RangeFilterDefault";

export { default as ModalFilter } from "./ModalFilter";

export { default as SidebarAccount } from "./SidebarAccount";
export { default as HeaderType } from "./HeaderType";

export { default as CardCurso } from "./Cards/CardCurso";
export { default as CardTerm } from "./Cards/CardTerm";
export { default as CardSuccess } from "./Cards/CardSuccess";
export { default as CardWait } from "./Cards/CardWait";
export { default as CardProduct } from "./Cards/CardProduct";
export { default as CardInstrutor } from "./Cards/CardInstrutor";

export { default as CarrouselOne } from "./Parts/Home/CarrouselOne";
export { default as SobreCurso } from "./Parts/Home/SobreCurso";
export { default as SobreCursoWhite } from "./Parts/Home/SobreCurso/SobreCursoWhite";
export { default as Eventos } from "./Parts/Home/Eventos";
export { default as Vantagens } from "./Parts/Home/Vantagens";
export { default as FaleConosco } from "./Parts/Home/FaleConosco";
export { default as FaleConoscoMobile } from "./Parts/Home/FaleConoscoMobile";
export { default as FaleConosco2 } from "./Parts/Home/FaleConosco2";

export { default as FiltroCursos } from "./Parts/Cursos/FiltroCursos";
export { default as FiltroCursosSelected } from "./Parts/Cursos/FiltroCursosSelected";
export { default as ListaCards } from "./Parts/Cursos/ListaCards";
export { default as ModalListaEspera } from "./Parts/Cursos/ModalListaEspera";

export { default as FiltroEventos } from "./Parts/Eventos/FiltroEventos";
export { default as SingleEventos } from "./Parts/Eventos/SingleEventos";
export { default as ListEventos } from "./Parts/Eventos/ListEventos";

export { default as ConteudoEvento } from "./Parts/EventoAberto/ConteudoEvento";
export { default as HeaderEventoAberto } from "./Parts/EventoAberto/HeaderEventoAberto";
export { default as NavigationEvento } from "./Parts/EventoAberto/NavigationEvento";
export { default as SobreEvento } from "./Parts/EventoAberto/SobreEvento";

export { default as CardPay } from "./Parts/Checkout/CardPay";
export { default as BoletoPay } from "./Parts/Checkout/BoletoPay";
export { default as MethodPay } from "./Parts/Checkout/MethodPay";
export { default as HeaderCheckout } from "./Parts/Checkout/HeaderCheckout";

export { default as BonusCurso } from "./Parts/CursoAberto/BonusCurso";
export { default as HeaderCursosAberto } from "./Parts/CursoAberto/HeaderCursosAberto";
export { default as HeaderCursoAbertoMobile } from "./Parts/CursoAberto/HeaderCursoAbertoMobile";
export { default as InstrutorCurso } from "./Parts/CursoAberto/InstrutorCurso";
export { default as ModulosCurso } from "./Parts/CursoAberto/ModulosCurso";
export { default as PerguntasFrequentes } from "./Parts/CursoAberto/PerguntasFrequentes";
export { default as SobreCursoAberto } from "./Parts/CursoAberto/SobreCurso";
export { default as Aula } from "./Parts/CursoAndamento/Aula";

export { default as VantagensCurso } from "./Parts/CursoAberto/VantagensCurso";
export { default as NagivationCourse } from "./Parts/CursoAberto/NagivationCourse";

export { default as HeaderCursoAndamento } from "./Parts/CursoAndamento/HeaderCursoAndamento";
export { default as NagivationAndamento } from "./Parts/CursoAndamento/NagivationAndamento";

export { default as CallCategoria } from "./Parts/ProdutoListagem/CallCategoria";
export { default as CarouselProdutos } from "./Parts/ProdutoListagem/CarouselProdutos";

// Home 2
export { default as Header2 } from "./Header2";
export { default as BannerFull } from "./BannerFull";
export { default as CardAbout } from "./CardAbout";
export { default as Depoimentos } from "./Depoimentos";
export { default as CarrouselFull } from "./CarrouselFull";
export { default as CardProductSimple } from "./CardProductSimple";
export { default as Benefits } from "./Benefits";
export { default as EventosDate } from "./EventosDate";
export { default as ContatoArea } from "./ContatoArea";
export { default as NewsletterPurple } from "./NewsletterPurple";
export { default as LoginMenu } from "./LoginMenu";
export { default as ScriptWrapper } from "./ScriptWrapper";
export { default as ListaCurso } from "./ListaCurso";
export { default as LoggedMenu } from "./LoggedMenu";
export { default as FullScreenDialog } from "./FullScreenDialog";
export { default as DropMenu } from "./DropMenu";
export { default as ModulosCursoDetail } from "./Parts/CursoAndamento/ModulosCursoDetail";
export { default as Drawer } from "./Drawer";
export { default as DrawerConta } from "./DrawerConta";
export { default as CarouselTwo } from "./Parts/Home/CarouselTwo";

export { default as NavFilter } from "./NavFilter";

export { default as TagFilter } from "./TagFilter";

export { default as FilterModal } from "./FilterModal";

export { default as FilterTagg } from "./FilterTag";

export { default as Loading } from "./Loading";

export { default as IconAlert } from "./IconAlert";

export { default as MyDocument } from "./MyDocument";

export { default as TimerPay } from "./TimerPay";

export { default as CourseFinished } from "./CourseFinished";

export { default as CourseFinished2 } from "./CourseFinished2";

export { default as CardMobile } from "./CardMobile";

export { default as DepoNew } from "./DepoimentosNew";

export { default as CheckoutExpired } from "./CheckoutExpired";

export { default as EditorConfig } from "./EditorComponent";

export { default as FilterComponentConfig } from "./FilterComponentConfig";

export { default as EditorContent } from "./EditorContent";

export { default as Calendario } from "./Calendario";

export { default as QuillEditor } from "./QuillEditor";

export { default as SectionColorBack } from "./SectionColorBackground";

export { default as ButtonTemplate } from "./ButtonTemplate";

export { default as InsertImage } from "./InsertImage";

export { default as CheckEdit } from "./CheckEdit";

export { default as NoResults } from "./NoResults";
