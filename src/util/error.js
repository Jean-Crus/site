import history from "@routes/history";
import { put } from "redux-saga/effects";
import { logoutRequest } from "@store/modules/auth/actions";
import { checkoutFailure } from "@store/modules/checkout/actions";
import { toast } from "react-toastify";
import { signFailure } from "../store/modules/auth/actions";

export function* newError(error, id = "create-account") {
    const { data } = error;
    if (id === "card") {
        data.code = error.status;
    }
    switch (data.code) {
        case 30400:
        case 53015:
            yield data.message.map(erro => toast.error(erro));
            break;
        case "payment-failed":
            yield put(checkoutFailure());
            yield toast.error(data.message);
            break;
        case 401:
            if (data.error === "payment-failed") {
                yield toast.error(data.message);
                yield put(checkoutFailure());
                break;
            }

            if (data.message === "Token has expired") {
                yield put(logoutRequest());
                yield toast.error("Token expirado, refaça seu login!");
                yield history.push("/login");
                break;
            }
            if (id === "card") {
                const { code, ...rest } = data;
                Object.values(rest).map(msg => {
                    return msg.map(e => {
                        return toast.error(e);
                    });
                });
                yield put(checkoutFailure());
                break;
            }
            // yield put(logoutRequest());
            toast.error(data.message);
            history.push("/login");
            break;
        case 403:
            toast.error(data.message);
            history.push(`/checkoutExpired/${id}`);
            break;
        case 404:
            history.push("/404");
            break;
        case 422:
            if (data.errors) {
                Object.values(data.errors).map(errors => {
                    return errors.map((item, index) => {
                        return toast.error(
                            `${Object.keys(data.errors)[index]} ${item}`
                        );
                    });
                });

                break;
            }
            if (data.error) {
                yield toast.error(data.error);
                yield put(signFailure());
                break;
            }

            history.push(`/${id}`);
            break;
        default:
            break;
    }
}
