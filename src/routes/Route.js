import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { HeaderType, Footer } from '@components';
import { store } from '@store';

export default function RouteWrapper({
    component: Component,
    isPrivate,
    login,
    checkout,
    ...rest
}) {
    const { signed } = store.getState().auth;
    if (!signed && isPrivate) {
        return <Redirect to="/create-account" />;
    }
    if (signed && login) {
        return <Redirect to="/" />;
    }
    let header;
    if (checkout) header = 'HeaderCheckout';
    else header = signed ? 'HeaderConta' : 'Header';

    return (
        <Route
            {...rest}
            render={props => (
                <>
                    <HeaderType {...props} signed={header} />
                    <Component {...props} />
                    <Footer {...props} />
                </>
            )}
        />
    );
}

RouteWrapper.propTypes = {
    isPrivate: PropTypes.bool,
    login: PropTypes.bool,
    component: PropTypes.oneOfType([PropTypes.element, PropTypes.func])
        .isRequired,
};

RouteWrapper.defaultProps = {
    isPrivate: false,
    login: false,
};
