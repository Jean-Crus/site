import React from "react";
import { Switch, Redirect } from "react-router-dom";
import Home from "@app/Pages/Home/";
import Home2 from "@app/Pages/Home2/";
import Cursos from "@app/Pages/Cursos/";
import Eventos from "@app/Pages/Eventos/";
import Produtos from "@app/Pages/Produtos/";
import Artigos from "@app/Pages/Artigos/";
import Sobre from "@app/Pages/Sobre/";
import Contato from "@app/Pages/Contato/";
import Uikit from "@app/Pages/Uikit/";
import Carrinho from "@app/Pages/Carrinho/";
import Error404 from "@app/Pages/404/";
import Categoria from "@app/Pages/Categoria/";
import Checkout from "@app/Pages/Checkout/";
import Thankyou from "@app/Pages/Thankyou/";
import CursoAberto from "@app/Pages/CursoAberto/";
import CursoAndamento from "@app/Pages/CursoAndamento/";
import Depoimentos from "@app/Pages/Depoimentos/";
import EventoAberto from "@app/Pages/EventoAberto/";
import FaleComTutorInterna from "@app/Pages/FaleComTutorInterna/";
import ForumPage from "@app/Pages/Forum/";
import ProdutoListagem from "@app/Pages/ProdutoListagem/";
import SimpleProduto from "@app/Pages/SimpleProduto/";
import SearchResults from "@app/Pages/SearchResults/";

import Account from "@app/Pages/Account/";
import CreateAccount from "@app/Pages/Account/CreateAccount/";
import Login from "@app/Pages/Account/Login/";
import ForgotLogin from "@app/Pages/Account/Login/Forgot";
import NewPassword from "@app/Pages/Account/Login/NewPassword";

import CursoApresentacao from "@app/Pages/CursoApresentacao";
import { DepoNew, CheckoutExpired } from "@components";
import Route from "./Route";

function RoutesComponent() {
    return (
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/home" component={Home} />
            {/* <Route exact path="/" component={Home2} /> */}
            <Route path="/cursos" component={Cursos} />
            {/* <Route path="/eventos" component={Eventos} />
            <Route path="/produtos" component={Produtos} />
            <Route path="/artigos" component={Artigos} /> */}
            <Route path="/sobre" component={Sobre} />
            <Route path="/contato" component={Contato} />
            <Route path="/uikit" component={Uikit} />
            <Route path="/login" component={Login} login />
            <Route path="/reset-password" component={ForgotLogin} login />
            <Route path="/new-password" component={NewPassword} login />

            <Route path="/create-account" component={CreateAccount} login />
            <Route path="/carrinho" component={Carrinho} />
            {/* <Route path="/categoria" component={Categoria} /> */}
            <Route path="/checkout" component={Checkout} isPrivate checkout />
            <Route path="/thankyou/:id" component={Thankyou} />
            <Route exact path="/curso-aberto/:id" component={CursoAberto} />
            <Route
                path="/curso-andamento/:curso/:id"
                component={CursoAndamento}
                isPrivate
            />
            <Route
                path="/curso-apresentacao/:curso"
                component={CursoApresentacao}
                isPrivate
            />
            <Route path="/depoimentos" component={DepoNew} />
            {/* <Route path="/evento-aberto" component={EventoAberto} /> */}
            {/* <Route
                path="/fale-com-tutor-interna"
                component={FaleComTutorInterna}
            /> */}
            {/* <Route path="/forum" component={ForumPage} /> */}
            {/* <Route path="/produto-listagem" component={ProdutoListagem} /> */}
            {/* <Route path="/simple-produto" component={SimpleProduto} /> */}
            {/* <Route path="/search-results" component={SearchResults} /> */}
            <Route path="/conta" component={Account} isPrivate />
            <Route
                path="/checkoutExpired/:id"
                component={CheckoutExpired}
                isPrivate
            />
            <Route exact path="*" component={Error404} />
            <Route path="/404" component={Error404} />
            <Redirect from="*" to="/404" />
        </Switch>
    );
}

export default RoutesComponent;
