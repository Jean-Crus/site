// const dropdown = () => {
//     // Entrar e Criar Conta
//     window.jQuery('#login_create').click(() => {
//         window.jQuery('#login_create').toggleClass('show');
//     });
//     const $menu = window.jQuery('.login');
//     window.jQuery(document).mouseup(e => {
//         if (!$menu.is(e.target) && $menu.has(e.target).length === 0) {
//             window.jQuery('#login_create').removeClass('show');
//         }
//     });
// };

// const openSelect = () => {
//     window.jQuery('.tr-multiSelect').click(function() {
//         window.jQuery(this).toggleClass('open');
//     });
//     const $menu = window.jQuery('.tr-multiSelect');
//     window.jQuery(document).mouseup(e => {
//         if (!$menu.is(e.target) && $menu.has(e.target).length === 0) {
//             window.jQuery('.tr-multiSelect').removeClass('open');
//         }
//     });
// };

// const carrousel = () => {
//     const sync1 = window.jQuery('#slider');
//     const sync2 = window.jQuery('#navigation');
//     const slides = sync1
//         .owlCarousel({
//             items: 1,
//             loop: true,
//             margin: 10,
//             mouseDrag: false,
//             nav: false,
//             dots: true,
//         })
//         .on('changed.owl.carousel', e => {
//             const current = e.item.index;
//         });

//     const thumbs = sync2
//         .owlCarousel({
//             items: 3,
//             loop: false,
//             // autoplay: false,
//             autoplay: true,
//             autoplayTimeout: 1000,
//             autoplayHoverPause: true,
//             nav: false,
//             dots: false,
//         })
//         .on('click', '.owl-item', e => {
//             // console.log(e.target)
//             e.preventDefault();
//             sync1.trigger('to.owl.carousel', [
//                 window
//                     .jQuery(e.target)
//                     .parents('.owl-item')
//                     .index(),
//                 300,
//                 true,
//             ]);
//         })
//         .data('owl.carousel');

//     window.jQuery('#navigation .owl-item.active').on('click', function() {
//         window
//             .jQuery('#navigation .owl-item.active.current-slide')
//             .each(function() {
//                 window.jQuery(this).removeClass('current-slide');
//             });
//         window.jQuery(this).addClass('current-slide');
//     });

//     window
//         .jQuery('#navigation .owl-item.active')
//         .first()
//         .addClass('current-slide');
// };

// const videoAbout = () => {
//     window.jQuery('#playing-video').click(() => {
//         window.jQuery('#playing-video').toggleClass('pause');
//         window.jQuery('#playing-video video').attr('controls', 'true');
//         window.jQuery('#playing-video video').trigger('play');
//     });
// };

// const carrouselTestimonial = () => {
//     const test = window.jQuery('#testimonial');

//     const testmonial = test
//         .owlCarousel({
//             items: 1,
//             loop: true,
//             nav: false,
//             autoplay: true,
//             autoplayTimeout: 2000,
//             autoplayHoverPause: true,
//             dots: false,
//         })
//         .on('click', '.owl-item', e => {
//             // console.log(e.target)
//         })
//         .data('owl.carousel');
// };

// const countDown = () => {
//     const second = 1000;
//     const minute = second * 60;
//     const hour = minute * 60;
//     const day = hour * 24;

//     const countDown = new Date(
//         new Date().setDate(new Date().getDate() + 2)
//     ).getTime();
//     const x = setInterval(function() {
//         const now = new Date().getTime();
//         const distance = countDown - now;
//         (document.getElementById('days').innerText = Math.floor(
//             distance / day
//         )),
//             (document.getElementById('hours').innerText = Math.floor(
//                 (distance % day) / hour
//             )),
//             (document.getElementById('minutes').innerText = Math.floor(
//                 (distance % hour) / minute
//             )),
//             (document.getElementById('seconds').innerText = Math.floor(
//                 (distance % minute) / second
//             ));
//     }, second);
// };

// const openDropdown = () => {
//     window
//         .jQuery(
//             '.modules-dropdown .list-modules>li .title, .bonus-dropdown .list-bonus>li .title'
//         )
//         .click(function() {
//             if (
//                 window
//                     .jQuery(this)
//                     .parent()
//                     .hasClass('active')
//             ) {
//                 window
//                     .jQuery(this)
//                     .parent()
//                     .removeClass('active');
//             } else {
//                 window
//                     .jQuery(
//                         '.modules-dropdown .list-modules>li .title, .bonus-dropdown .list-bonus>li .title'
//                     )
//                     .parent()
//                     .removeClass('active');
//                 window
//                     .jQuery(this)
//                     .parent()
//                     .toggleClass('active');
//             }
//         });
// };

// const WaitList = () => {
//     window.jQuery('#listadeesperafinal').click(() => {
//         window.jQuery('.tr-card-wait-list').addClass('d-none');
//         window.jQuery('.tr-card-sucess').addClass('show');
//     });
// };

// const cardTwoOption = () => {
//     window.jQuery('.alter-card-option').click(() => {
//         window.jQuery('#one-card').toggleClass('d-none');
//         window.jQuery('#two-card').toggleClass('d-none');
//     });
// };

// const inputFocus = () => {
//     window.jQuery('input,textarea').focus(function() {
//         window
//             .jQuery(this)
//             .next('label')
//             .addClass('focused');
//     });

//     window.jQuery('input,textarea').blur(function() {
//         const inputValue = window.jQuery(this).val();
//         if (inputValue == '') {
//             window
//                 .jQuery(this)
//                 .next('label')
//                 .removeClass('focused');
//         }
//     });
//     window.jQuery('input').each(function() {
//         const inputValue = window.jQuery(this).val();
//         inputValue
//             ? window
//                   .jQuery(this)
//                   .next('label')
//                   .addClass('focused')
//             : '';
//     });
// };

// const toggleOrder = () => {
//     window.jQuery('.orders-table .content .order-item').click(function() {
//         window.jQuery(this).toggleClass('open');
//         window
//             .jQuery(this)
//             .next()
//             .toggleClass('show');
//     });
// };

// const OwlCarousel = () => {
//     window.jQuery('.carousel-produtos .owl-carousel').owlCarousel({
//         loop: true,
//         nav: true,
//         dots: false,
//         items: 4,
//         margin: 0,
//     });
// };

// const InternaOwlCarousel = () => {
//     window.jQuery('.product-carousel-interna .owl-carousel').owlCarousel({
//         loop: true,
//         nav: false,
//         dots: false,
//         items: 1,
//         margin: 0,
//         dotsContainer: '#custom-dots',
//     });
// };

// const InternaOwlCarouselDots = () => {
//     window
//         .jQuery('.product-details .product-carousel-interna #custom-dots .dot')
//         .click(function() {
//             window
//                 .jQuery(
//                     '.product-details .product-carousel-interna #custom-dots .dot'
//                 )
//                 .removeClass('active');
//             window.jQuery(this).addClass('active');
//         });
// };

// const toggleHeart = () => {
//     window
//         .jQuery('.product-details .product-carousel-interna .like-product')
//         .click(function() {
//             window.jQuery(this).toggleClass('active');
//         });
// };

// const carrouselInstrutor = () => {
//     const test = window.jQuery('.instrutor-carrousel .owl-carousel');

//     const testmonial = test
//         .owlCarousel({
//             items: 3,
//             loop: true,
//             nav: true,
//             dots: false,
//             margin: 15,
//         })
//         .on('click', '.owl-item', e => {
//             // console.log(e.target)
//         })
//         .data('owl.carousel');
// };

// const carrouselDepoimento = () => {
//     const test = window.jQuery('.carrousel-todos-depoimentos .owl-carousel');

//     const testmonial = test
//         .owlCarousel({
//             items: 7,
//             loop: false,
//             nav: true,
//             dots: false,
//             margin: 100,
//         })
//         .on('click', '.owl-item', e => {
//             // console.log(e.target)
//         })
//         .data('owl.carousel');
// };

// window.jQuery(document).ready(() => {
//     // Timeout Descontos
//     setTimeout(() => {
//         window.jQuery('.applied-discount').removeClass('open');
//     }, 2000);

//     const count = jQuery('.tr-header-course-open, .tr-header-event-open')
//         .length;
//     const exclude = jQuery('.tr-nagivation-course-close').length;
//     if (count == 1 && !exclude) {
//         countDown();
//     }

//     dropdown();
//     carrousel();
//     videoAbout();
//     carrouselTestimonial();
//     openDropdown();
//     WaitList();
//     cardTwoOption();
//     inputFocus();
//     openSelect();
//     toggleOrder();
//     OwlCarousel();
//     InternaOwlCarousel();
//     toggleHeart();
//     InternaOwlCarouselDots();
//     floatingdiv();
//     carrouselInstrutor();
//     carrouselDepoimento();

//     window.jQuery('.btn.tr-circle-primary').click(() => {
//         window.history.back();
//     });
// });
