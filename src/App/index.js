import React from "react";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import "../config/ReactotronConfig";
import { ToastContainer } from "react-toastify";
import { Router } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import { store, persistor } from "@store";
import { customTheme } from "@theme";
import history from "../routes/history";
import RoutesComponent from "../routes/RoutesComponent";

// Styles Global
import "bootstrap/dist/css/bootstrap.css";
import "../Assets/css/style.css";
import "react-toastify/dist/ReactToastify.css";
// import '@globalStyle';
function App() {
    return (
        <>
            <Provider store={store}>
                <PersistGate persistor={persistor}>
                    <Router history={history}>
                        <ThemeProvider theme={customTheme}>
                            <RoutesComponent />
                            <ToastContainer autoClose={3000} />
                        </ThemeProvider>
                    </Router>
                </PersistGate>
            </Provider>
        </>
    );
}

export default App;
