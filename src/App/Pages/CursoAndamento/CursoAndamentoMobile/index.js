import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid, Icon } from "@material-ui/core";
import { Link } from "react-router-dom";
import { Spinner } from "react-bootstrap";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import * as UserActions from "@store/modules/user/actions";
import history from "@routes/history";
import { ButtonTriade, CourseFinished, CourseFinished2 } from "@components";
import ReactPlayer from "react-player";
import {
    CursoApresentacaoMobileHeader,
    CursoApresentacaoMobile,
    CursoApresentacaoMobileBody,
    IconStarBorder,
    IconAssignment,
    IconSchedule,
    Progress,
    Modulos,
    IconCircle,
    ArrowPlay,
    RadioIconChecked,
    RadioIconUnchecked
} from "../styles";

import {
    Checked,
    CurrentClass,
    UnChecked
} from "../../../../components/Parts/CursoAndamento/NagivationAndamento/styles";
import { Lesson } from "../../CursoApresentacao/styles";

const AndamentoMobile = ({ curso, id }) => {
    const dispatch = useDispatch();
    const loadingCourse = useSelector(state => state.user.loadingCourse);
    const course = useSelector(state => state.user.course);
    const lesson = useSelector(state => state.user.lesson);
    const modules = useSelector(state => state.user.modules);
    const handleExpand = item => {
        dispatch(UserActions.expandList(item));
    };
    const check = (finished, active) => {
        const number = parseInt(course, 10);
        if (finished) {
            return <Checked />;
        }
        if (number === active) {
            return <CurrentClass />;
        }
        return <UnChecked color="disabled" />;
    };

    return loadingCourse ? (
        <Grid container justify="center" className="mt-4 mb-4">
            <Spinner animation="border" role="status" />
        </Grid>
    ) : (
        <CursoApresentacaoMobile>
            <CursoApresentacaoMobileHeader>
                <div className="title">
                    <IconCircle
                        small
                        onClick={() =>
                            history.push(`/curso-apresentacao/${curso}`)
                        }
                    >
                        <i className="arrow left" />
                    </IconCircle>
                    <span className="title">{course.name}</span>
                </div>
                <div className="multi-tag">
                    <div>
                        <IconStarBorder />
                        <span>{course.difficulty.name}</span>
                    </div>
                    <div>
                        <IconAssignment />
                        <span>
                            {course.modules.length < 2
                                ? `${course.modules.length} Módulo`
                                : `${course.modules.length} Módulos`}
                        </span>
                    </div>
                    <div>
                        <IconSchedule />
                        <span>
                            {course.workload < 2
                                ? `${course.workload} Hora`
                                : `${course.workload} Horas`}
                        </span>
                    </div>
                </div>
                <div className="progress-line">
                    <Progress
                        variant="determinate"
                        value={course.user_progress}
                    />
                    <small>{course.user_progress}% completo</small>
                </div>
                {/* <div className="button-box">
                    <Link to="/">
                        <ButtonTriade button="primary" text="Forum" />
                    </Link>
                    <Link to="/">
                        <ButtonTriade
                            button="secondary"
                            text="Fale com o tutor"
                        />
                    </Link>
                </div> */}
            </CursoApresentacaoMobileHeader>

            <CursoApresentacaoMobileBody>
                {course.user_progress === 100 && id === "0" ? (
                    <CourseFinished />
                ) : (
                    // <CourseFinished2 url="https://images.pexels.com/photos/349609/pexels-photo-349609.jpeg?cs=srgb&dl=alho-poro-alimento-aspero-349609.jpg&fm=jpg" />
                    <div className="video-aula">
                        {!!lesson.url_video && (
                            <div className="img-video">
                                <ReactPlayer
                                    width="100%"
                                    url={lesson.url_video}
                                    controls
                                />
                            </div>
                        )}
                        <div className="title-aula">
                            <h2>{!!lesson && lesson.name}</h2>
                            <p>{!!lesson && lesson.description}</p>
                        </div>

                        {!!lesson.file_link && (
                            <div className="archive">
                                <p
                                    style={{ margin: 0 }}
                                    onClick={() =>
                                        window.open(lesson.file_link)
                                    }
                                >
                                    <Icon className="fas fa-file" />
                                </p>
                                <div
                                    onClick={() =>
                                        window.open(lesson.file_link)
                                    }
                                >
                                    Acesse aqui o conteúdo da aula
                                </div>
                            </div>
                        )}
                    </div>
                )}

                {course.user_progress === 100 && id === "0" ? (
                    <div className="button-cert">
                        <ButtonTriade
                            // button="Modelo 02"
                            // cor="Modelo 02"
                            // letterColor="Modelo 02"
                            text="Emitir certificado"
                            disabled
                        />
                        <ButtonTriade
                            button="Modelo 02"
                            cor="Modelo 02"
                            letterColor="Modelo 02"
                            className="ml-2"
                            text="Próximo curso"
                            onClick={() => history.push("/cursos")}
                        />
                    </div>
                ) : (
                    <ButtonTriade
                        button="Modelo 02"
                        type="button"
                        letterColor="Modelo 02"
                        text={
                            lesson.finished ? "Próxima Aula" : "Finalizar Aula"
                        }
                        readOnly
                        onClick={() =>
                            dispatch(
                                UserActions.getFinishedRequest(
                                    id,
                                    curso,
                                    lesson.finished
                                )
                            )
                        }
                    />
                )}

                <h2>Todos os módulos</h2>
                <div className="modulos">
                    {modules.map(item => {
                        return (
                            <Modulos
                                key={item.id}
                                onClick={() => handleExpand(item.id)}
                                active={item.active}
                            >
                                <header>
                                    <span>{item.name}</span>
                                    {item.active ? (
                                        <ExpandLessIcon />
                                    ) : (
                                        <ExpandMoreIcon />
                                    )}
                                </header>
                                {item.lessons.map(lesson => {
                                    return (
                                        <div key={lesson.id}>
                                            <Link
                                                to={`/curso-andamento/${curso}/${lesson.id}`}
                                            >
                                                {check(
                                                    lesson.finished,
                                                    lesson.id
                                                )}

                                                <Lesson
                                                    finished={lesson.finished}
                                                >
                                                    {lesson.name} (
                                                    {lesson.minute} min)
                                                </Lesson>
                                            </Link>
                                        </div>
                                    );
                                })}
                            </Modulos>
                        );
                    })}
                </div>
                {/*
                <h2>Bonus</h2>
                <div className="modulos">
                    {expand.map(item => {
                        return (
                            <Modulos
                                className="modulos-div"
                                key={item.id}
                                onClick={() => handleExpand(item.id)}
                                active={item.active}
                                bonus="true"
                            >
                                <header>
                                    <span>Pedido #121212</span>
                                    {item.active ? (
                                        <ExpandLessIcon />
                                    ) : (
                                        <ExpandMoreIcon />
                                    )}
                                </header>
                                <div className="aula-check">
                                    <RadioIconChecked />
                                    <span>Nome da aula(6hrs)</span>
                                </div>
                            </Modulos>
                        );
                    })}
                </div> */}
            </CursoApresentacaoMobileBody>
        </CursoApresentacaoMobile>
    );
};

export default AndamentoMobile;
