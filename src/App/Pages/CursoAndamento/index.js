import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid } from "@material-ui/core";
import * as UserActions from "@store/modules/user/actions";
import { Spinner } from "react-bootstrap";
import history from "@routes/history";
import {
    HeaderCursoAndamento,
    NagivationAndamento,
    ButtonTriade,
    Aula,
    CourseFinished2
} from "@components";
import win from "@images/win.svg";
import AndamentoMobile from "./CursoAndamentoMobile";

import { Container } from "./styles";

function CursoAndamento({
    match: {
        params: { curso, id }
    }
}) {
    const loadingCourse = useSelector(state => state.user.loading);
    const course = useSelector(state => state.user.course);

    const lesson = useSelector(state => state.user.lesson);

    const dispatch = useDispatch();
    useEffect(() => {
        if (!course.launch_open) {
            dispatch(UserActions.getOneCourseRequest(curso));
            dispatch(UserActions.getLessonRequest(id));
        } else {
            history.push(`/curso-apresentacao/${curso}`);
        }
    }, [course.launch_open, curso, dispatch, id]);

    return loadingCourse ? (
        <Grid container justify="center" className="mt-4 mb-4">
            <Spinner animation="border" role="status" />
        </Grid>
    ) : (
        <>
            <Container className="mb-5 pb-5">
                {/* <!-- Header curso cursando --> */}
                <div className="tr-header-cursando">
                    <HeaderCursoAndamento
                        back={`/curso-apresentacao/${curso}`}
                        title={course && course.name}
                    />
                </div>

                <div className="container mt-5">
                    <div className="row m-0 d-flex">
                        <div className="col-lg-4">
                            <div className="tr-nagivation-course-andamento">
                                <NagivationAndamento
                                    modules={course.modules && course.modules}
                                    curso={curso}
                                    id={id}
                                />
                            </div>
                        </div>
                        <div className="col-lg-8 pr-0 mr-0 pl-4">
                            <div className="title-1 size-50 black wh-600">
                                {!!lesson && id !== "0" && lesson.name}
                            </div>
                            {course.user_progress === 100 && id === "0" ? (
                                <CourseFinished2 url={win} />
                            ) : (
                                <Aula
                                    cursoandamento
                                    introduction={!!lesson && lesson}
                                />
                            )}
                            {course.user_progress === 100 && id === "0" ? (
                                <div className="button-next mt-5 d-flex row justify-content-center">
                                    <ButtonTriade
                                        // button="Modelo 02"
                                        // cor="Modelo 02"
                                        // letterColor="Modelo 02"
                                        className="mr-2"
                                        text="Emitir certificado"
                                        disabled
                                    />

                                    <ButtonTriade
                                        button="Modelo 02"
                                        type="button"
                                        letterColor="Modelo 02"
                                        readOnly
                                        text="Próximo curso"
                                        onClick={() => history.push("/cursos")}
                                    />
                                </div>
                            ) : (
                                <div className="button-next mt-5 d-flex row justify-content-end">
                                    <ButtonTriade
                                        button="Modelo 02"
                                        type="button"
                                        letterColor="Modelo 02"
                                        text={
                                            lesson.finished
                                                ? "Próxima Aula"
                                                : "Finalizar Aula"
                                        }
                                        readOnly
                                        onClick={() =>
                                            dispatch(
                                                UserActions.getFinishedRequest(
                                                    id,
                                                    curso,
                                                    lesson.finished
                                                )
                                            )
                                        }
                                    />
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </Container>
            <AndamentoMobile curso={curso} id={id} />
        </>
    );
}

export default CursoAndamento;
