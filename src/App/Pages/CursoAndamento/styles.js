import styled from "styled-components";
import ScheduleIcon from "@material-ui/icons/Schedule";
import AssignmentIcon from "@material-ui/icons/Assignment";
import StarBorderIcon from "@material-ui/icons/StarBorder";
import { LinearProgress } from "@material-ui/core";
import Slider from "react-slick";
import ReactPlayer from "react-player";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";

import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";

export const RadioIconUnchecked = styled(RadioButtonCheckedIcon)``;

export const RadioIconChecked = styled(RadioButtonUncheckedIcon)``;

export const CursoApresentacaoMobileHeader = styled.header`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 30px 40px;
    background: linear-gradient(0deg, #012a50 0%, #0058a8 100%);

    .title {
        display: flex;
        justify-content: space-evenly;
        align-items: center;
        color: ${props => props.theme.letter};
        font-family: Montserrat, sans-serif;
        font-size: 20px;
        font-weight: bold;
        span {
            padding-left: 5px;
        }
    }

    .multi-tag {
        display: flex;
        justify-content: space-between;
        align-items: center;
        flex-wrap: wrap;
        width: 80%;
        margin-top: 30px;
        div {
            span {
                margin-left: 5px;
                color: ${props => props.theme.letter};
                font-family: Montserrat;
                font-size: 11px;
            }
        }
    }

    .progress-line {
        margin-top: 30px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        width: 100%;
        small {
            margin-top: 5px;
            width: 80%;
            font-weight: bold;
            line-height: 14px;
            color: ${props => props.theme.letter};
            font-family: Montserrat;
            font-size: 11px;
        }
    }

    .button-box {
        padding: 50px 0px 40px 0px;
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
        align-items: center;
        button {
            &:last-child {
                margin-left: 10px;
            }
            height: 45px !important;
            width: 150px;
            margin-top: 15px;
        }
    }

    @media only screen and (min-width: 999px) {
        display: none;
    }
`;

export const IconSchedule = styled(ScheduleIcon)`
    color: #fff;
`;

export const IconAssignment = styled(AssignmentIcon)`
    color: #fff;
`;

export const IconStarBorder = styled(StarBorderIcon)`
    color: #fff;
`;

export const Container = styled.div`
    .tr-header-cursando {
        background: ${props =>
            `linear-gradient(0deg, #012a50 0%, #0058a8 100%)`} !important;
    }
    @media only screen and (max-width: 999px) {
        display: none;
    }
`;

export const Progress = styled(LinearProgress)`
    color: #fff;
    width: 80%;
`;

export const CursoApresentacaoMobile = styled.div`
    @media only screen and (min-width: 999px) {
        display: none;
    }
    button {
        margin: 20px 0;
    }
    @media only screen and (max-width: 476px) {
        .button-cert {
            display: flex;
            flex-direction: column;
            align-items: center;
            button {
                margin: 0;
                padding: 0 !important;
                margin-top: 10px;
                &:last-child {
                    margin: 10px 0 !important;
                }
            }
        }
    }
`;

export const CursoApresentacaoMobileBody = styled.main`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 25px;
    h2 {
        color: ${props => props.theme.background};
        font-family: Montserrat;
        font-size: 16px;
        font-weight: bold;
        line-height: 25px;
        margin-bottom: 20px;
    }
    small {
        font-family: Montserrat;
        color: ${props => props.theme.letter2};
        font-size: 12px;
        font-weight: 300;
        line-height: 20px;
        margin-bottom: 16px;
    }
    .video-aula {
        display: flex;
        justify-content: center;
        flex-direction: column;
        width: 100%;
        border: 1px solid #efefef;
        border-radius: 5px;
        background-color: #ffffff;
        .img-video {
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 5px;
            background-image: ${props => `url(${props.img})`};
            background-position: center center;
            background-size: cover;
            width: 100%;
            margin-bottom: 40px;
            iframe {
                border-top-left-radius: 5px;
                border-top-right-radius: 5px;
            }
        }
        .title-aula {
            padding: 15px;
            p {
                font-family: Montserrat;
                font-size: 12px;
                font-size: 12px;
                color: ${props => props.theme.letter2};
            }
        }
        .archive {
            border-radius: 0 0 5px 5px;
            background-color: #f3f3f3;
            padding: 15px;
            display: flex;
            align-items: center;
            p,
            div {
                display: flex;
                align-items: center;
                font-family: Montserrat;
                font-size: 0.8rem;
                font-weight: bold;
                text-align: justify;
                color: ${props => props.theme.primary};
                text-decoration: underline;
            }
        }
    }
    .button-prox {
        margin: 40px 0;
        button {
            width: 200px;
            height: 45px;
        }
    }
    .modulos {
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }
`;

export const ReactSlider = styled(Slider)`
    width: 80%;
    max-width: 1200px;

    margin: 20px auto;
    .slick-list {
        margin: 0 30px;
        .slick-slide {
            display: flex;
            justify-content: center;
        }
    }
    .slick-next {
        right: 0;
    }
    .slick-prev {
        left: 0;
    }
    .slick-dots {
        display: flex !important;
        justify-content: center;
        align-items: center;
        li {
            display: flex !important;
            justify-content: center;
            align-items: center;
        }
        .slick-active {
            border-radius: 50%;
            border: ${props => `2px solid ${props.theme.primary}`};
        }
    }
    .dots {
        display: flex;
        justify-content: center;
        align-items: center;
        .circle {
            width: 4px;
            height: 4px;
            border-radius: 20px;
            background-color: ${props => props.theme.letter2};
        }
    }
`;

export const Player = styled(ReactPlayer)``;

export const Modulos = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    background-color: #ffffff;
    border: 1px solid #ececec;
    border-bottom: none;
    width: 267px;
    border-radius: 5px;
    &:last-child {
        border: 1px solid #ececec;
        margin-bottom: 20px;
    }

    svg {
        color: ${props => {
            if (props.active) return "#fff";
            if (props.bonus) return "red";
            return props.theme.primary;
        }};
    }
    header {
        display: flex;
        width: 100%;
        justify-content: space-between;
        padding: 10px;
        border-radius: 5px;
        background-color: ${props => {
            if (props.bonus && props.active) return "red";
            if (props.active) return props.theme.primary;
            return props.theme.letter;
        }};

        span {
            color: ${props =>
                props.active ? props.theme.letter : props.theme.background};
            font-family: Montserrat;
            font-size: 12px;
            font-weight: bold;
            line-height: 24px;
        }
    }
    div {
        width: 100%;
        display: ${props => (props.active ? "flex" : "none")};
        border-bottom: 1px solid #ececec;
        padding: 10px;
        background-color: #d8d8d82e;
        svg {
            color: #ececec;
        }
        span {
            padding-left: 10px;
            font-family: Montserrat;
            color: ${props => props.theme.background};
            font-size: 12px;
            font-weight: 300;
            line-height: 22px;
        }
        &:last-child {
            border-bottom: none;
        }
    }
`;

export const InstrutorMobile = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    width: 265px;
    border-radius: 5px;
    border: 1px solid #e6e6e6;
    background-color: #ffffff;
    padding: 20px;
    header {
        display: flex;
        flex-direction: column;
        width: 100%;
        .img-profile {
            display: flex;
            img {
                height: 40px;
                width: 40px;
                border-radius: 100px;
            }
            .user {
                margin-left: 10px;
                display: flex;
                /* justify-content: center;
                align-items: center; */
                flex-direction: column;

                span {
                    font-family: Montserrat;
                    font-size: 14px;
                    color: ${props => props.theme.background};
                    margin-bottom: 0;
                }
                small {
                    line-height: 5px;
                    font-family: Montserrat;
                    font-size: 10px;
                    font-weight: 300;
                    color: ${props => props.theme.letter2};
                }
            }
        }
        .divider-box {
            position: relative;
            display: flex;
            width: 100%;
            height: 20px;
            margin-top: 19px;
            margin-bottom: 13px;
            small {
                position: absolute;
                z-index: 2;
                width: 80px;
                text-align: center;
                background-color: #ffffff;
                left: 50%;
                transform: translateX(-50%);
            }
            .divider {
                position: absolute;
                z-index: 1;
                height: 2px;
                width: 100%;
                top: 50%;
                background-color: #e6e6e6;
            }
        }
        .social-icons {
            display: flex;
            justify-content: space-evenly;
            align-items: center;
            margin-bottom: 30px;
            svg {
                color: ${props => props.theme.primary};
            }
        }
    }
    span {
        font-family: Montserrat;
        color: ${props => props.theme.letter2};
        font-size: 12px;
        font-weight: 300;
        text-align: center;
        line-height: 18px;
        margin-bottom: 30px;
    }
`;

export const IconCircle = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: ${props => (props.small ? "35px" : "50px")};
    width: ${props => (props.small ? "35px" : "50px")};
    min-width: ${props => (props.small ? "35px" : "50px")};
    border-radius: 50%;
    background-color: ${props => props.theme.primary};
    box-shadow: 0 10px 30px 0 rgba(0, 0, 0, 0.2);
    i {
        border: solid #fff;
        border-width: 0 3px 3px 0;
        display: inline-block;
        padding: 3px;
    }
    .left {
        transform: rotate(135deg);
        -webkit-transform: rotate(135deg);
    }
`;

export const ArrowPlay = styled(PlayArrowIcon)`
    color: ${props => props.theme.letter};
`;

export const CardVantagens = styled.div`
    width: 216px !important;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    img {
        height: 48px;
        width: 50px;
        margin-bottom: 10px;
    }
    div {
        padding-top: 10px;
        display: flex;
        flex-direction: column;
        align-items: center;
        span {
            color: ${props => props.theme.background};
            font-family: Montserrat;
            font-size: 14px;
            font-weight: bold;
            text-align: center;
        }
        small {
            font-family: Montserrat;
            color: ${props => props.theme.letter2};
            font-weight: 200;
            text-align: center;
            font-size: 12px;
        }
    }
`;
