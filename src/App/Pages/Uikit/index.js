import React from 'react';
import { product1, curso3, curso1 } from '@images';

import {
    ButtonTriade,
    CardCurso,
    CardTerm,
    CardSuccess,
    CardWait,
    CardProduct,
    SelectMult,
} from '@components';

class Uikit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectItems: [
                {
                    name: 'Categorias',
                    subItems: [
                        { name: 'Arquitetura' },
                        { name: 'Design' },
                        { name: 'Desenvolvimento' },
                        { name: 'Health' },
                        { name: 'Marketing' },
                    ],
                },
                { name: 'Nivel' },
                { name: 'Duração' },
                { name: 'Preço' },
            ],
        };
    }

    render() {
        return (
            <>
                <div className="container d-flex justify-content-around align-content-center p-4 flex-wrap">
                    <div className="input-error">
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nome"
                            />
                            <span className="error-alert">
                                Mensagem Inválida
                            </span>
                        </div>
                    </div>

                    {/* ButtonTriade */}

                    <div className="opcao_de_botoes">
                        <div className="title-1 black size-25 mb-3">
                            Opção de Botão triade
                        </div>
                        <div className="content d-flex justify-content-around align-items-center flex-xl-wrap">
                            <ButtonTriade
                                button="primary"
                                className="mx-2"
                                text="btn tr-primary"
                            />
                            <ButtonTriade
                                button="secondary"
                                className="mx-2"
                                text="btn tr-secondary"
                            />
                            <ButtonTriade
                                button="third"
                                className="mx-2"
                                text="btn tr-third"
                            />
                            <ButtonTriade
                                button="primary"
                                size="small"
                                className="mx-2"
                                text="btn tr-primary small"
                            />
                            <ButtonTriade
                                button="secondary"
                                size="small"
                                className="mx-2"
                                text="btn tr-secondary small"
                            />
                            <ButtonTriade
                                button="third"
                                size="small"
                                className="mx-2"
                                text="btn tr-third small"
                            />
                            <ButtonTriade
                                button="circle-primary"
                                className="mx-2"
                            />
                        </div>
                    </div>

                    {/* TextTriade */}

                    <div className="opcao_de_texto mt-5">
                        <div className="title-1 black size-25 mb-2">
                            Opção de Texto triade
                        </div>
                        <div className="content d-flex justify-content-around align-items-center flex-wrap">
                            <div className="title-1 black mx-2">
                                title-1 black
                            </div>
                            <div className="title-1 black size-50 mx-2">
                                title-1 black size-50
                            </div>
                            <div className="title-1 black size-25 mx-2">
                                title-1 black size-25
                            </div>
                            <div className="title-2 black mx-2">
                                title-2 black
                            </div>
                            <div className="title-2 black size-50 mx-2">
                                title-2 black size-50
                            </div>
                            <div className="title-2 black size-25 mx-2">
                                title-2 black size-25
                            </div>
                            <div className="title-3 black mx-2">
                                title-3 black
                            </div>
                            <div className="title-3 black size-50 mx-2">
                                title-3 black size-50
                            </div>
                            <div className="title-4 black mx-2">
                                title-4 black
                            </div>
                            <div className="title-5 black mx-2">
                                title-5 black
                            </div>
                            <div className="title-6 black mx-2">
                                title-6 black
                            </div>
                            <div className="description-1 mx-2">
                                description-1
                            </div>
                            <div className="description-1 italic mx-2">
                                description-1 italic
                            </div>
                            <div className="description-2  mx-2">
                                description-2
                            </div>
                            <div className="description-3  mx-2">
                                description-3
                            </div>
                        </div>
                    </div>

                    {/* CardCurso */}

                    <div className="opcao_de_cards w-100 mt-5">
                        <div className="content d-flex justify-content-around flex-wrap w-100">
                            <CardCurso
                                image={curso3}
                                title="Curso de Lorem Ipsum Avançado"
                                category="Design"
                                colorCategory="purple"
                                difficulty="Avançado"
                                closed={false}
                                price="250,00"
                                salePrice="99,00"
                                hours="18"
                                modules="6"
                                description="“Today Web UI Kit is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been."
                            />

                            <CardCurso
                                image={curso1}
                                title="Curso de Lorem Ipsum Avançado"
                                category="Desenvolvimento"
                                colorCategory="red"
                                difficulty="Avançado"
                                closed
                                price="250,00"
                                salePrice="99,00"
                                hours="18"
                                modules="6"
                                description="“Today Web UI Kit is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been."
                            />

                            <CardTerm />

                            <div className="mt-5 d-flex">
                                <CardSuccess
                                    title="Obrigado, Raul"
                                    description="Agora, você está na lista de espera do Curso de Lorem ipsum Avançado"
                                />
                            </div>

                            <div className="mt-5 d-flex">
                                <CardWait />
                            </div>
                        </div>
                    </div>

                    <div className="card_produtos mt-5">
                        <div className="content d-flex justify-content-around flex-wrap w-100">
                            <div className="mr-5">
                                <CardProduct
                                    image={product1}
                                    title="SACETT™ Suction Above Cuff Endotracheal Tube"
                                    price="250,00"
                                    salePrice="99,00"
                                    discount="30%"
                                    newProd={false}
                                />
                            </div>
                            <CardProduct
                                image={product1}
                                title="SACETT™ Suction Above Cuff Endotracheal Tube"
                                price="250,00"
                                salePrice="99,00"
                                discount="30%"
                                newProd
                            />
                        </div>
                    </div>

                    <div className="mt-5">
                        <SelectMult selectItems={this.state.selectItems} />
                    </div>
                </div>
            </>
        );
    }
}

export default Uikit;
