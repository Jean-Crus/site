import React from 'react';
import { bannerFull } from '@images';

import {
    Newsletter, 
    BannerPage,
    FiltroCursos,
    CallCategoria,
    CarouselProdutos
} from '@components';

const ProdutoListagem = (props) => {
	
	return(
		<React.Fragment>

            {/* <!-- Listagem de cursos --> */}
            <div className="tr-header-course">
                <BannerPage title="Cursos" banner={bannerFull}/>
            </div>

            {/* <!-- Filtro de cursos --> */}
            <div className="tr-filtered-course">
                <FiltroCursos/>
            </div>

            {/* <!-- Carrousel Produtos --> */}
            <CarouselProdutos/>

            {/* <!-- Call Categoria --> */}
            <CallCategoria/>

            {/* <!-- Carrousel Produtos --> */}
            <CarouselProdutos/>
            <CarouselProdutos/>
            <CarouselProdutos/>

            {/* <!-- NEWSLETTER --> */}
            <div className="tr-newsletter purple">
                <Newsletter/>
            </div>
		</React.Fragment>
	);	
}

export default ProdutoListagem;