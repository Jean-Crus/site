import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as CursosActions from "@store/modules/curso/actions";
import * as CartActions from "@store/modules/cart/actions";
import * as AppActions from "@store/modules/app/actions";

import {
    CarrouselOne,
    ListaCurso,
    SobreCurso,
    Newsletter,
    Eventos,
    Vantagens,
    FaleConosco,
    FaleConoscoMobile,
    FaleConosco2,
    CarouselTwo,
    CardMobile,
    EditorConfig
} from "@components";
import {
    IconCircle,
    Arrow1,
    Arrow2,
    ModalPlayer,
    Player,
    BoxFale,
    ContainerCarousel,
    News,
    Vantag
} from "./styles";

const Home = () => {
    const [open, setOpen] = useState(false);
    const handleOpen = () => {
        setOpen(!open);
    };
    const dispatch = useDispatch();
    const themeHome = useSelector(state => state.app.themeHome1);
    const themeDestaque = useSelector(state => state.app.themeCourseDestaque);
    useEffect(() => {
        dispatch(CursosActions.getCursosDestaqueRequest());
    }, [dispatch]);
    return (
        <>
            {/* CARROUSEL INICIAL */}
            <EditorConfig
                color="white"
                iconColor="#d2d2d2"
                edit="homeOne"
                background={themeHome.background}
                readOnly
            >
                <CarouselTwo />
                <ContainerCarousel
                    className="tr-carrousel-vh"
                    backOne={themeHome.background.colorOne}
                    backTwo={themeHome.background.colorTwo}
                >
                    <div className="container">
                        <CarrouselOne readOnly />
                    </div>
                    <IconCircle background={themeHome.circle}>
                        <Arrow1 cor={themeHome.circleColor.colorOne} />
                        <Arrow2 cor={themeHome.circleColor.colorOne} />
                    </IconCircle>
                </ContainerCarousel>
            </EditorConfig>
            {/* CURSOS EM DESTAQUE */}

            <EditorConfig
                edit="courseDestaque"
                background={themeDestaque.background}
                iconColor="#d2d2d2"
                // color="white"
                readOnly
            >
                <ListaCurso
                    title='<h3 class="ql-align-center"><span class="ql-font-Montserrat">Cursos destaque</span></h3>'
                    destaque
                    readOnly
                />
            </EditorConfig>
            {/* SOBRE O CURSINHO */}
            <ContainerCarousel
                backOne={themeHome.background.colorOne}
                backTwo={themeHome.background.colorTwo}
                className="tr-about-home"
            >
                <SobreCurso onClick={handleOpen} />
                <ModalPlayer open={open} onClose={handleOpen}>
                    <Player url="https://youtu.be/Ij3u77NrCaY" />
                </ModalPlayer>
            </ContainerCarousel>
            {/* NEWSLETTER */}
            {/* <div className="tr-newsletter green">
                <Newsletter />
            </div> */}
            {/* EVENTOS */}
            {/* <div className="tr-event">
                <Eventos />
            </div> */}
            {/* VANTAGENS */}
            {/* <Vantag className="tr-vantagens">
                <Vantagens />
            </Vantag> */}
            {/* FALE CONOSCO */}
            <BoxFale className="tr-fale-conosco mb-5">
                <FaleConosco />
            </BoxFale>
            {/* <FaleConoscoMobile /> */}
            <FaleConoscoMobile />
            {/* NEWSLETTER */}
            {/* <News className="tr-newsletter purple mt-4">
                <Newsletter />
            </News> */}
        </>
    );
};

export default Home;
