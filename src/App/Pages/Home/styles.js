import styled from 'styled-components';
import ReactPlayer from 'react-player';
import { Modal } from '@material-ui/core';

export const ContainerCarousel = styled.div`
    background: ${props =>
        `linear-gradient(0deg, ${props.backOne} 0%, ${props.backTwo} 100%)`} !important;
    margin-top: 0 !important;
    margin-bottom: 50px;
`;

export const Vantag = styled.div`
    min-height: initial !important;
    max-height: initial !important;
`;

export const News = styled.div`
    background-color: ${props => props.theme.primary} !important;
    height: 100% !important;
    padding: 20px;
    .title-2 {
        color: ${props => props.theme.letter} !important;
    }
`;

export const IconCircle = styled.div`
    display: flex;
    justify-content: center;
    position: absolute;
    top: 95%;
    align-items: center;
    height: 67px;
    width: 67px;
    border-radius: 67px;
    background: ${props =>
        `linear-gradient(0deg, ${props.background.colorOne} 0%, ${props.background.colorTwo} 100%)`};
    box-shadow: 0 10px 30px 0 rgba(0, 0, 0, 0.2);
`;

export const Arrow1 = styled.div`
    position: absolute;
    top: 30%;
    height: 12px;
    width: 12px;
    border: ${props => `solid ${props.cor}`};
    border-width: 0 3px 3px 0;
    opacity: 0.5;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
`;

export const Arrow2 = styled.div`
    position: absolute;
    top: 50%;
    height: 12px;
    width: 12px;
    border: ${props => `solid ${props.cor}`};
    border-width: 0 3px 3px 0;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
`;

export const Player = styled(ReactPlayer)``;

export const ModalPlayer = styled(Modal)`
    display: flex;
    align-items: center;
    justify-content: center;
    /* top: 25%; */
    margin: auto;
`;

export const BoxFale = styled.div`
    min-height: initial !important;
    @media only screen and (max-width: 980px) {
        display: none !important;
    }
`;
