import styled from "styled-components";
import Pagination from "react-js-pagination";
import { Grid, Button } from "@material-ui/core";
import { darken } from "polished";

export const Box = styled(Grid)`
    margin-bottom: 60px;
    .pagination {
        align-items: center;
        justify-content: center;
        li {
            &:hover {
                a {
                    opacity: 0.4;
                }
                cursor: pointer;
            }
            padding: 10px;
            a {
                font-size: 16px;
                font-family: MontSerrat, sans-serif;
                color: ${props => props.theme.background};
                font-weight: bold;
            }
        }
        .active {
            a {
                position: relative;
                width: 12px;
                &:after {
                    content: "";
                    position: absolute;
                    bottom: -2px;
                    left: -2px;
                    right: -2px;
                    height: 2px;
                    background-color: ${props => props.theme.primary};
                }
            }
        }
    }
`;

export const ReactPagination = styled(Pagination)``;

export const IconCircle = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 47px;
    width: 47px;
    border-radius: 50%;
    background-color: ${props => props.theme.primary};
    box-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.2);
    &:hover {
        div {
            opacity: 1;
        }
    }
`;

export const ArrowLeft = styled.div`
    height: 12px;
    width: 12px;
    border: ${props => `1px solid ${props.theme.letter}`};
    border-width: 0 3px 3px 0;
    opacity: 0.5;
    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
`;

export const ArrowRight = styled.div`
    height: 12px;
    width: 12px;
    border: ${props => `1px solid ${props.theme.letter}`};
    border-width: 0 3px 3px 0;
    opacity: 0.5;
    transform: rotate(-45deg);
    -webkit-transform: rotate(-45deg);
`;

export const CardContainer = styled.div`
    .tr-card {
        .hover-card {
            .header {
                .badge {
                    display: none;
                }
            }
        }
        width: 280px;
        margin: 0 0px 20px 0px !important;
        @media only screen and (max-width: 400px) {
            width: 300px !important;
        }

        .image-full {
            height: 278px;
            .file-image {
                background-size: 100%;
                background-repeat: no-repeat;
                background-position: center center;
            }
        }
    }
`;

export const BoxContainer = styled.div`
    margin-top: 0 !important;

    .alternar {
        display: flex;
        justify-content: space-between;
        .fa-grip-horizontal {
            color: ${props =>
                props.calendario ? "#7c7c7c" : props.theme.primary};
        }
        .fa-calendar {
            color: ${props =>
                props.calendario ? props.theme.primary : "#7c7c7c"};
            font-size: 23px;
        }
        .icons-pag {
            span {
                margin-right: 10px;
                &:hover {
                    color: ${props => darken(0.3, "#7c7c7c")};
                }
            }
        }
    }
    .tr-cursos-destaque {
        display: flex;
        margin-top: 20px;

        .main-curso {
            width: 100%;
            display: flex;
            flex-direction: column;
            main {
                margin-bottom: 20px;
            }
            .destaque {
                width: 100%;
                flex-basis: auto;
                justify-content: center;
                margin-top: 0px;
                .tr-card {
                    margin: 0 15px 20px 0px;
                }
            }
        }
    }
    .results {
        display: flex;
        align-items: center;
    }
    @media only screen and (max-width: 768px) {
        .alternar {
            display: flex;
            flex-direction: column;
        }
        .search-results-breadcumb {
            margin: 0 !important;
            padding: 0 20px !important;
        }
        .results {
            align-items: flex-start;
        }
        .order-grid {
            display: none !important;
        }
    }
`;

export const ButtonFilter = styled(Button)`
    display: flex;
    height: 56px;
    justify-content: space-between;
    background: ${props => `linear-gradient(0deg, #ef9300 0%, #ef7300 100%)`};
    color: #fff !important;
    padding: 12px !important;
    div {
        font-family: Montserrat;
        font-weight: bold;
        text-transform: initial;
    }
`;

export const FilterMobContainer = styled(Grid)`
    width: 100%;
    padding: 20px;
    background: #efefef;
    .MuiFormControl-marginNormal {
        margin: 0;
    }
    .MuiGrid-grid-sm-4 {
        display: flex;
        width: 100%;
        justify-content: flex-end;
    }
    @media only screen and (min-width: 769px) {
        display: none !important;
    }
`;
export const SearchDesktop = styled(Grid)`
    padding: 20px;
    background: #efefef;
    div {
        display: flex;
        align-items: center;
    }
    @media only screen and (max-width: 768px) {
        display: none !important;
    }
`;
