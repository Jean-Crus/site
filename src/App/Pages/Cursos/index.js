import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as CursoActions from "@store/modules/curso/actions";
import * as CartActions from "@store/modules/cart/actions";
import * as FilterActions from "@store/modules/filter/actions";

import { Icon, Grid } from "@material-ui/core";
import { bannerFull } from "@images";
import {
    NavFilter,
    CursosDestaque,
    BannerPage,
    TagFilter,
    Loading,
    Calendario,
    Newsletter,
    SelectDefault,
    ModalFilter,
    NoResults,
    SearchDefault
} from "@components";
import {
    Box,
    ReactPagination,
    ArrowLeft,
    IconCircle,
    ArrowRight,
    CardContainer,
    BoxContainer,
    ButtonFilter,
    FilterMobContainer,
    SearchDesktop
} from "./styles";

const Cursos = () => {
    const [calendario, setCalendario] = useState(false);
    const curso = useSelector(state => state.curso.data);
    const loading = useSelector(state => state.curso.loading);
    const pagination = useSelector(state => state.curso.pagination);
    const dispatch = useDispatch();
    const selected = useSelector(state => state.filterCurso.selected);
    const historyFilter = useSelector(state => state.filterCurso.historyFilter);

    useEffect(() => {
        dispatch(CursoActions.getCursosPrincipalRequest());
    }, [dispatch]);

    const addCurso = id => {
        dispatch(CartActions.addToCartRequest(id));
    };

    const handlePage = pageNumber => {
        dispatch(CursoActions.getCursosPrincipalRequest(pageNumber));
        window.scrollTo(0, 300);
    };

    // const handleCalendar = () => {
    //     setCalendario(!calendario);
    // };
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    return (
        <>
            {/* Listagem de cursos */}
            <div className="tr-header-course">
                <BannerPage title="Cursos" banner={bannerFull} />
            </div>

            {/* Lista de Cursos(cards) */}
            <BoxContainer className="tr-list-course" calendario={calendario}>
                <SearchDesktop container justify="center" className="mb-2 ">
                    <Grid item xs={6} sm={6} md={6}>
                        <SearchDefault
                            label="Pesquise pelo curso"
                            onSubmit={value =>
                                dispatch(
                                    FilterActions.getNameFilterRequest(value)
                                )
                            }
                        />
                    </Grid>
                </SearchDesktop>
                <FilterMobContainer container className="mb-3">
                    <Grid item xs={12} sm={12} md={12} className="mb-3">
                        <SearchDefault
                            label="Pesquise pelo curso"
                            onSubmit={value =>
                                dispatch(
                                    FilterActions.getNameFilterRequest(value)
                                )
                            }
                        />
                    </Grid>
                    <Grid item xs={8} sm={8} md={8}>
                        <SelectDefault
                            label="Ordenar por"
                            value={historyFilter.order}
                            onChange={event => {
                                dispatch(
                                    FilterActions.orderFilterRequest(
                                        event.target.value
                                    )
                                );
                            }}
                            campos={[
                                {
                                    id: "asc",
                                    name: "Mais novos",
                                    checked: false
                                },
                                {
                                    id: "desc",
                                    name: "Mais antigos",
                                    checked: false
                                }
                            ]}
                        />
                    </Grid>
                    <Grid item xs={4} sm={4} md={4}>
                        <ButtonFilter onClick={handleClickOpen}>
                            <div>Filtro</div>
                            <Icon className="fas fa-filter" />
                        </ButtonFilter>
                    </Grid>
                    <ModalFilter open={open} handleClose={handleClose} />
                </FilterMobContainer>
                <div className="search-results-breadcumb container">
                    {/* {calendario ? (
                        <>
                            <div className="alternar">
                                <div className="text-pag">
                                    Mostrando {pagination.to} de{' '}
                                    {pagination.total} resultados
                                </div>
                                <div className="icons-pag">
                                    <Icon
                                        className="fas fa-grip-horizontal"
                                        onClick={handleCalendar}
                                    />
                                    <Icon
                                        className="far fa-calendar"
                                        onClick={handleCalendar}
                                    />
                                </div>
                            </div>
                        </>
                    ) : ( */}
                    <>
                        <div className="alternar">
                            {!loading && (
                                <Grid className="text-pag" container>
                                    <Grid
                                        item
                                        xs={8}
                                        sm={8}
                                        md={8}
                                        className="results"
                                    >
                                        {pagination.total > 0
                                            ? `Mostrando ${pagination.to} de ${pagination.total}
                                resultados`
                                            : `Sem resultados para sua busca`}
                                    </Grid>
                                    <Grid
                                        item
                                        xs={4}
                                        sm={4}
                                        md={4}
                                        className="order-grid"
                                    >
                                        <SelectDefault
                                            label="Ordenar por"
                                            value={historyFilter.order}
                                            onChange={event => {
                                                dispatch(
                                                    FilterActions.orderFilterRequest(
                                                        event.target.value
                                                    )
                                                );
                                            }}
                                            campos={[
                                                {
                                                    id: "asc",
                                                    name: "Mais novos",
                                                    checked: false
                                                },
                                                {
                                                    id: "desc",
                                                    name: "Mais antigos",
                                                    checked: false
                                                }
                                            ]}
                                        />
                                    </Grid>
                                </Grid>
                            )}
                            {/* <div className="icons-pag">
                                <Icon
                                    className="fas fa-grip-horizontal"
                                    onClick={handleCalendar}
                                />
                                <Icon
                                    className="far fa-calendar"
                                    onClick={handleCalendar}
                                />
                            </div> */}
                        </div>
                    </>
                </div>

                <div className="tr-cursos-destaque container">
                    {/* {calendario ? (
                        <Calendario />
                    ) : ( */}

                    <NavFilter />
                    <div className="main-curso">
                        <TagFilter
                            selected={selected}
                            onClick={(filter, idFilter) =>
                                dispatch(
                                    FilterActions.removeFilterRequest(
                                        filter,
                                        idFilter
                                    )
                                )
                            }
                        />

                        {loading ? (
                            <Loading />
                        ) : (
                            <CardContainer
                                className="destaque"
                                id="cursos-destaque"
                            >
                                {curso.map(item => {
                                    return (
                                        <CursosDestaque
                                            onClick={() => addCurso(item.id)}
                                            link={item.link}
                                            image={item.cover}
                                            priceFormatted={item.priceFormatted}
                                            price={item.product.price}
                                            salePrice={item.product.sale_price}
                                            category={item.category}
                                            introduction={item.introduction}
                                            description={item.description}
                                            salePriceFormatted={
                                                item.salePriceFormatted
                                            }
                                            title={item.name}
                                            key={item.id}
                                            id={item.id}
                                            label={item.label}
                                            hours={item.learning_time}
                                            modules={item.modules}
                                        />
                                    );
                                })}
                            </CardContainer>
                        )}
                        {pagination.total < 1 && <NoResults />}
                    </div>
                </div>
                {/* <ListaCards /> */}
            </BoxContainer>

            {/* paginação */}
            {calendario ? (
                ""
            ) : (
                <Box container justify="center">
                    <ReactPagination
                        hideDisabled
                        hideFirstLastPages
                        prevPageText={
                            <IconCircle>
                                <ArrowLeft />
                            </IconCircle>
                        }
                        nextPageText={
                            <IconCircle>
                                <ArrowRight />
                            </IconCircle>
                        }
                        activePage={pagination.current_page}
                        itemsCountPerPage={pagination.per_page}
                        totalItemsCount={pagination.total}
                        pageRangeDisplayed={pagination.per_page}
                        onChange={handlePage}
                    />
                </Box>
            )}

            {/* NEWSLETTER */}
            {/* <News className="tr-newsletter purple mt-5">
                <Newsletter />
            </News> */}

            {/* Modal de Espera */}
            {/* <ModalListaEspera /> */}
        </>
    );
};

export default Cursos;
