import React from 'react';
import { bannerFull } from '@images';

import {
	BannerPage,
	FiltroEventos,
	ModalListaEspera,
	Newsletter,
	SingleEventos,
	ListEventos,
	CursosDestaque
} from '@components';

const Eventos = (props) => {
	
	return(
		<React.Fragment>
			{/* Header Eventos */}
			<div className="tr-header-course">
				<BannerPage title="Eventos" banner={bannerFull}/>
			</div>
			
			{/* Filtro de cursos */}
			<div className="tr-filtered-event">
				<FiltroEventos/>
			</div>
			
			<div className="tr-event-single container mt-5">
				<SingleEventos/>
			</div>
			
			
			{/* EVENTOS */}
			<div className="events-show-event">
				<div className="tr-event">
					<ListEventos/>
				</div>
			</div>
			
			{/* CURSOS EM DESTAQUE */}
			<div className="tr-cursos-destaque container pb-5 mb-5">
				<div className="title-1 size-50 black wh-700">
					Cursos em destaque
				</div>
				<CursosDestaque/>
			</div>

			{/* NEWSLETTER */}
			<div className="tr-newsletter purple mt-5">
				<Newsletter/>
			</div>
			

			{/* Modal de Espera */}
			<ModalListaEspera/>
		</React.Fragment>
	);	
}

export default Eventos;