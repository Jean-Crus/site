import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid, Icon } from "@material-ui/core";
import { Link } from "react-router-dom";
import { Spinner } from "react-bootstrap";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import * as UserActions from "@store/modules/user/actions";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import history from "@routes/history";
import { ButtonTriade, CardInstrutor } from "@components";
import ReactPlayer from "react-player";
import ReactHtmlParser from "react-html-parser";

import {
    CursoApresentacaoMobileHeader,
    CursoApresentacaoMobile,
    CursoApresentacaoMobileBody,
    IconStarBorder,
    IconAssignment,
    IconSchedule,
    Progress,
    ReactSlider,
    Modulos,
    IconCircle,
    CardVantagens,
    Lesson
} from "../styles";
import {
    Checked,
    CurrentClass,
    UnChecked
} from "../../../../components/Parts/CursoAndamento/NagivationAndamento/styles";

export const ApresentacaoMobile = ({ course, cursoParam, forward }) => {
    const dispatch = useDispatch();
    const loadingCourse = useSelector(state => state.user.loadingCourse);
    const curso = useSelector(state => state.user.course);
    const modules = useSelector(state => state.user.modules);
    const handleExpand = id => {
        dispatch(UserActions.expandList(id));
    };
    const check = (finished, active) => {
        const number = parseInt(course, 10);
        if (finished) {
            return <Checked />;
        }
        if (number === active) {
            return <CurrentClass />;
        }
        return <UnChecked color="disabled" />;
    };
    const settings = {
        customPaging() {
            return (
                <div className="dots">
                    <div className="circle" />
                </div>
            );
        },
        arrows: false,
        dots: true,
        // fade: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
        // autoplay: true,
        // autoplaySpeed: 3000,
    };
    return loadingCourse ? (
        <Grid container justify="center" className="mt-4 mb-4">
            <Spinner animation="border" role="status" />
        </Grid>
    ) : (
        <CursoApresentacaoMobile>
            <CursoApresentacaoMobileHeader>
                <div className="title">
                    <Link to="/conta">
                        <IconCircle small>
                            <i className="arrow left" />
                        </IconCircle>
                    </Link>
                    <span className="title">{curso.name}</span>
                </div>
                <div className="multi-tag">
                    <div>
                        <IconStarBorder />
                        <span>{curso.difficulty.name}</span>
                    </div>
                    <div>
                        <IconAssignment />
                        <span>
                            {curso.modules.length < 2
                                ? `${curso.modules.length} Módulo`
                                : `${curso.modules.length} Módulos`}
                        </span>
                    </div>
                    <div>
                        <IconSchedule />
                        <span>
                            {course.workload < 2
                                ? `${curso.workload} Hora`
                                : `${curso.workload} Horas`}
                        </span>
                    </div>
                </div>
                <div className="progress-line">
                    <Progress
                        variant="determinate"
                        value={curso.user_progress}
                    />
                    <small>{curso.user_progress}% completo</small>
                </div>
                <ButtonTriade
                    button="Modelo 02"
                    cor="Modelo 02"
                    className="mt-2"
                    letterColor="Modelo 02"
                    text="Continuar"
                    onClick={() => history.push(forward)}
                />
            </CursoApresentacaoMobileHeader>
            <CursoApresentacaoMobileBody>
                {!!curso.introduction && (
                    <>
                        <h2>Sobre o curso</h2>
                        {!!curso.url_video && (
                            <div className="img-video">
                                <ReactPlayer
                                    width="100%"
                                    url={curso.url_video}
                                    controls
                                />
                            </div>
                        )}
                        <small>{ReactHtmlParser(curso.introduction)}</small>
                    </>
                )}
                {!!curso.benefits && (
                    <>
                        <h2>Vantagens</h2>
                        <div className="vantagens">
                            <ReactSlider {...settings}>
                                {curso.benefits.map(vantagens => {
                                    return (
                                        <CardVantagens key={vantagens.id}>
                                            <div>
                                                <Icon
                                                    className={vantagens.icon}
                                                    fontSize="large"
                                                />
                                            </div>
                                            <div>
                                                <span className="qualidade">
                                                    {vantagens.name}
                                                </span>
                                                <small>
                                                    {vantagens.description}
                                                </small>
                                            </div>
                                        </CardVantagens>
                                    );
                                })}
                            </ReactSlider>
                        </div>
                    </>
                )}
                {!!curso.modules && (
                    <>
                        <h2>Módulos</h2>
                        <div className="modulos">
                            {modules.map((item, index) => {
                                return (
                                    <Modulos
                                        key={item.id}
                                        onClick={() => handleExpand(item.id)}
                                        active={item.active}
                                    >
                                        <header>
                                            <span>
                                                {index + 1}. {item.name}
                                            </span>
                                            {item.active ? (
                                                <ExpandLessIcon />
                                            ) : (
                                                <ExpandMoreIcon />
                                            )}
                                        </header>
                                        {item.lessons.map(lesson => {
                                            return (
                                                <div key={lesson.id}>
                                                    <Link
                                                        to={`/curso-andamento/${cursoParam}/${lesson.id}`}
                                                    >
                                                        {check(
                                                            lesson.finished,
                                                            lesson.id
                                                        )}

                                                        <Lesson
                                                            finished={
                                                                lesson.finished
                                                            }
                                                        >
                                                            {lesson.name}
                                                        </Lesson>
                                                    </Link>
                                                </div>
                                            );
                                        })}
                                    </Modulos>
                                );
                            })}
                        </div>
                    </>
                )}

                {!!curso.bonus && (
                    <>
                        <h2>Bonus</h2>
                        <div className="modulos">
                            {modules.map(item => {
                                return (
                                    <Modulos
                                        key={item.id}
                                        onClick={() => handleExpand(item.id)}
                                        active={item.active}
                                        bonus="true"
                                    >
                                        <header>
                                            <span>Pedido #121212</span>
                                            {item.active ? (
                                                <ExpandLessIcon />
                                            ) : (
                                                <ExpandMoreIcon />
                                            )}
                                        </header>
                                        <div>
                                            <RadioButtonCheckedIcon />
                                            <span>Nome da aula(6hrs)</span>
                                        </div>
                                    </Modulos>
                                );
                            })}
                        </div>
                    </>
                )}
                {!!curso.responsable && (
                    <>
                        <h2>Sobre o instrutor</h2>
                        <div className="instrutor-card">
                            {course.responsable.map(instructor => {
                                return (
                                    <CardInstrutor
                                        key={instructor.id}
                                        instrutor={instructor}
                                    />
                                );
                            })}
                        </div>
                    </>
                )}
            </CursoApresentacaoMobileBody>
        </CursoApresentacaoMobile>
    );
};
