import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import YouTubeIcon from "@material-ui/icons/YouTube";
import FacebookIcon from "@material-ui/icons/Facebook";
import * as UserActions from "@store/modules/user/actions";
import { Spinner } from "react-bootstrap";
import { Grid } from "@material-ui/core";
import {
    HeaderCursoAndamento,
    InstrutorCurso,
    ModulosCursoDetail,
    PerguntasFrequentes,
    SobreCursoAberto,
    CardInstrutor,
    VantagensCurso,
    ButtonTriade
} from "@components";
import style from "./style";

import { Container, InstrutorMobile } from "./styles";
import { ApresentacaoMobile } from "./CursoApresentacaoMobile";

function CursoApresentacao({
    match: {
        params: { curso }
    }
}) {
    const dispatch = useDispatch();
    const loadingCourse = useSelector(state => state.user.loadingCourse);
    const course = useSelector(state => state.user.course);
    useEffect(() => {
        dispatch(UserActions.getOneCourseRequest(curso));
    }, [curso, dispatch]);

    return loadingCourse ? (
        <Grid container justify="center" className="mt-4 mb-4">
            <Spinner animation="border" role="status" />
        </Grid>
    ) : (
        <>
            <Container className="mb-5 pb-5">
                {/* <!-- Header curso cursando --> */}
                <div className="tr-header-cursando">
                    <HeaderCursoAndamento
                        back="/conta"
                        forward={`/curso-andamento/${curso}/${course.user_last_lesson}`}
                    />
                </div>

                <div className="container mt-5 main">
                    <div className="row" style={style}>
                        <div className="col-lg-9">
                            <div className="tr-sobreo-curso">
                                <div className="instrutor-card">
                                    <SobreCursoAberto
                                        introduction={course}
                                        title="Sobre o curso"
                                    />
                                </div>
                            </div>
                            <div className="tr-vantagens-curso">
                                <div className="instrutor-card">
                                    <VantagensCurso
                                        vantagens={course.benefits}
                                        title="Vantagens"
                                    />
                                </div>
                            </div>
                            <ModulosCursoDetail
                                modules={course.modules}
                                curso={curso}
                            />
                            {/* <div className="tr-bonus-curso">
                            <BonusCurso />
                        </div> */}
                            {course.responsable.length > 0 && (
                                <div className="instrutor-card">
                                    <div className="sobre-instrutor">
                                        Sobre o instrutor
                                    </div>
                                    {course.responsable.map(instructor => {
                                        return (
                                            <CardInstrutor
                                                key={instructor.id}
                                                instrutor={instructor}
                                            />
                                        );
                                    })}
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </Container>
            <ApresentacaoMobile
                course={course}
                cursoParam={curso}
                back="/conta"
                forward={`/curso-andamento/${curso}/${course.user_last_lesson}`}
            />
        </>
    );
}

export default CursoApresentacao;
