import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as CursoDetailActions from "@store/modules/cursoDetail/actions";
import { Spinner } from "react-bootstrap";
import history from "@routes/history";
import { Grid } from "@material-ui/core";
import {
    Link,
    Element,
    Events,
    animateScroll as scroll,
    scrollSpy,
    scroller
} from "react-scroll";

import {
    HeaderCursosAberto,
    InstrutorCurso,
    ModulosCurso,
    PerguntasFrequentes,
    SobreCursoAberto,
    VantagensCurso,
    NagivationCourse,
    HeaderCursoAbertoMobile,
    ListaCurso
} from "@components";
import { Container, Main } from "./styles";

function CursoAberto({
    match: {
        params: { id }
    }
}) {
    const dispatch = useDispatch();
    useEffect(() => {
        function loadCursoDetail(curso) {
            dispatch(CursoDetailActions.getCursoDetailRequest(curso));
        }
        loadCursoDetail(id);
    }, [dispatch, id]);

    const cursoDetail = useSelector(state => state.cursoDetail);

    const state = {
        isAberto: true
    };
    return cursoDetail.loading ? (
        <Grid container justify="center" style={{ margin: "60px 0" }}>
            <Spinner animation="border" role="status" />
        </Grid>
    ) : (
        <div>
            {/* <!-- Header curso aberto --> */}
            <Container className="tr-header-course-open">
                <HeaderCursosAberto
                    title={cursoDetail.data.data.name}
                    category={cursoDetail.data.data.category}
                    hours={cursoDetail.data.data.learning_time}
                    modules={cursoDetail.data.data.modules}
                    isAberto={state.isAberto}
                    id={id}
                    difficulty={cursoDetail.data.data.difficulty}
                    lessonAddress={cursoDetail.data.data.lesson_address}
                />
            </Container>
            <HeaderCursoAbertoMobile
                title={cursoDetail.data.data.name}
                category={cursoDetail.data.data.category}
                hours={cursoDetail.data.data.learning_time}
                modules={cursoDetail.data.data.modules}
                isAberto={state.isAberto}
                id={id}
                difficulty={cursoDetail.data.data.difficulty}
                lessonAddress={cursoDetail.data.data.lesson_address}
            />

            <Main className="container mt-5 main">
                <div className="row">
                    <div className="col-lg-3">
                        {/* <!-- Header curso aberto --> */}

                        <div
                            className={
                                state.isAberto
                                    ? "tr-nagivation-course floatingNavigation"
                                    : "tr-nagivation-course-close floatingNavigation"
                            }
                            style={{
                                position: "sticky",
                                marginBottom: "30px",
                                top: "120px"
                            }}
                        >
                            <NagivationCourse
                                isAberto={state.isAberto}
                                price={cursoDetail.data.priceFormatted}
                                id={id}
                                cursoDetail={cursoDetail.data.data}
                            />
                        </div>
                    </div>
                    <div className="col-lg-9">
                        <Element
                            name="test1"
                            className="element tr-sobreo-curso"
                        >
                            <SobreCursoAberto
                                introduction={cursoDetail.data.data}
                                title="Sobre o curso"
                                address={cursoDetail.data.data.address}
                            />
                        </Element>
                        <Element
                            className="tr-vantagens-curso test2"
                            name="test2"
                        >
                            <VantagensCurso
                                vantagens={cursoDetail.data.data.benefits}
                                title="Vantagens"
                            />
                        </Element>
                        <Element
                            className="tr-modulos-curso element"
                            name="test3"
                        >
                            <ModulosCurso />
                        </Element>
                        {/* <div className="tr-bonus-curso">
                            <BonusCurso />
                        </div> */}
                        <Element
                            className="tr-instrutor-curso arrow-adjust mt-5 element"
                            name="test4"
                        >
                            <InstrutorCurso
                                onClick={() => history.push("/sobre")}
                            />
                        </Element>
                        <Element
                            className="tr-perguntas-frequentes-curso pb-5 element"
                            name="test5"
                        >
                            <PerguntasFrequentes
                                perguntas={
                                    cursoDetail.data.data.frequent_questions
                                }
                            />
                        </Element>
                    </div>
                </div>
            </Main>

            {/* <!-- CURSOS EM DESTAQUE --> */}
            <ListaCurso relacionados={id} title="Cursos relacionados" />
            <Container className="tr-header-course-open">
                <HeaderCursosAberto
                    title={cursoDetail.data.data.name}
                    category={cursoDetail.data.data.category}
                    hours={cursoDetail.data.data.learning_time}
                    modules={cursoDetail.data.data.modules}
                    isAberto={state.isAberto}
                    difficulty={cursoDetail.data.data.difficulty}
                    id={id}
                />
            </Container>
            <HeaderCursoAbertoMobile
                title={cursoDetail.data.data.name}
                category={cursoDetail.data.data.category}
                hours={cursoDetail.data.data.learning_time}
                modules={cursoDetail.data.data.modules}
                isAberto={state.isAberto}
                id={id}
                difficulty={cursoDetail.data.data.difficulty}
            />
        </div>
    );
}

export default CursoAberto;
