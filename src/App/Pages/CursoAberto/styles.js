import styled from "styled-components";

export const Container = styled.div`
    background: linear-gradient(0deg, #012a50 0%, #0058a8 100%);

    @media only screen and (max-width: 999px) {
        display: none;
    }
`;

export const Main = styled.div`
    @media only screen and (max-width: 999px) {
        .row {
            display: flex;
            justify-content: center;
        }
        .col-lg-3 {
            display: none;
        }
    }
`;
