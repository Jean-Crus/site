import styled from "styled-components";
import ReactPlayer from "react-player";
import { Modal } from "@material-ui/core";

export const Player = styled(ReactPlayer)``;

export const ModalPlayer = styled(Modal)`
    display: flex;
    align-items: center;
    justify-content: center;
    /* top: 25%; */
    margin: auto;
`;

export const PaymentPayableLink = styled.div`
    border: 4px solid green;
    width: 100%;
    padding: 20px;
    font-size: 20px;
    font-weight: bold;
    color: green;
    margin: 10px 0px;
    transition: 0.4s;
    &:hover {
        background: green;
        color: white;
    }
`;

export const PaymentMsg = styled.div`
    background: ${props => props.background};
    width: 100%;
    padding: 20px;
    font-size: 20px;
    font-weight: bold;
    color: white;
    margin: 10px 0px;
`;
