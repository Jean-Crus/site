import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { Modal, Grid } from "@material-ui/core";
import ReactPlayer from "react-player";
import history from "@routes/history";
import * as CheckoutActions from "@store/modules/checkout/actions";
import Moment from "react-moment";
import { formatPrice } from "@util/format";
import { Spinner } from "react-bootstrap";
import { CursosDestaque, ButtonTriade } from "@components";
import { ModalPlayer, Player, PaymentMsg, PaymentPayableLink } from "./styles";

const Thankyou = ({
    match: {
        params: { id }
    }
}) => {
    const dispatch = useDispatch();
    const [open, setOpen] = useState(false);
    const handleOpen = () => {
        setOpen(!open);
    };
    useEffect(() => {
        dispatch(CheckoutActions.getThankuRequest(id));
        dispatch(CheckoutActions.emptyCart());
    }, [dispatch, id]);

    const ticket = useSelector(state => state.checkout.data);
    const total = useSelector(state => formatPrice(state.checkout.data.total));
    const loading = useSelector(state => state.checkout.loading);

    return loading ? (
        <Grid container justify="center" className="mt-4 mb-4">
            <Spinner animation="border" role="status" />
        </Grid>
    ) : (
        <div>
            <div className="container mt-5 text-center thankyou-card mb-5">
                <div className="title-1 size-50 wh-600 black">
                    Pedido <b>#{!loading && ticket.id}</b> realizado com
                    sucesso!
                </div>
                <div className="container content-details">
                    <div className="row">
                        <div className="col-lg-4">
                            <small>Data da compra:</small>
                            <p>
                                <Moment format="YYYY/MM/DD">
                                    {!loading && ticket.invoice_date}
                                </Moment>
                            </p>
                        </div>
                        <div className="col-lg-4">
                            <small>Total:</small>
                            <p>{!loading && total}</p>
                        </div>
                        <div className="col-lg-4">
                            <small>Método de pagamento:</small>
                            <p>{!loading && ticket.payment_method.name}</p>
                        </div>
                    </div>
                    {ticket.status.message && (
                        <div className="row">
                            <div className="col-lg-12">
                                <p
                                    className="payment-message"
                                    style={{
                                        backgroundColor: ticket.status.color
                                    }}
                                >
                                    {ticket.status.message}
                                </p>
                            </div>
                        </div>
                    )}
                </div>
                <div className="button-next new-flow mt-5">
                    {!!ticket.status && !ticket.gateway_payable_link && (
                        <PaymentMsg background={ticket.status.color}>
                            {ticket.status.message}
                        </PaymentMsg>
                    )}
                    {!!ticket.gateway_payable_link && (
                        <a
                            href={ticket.gateway_payable_link}
                            target="_blank"
                            rel="noreferrer noopener"
                        >
                            <PaymentPayableLink
                                background={ticket.status.color}
                            >
                                Clique aqui para gerar o boleto{" "}
                                <i className="far fa-arrow-alt-circle-right fa-w-16" />
                            </PaymentPayableLink>
                        </a>
                    )}
                    {/* <h2 className="title-1 size-50 wh-600 black">
                        Acessando a plataforma pela primeira vez?
                    </h2> */}
                    {/* <ButtonTriade
                        button="secondary"
                        text="Acesse sua conta"
                        // onClick={handleOpen}
                    /> */}
                    <Link to="/conta" className="tr-link">
                        Comece a estudar agora!
                    </Link>
                </div>
                {/* <ModalPlayer open={open} onClose={handleOpen}>
                    <Player url="https://youtu.be/Ij3u77NrCaY" />
                </ModalPlayer> */}
            </div>

            {/* <!-- CURSOS EM DESTAQUE --> */}
        </div>
    );
};

export default Thankyou;
