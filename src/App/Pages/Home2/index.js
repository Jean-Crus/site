import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import {
  BannerFull,
  CardAbout,
  Depoimentos,
  CarrouselFull,
  Benefits,
  EventosDate,
  ContatoArea,
  NewsletterPurple
} from "@components";

const dataCarrousel = [
  {
    title: "Aprenda algo novo de onde e ",
    title2: "quando quiser.",
    image: "http://localhost:3000/images/banner1.jpg",
    link: "http://localhost:3000/cursos"
  },
  {
    title: "Titulo 2",
    title2: "",
    image: "http://localhost:3000/images/banner1.jpg",
    link: "http://localhost:3000/cursos"
  }
];

const dataAbout = [
  {
    topTitle: "Sobre o Cursinho",
    title: "Aprenda algo novo de onde ",
    title2: "e quando quiser.",
    link: "http://localhost:3000/cursos",
    type: "video",
    linkMidia: "http://localhost:3000/video/video.mp4"
  }
];

const dataDepoimentos = [
  {
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea totam dolor inventore ratione nobis, odit numquam ipsum maxime ducimus earum ipsa laudantium, soluta debitis veniam velit similique blanditiis iusto voluptatibus!",
    name: "Wellington Trojan",
    country: "Brazil"
  },
  {
    content:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea totam dolor inventore ratione nobis, odit numquam ipsum maxime ducimus earum ipsa laudantium, soluta debitis veniam velit similique blanditiis iusto voluptatibus!",
    name: "Marcelo Quintanilha",
    country: "Pinhais"
  }
];

const dataCarrouselFull = {
  quant: 3,
  title: "Cursos em Destaque",
  itens: [
    {
      categories: [
        {
          name: "Design",
          url: "http://localhost:3000/"
        }
      ],
      image: "http://localhost:3000/images/product-01.png",
      title: "Curso de Lorem ipsum Avançado Nível Experd Master 01",
      rating: 3,
      productClass: "investimento",
      price: 150.55
    },
    {
      categories: [
        {
          name: "Design",
          url: "http://localhost:3000/"
        },
        {
          name: "UX",
          url: "http://localhost:3000/"
        }
      ],
      image: "http://localhost:3000/images/product-02.png",
      title: "Curso de Lorem ipsum Avançado Nível Experd Master 02",
      rating: 4,
      productClass: "acessibilidade",
      price: 50.55
    },
    {
      categories: [
        {
          name: "Design",
          url: "http://localhost:3000/"
        }
      ],
      image: "http://localhost:3000/images/product-03.png",
      title: "Curso de Lorem ipsum Avançado Nível Experd Master 03",
      rating: 5,
      productClass: "tecnologia",
      price: 250.55
    },
    {
      categories: [
        {
          name: "Design",
          url: "http://localhost:3000/"
        }
      ],
      image: "http://localhost:3000/images/product-01.png",
      title: "Curso de Lorem ipsum Avançado Nível Experd Master 01",
      rating: 3,
      productClass: "investimento",
      price: 150.55
    },
    {
      categories: [
        {
          name: "Design",
          url: "http://localhost:3000/"
        }
      ],
      image: "http://localhost:3000/images/product-02.png",
      title: "Curso de Lorem ipsum Avançado Nível Experd Master 02",
      rating: 4,
      productClass: "acessibilidade",
      price: 50.55
    },
    {
      categories: [
        {
          name: "Design",
          url: "http://localhost:3000/"
        }
      ],
      image: "http://localhost:3000/images/product-03.png",
      title: "Curso de Lorem ipsum Avançado Nível Experd Master 03",
      rating: 0,
      productClass: "tecnologia",
      price: 250.55
    }
  ]
};

const dataBenefits = {
  title: "Vantagens",
  background: "http://localhost:3000/images/vantagens.jpg",
  itens: [
    {
      image: "http://localhost:3000/images/icons/icon-01.svg",
      title: "Material 100% online.",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales magna pus semper  lorem.."
    },
    {
      image: "http://localhost:3000/images/icons/icon-02.svg",
      title: "Fórum Ipsum",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales magna pus semper  lorem.."
    },
    {
      image: "http://localhost:3000/images/icons/icon-03.svg",
      title: "Fórum IpsumLorem ipsum dolor",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales magna pus semper  lorem.."
    }
  ]
};

const dataEventos = {
  title: "Eventos",
  itens: [
    {
      day: "20",
      month: "FEV",
      title: "Lorem ipsum dolor asit amet utito tempo",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. sodales magna purus, semper malesuada mi condimentum utito tempo.",
      image: "http://localhost:3000/images/event-01.png"
    },
    {
      day: "21",
      month: "FEV",
      title: "Lorem ipsum dolor asit amet utito tempo",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. sodales magna purus, semper malesuada mi condimentum utito tempo.",
      image: "http://localhost:3000/images/event-02.png"
    },
    {
      day: "22",
      month: "FEV",
      title: "Lorem ipsum dolor asit amet utito tempo",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. sodales magna purus, semper malesuada mi condimentum utito tempo.",
      image: "http://localhost:3000/images/event-03.png"
    }
  ]
};

const dataContato = {
  title: "Contato",
  subTitle: "Aprenda algo novo de onde ",
  subTitle2: "e quando quiser.",
  description:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. sodales magna purus, semper malesuada mi condimentum vitae utito tempo.",
  image: "http://localhost:3000/images/macbook.png"
};

const dataNewsletter = {
  title: "Quer receber notícias ",
  subTitle: "em primeira mão?",
  background: "http://localhost:3000/images/vantagens.jpg"
};

const Home2 = props => {
  return (
    <React.Fragment>
      {/* CARROUSEL INICIAL */}
      <BannerFull content={dataCarrousel} />

      {/* CARD ABOUT */}
      <div className="container d-flex justify-content-center">
        <div className="col-sm-10">
          <div className="card-about-home">
            <CardAbout content={dataAbout} />
          </div>
        </div>
      </div>

      {/* Depoimentos */}
      <Depoimentos content={dataDepoimentos} />

      {/* Cursos Destaque */}
      <CarrouselFull content={dataCarrouselFull} />

      {/* Vantagens */}
      <Benefits content={dataBenefits} />

      {/* Eventos */}
      <div className="container d-flex justify-content-center">
        <div className="col-sm-10">
          <EventosDate content={dataEventos} />
        </div>
      </div>

      {/* Contato */}
      <ContatoArea content={dataContato} />

      {/* Newsletter Purple */}
      <NewsletterPurple content={dataNewsletter} />
    </React.Fragment>
  );
};

export default Home2;
