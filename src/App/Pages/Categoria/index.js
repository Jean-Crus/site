import React from 'react';
import { bannerFull, product1 } from '@images';
import { Link } from 'react-router-dom';

import { 
    FiltroCursos, 
    FiltroCursosSelected, 
    BannerPage,
    CardProduct,
    Pagination
} from '@components';

const Categoria = (props, state) => {

    state = {
        categorias:[
            {},{},{},{},{},{},{},{},{},{},{},{}
        ]
    }
	
	return(
		<React.Fragment>
			
            <div className="tr-header-course">
                <BannerPage title="Cursos" banner={bannerFull} />
            </div>
            
            <div className="tr-filtered-course">
                <FiltroCursos/>
            </div>

            
            <div className="tr-filtered-course-selected">
                <FiltroCursosSelected/>
            </div>

            <div className="categoria-interna">
                <div className="container mt-5 mb-5">
                    <div className="row">
                        <div className="col-sm-3">
                            <div className="sidebar-style">
                                <ul>
                                    <li className="active"><Link to="#">Categoria 1 <span className="count-alert">(4)</span></Link></li>
                                    <li><Link to="#">Categoria 2 <span className="count-alert">(4)</span></Link></li>
                                    <li><Link to="#">Categoria 3</Link></li>
                                    <li><Link to="#">Categoria 4</Link></li>
                                    <li><Link to="#">Categoria 5</Link></li>
                                    <li><Link to="#">Categoria 6</Link></li>
                                    <li><Link to="#">Categoria 7</Link></li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-sm-9">
                            <div className="categoria-title">
                                <h1 className="title-1 black">Categoria Infusores <span>(40)</span></h1>
                                <p className="description-1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet neque labore voluptas sequi culpa laudantium ab praesentium tempora optio suscipit impedit iste quae aut a ea, minus ipsam, odit vel?</p>
                            </div>
                            <div className="row">
                                {
                                    state.categorias.map((item, index) => ( 
                                    <div className="col-sm-4">
                                        <CardProduct
                                            image={product1}
                                            title="SACETT™ Suction Above Cuff Endotracheal Tube"
                                            price="250,00"
                                            salePrice="99,00"
                                            discount="30%"
                                            newProd={true}
                                        />
                                    </div>
                                    ))
                                }	
                            </div>
                            <div className="tr-pagination mt-5 pb-5">
                                <Pagination/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</React.Fragment>
	);	
}

export default Categoria;