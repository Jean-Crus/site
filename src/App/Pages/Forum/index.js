import React from 'react';
import { ButtonTriade } from '@components';
import { compress } from '@images';
import { Link } from 'react-router-dom';

const ForumPage = (props) => {
	
	return(
		<div className="mb-5 pb-5">

            {/* <!-- Header curso cursando --> */}
            <div className="tr-header-cursando">
                <div className="content container">
                    <div className="row mx-0">
                        <div className="col-lg-9 tr-flex-center">
                            <ButtonTriade button="circle-primary"/>
                            <div className="information-course">
                                <div className="title-1 size-50 ff-24">
                                    Dificuldades no módulo 3. Não consigo fazer a 4
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 tr-flex-center px-0 mt-2">
                            <div className="button-continue">
                                <ButtonTriade to="/fale-com-tutor-interna" button="secondary" text="Responder"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container mt-5 pb-5">
                <div className="row d-flex justify-content-center align-items-center flex-column">
                    <div className="col-lg-9">
                        <div className="tr-forum-pergunta">
                            <div className="card">
                                <div className="spacing">
                                    <div className="content">
                                        <div className="author">
                                            <div className="avatar">
                                                <img src={compress} />
                                            </div>
                                            <div className="description">
                                                <div className="name">
                                                    Matheus kindrazki
                                                </div>
                                                <div className="post">
                                                    Postou 30/07/2018
                                                </div>
                                            </div>
                                        </div>
                                        <div className="question">
                                            <div className="description-2 mt-4">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                Duis nec convallis ligula. Quisque vel diam ligula. 
                                                t quam magna, facilisis nec bibendum id, semper ac turpis. 
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                                Duis nec convallis ligula. Quisque vel diam ligula. Ut quam magna, 
                                                facilisis nec bibendum id, semper ac turpis. Lorem ipsum dolor sit amet, 
                                                consectetur adipiscing elit. 
                                            </div>
                                        </div>
                                        <div className="attachment">
                                            <Link to="#" download>
                                                <span className="icon-download"></span>
                                                ArquivoDessaAula.pdf
                                                <i className="closed"></i>
                                            </Link>
                                            <Link to="#" download>
                                                <span className="icon-download"></span>
                                                Arquivo_matheus_kindrazki_postou.pdf
                                                <i className="closed"></i>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="footer">
                                    <div className="report">
                                        <Link to="#">
                                            Reportar
                                        </Link>
                                    </div>
                                    <div className="response">
                                        <ButtonTriade button="secondary" className="ff-14" text="Responder"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tr-forum-resposta mt-4">
                            <i className="arrow-right"></i>
                            <div className="card">
                                <div className="spacing">
                                    <div className="content">
                                        <div className="author">
                                            <div className="avatar">
                                                <img src={compress} />
                                            </div>
                                            <div className="description">
                                                <div className="name">
                                                    Matheus kindrazki
                                                </div>
                                                <div className="post">
                                                    Postou 30/07/2018
                                                </div>
                                            </div>
                                        </div>
                                        <div className="question">
                                            <div className="description-2 mt-4">
                                                Lorem ipsum dolor sit amet, consectetur 
                                                adipiscing elit. Duis nec convallis ligula. 
                                                Quisque vel diam ligula. Ut quam magna, 
                                                facilisis nec bibendum id, semper ac turpis. Lorem ipsum dolor sit amet,
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="footer">
                                    <div className="report">
                                        <Link to="#">
                                            Reportar
                                        </Link>
                                    </div>
                                    <div className="response">
                                        <ButtonTriade button="primary" className="ff-14" text="Responder"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	);	
}

export default ForumPage;