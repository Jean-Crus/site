import React from 'react';
import { Pagination, Newsletter } from '@components';
import { curso1 } from '@images';

const SearchResults = (props) => {

    const style = {
        curso1: {
            backgroundImage: "url('" + curso1 + "')"
        }
    }
	
	return(
		<React.Fragment>

            <div className="tr-header-searchResults">
                <div className="content container">
                    <div className="row mx-0 tr-flex-center">
                        <div className="col-lg-6">
                            <div className="title-2 ff-16 wh-600">Resultado da pesquisa</div>
                            <div className="form-group label-animate">
                                <input type="text" className="form-control" value="Curso de Medicina"  placeholder="Pesquise"/>
                                <label>Pesquise</label>
                                <button type="submit" className='search'></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="tr-card-searchResults container">
                <div className="row d-flex justify-content-center align-items-center flex-column">
                    <div className="col-lg-10 cards">
                        <div className="content">
                            <div className="item">
                                <div className="image">
                                    <span className="image-full" style={style.curso1}></span>
                                </div>
                                <div className="description">
                                    <div className="title-2 black ff-18 wh-500">
                                        Exura vita amex dolem pratitus <b>pesquisa lorem ipsum</b> amet utyityo tempo
                                    </div>
                                    <div className="description-3">
                                        Descrição do conteúdo, se pertinente vai aqui. Nada muito extenso, mas longo o suficiente para ser percebido
                                    </div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="image">
                                    <span className="image-full" style={style.curso1}></span>
                                </div>
                                <div className="description">
                                    <div className="title-2 black ff-18 wh-500">
                                        Exura vita amex dolem pratitus <b>pesquisa lorem ipsum</b> amet utyityo tempo
                                    </div>
                                    <div className="description-3">
                                        Descrição do conteúdo, se pertinente vai aqui. Nada muito extenso, mas longo o suficiente para ser percebido
                                    </div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="image">
                                    <span className="image-full" style={style.curso1}></span>
                                </div>
                                <div className="description">
                                    <div className="title-2 black ff-18 wh-500">
                                        Exura vita amex dolem pratitus <b>pesquisa lorem ipsum</b> amet utyityo tempo
                                    </div>
                                    <div className="description-3">
                                        Descrição do conteúdo, se pertinente vai aqui. Nada muito extenso, mas longo o suficiente para ser percebido
                                    </div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="image">
                                    <span className="image-full" style={style.curso1}></span>
                                </div>
                                <div className="description">
                                    <div className="title-2 black ff-18 wh-500">
                                        Exura vita amex dolem pratitus <b>pesquisa lorem ipsum</b> amet utyityo tempo
                                    </div>
                                    <div className="description-3">
                                        Descrição do conteúdo, se pertinente vai aqui. Nada muito extenso, mas longo o suficiente para ser percebido
                                    </div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="image">
                                    <span className="image-full" style={style.curso1}></span>
                                </div>
                                <div className="description">
                                    <div className="title-2 black ff-18 wh-500">
                                        Exura vita amex dolem pratitus <b>pesquisa lorem ipsum</b> amet utyityo tempo
                                    </div>
                                    <div className="description-3">
                                        Descrição do conteúdo, se pertinente vai aqui. Nada muito extenso, mas longo o suficiente para ser percebido
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* <!-- paginação --> */}
            <div className="tr-pagination mt-5 pb-5">
                <Pagination/>
            </div>

            {/* <!-- NEWSLETTER --> */}
            <div className="tr-newsletter purple mt-5 pt-5">
                <Newsletter/>
            </div>

		</React.Fragment>
	);	
}

export default SearchResults;