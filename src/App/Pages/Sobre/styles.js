import styled from "styled-components";

export const Container = styled.div`
    .tr-header-sobre {
        background: ${props =>
            `linear-gradient(0deg, #012a50 0%, #0058a8 100%)`} !important;
    }
`;
