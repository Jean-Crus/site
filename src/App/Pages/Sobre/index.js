import React from "react";
import { useSelector, useDispatch } from "react-redux";
import Carousel from "react-multi-carousel";
import { CursosDestaque, Newsletter, ListaCurso } from "@components";
import bitmap from "@images/sobre.svg";
import "react-multi-carousel/lib/styles.css";
import { Container } from "./styles";

const Sobre = props => {
    const curso = useSelector(state => state.curso);

    return (
        <Container>
            <div className="tr-header-sobre">
                <div className="content container">
                    <div className="row mx-0">
                        <div className="col-lg-8 tr-flex-center">
                            <div className="title-2 size-50 wh-600">Sobre</div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="tr-card-sobre container">
                <div className="row d-flex justify-content-center align-items-center flex-column">
                    <div className="col-lg-11 cards">
                        <div className="sobre">
                            <div className="image">
                                <img src={bitmap} alt="Bitmap" />
                            </div>
                        </div>
                        <div className="content">
                            <b>Conheça a história</b>
                            <p>
                                Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Dicta enim nobis voluptatibus
                                praesentium expedita, reiciendis facilis error
                                cupiditate? Nisi, aspernatur recusandae. Sed cum
                                unde repellat ipsum aut earum tenetur
                                asperiores?
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Dicta enim nobis voluptatibus
                                praesentium expedita, reiciendis facilis error
                                cupiditate? Nisi, aspernatur recusandae. Sed cum
                                unde repellat ipsum aut earum tenetur
                                asperiores?
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Dicta enim nobis voluptatibus
                                praesentium expedita, reiciendis facilis error
                                cupiditate? Nisi, aspernatur recusandae. Sed cum
                                unde repellat ipsum aut earum tenetur
                                asperiores?
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            {/* <ListaCurso title="Cursos destaque" destaque /> */}

            {/* <div className="tr-newsletter purple mt-4">
                <Newsletter />
            </div> */}
        </Container>
    );
};

export default Sobre;
