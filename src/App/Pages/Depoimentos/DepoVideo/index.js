import React from 'react';
import { useSelector } from 'react-redux';

import { CardVideo, SliderWhite, LeftArrow, RightArrow, Box } from '../styles';
import { bitmap } from '@images';

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <RightArrow className={className} onClick={onClick} fontSize="large" />
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <LeftArrow className={className} onClick={onClick} fontSize="large" />
    );
}

export default function SobreVideo({ handleClick }) {
    const depoiments = useSelector(state => state.depoiments.depos);
    const settings = {
        // fade: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        // autoplay: true,
        autoplaySpeed: 3000,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1266,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    fade: true,
                },
            },
        ],
    };
    const handleOpen = id => {
        handleClick(id);
    };
    return (
        <Box>
            <SliderWhite {...settings}>
                {depoiments.map(depoimentos => {
                    return (
                        <CardVideo key={depoimentos.id}>
                            <div className="video-card">
                                <div
                                    className="video"
                                    onClick={() => handleOpen(depoimentos.id)}
                                >
                                    <img src={depoimentos.imagem} />
                                </div>
                                <div className="text-video">
                                    <span>{depoimentos.name}</span>
                                    <small>{depoimentos.city}</small>
                                </div>
                            </div>
                        </CardVideo>
                    );
                })}
            </SliderWhite>
        </Box>
    );
}
