import React from 'react';
import { useSelector } from 'react-redux';

import {
    Container,
    CardVideo,
    SliderWhite,
    LeftArrow,
    RightArrow,
    AvatarBox,
    SliderText,
} from '../styles';
import { bitmap } from '@images';

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <RightArrow
            white
            className={className}
            onClick={onClick}
            fontSize="large"
        />
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <LeftArrow
            white
            className={className}
            onClick={onClick}
            fontSize="large"
        />
    );
}

export default function SobreVideo({ handleTextDepo }) {
    const depoiments = useSelector(state => state.depoiments.deposText);
    const activeDepoText = useSelector(state => state.depoiments.activeText);
    const settings = {
        // fade: true,
        infinite: true,
        slidesToShow: depoiments.length > 6 ? 6 : depoiments.length,
        slidesToScroll: 1,
        // autoplay: true,
        autoplaySpeed: 3000,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1266,
                settings: {
                    slidesToShow: depoiments.length > 4 ? 4 : depoiments.length,
                    slidesToScroll: 3,
                    infinite: true,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: depoiments.length > 2 ? 2 : depoiments.length,
                    slidesToScroll: 2,
                    initialSlide: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: depoiments.length > 1 ? 1 : depoiments.length,
                    slidesToScroll: 1,
                },
            },
        ],
    };
    const handleOpen = id => {
        handleTextDepo(id);
        window.scrollTo(0, 2100);
    };
    return (
        <Container>
            <SliderText {...settings}>
                {depoiments.map(depoimentos => {
                    return (
                        <AvatarBox
                            key={depoimentos.id}
                            active={activeDepoText.id === depoimentos.id}
                        >
                            <div
                                className="avatar-profile"
                                onClick={() => handleOpen(depoimentos.id)}
                            >
                                <img
                                    src={depoimentos.imagem}
                                    alt="Imagem avatar"
                                />
                            </div>
                            <div className="avatar-name">
                                <span>{depoimentos.name}</span>
                                <small>{depoimentos.city}</small>
                            </div>
                        </AvatarBox>
                    );
                })}
            </SliderText>
        </Container>
    );
}
