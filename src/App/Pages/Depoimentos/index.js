import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import * as DepoimentsActions from '@store/modules/depoiments/actions';
import { carrousel1, compress, curso2, bitmap } from '@images';

import { ListaCurso, Newsletter, ButtonTriade } from '@components';
import {
    Container,
    ModalPlayer,
    Player,
    MenuCircle,
    Arrow1,
    Arrow2,
} from './styles';
import DepoVideo from './DepoVideo';
import DepoTexto from './DepoTexto';

const Depoimentos = props => {
    const activeDepo = useSelector(state => state.depoiments.active);
    const activeDepoText = useSelector(state => state.depoiments.activeText);
    const dispatch = useDispatch();
    const style = {
        carrousel1: {
            backgroundImage: `url('${carrousel1}')`,
        },
        curso2: {
            backgroundImage: `url('${curso2}')`,
        },
        compress: {
            backgroundImage: `url('${compress}')`,
        },
    };
    const [open, setOpen] = useState(false);
    const handleOpen = () => {
        setOpen(!open);
    };
    return (
        <Container>
            <div className="new-depoimento">
                <div className="intro">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8">
                                <div className="title">
                                    <small className="title-2">
                                        Depoimentos
                                    </small>
                                    <h1 className="title-1">
                                        “Aprendo muito em qualquer lugar que eu
                                        estiver.”
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <img
                            className="picture"
                            src={carrousel1}
                            alt="Depoimentos"
                        />
                    </div>
                    <MenuCircle>
                        <Arrow1 />
                        <Arrow2 />
                    </MenuCircle>
                </div>
                <div className="intro-video">
                    <h2 className="title-1 size-50 black wh-700">
                        Depoimentos em vídeo
                    </h2>
                    <div className="container">
                        <div className="align-items-center row">
                            <div className="col-sm-4">
                                <div className="content">
                                    <h3 className="title-1 size-50 black wh-700">
                                        {/* Tristan Olsen */}
                                        {activeDepo.name}
                                    </h3>
                                    <small className="title-2 black">
                                        {/* São Paulo (BR) */}
                                        {activeDepo.city}
                                    </small>
                                    <p>
                                        {/* “Eu fiz o curso de Lorem Ipsum e isso me
                                        abriu as portas para o mercado de
                                        trabalho na área que eu sempre quis” */}
                                        {activeDepo.text}
                                    </p>
                                </div>
                            </div>
                            <div className="col-sm-7 offset-sm-1">
                                <div className="video pause" id="playing-video">
                                    <video
                                        poster={bitmap}
                                        onClick={handleOpen}
                                    />
                                    <ModalPlayer
                                        open={open}
                                        onClose={handleOpen}
                                    >
                                        <Player url={activeDepo.video} />
                                    </ModalPlayer>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mais-videos">
                    <div className="container">
                        <DepoVideo
                            handleClick={id =>
                                dispatch(DepoimentsActions.setDepo(id))
                            }
                        />
                    </div>
                </div>
                <div className="todos-depoimentos">
                    <div className="container">
                        <h2 className="title-1 size-50 wh-700">
                            Todos os depoimentos
                        </h2>
                        <div className="align-items-center row">
                            <div className="col-sm-4">
                                <div className="content">
                                    <h3 className="title-1 size-50 black wh-700">
                                        {activeDepoText.name}
                                    </h3>
                                    <small className="title-2 black">
                                        {activeDepoText.city}
                                    </small>
                                    <p>{activeDepoText.text}</p>
                                </div>
                            </div>
                            <div className="col-sm-7 offset-sm-1">
                                <div
                                    className="picture"
                                    style={style.carrousel1}
                                />
                            </div>
                        </div>
                        <div className="carrousel-todos-depoimentos">
                            <h4 className="title-6">Todos os depoimentos</h4>
                            <DepoTexto
                                handleTextDepo={id =>
                                    dispatch(DepoimentsActions.setDepoText(id))
                                }
                            />
                        </div>
                    </div>
                </div>
            </div>

            <ListaCurso destaque />

            {/* <!-- NEWSLETTER --> */}
            {/* <div className="tr-newsletter purple mt-4">
                <Newsletter />
            </div> */}
        </Container>
    );
};

export default Depoimentos;
