import styled from 'styled-components';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import Slider from 'react-slick';
import ReactPlayer from 'react-player';
import { Modal, Grid } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { darken } from 'polished';

export const Container = styled.div`
    .new-depoimento {
        .intro {
            min-height: 600px;

            .container {
                .picture {
                }
            }
        }
        .intro-video {
            h2 {
                margin-bottom: 80px;
            }
        }
        .mais-videos {
            margin-top: 70px;
        }
    }
`;

export const Box = styled(Grid)`
    padding-top: 40px;
    width: 100%;
    /* display: flex;
    justify-content: center; */
    background-color: #fcfcfc;
    /* padding: 100px; */
    .title-1 {
        margin-bottom: 40px;
        text-align: center;
        color: black;
        font-size: 1.5625rem;
    }
`;

export const SliderWhite = styled(Slider)`
    width: 100%;
    max-width: 1200px;
    margin: 0 auto;
    .slick-list {
        margin: 0 30px;
        .slick-slide {
            display: flex;
            justify-content: center;
        }
    }
    .slick-next {
        right: 0;
    }
    .slick-prev {
        left: 0;
    }
`;

export const CardVideo = styled.div`
    .video-card {
        display: flex;
        justify-content: center;
        flex-direction: column;
        border-radius: 5px;
        width: 250px;
        max-height: 400px;
        &:hover {
            border: 1px solid #ececec;
            background-color: #ffffff;
            box-shadow: 0 2px 9px 0 rgba(0, 0, 0, 0.08);
        }

        .video {
            position: relative;
            margin-top: 10px;
            display: flex;
            justify-content: center;
            &:hover {
                cursor: pointer;
                .play-button {
                    background-color: ${props =>
                        darken(0.2, `${props.theme.primary}`)};
                    svg {
                        color: ${props => darken(0.2, props.theme.letter)};
                    }
                }
            }
            img {
                width: 186.66px;
                border-radius: 5px;
                height: 280px;
            }
        }

        .text-video {
            display: flex;
            flex-direction: column;
            line-height: 14px;
            margin-top: 20px;
            margin-bottom: 10px;
            margin-left: 12px;
            span {
                color: ${props => props.theme.letter2};
                font-family: Montserrat;
                font-size: 16px;
            }
            small {
                color: #afafaf;
                font-family: Montserrat;
                font-size: 12px;
            }
        }
    }
`;

export const IconCircle = styled.div`
    display: flex;
    justify-content: center;
    position: absolute;
    top: 35%;
    align-items: center;
    height: 50px;
    width: 50px;
    border-radius: 67px;
    background-color: ${props => props.theme.primary};
    box-shadow: 0 10px 30px 0 rgba(0, 0, 0, 0.2);
`;

export const ArrowPlay = styled(PlayArrowIcon)`
    color: ${props => props.theme.letter};
`;

export const LeftArrow = styled(ArrowBackIosIcon)`
    color: ${props => (props.white ? '#fff !important' : 'black !important')};
    &:hover {
        opacity: 0.5;
    }
`;
export const RightArrow = styled(ArrowForwardIosIcon)`
    color: ${props => (props.white ? '#fff !important' : 'black !important')};
    &:hover {
        opacity: 0.5;
    }
`;

export const Player = styled(ReactPlayer)``;

export const ModalPlayer = styled(Modal)`
    display: flex;
    align-items: center;
    justify-content: center;
    /* top: 25%; */
    margin: auto;
`;

export const VejaMais = styled(Link)`
    margin-top: 20px;
    font-family: MontSerrat, sans-serif;
    font-size: 0.875rem;
    font-weight: bold;
    color: ${props => props.theme.background};
    text-decoration: underline;
    &:hover {
        text-decoration: none;
        color: ${props => props.theme.background};
    }
`;

export const SliderText = styled(Slider)`
    width: 100%;
    max-width: 1200px;
    margin: 0 auto;
    .slick-list {
        margin: 0 30px;
        .slick-slide {
            display: flex;
            justify-content: center;
        }
    }
    .slick-next {
        right: 0;
    }
    .slick-prev {
        left: 0;
    }
`;

export const AvatarBox = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    .avatar-profile {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        img {
            cursor: pointer;
            height: 91px;
            width: 91px;
            border-radius: 50%;
            border: ${props => (props.active ? '3px solid #6236ff' : '')};
        }
    }
    .avatar-name {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center;
        span {
            font-family: Montserrat, sans-serif;
            color: ${props => props.theme.letter};
            font-size: 18px;
            font-weight: bold;
        }
        small {
            font-family: Montserrat, sans-serif;
            color: #afafaf;
            font-size: 12px;
            font-weight: bold;
        }
    }
`;

export const MenuCircle = styled.div`
    display: flex;
    justify-content: center;
    position: absolute;
    top: 95%;
    left: 50%;
    transform: translateX(-50%);
    align-items: center;
    height: 67px;
    width: 67px;
    border-radius: 67px;
    background-color: ${props => props.theme.primary};
    box-shadow: 0 10px 30px 0 rgba(0, 0, 0, 0.2);
`;

export const Arrow1 = styled.div`
    position: absolute;
    top: 30%;
    height: 12px;
    width: 12px;
    border: ${props => `solid ${props.theme.letter}`};
    border-width: 0 3px 3px 0;
    opacity: 0.5;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
`;

export const Arrow2 = styled.div`
    position: absolute;
    top: 50%;
    height: 12px;
    width: 12px;
    border: ${props => `solid ${props.theme.letter}`};
    border-width: 0 3px 3px 0;
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
`;
