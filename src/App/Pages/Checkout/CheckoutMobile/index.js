import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Formik } from "formik";
import * as Yup from "yup";
import { Grid, TextField, MenuItem } from "@material-ui/core";
import Script from "react-load-script";
import * as CheckoutActions from "@store/modules/checkout/actions";
import { Spinner } from "react-bootstrap";
import InputMask from "react-input-mask";
import { tokenJavascript } from "@api";
import { ButtonTriade, TimerPay } from "@components";
import { CheckoutMobileContainer } from "../styles";
import useStyle from "./useStyle";

const initialValues = {
    numberCard: "",
    nomeTitular: "",
    tel: "",
    validade: "",
    ccv: "",
    cpf: "",
    birthDate: "",
    opcaoPagamento: "1"
};

const schema = Yup.object().shape({
    nomeTitular: Yup.string().required("Nome do titular obrigatório"),
    numberCard: Yup.string().required("O número do cartão é obrigatório"),
    cpf: Yup.string().required("O cpf é obrigatório"),
    birthDate: Yup.string().required(
        'Data de nascimento obrigatória Ex: "12/12/2012'
    ),
    tel: Yup.string()
        .matches(
            /^\([0-9]{2}\)[0-9]?[0-9]{4}-[0-9]{4}$/,
            "Telefone inválido. Ex: (xx)xxxxx-xxxx"
        )
        .required("Número de telefone celular obrigatório"),
    validade: Yup.string().required("Validade obrigatória Ex: 12/2012"),
    ccv: Yup.string().required("CCV é obrigatório"),
    opcaoPagamento: Yup.number().required("Selecione o valor do pagamento")
});

const CheckoutMob = () => {
    const { buttonGrid, title, textField } = useStyle();
    const [cardBrand, setCardBrand] = useState(null);
    const loading = useSelector(state => state.checkout.loading);

    const totalFormatted = useSelector(state => state.cart.totalFormatted);
    const totalInstallments = useSelector(state =>
        state.cart.data.reduce((totalFormattedSum, product) => {
            return totalFormattedSum + product.price * product.amount;
        }, 0)
    );

    const dispatch = useDispatch();
    const handleLoad = () => {
        dispatch(CheckoutActions.getSessionCardRequest());
    };
    const handleBrand = numberCard => {
        window.PagSeguroDirectPayment.getBrand({
            cardBin: numberCard,
            success(response) {
                setCardBrand(response.brand.name);
            },
            error(err) {},
            complete(response) {}
        });
    };
    return (
        <CheckoutMobileContainer>
            <header>Pagamento</header>
            <TimerPay mobile />
            <main>
                <h3>Selecione o método de pagamento</h3>
                <div className="metodos">
                    <div className="cards">
                        <div className="icon-img">
                            <img
                                src="https://images.pexels.com/photos/356079/pexels-photo-356079.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt=""
                            />
                        </div>
                        <div className="pagamento-title">Cartão de crédito</div>
                    </div>
                    {/* <div className="cards">
                        <div className="icon-img">
                            <img
                                src="https://images.pexels.com/photos/356079/pexels-photo-356079.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt=""
                            />
                        </div>
                        <div className="pagamento-title">Boleto</div>
                    </div>
                    <div className="cards">
                        <div className="icon-img">
                            <img
                                src="https://images.pexels.com/photos/356079/pexels-photo-356079.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                                alt=""
                            />
                        </div>
                        <div className="pagamento-title">Paypal</div>
                    </div> */}
                </div>
                <div className="dados-cartao">
                    <Formik
                        initialValues={initialValues}
                        enableReinitialize
                        validationSchema={schema}
                        className="tr-form"
                        validate={props => handleBrand(props.numberCard)}
                        onSubmit={({
                            nomeTitular,
                            numberCard,
                            cpf,
                            birthDate,
                            validade,
                            ccv,
                            opcaoPagamento,
                            tel
                        }) => {
                            dispatch(
                                CheckoutActions.getCheckoutRequest(
                                    nomeTitular,
                                    numberCard,
                                    cpf,
                                    birthDate,
                                    validade,
                                    ccv,
                                    opcaoPagamento,
                                    cardBrand,
                                    tel
                                )
                            );
                        }}
                    >
                        {({
                            handleBlur,
                            handleChange,
                            handleSubmit,
                            values,
                            errors,
                            touched
                        }) => {
                            return (
                                <>
                                    <Script
                                        url={tokenJavascript}
                                        onLoad={handleLoad}
                                    />
                                    {loading ? (
                                        <Grid
                                            container
                                            justify="center"
                                            style={{ margin: "60px 0" }}
                                        >
                                            <Spinner
                                                animation="border"
                                                role="status"
                                            />
                                        </Grid>
                                    ) : (
                                        <form onSubmit={handleSubmit}>
                                            <div className="title">
                                                <div>Dados do cartão</div>
                                                {/* <div>Pagar com 2 cartões</div> */}
                                            </div>
                                            <div className="credit-card">
                                                <div className="card">
                                                    <div className="brand">
                                                        <img
                                                            src={`https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/68x30/${cardBrand}.png`}
                                                            alt=""
                                                        />
                                                    </div>
                                                    <div className="background" />
                                                    <div className="data-card">
                                                        <small>
                                                            Número do cartão
                                                        </small>
                                                        <div className="num-cartao">
                                                            {values.numberCard}
                                                        </div>
                                                        <small>
                                                            Nome do titular
                                                        </small>
                                                        <div className="nom-titular">
                                                            {values.nomeTitular}
                                                        </div>
                                                        <div className="ccv">
                                                            <div>
                                                                <small>
                                                                    Validade
                                                                </small>
                                                                <div>
                                                                    {
                                                                        values.validade
                                                                    }
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <small>
                                                                    CCV
                                                                </small>
                                                                <div>
                                                                    {values.ccv}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <Grid container>
                                                <Grid
                                                    item
                                                    lg={12}
                                                    xl={12}
                                                    xs={12}
                                                    sm={12}
                                                    md={12}
                                                >
                                                    <TextField
                                                        name="nomeTitular"
                                                        error={
                                                            touched.nomeTitular &&
                                                            errors.nomeTitular
                                                        }
                                                        helperText={
                                                            touched.nomeTitular &&
                                                            errors.nomeTitular
                                                        }
                                                        onChange={handleChange}
                                                        value={
                                                            values.nomeTitular
                                                        }
                                                        label="Nome do titular"
                                                        fullWidth
                                                        margin="normal"
                                                        variant="outlined"
                                                        onBlur={handleBlur}
                                                    />
                                                    <InputMask
                                                        mask="(99)99999-9999"
                                                        maskChar=" "
                                                        fullWidth
                                                        helperText={
                                                            touched.tel &&
                                                            errors.tel
                                                        }
                                                        error={
                                                            touched.tel &&
                                                            errors.tel
                                                        }
                                                        label="Telefone celular*"
                                                        margin="normal"
                                                        variant="outlined"
                                                        name="tel"
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        value={values.tel}
                                                    >
                                                        {props => (
                                                            <TextField
                                                                {...props}
                                                            />
                                                        )}
                                                    </InputMask>
                                                </Grid>
                                                <Grid container>
                                                    <Grid container>
                                                        <Grid
                                                            item
                                                            lg={6}
                                                            xl={6}
                                                            xs={12}
                                                            sm={12}
                                                            md={12}
                                                        >
                                                            <InputMask
                                                                mask="999.999.999-99"
                                                                maskChar=" "
                                                                fullWidth
                                                                label="CPF do Titular"
                                                                name="cpf"
                                                                margin="normal"
                                                                variant="outlined"
                                                                error={
                                                                    touched.cpf &&
                                                                    errors.cpf
                                                                }
                                                                helperText={
                                                                    touched.cpf &&
                                                                    errors.cpf
                                                                }
                                                                onChange={
                                                                    handleChange
                                                                }
                                                                onBlur={
                                                                    handleBlur
                                                                }
                                                                value={
                                                                    values.cpf
                                                                }
                                                            >
                                                                {props => (
                                                                    <TextField
                                                                        {...props}
                                                                    />
                                                                )}
                                                            </InputMask>
                                                        </Grid>
                                                        <Grid
                                                            item
                                                            lg={6}
                                                            xl={6}
                                                            xs={12}
                                                            sm={12}
                                                            md={12}
                                                        >
                                                            <InputMask
                                                                mask="99/99/9999"
                                                                maskChar=" "
                                                                fullWidth
                                                                type="text"
                                                                label="Data de nascimento"
                                                                name="birthDate"
                                                                margin="normal"
                                                                variant="outlined"
                                                                error={
                                                                    touched.birthDate &&
                                                                    errors.birthDate
                                                                }
                                                                helperText={
                                                                    touched.birthDate &&
                                                                    errors.birthDate
                                                                }
                                                                onChange={
                                                                    handleChange
                                                                }
                                                                onBlur={
                                                                    handleBlur
                                                                }
                                                                value={
                                                                    values.birthDate
                                                                }
                                                            >
                                                                {props => (
                                                                    <TextField
                                                                        {...props}
                                                                    />
                                                                )}
                                                            </InputMask>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                            <div className="title-1 size-25 black text-left mb-3 mt-4">
                                                Dados do Cartão
                                            </div>
                                            <Grid container>
                                                <InputMask
                                                    mask="9999999999999999"
                                                    maskChar=" "
                                                    name="numberCard"
                                                    error={
                                                        touched.numberCard &&
                                                        errors.numberCard
                                                    }
                                                    helperText={
                                                        touched.numberCard &&
                                                        errors.numberCard
                                                    }
                                                    onChange={handleChange}
                                                    value={values.numberCard}
                                                    label="Número do cartão"
                                                    fullWidth
                                                    margin="normal"
                                                    variant="outlined"
                                                    onBlur={handleBlur}
                                                >
                                                    {props => (
                                                        <TextField {...props} />
                                                    )}
                                                </InputMask>

                                                <Grid container>
                                                    <Grid
                                                        item
                                                        lg={6}
                                                        xl={6}
                                                        xs={12}
                                                        sm={12}
                                                        md={12}
                                                    >
                                                        <InputMask
                                                            mask="99/9999"
                                                            fullWidth
                                                            type="text"
                                                            label="Validade"
                                                            name="validade"
                                                            error={
                                                                touched.validade &&
                                                                errors.validade
                                                            }
                                                            helperText={
                                                                touched.validade &&
                                                                errors.validade
                                                            }
                                                            margin="normal"
                                                            variant="outlined"
                                                            onChange={
                                                                handleChange
                                                            }
                                                            onBlur={handleBlur}
                                                            value={
                                                                values.validade
                                                            }
                                                        >
                                                            {props => (
                                                                <TextField
                                                                    {...props}
                                                                />
                                                            )}
                                                        </InputMask>
                                                    </Grid>
                                                    <Grid
                                                        item
                                                        lg={6}
                                                        xl={6}
                                                        xs={12}
                                                        sm={12}
                                                        md={12}
                                                    >
                                                        <InputMask
                                                            mask="9999"
                                                            maskChar=" "
                                                            fullWidth
                                                            type="text"
                                                            label="CCV"
                                                            name="ccv"
                                                            error={
                                                                touched.ccv &&
                                                                errors.ccv
                                                            }
                                                            helperText={
                                                                touched.ccv &&
                                                                errors.ccv
                                                            }
                                                            margin="normal"
                                                            variant="outlined"
                                                            onChange={
                                                                handleChange
                                                            }
                                                            onBlur={handleBlur}
                                                            value={values.ccv}
                                                        >
                                                            {props => (
                                                                <TextField
                                                                    {...props}
                                                                />
                                                            )}
                                                        </InputMask>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                            <Grid className={title}>
                                                <div className="title-1 size-25 black text-left mb-3">
                                                    Opções de pagamento
                                                </div>
                                            </Grid>
                                            <TextField
                                                className={textField}
                                                select
                                                error={
                                                    touched.opcaoPagamento &&
                                                    errors.opcaoPagamento
                                                }
                                                helperText={
                                                    touched.opcaoPagamento &&
                                                    errors.opcaoPagamento
                                                }
                                                fullWidth
                                                name="opcaoPagamento"
                                                label="Opção de pagamento"
                                                margin="normal"
                                                variant="outlined"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.opcaoPagamento}
                                            >
                                                <MenuItem value={1}>
                                                    1x de {totalFormatted}
                                                </MenuItem>
                                            </TextField>

                                            <Grid
                                                className={buttonGrid}
                                                container
                                                justify="flex-end"
                                            >
                                                <ButtonTriade
                                                    type="submit"
                                                    button="Modelo 02"
                                                    letterColor="Modelo 02"
                                                    cor="Modelo 02"
                                                    readOnly
                                                    text={
                                                        loading
                                                            ? "Carregando..."
                                                            : "Efetuar pagamento"
                                                    }
                                                />
                                            </Grid>
                                        </form>
                                    )}
                                </>
                            );
                        }}
                    </Formik>
                </div>
            </main>
        </CheckoutMobileContainer>
    );
};

export default CheckoutMob;
