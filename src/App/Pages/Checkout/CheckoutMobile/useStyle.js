import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    textField: {
        color: "#5433f1"
    },
    buttonGrid: {
        marginTop: theme.spacing(5)
    },
    title: {
        marginTop: theme.spacing(3)
    }
}));
export default useStyles;
