import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import history from "@routes/history";
import { MethodPay, CardPay, TimerPay, BoletoPay } from "@components";
// import * as CheckoutActions from "@store/modules/checkout/actions";
import { Container, CardPayTr, PagMobileHeader } from "./styles";
// import CheckoutMob from "./CheckoutMobile";

const Checkout = props => {
    // const dispatch = useDispatch();
    const checkoutMethod = useSelector(state => state.checkout.payMethod);
    const amount = useSelector(state => state.cart.amount);
    useEffect(() => {
        if (!amount) {
            history.push("/404");
        }
        // dispatch(CheckoutActions.getInstallmentRequest());
    });
    return (
        <>
            <PagMobileHeader>Pagamento</PagMobileHeader>
            <TimerPay />
            <TimerPay mobile />
            <Container className="container mt-5 text-center">
                <div className="title-1 size-50 wh-600 black">
                    Pagamento via cartão de crédito
                </div>
                <div className="description-1">
                    Selecione o método de pagamento
                </div>
                <div className="tr-method-pay mt-3">
                    <MethodPay />
                </div>
                <CardPayTr className="tr-card-pay mt-5 mb-5">
                    {checkoutMethod === "credit-card" && <CardPay />}
                    {checkoutMethod === "boleto" && <BoletoPay />}
                </CardPayTr>
            </Container>

            {/* <CheckoutMob /> */}
        </>
    );
};

export default Checkout;
