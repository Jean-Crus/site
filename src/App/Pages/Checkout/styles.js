import styled from "styled-components";
import CardOval from "../../../Assets/images/icons/card-oval.svg";

export const Container = styled.div`
    @media only screen and (max-width: 1280px) {
        .tr-card-pay .card .content {
            flex-direction: column;
        }
        .tr-card-pay .card .content .card-sample .credit-card {
            max-width: 330px;
        }
    }
`;

export const CheckoutMobileContainer = styled.div`
    width: 100%;
    .encerrar-sessao {
        padding: 10px;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: ${props => props.theme.primary};
        font-family: Montserrat;
        font-size: 10px;
        color: ${props => props.theme.letter};
    }
    header {
        background-color: ${props => props.theme.background};
        padding: 20px 0;
        display: flex;
        justify-content: center;
        align-items: center;
        color: ${props => props.theme.letter};
        font-family: Montserrat;
        font-size: 16px;
        font-weight: bold;
    }

    main {
        display: flex;
        flex-direction: column;
        align-items: center;
        margin: 20px 10px;
        label {
            background-color: #fff !important;
            padding: 0 5px;
        }
        h3 {
            font-family: Montserrat;
            color: ${props => props.theme.letter2};
            font-size: 14px;
            text-align: center;
        }
        .metodos {
            width: 100%;
            display: flex;
            justify-content: space-evenly;
            align-items: center;
            margin-bottom: 20px;
            .cards {
                width: 80px;
                display: flex;
                height: 91px;
                flex-direction: column;
                align-items: center;
                line-height: 12px;
                color: ${props => props.theme.background};
                font-family: Montserrat;
                font-size: 11px;
                font-weight: bold;
                text-align: center;
                background-color: #ffffff;
                border-radius: 5px;
                border: 1px solid #efefef;
                .icon-img {
                    width: 100%;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    height: 51px;
                    img {
                        width: 25px;
                        height: 25px;
                    }
                }
                .pagamento-title {
                    display: flex;
                    height: 40px;
                    justify-content: center;
                    align-items: center;
                }
            }
        }
        .dados-cartao {
            display: flex;
            flex-direction: column;
            align-items: center;
            border-radius: 5px;
            background-color: #ffffff;
            border: 1px solid #efefef;
            padding: 10px;
            .title {
                display: flex;
                width: 100%;
                justify-content: space-between;
                align-items: center;
                flex-wrap: wrap;
                margin: 10px 0;
                div:not(.ql-container) {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    &:first-child {
                        color: ${props => props.theme.background};
                        font-family: Montserrat;
                        font-size: 14px;
                        font-weight: bold;
                    }
                    &:last-child {
                        font-family: Montserrat;
                        font-size: 9px;
                        font-weight: bold;
                        text-decoration: underline;
                        max-width: 80px;
                        text-align: center;
                        color: ${props => props.theme.background};
                    }
                }
            }
        }
        .credit-card {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            margin-top: 20px;
            margin-bottom: 10px;
            .card {
                width: 245px;
                height: 158px;
                background-color: #333834;
                border-radius: 5px;
                position: relative;
                padding: 10px;
                font-family: Montserrat;
                justify-content: flex-end;
                display: flex;
                flex-direction: column;
                .brand {
                    display: flex;
                    justify-content: flex-end;
                }
                .background {
                    position: absolute;
                    top: 0;
                    right: 0;
                    background-image: url(${CardOval});
                    width: 100%;
                    height: 100%;
                    color: white;
                    background-size: cover;
                    background-position: center center;
                    background-repeat: no-repeat;
                }
                .data-card {
                    small {
                        font-family: Montserrat;
                        color: ${props => props.theme.letter};
                        opacity: 0.5;
                        font-size: 8px;
                        font-weight: 300;
                    }
                    .num-cartao {
                        font-size: 14px;
                        color: ${props => props.theme.letter};
                    }
                    .nom-titular {
                        font-size: 10px;
                        color: ${props => props.theme.letter};
                    }
                    .ccv {
                        display: flex;
                        justify-content: space-between;
                        div {
                            display: flex;
                            flex-direction: column;
                            div {
                                color: ${props => props.theme.letter};
                                font-size: 10px;
                            }
                        }
                    }
                }
            }
        }
    }
    @media only screen and (min-width: 1281px) {
        display: none;
        margin: 0 !important;
    }
    @media only screen and (max-width: 999px) {
        margin: 10px 0 !important;
        /* .makeStyles-buttonGrid-166 {
            display: flex !important;
            justify-content: center !important;
        } */
    }
`;

export const CardPayTr = styled.div`
    width: 90% !important;
`;

export const PagMobileHeader = styled.header`
    display: flex;
    padding: 20px;
    justify-content: center;
    align-items: center;
    font-family: Montserrat;
    font-size: 1.2rem;
    font-weight: bold;
    margin-top: 10px;
    color: #fff;
    background: linear-gradient(0deg, #012a50 0%, #0058a8 100%);

    @media only screen and (min-width: 1201px) {
        display: none;
    }
`;
