import React from "react";
import { Link } from "react-router-dom";
import history from "@routes/history";

import { ButtonTriade } from "@components";
import { Container, ErrorBody } from "./styles";

const Error404 = () => {
    const handleClick = () => {
        history.push("/");
    };
    return (
        <Container className="bg-black tr-404">
            <ErrorBody>
                <div className="title-error">
                    <div className="title-1">ERROR 404</div>
                    <div className="description-1">Essa página não existe</div>
                </div>
                <ButtonTriade
                    button="Modelo 02"
                    letterColor="Modelo 02"
                    cor="Modelo 02"
                    text="Voltar para o site"
                    onClick={handleClick}
                />
            </ErrorBody>
        </Container>
    );
};

export default Error404;
