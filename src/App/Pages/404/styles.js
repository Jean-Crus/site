import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #ffffff !important;
`;

export const ErrorBody = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    border-radius: 5px;
    border: ${props => `1px solid ${props.theme.letter2}`};
    padding: 70px 60px;
    background: linear-gradient(0deg, #0058a8 0%, #0058a8 100%);
    margin: 0 10px;
    .description-1 {
        color: #ffffff;
    }
    .title-error {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;
        flex-direction: column;
        line-height: 40px;
        margin-bottom: 20px;
        text-align: center;
    }
`;
