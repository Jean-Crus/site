import React from "react";
import { ButtonTriade } from "@components";
import { compress } from "@images";

const FaleComTutorInterna = props => {
    return (
        <div className="mb-5 pb-5">
            {/* <!-- Header curso cursando --> */}
            <div className="tr-header-cursando">
                <div className="content container">
                    <div className="row mx-0">
                        <div className="col-lg-9 tr-flex-center">
                            <ButtonTriade button="circle-primary" />
                            <div className="information-course mb-2">
                                <small className="description-2">
                                    Curso Avançado de Bibliotecaria
                                </small>
                                <br />
                                <div className="title-1 size-50 ff-24">
                                    Dificuldades no módulo 3. Não consigo fazer
                                    a 4
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 tr-flex-center px-0 mt-2">
                            <div className="button-continue">
                                <ButtonTriade
                                    to="/checkout"
                                    button="secondary"
                                    text="Responder"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container mt-5 pb-5">
                <div className="row d-flex justify-content-center align-items-center flex-column">
                    <div className="col-lg-9">
                        <div className="tr-fale_tutor-pergunta">
                            <div className="card">
                                <div className="spacing">
                                    <div className="content">
                                        <div className="author">
                                            <div className="avatar">
                                                <img src={compress} />
                                            </div>
                                            <div className="description">
                                                <div className="name">
                                                    Matheus kindrazki
                                                </div>
                                                <div className="post">
                                                    Postou 30/07/2018
                                                </div>
                                            </div>
                                        </div>
                                        <div className="question">
                                            <div className="description-2 mt-4">
                                                Lorem ipsum dolor sit amet,
                                                consectetur adipiscing elit.
                                                Duis nec convallis ligula.
                                                Quisque vel diam ligula. t quam
                                                magna, facilisis nec bibendum
                                                id, semper ac turpis. Lorem
                                                ipsum dolor sit amet,
                                                consectetur adipiscing elit.
                                                Duis nec convallis ligula.
                                                Quisque vel diam ligula. Ut quam
                                                magna, facilisis nec bibendum
                                                id, semper ac turpis. Lorem
                                                ipsum dolor sit amet,
                                                consectetur adipiscing elit.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tr-fale_tutor-resposta mt-5">
                            <div className="content">
                                <div className="title-1 black ff-18 wh-700">
                                    Deixe sua resposta
                                </div>
                                <div className="form-group mt-2">
                                    <form>
                                        <textarea
                                            className="form-control"
                                            placeholder="Mensagem"
                                            name=""
                                            id=""
                                            rows="3"
                                         />
                                        <div className="submit">
                                            <input
                                                type="file"
                                                className="form-control-file"
                                                id="exampleFormControlFile1"
                                            />
                                            <label
                                                className="select-archive"
                                                htmlFor="exampleFormControlFile1"
                                            >
                                                Adicionar um anexo
                                            </label>
                                            <label className="selected-archive d-none">
                                                arquivoAdicionado.pdf
                                            </label>
                                            <ButtonTriade
                                                button="secondary"
                                                className="ff-14"
                                                text="Responder"
                                            />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FaleComTutorInterna;
