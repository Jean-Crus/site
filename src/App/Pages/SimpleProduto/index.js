import React from "react";
import { CarouselProdutos, ButtonTriade } from "@components";
import { shipping, machine } from "@images";
import { Link } from "react-router-dom";

const SimpleProduto = props => {
    const style = {
        machine: {
            backgroundImage: `url('${  machine  }')`
        }
    };

    return (
        <>
            <div className="interna-produto">
                <div className="container">
                    <div className="awesome-breadcrumb">
                        <ul>
                            <li>
                                <Link to="#">Link 1</Link>
                            </li>
                            <li>
                                <Link to="#">Link 2</Link>
                            </li>
                            <li>
                                <span className="description-1">
                                    Página Atual
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div className="product-details">
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="product-carousel-interna">
                                    <div className="like-product">
                                        <i className="fas fa-heart"></i>
                                    </div>
                                    <div className="owl-carousel owl-theme">
                                        <div className="item">
                                            <img src={machine} />
                                        </div>
                                        <div className="item">
                                            <img src={machine} />
                                        </div>
                                        <div className="item">
                                            <img src={machine} />
                                        </div>
                                        <div className="item">
                                            <img src={machine} />
                                        </div>
                                        <div className="item">
                                            <img src={machine} />
                                        </div>
                                    </div>
                                    <div id="custom-dots">
                                        <div
                                            className="dot active"
                                            style={style.machine}
                                        >
                                            1
                                        </div>
                                        <div
                                            className="dot"
                                            style={style.machine}
                                        >
                                            2
                                        </div>
                                        <div
                                            className="dot"
                                            style={style.machine}
                                        >
                                            3
                                        </div>
                                        <div
                                            className="dot"
                                            style={style.machine}
                                        >
                                            4
                                        </div>
                                        <div
                                            className="dot"
                                            style={style.machine}
                                        >
                                            5
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="product-content-interna">
                                    <div className="title">
                                        <small className="description-2">
                                            Código : ADP0005
                                        </small>
                                        <h1 className="title-1 black size-50">
                                            SACETT™ Suction Above Cuff
                                            Endotracheal Tube
                                        </h1>
                                    </div>
                                    <div className="price">
                                        <div className="top">
                                            <p className="de">
                                                De <del>R$ 250,00</del>
                                            </p>
                                            <p className="por">
                                                Por <span>R$ 99,00</span>
                                            </p>
                                        </div>
                                        <div className="bottom">
                                            <p>
                                                Até <strong>3x</strong> de{" "}
                                                <strong>R$ 8,50</strong> sem
                                                juros
                                            </p>
                                        </div>
                                    </div>
                                    <div className="shipping-preview">
                                        <img src={shipping} />
                                        <strong>Calcular Frete</strong>
                                        <div className="input-group">
                                            <input type="text" />
                                        </div>
                                        <button>OK</button>
                                    </div>
                                    <div className="product-buyer">
                                        <div className="quantity">
                                            <small className="description-2">
                                                Quantidade
                                            </small>
                                            <div className="input-area">
                                                <span className="plus">+</span>
                                                <input type="number" />
                                                <span className="minus">-</span>
                                            </div>
                                        </div>
                                        <div className="go-buy">
                                            <ButtonTriade
                                                button="secondary"
                                                className="mx-2"
                                                text="Comprar"
                                                icon="fas fa-shopping-cart"
                                            />
                                            <small className="description-2 italic">
                                                Sua compra 100% segura
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="product-info">
                        <div className="top">
                            <ul>
                                <li>Características</li>
                            </ul>
                        </div>
                        <div className="bottom">
                            <ul>
                                <li>
                                    <span>Lorem Ipsum</span>
                                    <strong>24 volts</strong>
                                </li>
                                <li>
                                    <span>Lorem Ipsum</span>
                                    <strong>24 volts</strong>
                                </li>
                                <li>
                                    <span>Lorem Ipsum</span>
                                    <strong>24 volts</strong>
                                </li>
                                <li>
                                    <span>Lorem Ipsum</span>
                                    <strong>24 volts</strong>
                                </li>
                                <li>
                                    <span>Lorem Ipsum</span>
                                    <strong>24 volts</strong>
                                </li>
                                <li>
                                    <span>Lorem Ipsum</span>
                                    <strong>24 volts</strong>
                                </li>
                                <li>
                                    <span>Lorem Ipsum</span>
                                    <strong>24 volts</strong>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            {/* <!-- Carrousel Produtos --> */}
            <CarouselProdutos />
        </>
    );
};

export default SimpleProduto;
