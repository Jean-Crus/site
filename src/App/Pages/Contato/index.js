import React from "react";
import { FaleConosco, ListaCurso } from "@components";
import { Container } from "./styles";

const Contato = () => {
    return (
        <Container>
            <div className="mb-5">
                <div className="tr-header-contato">
                    <div className="content container">
                        <div className="row mx-0">
                            <div className="col-lg-8 tr-flex-center">
                                <div className="title-2 size-50 wh-600">
                                    Contato
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tr-fale-conosco">
                    <FaleConosco />
                </div>

                {/* <ListaCurso
                    title='<h3 class="ql-align-center"><span class="ql-font-Montserrat">Cursos destaque</span></h3>'
                    destaque
                    readOnly
                /> */}
            </div>
        </Container>
    );
};

export default Contato;
