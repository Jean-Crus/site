import React from 'react';

import {
    ConteudoEvento,
    HeaderEventoAberto,
    NavigationEvento,
    SobreEvento,
    InstrutorCurso
} from '@components';

const EventoAberto = (props) => {

    const style = {
        w_100: {
            width: "100%"
        }
    }
	
	return(
		<React.Fragment>

            {/* <!-- Header evento aberto --> */}
            <div className="tr-header-event-open">
                <HeaderEventoAberto/>
            </div>

            <div className="container mt-5">
                <div className="row">
                    <div className="col-lg-3">
                        {/* <!-- Header avento aberto --> */}
                        <div className="tr-nagivation-event">
                            <NavigationEvento/>
                        </div>
                    </div>
                    <div className="col-lg-9">
                        <div className="tr-sobreo-evento">
                            <SobreEvento/>
                        </div>
                        <div className="tr-conteudo-evento mt-5">
                            <ConteudoEvento/>
                        </div>
                        <div className="tr-instrutor-curso mt-5 pt-5 mb-5 pb-5">
                            <InstrutorCurso/>
                        </div>
                        <div className="title-1 black size-50 wh-600 mt-5 pt-4">
                            Localização
                        </div>
                        <div className="card localizacao mt-3">
                            <div style={style.w_100}>
                                <iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&height=600&hl=en&q=Malet%20St%2C%20London%20WC1E%207HU%2C%20United%20Kingdom+(Your%20Business%20Name)&ie=UTF8&t=&z=14&iwloc=B&output=embed"
                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                                    <a href="https://www.mapsdirections.info/en/journey-planner.htm">Route Finder</a>
                                </iframe>
                            </div>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>

            <div className="tr-header-event-open mb-0 border-bottom mt-5">
                <HeaderEventoAberto/>
            </div>
		</React.Fragment>
	);	
}

export default EventoAberto;