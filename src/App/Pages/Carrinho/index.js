import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as CartActions from "@store/modules/cart/actions";
import { Link } from "react-router-dom";
import history from "@routes/history";

import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import { IconButton, Tooltip } from "@material-ui/core";

import { ButtonTriade, ListaCurso } from "@components";
import { Container } from "./styles";
import CarrinhoMobile from "./CarrinhoMobile";

const Carrinho = () => {
    const cart = useSelector(state => state.cart);

    const dispatch = useDispatch();

    const removeProd = id => {
        dispatch(CartActions.removeFromCart(id));
    };

    return (
        <div className="mb-5 pb-5">
            {cart.empty ? (
                <div className="container mt-5">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="carrinho_vazio">
                                <div className="title-1  wh-600 size-50 black">
                                    Carrinho
                                </div>
                                <div className="description-1">
                                    Confira os itens adicionados no seu carrinho
                                </div>
                                <div className="card content mt-3">
                                    <div className="title-1 black">
                                        O seu carrinho não <br />
                                        possui nenhum item
                                    </div>
                                    <div className="button mt-4">
                                        <ButtonTriade
                                            button="Modelo 02"
                                            cor="Modelo 02"
                                            letterColor="Modelo 02"
                                            type="button"
                                            text="Voltar para o site"
                                            onClick={() => history.push("/")}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ) : (
                <>
                    <Container className="container mt-5">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="carrinho_aberto">
                                    <div className="row">
                                        <div className="col-lg-10">
                                            <div className="title-1  wh-600 size-50 black">
                                                Carrinho
                                            </div>
                                            <div className="description-1">
                                                Confira os itens adicionados no
                                                seu carrinho
                                            </div>
                                        </div>
                                        <div className="col-lg-2">
                                            <div className="button">
                                                <ButtonTriade
                                                    button="Modelo 02"
                                                    cor="Modelo 02"
                                                    letterColor="Modelo 02"
                                                    type="button"
                                                    text="Checkout"
                                                    onClick={() =>
                                                        history.push(
                                                            "/checkout"
                                                        )
                                                    }
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card content mt-4">
                                        <div className="product">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" />
                                                        <th scope="col">
                                                            Produto
                                                        </th>
                                                        <th scope="col">
                                                            Excluir
                                                        </th>
                                                        <th scope="col">
                                                            Valor total
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {cart.data.map(product => {
                                                        return (
                                                            <tr
                                                                key={product.id}
                                                            >
                                                                <td
                                                                    className="product-description"
                                                                    scope="row"
                                                                >
                                                                    <div className="image-full">
                                                                        <span
                                                                            className="image"
                                                                            style={{
                                                                                backgroundImage: `url('${product.cover}')`
                                                                            }}
                                                                        />
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="description">
                                                                        <Link
                                                                            to={`/curso-aberto/${product.id}`}
                                                                            className="title-1 size-25 black wh-500"
                                                                        >
                                                                            {
                                                                                product.name
                                                                            }
                                                                        </Link>
                                                                        <span
                                                                            className="badge"
                                                                            style={{
                                                                                backgroundColor: `${!!product.category &&
                                                                                    product
                                                                                        .category
                                                                                        .color}`
                                                                            }}
                                                                        >
                                                                            {product.category &&
                                                                                product
                                                                                    .category
                                                                                    .name}
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                                <td className="price">
                                                                    <div className="flex">
                                                                        <Tooltip title="Remover do carrinho">
                                                                            <IconButton
                                                                                size="small"
                                                                                onClick={() =>
                                                                                    removeProd(
                                                                                        product.id
                                                                                    )
                                                                                }
                                                                            >
                                                                                <HighlightOffIcon color="error" />
                                                                            </IconButton>
                                                                        </Tooltip>
                                                                    </div>
                                                                </td>
                                                                <td className="price">
                                                                    <div className="flex">
                                                                        {
                                                                            product.priceFormatted
                                                                        }
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        );
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>
                                        <div className="footer-price">
                                            <div className="title-1 black">
                                                {cart.totalFormatted}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row d-flex align-items-center mt-4">
                                        <div className="col-lg-10">
                                            <div className="button float-left">
                                                <Link
                                                    to="/"
                                                    className="tr-link"
                                                >
                                                    voltar para o site
                                                </Link>
                                            </div>
                                        </div>
                                        <div className="col-lg-2">
                                            <div className="button">
                                                <ButtonTriade
                                                    button="Modelo 02"
                                                    cor="Modelo 02"
                                                    letterColor="Modelo 02"
                                                    type="button"
                                                    text="Checkout"
                                                    onClick={() =>
                                                        history.push(
                                                            "/checkout"
                                                        )
                                                    }
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                    <CarrinhoMobile />
                </>
            )}
            <ListaCurso
                title='<h3 class="ql-align-center"><span class="ql-font-Montserrat">Você pode gostar desses cursos</span></h3>'
                cartCursos
                readOnly
            />
        </div>
    );
};

export default Carrinho;
