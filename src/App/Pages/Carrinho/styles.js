import styled from "styled-components";

export const Container = styled.div`
    td {
        vertical-align: middle !important;
        .description {
            display: flex;
            flex-direction: column;
            justify-content: center;
            .badge {
                position: static !important;
            }
            a {
                font-size: 1.4rem !important;
            }
        }
    }
    @media only screen and (max-width: 999px) {
        display: none;
    }
`;

export const CarrinhoMobile = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 10px;
    width: 100%;
    flex-direction: column;
    span {
        font-family: Montserrat;
        color: ${props => props.theme.letter2};
        font-size: 14px;
        font-weight: 300;
        text-align: center;
    }

    header {
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: ${props => props.theme.background};
        padding: 25px;
        width: 100%;
        h1 {
            color: ${props => props.theme.letter};
            font-family: Montserrat;
            font-size: 16px;
            font-weight: bold;
            text-align: center;
        }
    }
    main {
        margin: 20px 10px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        border: 1px solid #c1c1c1;
        border-radius: 5px;
        width: 80%;
        padding: 5px 10px 10px 10px;
        .header {
            display: flex;
            align-items: center;
            width: 100%;
            height: 28px;
            div {
                font-family: Montserrat;
                color: #7d7474;
                font-size: 12px;
                font-weight: 300;
            }
        }
        .excluir-title {
            text-align: center;
        }
    }

    @media only screen and (min-width: 999px) {
        display: none;
    }
`;

export const CarrinhoCheckout = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid #c1c1c1;
    padding: 10px 0;
    &:last-child {
        border-bottom: none;
    }
    .img-product {
        display: flex;
        justify-content: center;
        align-items: center;
        img {
            width: 58px;
            height: 58px;
            border-radius: 5px;
        }
    }
    .preco {
        display: flex;
        flex-direction: column;
        justify-content: center;
        flex-wrap: wrap;
        width: 40%;
        text-align: left;
        line-height: 20px;
        .title-curso {
            color: ${props => props.theme.background};
            font-family: Montserrat;
            font-size: 15px;
            font-weight: bold;
        }
        .valor-curso {
            color: ${props => props.theme.primary};
            font-family: Montserrat;
            font-size: 13px;
            font-weight: bold;
            text-align: left;
        }
    }
    .excluir {
        display: flex;
        justify-content: center;
        align-items: center;
    }
`;

export const Total = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-radius: 5px;
    background-color: rgba(236, 236, 236, 0.5);
    border: 1px solid #efefef;
    width: 80%;
    margin: 20px 10px;
    padding: 0 10px;
    small {
        font-family: Montserrat;
        color: #afafaf;
        font-size: 12px;
        font-weight: 300;
    }
    span {
        font-family: Montserrat;
        color: ${props => props.theme.background};
        font-weight: bold;
        font-size: 24px;
    }
`;

export const Cupom = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 80%;
    margin: 20px 10px;
    small {
        font-family: Montserrat;
        color: #afafaf;
        font-size: 12px;
        font-weight: 300;
    }
`;

export const Form = styled.div`
    display: flex;
    width: 80%;
    align-items: center;
    justify-content: space-between;
    small {
        font-family: Montserrat;
        color: #afafaf;
        font-size: 12px;
        font-weight: 300;
        margin-right: 10px;
    }
    form {
        border: 1px solid #c1c1c1;
        padding: 10px;
        border-radius: 5px;
        input[type="text"] {
            outline: none;
            border: none;
            font-family: Montserrat;
            font-weight: bold;
            padding: 0 10px;
            max-width: 200px;
            @media only screen and (max-width: 480px) {
                max-width: 100px;
            }
            @media only screen and (max-width: 355px) {
                max-width: 70px;
            }
        }
        button {
            border: none;
            background-color: transparent;
            font-family: Montserrat;
            font-size: 10px;
            font-weight: bold;
            color: #7d7474;
            text-decoration: underline;
        }
    }
`;
