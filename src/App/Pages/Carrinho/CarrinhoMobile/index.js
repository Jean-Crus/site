import React from "react";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import { Link } from "react-router-dom";
import { IconButton, Tooltip, Grid } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import * as CartActions from "@store/modules/cart/actions";
import { Formik } from "formik";
import * as Yup from "yup";
import { ButtonTriade } from "@components";
import { CarrinhoMobile, CarrinhoCheckout, Total, Form } from "../styles";

const initialValues = {
    name: "",
    email: "",
    tel: "",
    mensagem: ""
};

const schema = Yup.object().shape({
    name: Yup.string().required("Nome obrigatório*"),
    email: Yup.string()
        .email("Insira um e-mail válido")
        .required("E-mail obrigatório"),
    tel: Yup.string()
        .matches(
            /^\([0-9]{2}\)[0-9]?[0-9]{4}-[0-9]{4}$/,
            "Telefone inválido. Ex: (xx)xxxxx-xxxx"
        )
        .required("Número de telefone celular obrigatório")
});

const Carrinho = () => {
    const cart = useSelector(state => state.cart);

    const dispatch = useDispatch();

    const removeProd = id => {
        dispatch(CartActions.removeFromCart(id));
    };
    return (
        <CarrinhoMobile>
            <header>
                <h1>Carrinho</h1>
            </header>
            <span className="sub-title">
                Confira os itens adicionados no seu carrinho
            </span>
            <Link to="/checkout" className="checkout">
                <ButtonTriade
                    button="Modelo 02"
                    cor="Modelo 02"
                    letterColor="Modelo 02"
                    text="Checkout"
                />
            </Link>
            <main>
                <Grid container className="header">
                    <Grid item xs={4} sm={2} />
                    <Grid item xs={5} sm={7}>
                        Curso/Valor
                    </Grid>
                    <Grid item xs={3} sm={3} className="excluir-title">
                        Excluir
                    </Grid>
                </Grid>
                {cart.data.map(product => {
                    return (
                        <CarrinhoCheckout key={product.id}>
                            <Grid container>
                                <Grid
                                    item
                                    xs={4}
                                    sm={2}
                                    className="img-product"
                                >
                                    <img src={product.cover} alt="" />
                                </Grid>
                                <Grid item xs={5} sm={7} className="preco">
                                    <Link to={`/curso-aberto/${product.id}`}>
                                        <Grid className="title-curso">
                                            {product.name}
                                        </Grid>
                                        <Grid className="valor-curso">
                                            {product.priceFormatted}
                                        </Grid>
                                    </Link>
                                </Grid>

                                <Grid item xs={3} sm={3} className="excluir">
                                    <Tooltip title="Remover do carrinho">
                                        <IconButton
                                            size="small"
                                            onClick={() =>
                                                removeProd(product.id)
                                            }
                                        >
                                            <HighlightOffIcon color="error" />
                                        </IconButton>
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </CarrinhoCheckout>
                    );
                })}
            </main>
            <Formik
                initialValues={initialValues}
                enableReinitialize
                validationSchema={schema}
                className="tr-form"
                onSubmit={({ name, email, tel, password, cpf }) => {}}
            >
                {({
                    handleBlur,
                    handleChange,
                    handleSubmit,
                    values,
                    errors,
                    touched,
                    validateField
                }) => {
                    return (
                        <Form>
                            <small>Insira seu cupom</small>
                            <form>
                                <input type="text" />
                                <button type="submit">Aplicar</button>
                            </form>
                        </Form>
                    );
                }}
            </Formik>
            <Total>
                <small>Total</small>
                <span>{cart.totalFormatted}</span>
            </Total>
            <Link to="/checkout" className="checkout">
                <ButtonTriade
                    button="Modelo 02"
                    cor="Modelo 02"
                    letterColor="Modelo 02"
                    text="Checkout"
                />
            </Link>
        </CarrinhoMobile>
    );
};

export default Carrinho;
