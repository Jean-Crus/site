import React from 'react';

const Suporte = (props, state) => {
	
	return(
        <div className="account-session suporte">
            <div className="content-session mt-0">
                <div className="modules-dropdown">
                    <h2 className="title-1 black size-50"><strong>Perguntas Frequentes</strong></h2>
                    <ul className="list-modules mb-5"> 
                        <li className="">
                            <div className="title">
                                <span><strong>1.</strong>Curso de Programação</span>
                            </div>
                            <ul className="module-lessons">
                                <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro sunt eius mollitia sint libero ipsam modi voluptatum, omnis et neque labore voluptas itaque repellat odio provident tempore nam amet? Quibusdam!</li>
                            </ul>
                        </li>
                        <li className="">
                            <div className="title">
                                <span><strong>2.</strong>Legislação da Programação</span>
                            </div>
                            <ul className="module-lessons">
                                <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro sunt eius mollitia sint libero ipsam modi voluptatum, omnis et neque labore voluptas itaque repellat odio provident tempore nam amet? Quibusdam!</li>
                            </ul>
                        </li>
                        <li className="">
                            <div className="title">
                                <span><strong>3.</strong>O que é POG?</span>
                            </div>
                            <ul className="module-lessons">
                                <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro sunt eius mollitia sint libero ipsam modi voluptatum, omnis et neque labore voluptas itaque repellat odio provident tempore nam amet? Quibusdam!</li>
                            </ul>
                        </li>
                    </ul>
                    <h2 className="title-1 black size-50"><strong>Perguntas Frequentes</strong></h2>
                    <ul className="list-modules mb-5"> 
                        <li className="">
                            <div className="title">
                                <span><strong>1.</strong>Curso de Programação</span>
                            </div>
                            <ul className="module-lessons">
                                <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro sunt eius mollitia sint libero ipsam modi voluptatum, omnis et neque labore voluptas itaque repellat odio provident tempore nam amet? Quibusdam!</li>
                            </ul>
                        </li>
                        <li className="">
                            <div className="title">
                                <span><strong>2.</strong>Legislação da Programação</span>
                            </div>
                            <ul className="module-lessons">
                                <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro sunt eius mollitia sint libero ipsam modi voluptatum, omnis et neque labore voluptas itaque repellat odio provident tempore nam amet? Quibusdam!</li>
                            </ul>
                        </li>
                        <li className="">
                            <div className="title">
                                <span><strong>3.</strong>O que é POG?</span>
                            </div>
                            <ul className="module-lessons">
                                <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro sunt eius mollitia sint libero ipsam modi voluptatum, omnis et neque labore voluptas itaque repellat odio provident tempore nam amet? Quibusdam!</li>
                            </ul>
                        </li>
                    </ul>
                    <h2 className="title-1 black size-50"><strong>Perguntas Frequentes</strong></h2>
                    <ul className="list-modules mb-5"> 
                        <li className="">
                            <div className="title">
                                <span><strong>1.</strong>Curso de Programação</span>
                            </div>
                            <ul className="module-lessons">
                                <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro sunt eius mollitia sint libero ipsam modi voluptatum, omnis et neque labore voluptas itaque repellat odio provident tempore nam amet? Quibusdam!</li>
                            </ul>
                        </li>
                        <li className="">
                            <div className="title">
                                <span><strong>2.</strong>Legislação da Programação</span>
                            </div>
                            <ul className="module-lessons">
                                <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro sunt eius mollitia sint libero ipsam modi voluptatum, omnis et neque labore voluptas itaque repellat odio provident tempore nam amet? Quibusdam!</li>
                            </ul>
                        </li>
                        <li className="">
                            <div className="title">
                                <span><strong>3.</strong>O que é POG?</span>
                            </div>
                            <ul className="module-lessons">
                                <li>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro sunt eius mollitia sint libero ipsam modi voluptatum, omnis et neque labore voluptas itaque repellat odio provident tempore nam amet? Quibusdam!</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
	);	
}

export default Suporte;