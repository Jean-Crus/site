import React from 'react';
import { Link } from 'react-router-dom';
import {
    Container,
    TutorialMobile,
    ButtonSubmit,
    SearchBar,
    InputText,
    Voltar,
} from './styles';
import { IconSearch } from '../../../../components/Drawer/styles';

const Tutorial = () => {
    const handleSubmit = value => {
        alert(JSON.stringify(value));
    };
    return (
        <>
            <Container className="account-session tutorial">
                <div className="title-session">
                    <div>
                        <h1 className="title-1 black size-25">Tutorial</h1>
                        <p className="description-1">
                            Olá Raul Soares Silveira. Lorem ipsum dolor asit
                            amet.
                        </p>
                    </div>
                </div>
                <div className="content-session">
                    <div className="tutorial-area">
                        <div className="video">
                            <iframe src="https://www.youtube.com/embed/NOHLENkL8oE" />
                        </div>
                        <div className="content">
                            <p>
                                Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Delectus rerum necessitatibus
                                fugit voluptatum blanditiis, aliquid
                                reprehenderit amet officia a ducimus{' '}
                                <strong>laborum</strong> exercitationem,
                                laudantium quam, placeat ratione dolore.
                                Voluptatibus, cupiditate doloremque.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Delectus rerum necessitatibus
                                fugit voluptatum blanditiis, aliquid
                                reprehenderit amet officia a ducimus laborum
                                exercitationem, laudantium quam, placeat ratione
                                dolore. Voluptatibus, cupiditate doloremque.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur
                                adipisicing elit. Delectus rerum necessitatibus
                                fugit voluptatum blanditiis, aliquid
                                reprehenderit amet officia a{' '}
                                <Link to="#">ducimus</Link> laborum
                                exercitationem, laudantium quam, placeat ratione
                                dolore. Voluptatibus, cupiditate doloremque.
                            </p>
                        </div>
                    </div>
                </div>
            </Container>
            <TutorialMobile>
                <header>
                    <h2>Tutorial</h2>
                </header>
                <div className="search-bar">
                    <SearchBar onSubmit={value => handleSubmit(value)}>
                        <InputText
                            type="text"
                            name="searchfield"
                            id=""
                            placeholder="O que está procurando saber?"
                        />
                        <ButtonSubmit type="submit">
                            <IconSearch />
                        </ButtonSubmit>
                    </SearchBar>
                </div>
                <main>
                    <Voltar to="/">Voltar</Voltar>
                    <h2>
                        Sushendisse lorem ipsum dolor asit amet utito tempo
                        prosencs
                    </h2>
                    <p>
                        Suspendisse lorem ipsum dolor asit amet utito tempo
                        prosencs blagsovic blandit. Suspendisse vitae justo non
                        erat maximus blandit sagittis eu dui.
                    </p>
                    <small>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nunc ante velit, placerat volutpat dolor vel, congue
                        pretium erat. Donec at nibh at ipsum gravida imperdiet.
                        Suspendisse ex nibh, tincidunt id mollis sodales,
                        lacinia ac odio. Sed nisl eros, tincidunt in erat a,
                        tempor porta quam. Orci varius natoque penatibus et
                        magnis dis parturient montes, nascetur ridiculus mus.
                        Aliquam efficitur risus eu nunc sodales, a egestas purus
                        lorem pellentesque.
                    </small>
                    <div className="image">
                        <img
                            src="https://images.pexels.com/photos/556416/pexels-photo-556416.jpeg?cs=srgb&dl=aco-area-arvores-556416.jpg&fm=jpg"
                            alt=""
                        />
                    </div>

                    <small>
                        Nullam metus augue, lobortis in hendrerit non, tincidunt
                        in tortor. Suspendisse faucibus ex turpis, eget pharetra
                        leo tempor non. Curabitur vel egestas libero, at
                        vehicula felis. Pellentesque luctus maximus aliquet.
                        Maecenas faucibus eget urna sit amet porta. Proin ligula
                        est, varius nec enim molestie, consequat suscipit risus.
                        Duis et libero eu est condimentum ultrices aliquet sit
                        amet nibh. In scelerisque ante ac dolor tristique
                        laoreet. Nullam lacinia sapien lobortis magna cursus, ac
                        varius purus imperdiet. Nam suscipit arcu non posuere
                        malesuada. Cras a auctor arcu. Ut venenatis euismod
                        faucibus. Sed lobortis lobortis tortor vel commodo.
                    </small>

                    <div className="card-tutorial">
                        <div className="img-card">
                            <img
                                src="https://images.pexels.com/photos/356079/pexels-photo-356079.jpeg?cs=srgb&dl=ajuda-apoio-356079.jpg&fm=jpg"
                                alt=""
                            />
                        </div>

                        <div>
                            <ul>
                                <h3>Título da categoria</h3>
                                <li>- Sobre o curso</li>
                                <li>- Estrutura e didática das aulas</li>
                                <li>
                                    - Certificado amescipi lorems practmands
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="card-tutorial">
                        <div className="img-card">
                            <img
                                src="https://images.pexels.com/photos/356079/pexels-photo-356079.jpeg?cs=srgb&dl=ajuda-apoio-356079.jpg&fm=jpg"
                                alt=""
                            />
                        </div>

                        <div>
                            <ul>
                                <h3>Título da categoria</h3>
                                <li>- Sobre o curso</li>
                                <li>- Estrutura e didática das aulas</li>
                                <li>
                                    - Certificado amescipi lorems practmands
                                </li>
                            </ul>
                        </div>
                    </div>
                </main>
            </TutorialMobile>
        </>
    );
};

export default Tutorial;
