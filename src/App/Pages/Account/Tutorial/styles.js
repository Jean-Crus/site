import styled from 'styled-components';
import SearchIcon from '@material-ui/icons/Search';
import { Link } from 'react-router-dom';

export const IconSearch = styled(SearchIcon)`
    color: #fff;
`;

export const Container = styled.div`
    @media only screen and (max-width: 999px) {
        display: none;
    }
`;
export const ButtonSubmit = styled.button`
    background-color: transparent;
    border: none;
    padding: 0;
`;

export const TutorialMobile = styled.div`
    margin-top: 10px;
    header {
        border-bottom: ${props => `1px solid ${props.theme.letter2}`};
        display: flex;
        height: 40px;
        width: 100%;
        justify-content: center;
        align-items: center;
        background-color: ${props => props.theme.background};
        h2 {
            font-family: Montserrat, sans-serif;
            font-size: 16px;
            font-weight: bold;
            color: ${props => props.theme.letter};
            text-align: center;
        }
    }
    .search-bar {
        display: flex;
        justify-content: center;
        align-content: center;
    }
    main {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        margin: 0px 20px 20px 20px;

        h2 {
            font-family: Montserrat, sans-serif;
            color: ${props => props.theme.background};
            font-size: 18px;
            font-weight: 600;
            margin-bottom: 17px;
            line-height: 28px;
        }
        p {
            font-family: Montserrat, sans-serif;
            color: ${props => props.theme.letter2};
            font-size: 14px;
            font-weight: 500;
            margin-bottom: 17px;
            line-height: 25px;
        }
        small {
            font-family: Montserrat, sans-serif;
            color: ${props => props.theme.letter2};
            font-size: 12px;
            font-weight: 300;
            line-height: 20px;
            margin-bottom: 17px;
        }
        .image {
            margin-bottom: 17px;
            img {
                width: 100%;
                border-radius: 5px;
                max-width: 600px;
            }
        }

        .card-tutorial {
            display: flex;
            flex-direction: column;
            border: 1px solid #efefef;
            width: 290px;
            background-color: #ffffff;
            height: 355px;
            border-radius: 5px;
            margin-bottom: 17px;
            .img-card {
                display: flex;
                justify-content: center;
                align-items: center;
                img {
                    margin: 30px 0;
                    height: 80px;
                    width: 80px;
                }
            }
            div {
                display: flex;
                flex-direction: column;
                justify-content: center;
                ul {
                    h3 {
                        font-family: Montserrat, sans-serif;
                        color: #2f2f2f;
                        font-size: 16px;
                        font-weight: bold;
                        line-height: 19px;
                        margin-bottom: 15px;
                    }
                    li {
                        font-family: Montserrat, sans-serif;
                        color: ${props => props.theme.letter2};
                        font-size: 12px;
                        line-height: 22px;
                        list-style-type: none;
                        margin-bottom: 15px;
                        max-width: 200px;
                    }
                }
            }
        }
    }
    @media only screen and (min-width: 999px) {
        display: none;
    }
`;

export const SearchBar = styled.form`
    margin: 20px;
    height: 50px;
    display: flex;
    width: 100%;
    max-width: 600px;
    border: 1px solid #afafaf;
    border-radius: 5px;
    align-items: center;
    justify-content: center;
    .MuiSvgIcon-root {
        width: 37px;
        padding-right: 10px;
        height: 37px;
        border-radius: 5px;
        color: ${props => props.theme.primary};
        text-align: center;
    }
`;

export const InputText = styled.input`
    /* font-family: Montserrat, sans-serif; */
    color: #fff;
    background-color: transparent;
    padding-left: 10px;
    border: none;
    width: 100%;
    &::placeholder {
        font-family: Montserrat, sans-serif;
        color: #afafaf;
        font-size: 14px;
        line-height: 18px;
        font-weight: 300;
    }
    &:focus {
        outline: none;
    }
`;

export const Voltar = styled(Link)`
    align-self: flex-start;
    color: ${props => props.theme.primary};
    font-family: Montserrat, sans-serif;
    font-size: 11px;
    font-weight: bold;
    text-decoration: underline;
    margin-bottom: 17px;
`;
