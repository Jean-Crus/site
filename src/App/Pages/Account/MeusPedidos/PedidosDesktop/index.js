import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid } from "@material-ui/core";
import DoneIcon from "@material-ui/icons/Done";
import { Link } from "react-router-dom";
import * as OrderActions from "@store/modules/order/actions";
import { formatPrice } from "@util/format";
import Moment from "react-moment";
import { Loading } from "@components";
import {
    Container,
    IconExpandLess,
    IconExpandMore,
    ContainerPedido,
    GridContainer,
    StepPedidos,
    CheckBox
} from "../styles";

const PedidosDesktop = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(OrderActions.getOrdersRequest());
    }, [dispatch]);
    const user = useSelector(state => state.user);
    const orders = useSelector(state => state.order.data);
    const loading = useSelector(state => state.order.loading);
    const virtual = useSelector(state =>
        state.order.data.map(prod => prod.items.filter(item => item.virtual))
    );
    const handleExpand = id => {
        dispatch(OrderActions.expandTable(id));
    };
    return (
        <Container className="account-session meus-cursos">
            <div className="title-session">
                <div>
                    <h1 className="title-1 black size-25">Meus Pedidos</h1>
                    <p className="description-1">Olá {user.profile.name}.</p>
                </div>
            </div>
            <div className="content-session">
                <h2 className="title-1 black size-25">Pagamentos</h2>
                {loading ? (
                    <Loading />
                ) : (
                    <div className="orders-table">
                        <div className="head">
                            <GridContainer justify="center" container>
                                <Grid item lg={3} md={3}>
                                    Pedido
                                </Grid>
                                <Grid item lg={2} md={3}>
                                    Data
                                </Grid>

                                <Grid item lg={2} md={3}>
                                    Status
                                </Grid>
                                <Grid item lg={2} md={2}>
                                    Valor
                                </Grid>
                                <Grid item lg={1} md={1} />
                            </GridContainer>
                        </div>
                        {orders.map((item, index) => (
                            <ContainerPedido active={item.active} key={item.id}>
                                <GridContainer
                                    justify="center"
                                    container
                                    status={item.status.color}
                                    onClick={() => handleExpand(item.id)}
                                >
                                    <Grid item lg={3} md={3}>
                                        #{item.id}
                                    </Grid>
                                    <Grid item lg={2} md={3}>
                                        <Moment format="DD/MM/YYYY">
                                            {item.invoice_date}
                                        </Moment>
                                    </Grid>

                                    <Grid item lg={2} md={3}>
                                        {item.status.name}
                                    </Grid>
                                    <Grid item lg={2} md={2}>
                                        {formatPrice(item.total)}
                                    </Grid>
                                    <Grid item lg={1} md={1}>
                                        {item.active ? (
                                            <IconExpandLess />
                                        ) : (
                                            <IconExpandMore />
                                        )}
                                    </Grid>
                                </GridContainer>
                                <GridContainer
                                    justify="center"
                                    container
                                    className="pedido-details"
                                >
                                    <Grid item md={12} className="title">
                                        Detalhes
                                    </Grid>
                                    <Grid item md={12} className="small-title">
                                        Status do pagamento
                                    </Grid>
                                    <Grid item md={12}>
                                        <StepPedidos
                                            steps={item.status_history.length}
                                            virtual={
                                                virtual[index].length
                                                    ? undefined
                                                    : true
                                            }
                                        >
                                            <div className="steps">
                                                {item.status_history.map(
                                                    status => {
                                                        return (
                                                            <div
                                                                key={status.id}
                                                                className="step-item"
                                                            >
                                                                <CheckBox className="check">
                                                                    <DoneIcon />
                                                                </CheckBox>
                                                                <div className="step-title">
                                                                    {
                                                                        status.name
                                                                    }
                                                                </div>
                                                                <div className="step-content">
                                                                    <Moment format="DD/MM/YYYY">
                                                                        {
                                                                            status.updated_at
                                                                        }
                                                                    </Moment>{" "}
                                                                    às{" "}
                                                                    <Moment format="HH:mm:ss">
                                                                        {
                                                                            status.updated_at
                                                                        }
                                                                    </Moment>
                                                                </div>
                                                            </div>
                                                        );
                                                    }
                                                )}

                                                {/* <div className="step-item">
                                                    <CheckBox className="check">
                                                        <DoneIcon />
                                                    </CheckBox>
                                                    <div className="step-title">
                                                        Aguardando aprovação
                                                    </div>
                                                    <div className="step-content">
                                                        12/10/2016 às 14:39:00
                                                    </div>
                                                </div>
                                                <div className="step-item ">
                                                    <CheckBox className="check">
                                                        <DoneIcon />
                                                    </CheckBox>
                                                    <div className="step-title">
                                                        Pagamento
                                                    </div>
                                                    <div className="step-content">
                                                        12/10/2016 às 14:39:00
                                                    </div>
                                                </div> */}
                                            </div>
                                        </StepPedidos>
                                    </Grid>
                                    {virtual[index].length ? (
                                        <>
                                            <Grid
                                                item
                                                md={12}
                                                className="small-title"
                                            >
                                                Resumo
                                            </Grid>
                                            <Grid
                                                item
                                                md={12}
                                                container
                                                className="item-table"
                                            >
                                                <Grid item md={4}>
                                                    <div>
                                                        Endereço de entrega
                                                    </div>
                                                    <div>
                                                        R.João da Silva,180
                                                    </div>
                                                    <div>830309-210</div>
                                                    <div>Curitiba-PR</div>
                                                </Grid>
                                                <Grid item md={4}>
                                                    <div>
                                                        Endereço de entrega
                                                    </div>
                                                    <div>
                                                        R.João da Silva,180
                                                    </div>
                                                    <div>830309-210</div>
                                                    <div>Curitiba-PR</div>
                                                </Grid>
                                                <Grid item md={4}>
                                                    <div>
                                                        Endereço de entrega
                                                    </div>
                                                    <div>
                                                        R.João da Silva,180
                                                    </div>
                                                    <div>830309-210</div>
                                                    <div>Curitiba-PR</div>
                                                </Grid>
                                            </Grid>
                                            <Grid
                                                item
                                                md={12}
                                                className="small-title"
                                            >
                                                Seu pedido foi entregue dia
                                                12/03/2019
                                            </Grid>
                                        </>
                                    ) : null}
                                    {!!item.items.length && (
                                        <Grid
                                            item
                                            md={12}
                                            container
                                            className="item-table product"
                                            direction="column"
                                        >
                                            <Grid
                                                container
                                                className="header-detail"
                                            >
                                                <Grid item md={6}>
                                                    Produto
                                                </Grid>
                                                <Grid item md={2}>
                                                    Quantidade
                                                </Grid>
                                                <Grid item md={2}>
                                                    Valor unitário
                                                </Grid>
                                                <Grid item md={2}>
                                                    Valor Total
                                                </Grid>
                                            </Grid>

                                            {item.items.map(prod => {
                                                return (
                                                    <Grid
                                                        container
                                                        className="body-detail"
                                                        key={prod.id_item}
                                                    >
                                                        <Grid
                                                            item
                                                            md={6}
                                                            className="prod"
                                                        >
                                                            {!!prod.cover && (
                                                                <img
                                                                    src={
                                                                        prod.cover
                                                                    }
                                                                    alt=""
                                                                />
                                                            )}
                                                            <div>
                                                                <div className="title-detail">
                                                                    {prod.name}
                                                                </div>
                                                                <Link
                                                                    to={`/curso-aberto/${prod.id_item}`}
                                                                >
                                                                    <div>
                                                                        Ver
                                                                        produto
                                                                    </div>
                                                                </Link>
                                                            </div>
                                                        </Grid>
                                                        <Grid item md={2}>
                                                            <div className="title-detail">
                                                                {prod.quantity}
                                                            </div>
                                                        </Grid>
                                                        <Grid item md={2}>
                                                            <div className="title-detail">
                                                                {formatPrice(
                                                                    prod.price
                                                                )}
                                                            </div>
                                                        </Grid>
                                                        <Grid item md={2}>
                                                            <div className="title-detail">
                                                                {formatPrice(
                                                                    prod.price *
                                                                        prod.quantity
                                                                )}
                                                            </div>
                                                        </Grid>
                                                    </Grid>
                                                );
                                            })}
                                        </Grid>
                                    )}
                                </GridContainer>
                            </ContainerPedido>
                        ))}
                    </div>
                )}
            </div>
        </Container>
    );
};
export default PedidosDesktop;
