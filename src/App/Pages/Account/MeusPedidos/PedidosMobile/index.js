import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as OrderActions from "@store/modules/order/actions";
import { formatPrice } from "@util/format";
import Moment from "react-moment";
import { Loading, IconAlert, MyDocument, ButtonTriade } from "@components";
import {
    MeusPedidosMobile,
    CardPedidoRecorrente,
    IconExpandLess,
    IconExpandMore,
    ButtonPDF,
    Button
} from "../styles";

const PedidosMobile = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(OrderActions.getOrdersRequest());
    }, [dispatch]);
    const user = useSelector(state => state.user);
    const orders = useSelector(state => state.order.data);
    const loading = useSelector(state => state.order.loading);
    const [state, setState] = useState([
        {
            status: "pending"
        },
        { status: "packing" },
        { status: "failed" },
        { status: "delivered" },
        { status: "shipping" },
        { status: "canceled" },
        { status: "actionRequired" },
        { status: "done" }
    ]);
    const handleExpand = id => {
        dispatch(OrderActions.expandTable(id));
    };

    return (
        <MeusPedidosMobile>
            <header>Meus Pedidos</header>
            <div className="apresentacao">
                <span>Olá {user.profile.name}.</span>
            </div>
            <main>
                <h3>Pagamentos únicos</h3>
                {loading ? (
                    <Loading />
                ) : (
                    orders.map((item, index) => {
                        return (
                            <CardPedidoRecorrente
                                key={item.id}
                                // active={item.active}
                            >
                                <div
                                    className="header-card flex10"
                                    // onClick={() => handleExpand(item.id)}
                                >
                                    <IconAlert />
                                    <span>Pedido #{item.id}</span>
                                    {/* {item.active ? (
                                        <IconExpandLess />
                                    ) : (
                                        <IconExpandMore />
                                    )} */}
                                </div>
                                <div className="body-card">
                                    <div className="data flex10">
                                        <span className="attr-title">
                                            <Moment format="DD/MM/YYYY">
                                                {item.invoice_date}
                                            </Moment>
                                        </span>
                                        <small>{formatPrice(item.total)}</small>
                                    </div>
                                    <div className="status flex10">
                                        <span className="attr-title">
                                            Status
                                        </span>
                                        <small>{item.status.name}</small>
                                    </div>
                                </div>
                                {/* <div className="total flex10">
                                    <ButtonPDF
                                        document={<MyDocument order={item} />}
                                        fileName="somename.pdf"
                                    >
                                        {({ blob, url, loading, error }) => {
                                            return loading ? (
                                                <ButtonTriade
                                                    type="button"
                                                    button="Modelo 02"
                                                    letterColor="Modelo 02"
                                                    cor="Modelo 02"
                                                    text="Pedido"
                                                />
                                            ) : (
                                                <ButtonTriade
                                                    button="Modelo 02"
                                                    letterColor="Modelo 02"
                                                    cor="Modelo 02"
                                                    type="button"
                                                    text="Carregando pedido..."
                                                />
                                            );
                                        }}
                                    </ButtonPDF>
                                    <ButtonTriade
                                        className="mt-2"
                                        type="button"
                                        // button="Modelo 02"
                                        // letterColor="Modelo 02"
                                        // cor="Modelo 02"
                                        text="Nota Fiscal"
                                        disabled
                                    />
                                </div> */}
                            </CardPedidoRecorrente>
                        );
                    })
                )}
            </main>
        </MeusPedidosMobile>
    );
};

export default PedidosMobile;
