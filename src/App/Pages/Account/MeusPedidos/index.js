import React, { useState, useEffect } from 'react';
import PedidosDesktop from './PedidosDesktop';
import PedidosMobile from './PedidosMobile';

const MeusPedidos = () => {
    return (
        <>
            <PedidosDesktop />
            <PedidosMobile />
        </>
    );
};

export default MeusPedidos;
