import styled from "styled-components";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Grid } from "@material-ui/core";
import { PDFDownloadLink } from "@react-pdf/renderer";
import { ButtonTriade } from "@components";

export const IconExpandMore = styled(ExpandMoreIcon)`
    color: ${props => props.theme.primary};
`;
export const IconExpandLess = styled(ExpandLessIcon)`
    color: ${props => props.theme.primary};
`;

export const Container = styled.div`
    .orders-table {
        box-shadow: none;
        border: 1px solid #ececec;
        border-bottom: none;
    }
    .head {
        border-bottom: 1px solid #ececec;
        .MuiGrid-container {
            .MuiGrid-item {
                font-family: Montserrat;
                font-size: 12px;
                font-weight: 300;
                color: #afafaf;
            }
        }
    }
    @media only screen and (max-width: 999px) {
        display: none;
    }
`;

export const MeusPedidosMobile = styled.div`
    margin-top: 10px;
    min-height: 50vh;

    header {
        border-bottom: ${props => `1px solid ${props.theme.letter2}`};
        display: flex;
        height: 40px;
        justify-content: center;
        align-items: center;
        background: linear-gradient(0deg, #0058a8 0%, #0058a8 100%);
        font-family: Montserrat, sans-serif;
        font-size: 16px;
        font-weight: bold;
        color: ${props => props.theme.letter};
        text-align: center;
    }
    .apresentacao {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        margin: 25px 0;
        span {
            font-family: Montserrat, sans-serif;
            color: ${props => props.theme.letter2};
            font-size: 12px;
            font-weight: 300;
            line-height: 20px;
        }
    }
    main {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        flex-wrap: wrap;
        margin-bottom: 25px;

        h3 {
            font-family: Montserrat, sans-serif;
            color: #2f2f2f;
            font-size: 14px;
            font-weight: bold;
            line-height: 24px;
        }
    }
    @media only screen and (min-width: 999px) {
        display: none;
    }
`;

export const CardPedidoRecorrente = styled.div`
    display: flex;
    flex-direction: column;
    background-color: #ffffff;
    border: 1px solid #ececec;
    width: 267px;
    border-radius: 5px;
    margin-bottom: 20px;
    .attr-title {
        color: ${props => props.theme.letter2};
        font-family: Montserrat, sans-serif;
        font-size: 12px;
        line-height: 15px;
    }
    .flex10 {
        padding: 15px;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    .header-card {
        background-color: #f8f8f8;
        svg {
            font-size: 25px;
            color: ${props => {
                switch (props.status) {
                    case "pending":
                        return "#0080ff";
                    case "packing":
                        return props.theme.primary;
                    case "failed":
                        return "#da0202";
                    case "delivered":
                        return "#5bc100";
                    case "shipping":
                        return "#116293";
                    case "canceled":
                        return "#dc0707";
                    case "actionRequired":
                        return "#ff9a00";
                    case "done":
                        return "#5bc100";
                    default:
                        return null;
                }
            }};
            &:last-child {
                color: ${props => props.theme.primary};
            }
        }
        span {
            color: ${props => props.theme.background};
            font-family: Montserrat, sans-serif;
            font-size: 14px;
        }
    }

    .body-card {
        flex-direction: column;
    }

    .total {
        display: ${props => (props.active ? "flex" : "none")};
        flex-direction: column;
        border-top: 1px solid #e6e6e6;
        small {
            font-family: Montserrat, sans-serif;
            font-size: 16px;
            color: ${props => props.theme.background};
            line-height: 19px;
        }
    }

    .data {
        display: ${props => {
                switch (props.status) {
                    case "Pending":
                        return "none";
                    case "concluido":
                        return "none";
                    case "negado":
                        return "flex";

                    default:
                        return "flex";
                }
            }}
            small {
            color: ${props => props.theme.background};
            font-family: Montserrat, sans-serif;
            font-size: 12px;
            line-height: 15px;
        }
    }

    .status {
        font-family: Montserrat, sans-serif;
        font-size: 12px;
        line-height: 15px;
        color: ${props => {
            switch (props.status) {
                case "pending":
                    return "#0080ff";
                case "packing":
                    return "${props => props.theme.primary}";
                case "failed":
                    return "#da0202";
                case "delivered":
                    return "#5bc100";
                case "shipping":
                    return "#116293";
                case "canceled":
                    return "#dc0707";
                case "actionRequired":
                    return "#ff9a00";
                case "done":
                    return "#5bc100";
                default:
                    return null;
            }
        }};
    }
`;

export const ButtonPDF = styled(PDFDownloadLink)`
    button {
        /* height: 40px; */
    }
`;

export const ContainerPedido = styled.div`
    .MuiGrid-container {
        border-bottom: ${props =>
            props.active
                ? `2px solid ${props.theme.primary}`
                : "1px solid #ececec"};
        cursor: pointer;
    }

    .pedido-details {
        display: ${props => (props.active ? "flex" : "none")};
        flex-direction: column;
        border-bottom: 1px solid #ececec;
        cursor: default;
        font-family: Montserrat;
        .title {
            font-size: 14px;
            font-weight: bold;
            color: #31394d;
        }
        .small-title {
            font-size: 12px;
            font-weight: bold;
            color: #48484c;
            margin-bottom: 10px;
        }
        .MuiGrid-item {
            padding: 0;
            margin-top: 20px;
        }
        .item-table {
            background-color: #f7f7f7;
            border: none;
            padding: 20px;
            margin: 0;
            margin-bottom: 30px;
            cursor: default;
            .MuiGrid-item {
                margin: 0;
                div {
                    font-size: 12px;
                    color: #48484c;
                    font-weight: 300;
                    &:first-child {
                        font-size: 13px;
                        font-weight: bold;
                        color: #48484c;
                    }
                }
            }
        }
        .product {
            background-color: #ffffff;
            border: 1px solid #ececec;
            border-radius: 5px;
            padding: 0;
            .MuiGrid-container {
                cursor: default;
                border: none;
            }

            .MuiGrid-item {
                padding: 20px 0;
                padding-left: 10px;

                &:last-child {
                    border-left: 1px solid #ececec;
                    text-align: center;
                    padding-left: 0;
                    .title-detail {
                        width: 100%;
                        justify-content: center;
                    }
                }
            }
            .header-detail {
                border-bottom: 1px solid #ececec;
                .MuiGrid-item {
                    font-size: 12px;
                    font-weight: 300;
                    color: #7d7474;
                }
            }
            .body-detail {
                border-bottom: 1px solid #ececec;
                &:last-child {
                    border-bottom: none;
                }
                .title-detail {
                    color: #2e2e2e !important;
                    font-size: 14px !important;
                    font-weight: 600 !important;
                }
                div {
                    display: flex;
                    align-items: center;
                }
                .prod {
                    display: flex;
                    img {
                        height: 58px;
                        width: 87px;
                        margin-right: 20px;
                    }
                    div {
                        flex-direction: column;
                        align-items: flex-start;
                    }
                    a {
                        div {
                            font-size: 12px;
                            font-family: Montserrat;
                            color: ${props => props.theme.primary};
                            font-weight: 600px;
                            text-decoration: underline;
                        }
                    }
                }
            }
        }
    }
`;

export const GridContainer = styled(Grid)`
    width: 100%;
    padding: 0 30px;
    .MuiGrid-item {
        padding: 20px 0;
        font-family: Montserrat;
        font-size: 16px;
        font-weight: 600;
        &:nth-child(3) {
            color: ${props => `${props.status}`} !important;
        }
    }
`;

export const StepPedidos = styled.div`
    position: relative;
    height: 50px;
    text-align: left;
    font-family: Montserrat;
    margin-bottom: ${props => (props.virtual ? "30px" : "0")};
    .steps {
        display: flex;
        position: absolute;
        justify-content: space-between;
        z-index: 2;
        top: -15px;
    }

    .bar {
        width: 100%;
        position: absolute;
        height: 3px;
        top: 0;
        background-color: ${props => props.theme.primary};
        border-radius: 5px;
    }
    .step-item {
        display: flex;
        flex-direction: column;
        width: 200px;
        .step-title {
            font-size: 13px;
            color: #31394d;
            font-weight: bold;
        }
        .step-content {
            font-size: 12px;
            font-weight: 300;
            color: #48484c;
        }
        &::before {
            content: "";
            width: 100%;
            max-width: 200px;
            position: absolute;
            height: 3px;
            top: 15px;
            background-color: ${props => props.theme.primary};
            border-radius: 5px;
            z-index: 0;
        }
        &:last-child {
            &::before {
                background-color: white;
            }
        }
    }
`;

export const CheckBox = styled.div`
    display: flex;
    border-radius: 50%;
    width: 30px;
    height: 30px;
    background-color: ${props => props.theme.primary};
    justify-content: center;
    align-items: center;
    z-index: 1;

    svg {
        color: ${props => props.theme.letter};
    }
`;
