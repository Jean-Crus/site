import React from 'react';
import { pdf } from '@images';
import { Link } from 'react-router-dom';

const Biblioteca = (props, state) => {

    state = {
        cursos:[
            {},{},{},{}
        ]
    }
	
	return(
        <div className="account-session biblioteca">
            <div className="title-session">
                <div>
                    <h1 className="title-1 black size-25">Biblioteca</h1>
                    <p className="description-1">Olá Raul Soares Silveira. Lorem ipsum dolor asit amet.</p>
                </div>
            </div>
            <div className="content-session">
                <button id="select-biblioteca" className="tr-multiSelect">
                    <div className="select">Selecione</div>
                    <ul>
                        <li>Opção 1</li>
                        <li>Opção 2</li>
                        <li>Opção 3</li>
                    </ul>
                </button>
                <div className="row">
                    {
                        state.cursos.map((item, index) => ( 
                        <div className="col-sm-4">
                            <div className="biblioteca-item">
                                <h2 className="title-1 black"><Link to="#">Teste</Link></h2>
                                <p className="description-1">Conhaça o Curso de Reciclagem para CNH para condutores. </p>
                                <Link to="#" className="download-link"><img src={pdf} alt="PDF"/>Download do PDF</Link>
                            </div>
                        </div>
                        ))
                    }
                </div>
            </div>
        </div>
	);	
}

export default Biblioteca;