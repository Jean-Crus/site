import React from 'react';
import { SidebarAccount } from '@components';
import { curso3 } from '@images';
import { Link } from 'react-router-dom';

const MeusEventos = (props, state) => {

    state = {
        cursos:[
            {},{},{},{}
        ]
    }

    const style = {
        curso3:{
            backgroundImage: "url('" + curso3 + "')"
        }
    }
	
	return(
        <div className="account-session meus-cursos">
            <div className="title-session">
                <div>
                    <h1 className="title-1 black size-25">Meus Eventos <span className="count-alert">(2)</span></h1>
                    <p className="description-1">Olá Raul Soares Silveira. Lorem ipsum dolor asit amet.</p>
                </div>
                <div>
                    <div className="toggle-buttons">
                        <button className="active">Online (3)</button>
                        <button>Presenciais (3)</button>
                    </div>
                </div>
            </div>
            <div className="content-session">
                {
                    state.cursos.map((item, index) => ( 
                        <div className="tr-card-event-flat account-event">
                            <div className="date">
                                <div className="date-number">
                                    <p>26</p>
                                    <b>out</b>
                                </div>
                            </div>
                            <div className="information">
                                <div className="content">
                                    <Link to="#" className="title-2 size-50 black">
                                        Workshop Lorem Ipsum
                                    </Link>
                                    <div className="local">
                                        <div className="hours description-3"> 9h -10h </div>
                                        <div className="type-event description-3 local">Centro de Convenções</div>
                                    </div>
                                    <div className="description">
                                        <div className="description-2">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales magna pus semper  lorem..
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="image-full">
                                <span className="image" style={style.curso3}></span>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
	);	
}

export default MeusEventos;