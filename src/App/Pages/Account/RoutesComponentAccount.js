import React, { Component } from 'react';
import { Switch } from 'react-router-dom';
import MeusCursos from '@app/Pages/Account/MeusCursos/';
import MeusEventos from '@app/Pages/Account/MeusEventos/';
import Forum from '@app/Pages/Account/Forum/';
import Tutor from '@app/Pages/Account/Tutor/';
import Certificado from '@app/Pages/Account/Certificado/';
import Biblioteca from '@app/Pages/Account/Biblioteca/';
import Conquistas from '@app/Pages/Account/Conquistas/';
import MeusPedidos from '@app/Pages/Account/MeusPedidos/';
import Configurar from '@app/Pages/Account/Configurar/';
import Tutorial from '@app/Pages/Account/Tutorial/';
import Suporte from '@app/Pages/Account/Suporte/';
import Route from './Route';

function RoutesComponentAccount() {
    return (
        <Switch>
            <Route exact path="/conta" component={MeusCursos} />
            <Route path="/conta/meus-eventos" component={MeusEventos} />
            <Route path="/conta/forum" component={Forum} />
            <Route path="/conta/falar-com-tutor" component={Tutor} />
            <Route path="/conta/certificado" component={Certificado} />
            <Route path="/conta/biblioteca" component={Biblioteca} />
            <Route path="/conta/conquistas" component={Conquistas} />
            <Route path="/conta/meus-pedidos" component={MeusPedidos} />
            <Route path="/conta/configurar-conta" component={Configurar} />
            <Route path="/conta/tutorial" component={Tutorial} />
            <Route path="/conta/suporte-da-plataforma" component={Suporte} />
        </Switch>
    );
}

export default RoutesComponentAccount;
