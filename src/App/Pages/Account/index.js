import React from 'react';

import { SidebarAccount } from '@components';
import RoutesComponentAccount from './RoutesComponentAccount';
import { Container } from './styles';

function Account({ history }) {
    return (
        <Container className="my-account-area">
            <div className="container">
                <div className="row">
                    <div className="col-sm-3">
                        <SidebarAccount history={history.location} />
                    </div>
                    <div className="col-sm-9">
                        <RoutesComponentAccount />
                    </div>
                </div>
            </div>
        </Container>
    );
    // );
}

export default Account;
