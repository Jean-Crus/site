import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { TextField } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { signInRequest } from '@store/modules/auth/actions';
import { logo } from '@images';
import { ButtonTriade } from '@components';
import useStyles from './useStyle';
import { Container } from './styles';

const initialValues = {
    password: '',
    repassword: '',
};

const schema = Yup.object().shape({
    password: Yup.string().required('Password is required'),
    repassword: Yup.string().oneOf(
        [Yup.ref('password'), null],
        'As senhas devem ser iguais'
    ),
});

const NewPassword = props => {
    const loading = useSelector(state => state.app.loading);
    const dispatch = useDispatch();
    const { textField } = useStyles();

    return (
        <Container className="bg-black tr-entrar">
            <div className="container">
                <div className="content">
                    <div className="information">
                        <img src={logo} alt="Professor Carlos André" srcSet="" />
                        <div className="divider" />
                        <div className="title-1 ff-18">Recupere a senha</div>
                    </div>
                    <div className="form-login">
                        <Formik
                            initialValues={initialValues}
                            enableReinitialize
                            validationSchema={schema}
                            className="tr-form"
                            onSubmit={({ password, repassword }) => {
                                dispatch(signInRequest(password, repassword));
                            }}
                        >
                            {({
                                handleBlur,
                                handleChange,
                                handleSubmit,
                                values,
                                errors,
                                touched,
                                error,
                            }) => {
                                return (
                                    <form
                                        onSubmit={handleSubmit}
                                        className="tr-form"
                                    >
                                        <TextField
                                            fullWidth
                                            helperText={
                                                touched.password &&
                                                errors.password
                                            }
                                            error={
                                                !!touched.password &&
                                                !!errors.password
                                            }
                                            label="Senha*"
                                            type="password"
                                            margin="normal"
                                            variant="filled"
                                            name="password"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.password}
                                            className={textField}
                                        />
                                        <TextField
                                            fullWidth
                                            helperText={
                                                touched.repassword &&
                                                errors.repassword
                                            }
                                            error={
                                                !!touched.repassword &&
                                                !!errors.repassword
                                            }
                                            label="Repita a senha*"
                                            type="password"
                                            margin="normal"
                                            variant="filled"
                                            name="repassword"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.repassword}
                                            className={textField}
                                        />

                                        <div className="submit">
                                            <ButtonTriade
                                                type="submit"
                                                button="secondary"
                                                className="ff-14"
                                                icon="create_account"
                                                text={
                                                    loading
                                                        ? 'Carregando...'
                                                        : 'Enviar'
                                                }
                                            />
                                            <div>
                                                <span>
                                                    Primeiro acesso?{' '}
                                                    <Link to="/create-account">
                                                        Criar uma conta
                                                    </Link>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                );
                            }}
                        </Formik>
                    </div>
                </div>
            </div>
        </Container>
    );
};

export default NewPassword;
