import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginBottom: theme.spacing(1),
        borderRadius: '5px',
        color: '#fff',
        '& .MuiInputBase-input': {
            color: '#FFFFFFFF',
        },
        '& .MuiFormLabel-root': {
            color: '#AFAFAF',
        },
        '& .MuiOutlinedInput-notchedOutline': {
            border: '1px solid #AFAFAF',
        },
        '& .MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline': {
            border: `2px solid #3f51b5`,
            borderRadius: '5px',
        },
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
}));
export default useStyles;
