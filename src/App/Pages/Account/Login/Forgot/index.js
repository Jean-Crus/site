import React from "react";
import { useSelector, useDispatch } from "react-redux";
import qs from "qs";
import * as Yup from "yup";
import { Formik } from "formik";
import { TextField } from "@material-ui/core";
import { Link } from "react-router-dom";
import {
    signInRequest,
    lostPasswordRequest,
    resetPasswordRequest
} from "@store/modules/auth/actions";
import logo from "@images/logo.png";
import { ButtonTriade, TextDefault } from "@components";
import useStyles from "./useStyle";
import { Container } from "./styles";

const initialValues = {
    email: ""
};
const initialEmailValues = {
    email: "",
    password: "",
    confirmPassword: ""
};

const schema = Yup.object().shape({
    email: Yup.string()
        .email("E-mail inválido")
        .required("E-mail obrigatório")
    // password: Yup.string().required("Senha obrigatória"),
    // confirmPassword: Yup.string("Coloque sua senha")
    //     .required("Confirme sua senha")
    //     .oneOf([Yup.ref("password")], "Senhas diferentes")
});

const schemaEmail = Yup.object().shape({
    email: Yup.string()
        .email("E-mail inválido")
        .required("E-mail obrigatório"),
    password: Yup.string()
        .required("Senha obrigatória")
        .min(8, "Senha deve ter no mínimo 8 caracteres"),
    confirmPassword: Yup.string("Coloque sua senha")
        .required("Confirme sua senha")
        .oneOf([Yup.ref("password")], "Senhas diferentes")
});

const ForgotLogin = ({ location: { search } }) => {
    const tokenParam = qs.parse(search, { ignoreQueryPrefix: true }).token;
    const loading = useSelector(state => state.auth.loading);
    const dispatch = useDispatch();
    const { textField } = useStyles();
    return (
        <Container className="bg-black tr-entrar">
            <div className="container">
                <div className="content">
                    <div className="information">
                        <img
                            src={logo}
                            alt="Professor Carlos André"
                            srcSet=""
                        />
                        <div className="divider" />
                        <div className="title-1 ff-18">Esqueci a senha</div>
                    </div>
                    {!tokenParam && (
                        <div className="form-login">
                            <Formik
                                initialValues={initialValues}
                                enableReinitialize
                                validationSchema={schema}
                                className="tr-form"
                                onSubmit={({ email }) => {
                                    dispatch(lostPasswordRequest(email));
                                }}
                            >
                                {({
                                    handleBlur,
                                    handleChange,
                                    handleSubmit,
                                    values,
                                    errors,
                                    touched,
                                    error
                                }) => {
                                    return (
                                        <form
                                            onSubmit={handleSubmit}
                                            className="tr-form"
                                        >
                                            <TextDefault
                                                fullWidth
                                                helperText={
                                                    touched.email &&
                                                    errors.email
                                                }
                                                error={
                                                    !!touched.email &&
                                                    !!errors.email
                                                }
                                                label="E-mail*"
                                                type="text"
                                                margin="normal"
                                                variant="filled"
                                                name="email"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.email}
                                            />

                                            <div className="submit">
                                                <ButtonTriade
                                                    button="Modelo 02"
                                                    letterColor="Modelo 02"
                                                    cor="Modelo 02"
                                                    type="submit"
                                                    onClick={handleSubmit}
                                                    key={loading}
                                                    text={
                                                        loading
                                                            ? "Carregando..."
                                                            : "Enviar"
                                                    }
                                                />
                                                <div>
                                                    <span>
                                                        Primeiro acesso?{" "}
                                                        <Link to="/create-account">
                                                            Criar uma conta
                                                        </Link>
                                                    </span>
                                                </div>
                                            </div>
                                        </form>
                                    );
                                }}
                            </Formik>
                        </div>
                    )}
                    {tokenParam && (
                        <div className="form-login">
                            <Formik
                                initialValues={initialEmailValues}
                                enableReinitialize
                                validationSchema={schemaEmail}
                                className="tr-form"
                                onSubmit={({
                                    email,
                                    password,
                                    confirmPassword
                                }) => {
                                    dispatch(
                                        resetPasswordRequest(
                                            email,
                                            password,
                                            confirmPassword,
                                            tokenParam
                                        )
                                    );
                                }}
                            >
                                {({
                                    handleBlur,
                                    handleChange,
                                    handleSubmit,
                                    values,
                                    errors,
                                    touched,
                                    error
                                }) => {
                                    return (
                                        <form
                                            onSubmit={handleSubmit}
                                            className="tr-form"
                                        >
                                            <TextDefault
                                                fullWidth
                                                helperText={
                                                    touched.email &&
                                                    errors.email
                                                }
                                                error={
                                                    !!touched.email &&
                                                    !!errors.email
                                                }
                                                label="E-mail*"
                                                type="text"
                                                margin="normal"
                                                variant="filled"
                                                name="email"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.email}
                                            />
                                            <TextDefault
                                                fullWidth
                                                helperText={
                                                    touched.password &&
                                                    errors.password
                                                }
                                                error={
                                                    !!touched.password &&
                                                    !!errors.password
                                                }
                                                label="Senha*"
                                                type="password"
                                                margin="normal"
                                                variant="filled"
                                                name="password"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.password}
                                            />
                                            <TextDefault
                                                fullWidth
                                                helperText={
                                                    touched.confirmPassword &&
                                                    errors.confirmPassword
                                                }
                                                error={
                                                    !!touched.confirmPassword &&
                                                    !!errors.confirmPassword
                                                }
                                                label="Confirme a senha*"
                                                type="password"
                                                margin="normal"
                                                variant="filled"
                                                name="confirmPassword"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.confirmPassword}
                                            />

                                            <div className="submit">
                                                <ButtonTriade
                                                    button="Modelo 02"
                                                    letterColor="Modelo 02"
                                                    cor="Modelo 02"
                                                    type="submit"
                                                    onClick={handleSubmit}
                                                    key={loading}
                                                    text={
                                                        loading
                                                            ? "Carregando..."
                                                            : "Enviar"
                                                    }
                                                />
                                                <div>
                                                    <span>
                                                        Primeiro acesso?{" "}
                                                        <Link to="/create-account">
                                                            Criar uma conta
                                                        </Link>
                                                    </span>
                                                </div>
                                            </div>
                                        </form>
                                    );
                                }}
                            </Formik>
                        </div>
                    )}
                </div>
            </div>
        </Container>
    );
};

export default ForgotLogin;
