import styled from "styled-components";

export const Container = styled.div`
    width: 100%;
    background-color: #fff !important;

    .container {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .content {
        width: 100%;
        max-width: 600px;
        border: ${props => `1px solid ${props.theme.letter2}`};
        border-radius: 5px;
        padding: 40px;
        display: flex;
        background: ${props =>
            `linear-gradient(0deg, #012a50 0%, #0058a8 100%)`} !important;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        .form-login {
            width: 100%;
        }
        .information {
            margin: 40px 0 20px 0;
            display: flex;
            justify-content: center;
            align-items: center;
            img {
                width: 100px;
            }
            .divider {
                border-left: ${props => `3px solid ${props.theme.letter}`};
                height: 20px;
                margin-left: 5px;
            }
            .title-1 {
                margin-left: 5px;
            }
            @media only screen and (max-width: 568px) {
                flex-direction: column;
                .divider {
                    display: none;
                }
                .title-1 {
                    margin-top: 20px;
                    font-size: 18px;
                    font-weight: bold;
                }
            }
        }

        .forgot {
            display: flex;
            justify-content: space-between;
            margin: 20px 0 50px 0;

            a {
                font-family: Montserrat;
                color: #afafaf;
                font-size: 10px;
                font-weight: bold;
                text-decoration: underline;
            }
        }

        .submit {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;

            a {
                font-family: Montserrat;
                color: #afafaf;
                font-size: 10px;
                font-weight: bold;
                text-decoration: underline;
            }
            span {
                font-family: Montserrat;
                color: #afafaf;
                font-size: 10px;
            }
        }
    }
`;
