import React from "react";
import { useSelector, useDispatch } from "react-redux";
import * as Yup from "yup";
import { Formik } from "formik";
import { Link } from "react-router-dom";
import { signInRequest } from "@store/modules/auth/actions";
import logo from "@images/logo.png";
import { ButtonTriade, TextDefault } from "@components";
import { Grid } from "@material-ui/core";
import { Container } from "./styles";

const initialValues = {
    email: "",
    password: ""
};

const schema = Yup.object().shape({
    email: Yup.string()
        .email("E-mail inválido")
        .required("E-mail obrigatório"),
    password: Yup.string().required("Senha obrigatória")
});

const Login = props => {
    const loading = useSelector(state => state.auth.loading);
    const dispatch = useDispatch();

    return (
        <Container className="bg-black tr-entrar">
            <div className="container">
                <div className="content">
                    <div className="information">
                        <img
                            src={logo}
                            alt="Professor Carlos André"
                            srcSet=""
                        />
                        <div className="divider" />
                        <div className="title-1 ff-18">Entrar</div>
                    </div>
                    <Grid container className="form-login">
                        <Formik
                            initialValues={initialValues}
                            enableReinitialize
                            validationSchema={schema}
                            className="tr-form"
                            onSubmit={({ email, password }) => {
                                dispatch(signInRequest(email, password));
                            }}
                        >
                            {({
                                handleBlur,
                                handleChange,
                                handleSubmit,
                                values,
                                errors,
                                touched,
                                error
                            }) => {
                                return (
                                    <form
                                        onSubmit={handleSubmit}
                                        className="tr-form"
                                    >
                                        <TextDefault
                                            fullWidth
                                            helperText={
                                                touched.email && errors.email
                                            }
                                            error={
                                                !!touched.email &&
                                                !!errors.email
                                            }
                                            label="E-mail*"
                                            type="text"
                                            margin="normal"
                                            variant="filled"
                                            name="email"
                                            onChange={handleChange}
                                            errorColor="#fff"
                                            onBlur={handleBlur}
                                            value={values.email}
                                        />
                                        <TextDefault
                                            fullWidth
                                            helperText={
                                                touched.password &&
                                                errors.password
                                            }
                                            error={
                                                !!touched.password &&
                                                !!errors.password
                                            }
                                            label="Senha*"
                                            type="password"
                                            margin="normal"
                                            variant="filled"
                                            name="password"
                                            onChange={handleChange}
                                            errorColor="#fff"
                                            onBlur={handleBlur}
                                            value={values.password}
                                        />

                                        <div className="forgot">
                                            <Link to="/reset-password">
                                                Esqueceu sua senha?
                                            </Link>
                                        </div>
                                        <div className="submit">
                                            <ButtonTriade
                                                type="submit"
                                                cor="Modelo 02"
                                                letterColor="Modelo 02"
                                                button="Modelo 02"
                                                className="mt-3"
                                                icon="fas fa-create-account"
                                                key={loading}
                                                text={
                                                    loading
                                                        ? "Carregando..."
                                                        : "Entrar"
                                                }
                                            />
                                            <div>
                                                <span>
                                                    Primeiro acesso?{" "}
                                                    <Link to="/create-account">
                                                        Criar uma conta
                                                    </Link>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                );
                            }}
                        </Formik>
                    </Grid>
                </div>
            </div>
        </Container>
    );
};

export default Login;
