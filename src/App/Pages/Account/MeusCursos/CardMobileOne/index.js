import React from "react";
import { LinearProgress, Grid } from "@material-ui/core";
import { convertText } from "@util/tools";
import { Link } from "react-router-dom";
import waitPayment from "@images/waitPayment.svg";
import {
    Container,
    MeusCursosMobile,
    CardIcon,
    IconCard,
    CardOne,
    CardTwo
} from "../styles";

const CardMobileOne = ({ active, item }) => {
    return (
        <Link
            key={item.id}
            to={`/curso-apresentacao/${item.id}`}
            className="mt-3"
        >
            <CardOne
                active={active}
                tag={!!item.category && item.category.color}
                img={item.cover}
            >
                <div className="image">
                    {!!item.category && (
                        <div className="tag">
                            <span>{!!item.category && item.category.name}</span>
                        </div>
                    )}
                </div>

                <div className="title-progress">
                    <span>{convertText(item.name, 20)}</span>
                    <LinearProgress
                        color="primary"
                        variant="determinate"
                        value={item.user_progress}
                    />
                    <span>{item.user_progress}%</span>
                </div>
                {/* <Grid container className="container-waiting">
                    <Grid
                        item
                        xs={10}
                        sm={10}
                        md={10}
                        lg={10}
                        xl={10}
                        className="cont-text"
                    >
                        Aguardando pagamento
                    </Grid>
                    <Grid item xs={2} sm={2} md={2} lg={2} xl={2}>
                        <img src={waitPayment} alt="" srcSet="" />
                    </Grid>
                </Grid> */}
            </CardOne>
        </Link>
    );
};

export default CardMobileOne;
