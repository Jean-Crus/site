import React from "react";
import { LinearProgress, Grid } from "@material-ui/core";
import { convertText } from "@util/tools";
import { Link } from "react-router-dom";
import waitPayment from "@images/waitPayment.svg";
import {
    Container,
    MeusCursosMobile,
    CardIcon,
    IconCard,
    CardOne,
    CardTwo
} from "../styles";

const CardMobileTwo = ({ active, item }) => {
    return (
        <Link
            key={item.id}
            to={`/curso-apresentacao/${item.id}`}
            className="card-two mt-3"
        >
            <CardTwo
                active={active}
                tag={!!item.category && item.category.color}
            >
                <Grid container justify="center" className="mt-2">
                    <Grid
                        item
                        xs={4}
                        sm={4}
                        md={4}
                        lg={4}
                        xl={4}
                        className="img-course"
                    >
                        <img
                            className="back-course"
                            src={item.cover}
                            alt="Foto curso"
                        />
                    </Grid>
                    <Grid
                        container
                        item
                        justify="center"
                        xs={7}
                        sm={7}
                        md={7}
                        lg={7}
                        xl={7}
                    >
                        {!!item.category && (
                            <Grid
                                item
                                xs={10}
                                sm={10}
                                md={10}
                                lg={10}
                                xl={10}
                                className="tag"
                            >
                                {!!item.category && item.category.name}
                            </Grid>
                        )}
                        <Grid
                            item
                            xs={10}
                            sm={10}
                            md={10}
                            lg={10}
                            xl={10}
                            className="title-progress"
                        >
                            {convertText(item.name, 20)}
                        </Grid>
                    </Grid>
                </Grid>

                <Grid container className="barra mb-2">
                    <Grid item xs={10} sm={10} md={10} lg={10} xl={10}>
                        <LinearProgress
                            color="primary"
                            variant="determinate"
                            value={item.user_progress}
                        />
                    </Grid>
                    <Grid item xs={10} sm={10} md={10} lg={10} xl={10}>
                        <span>{item.user_progress}%</span>
                    </Grid>
                </Grid>
                {/* <Grid container className="container-waiting">
                    <Grid
                        item
                        xs={10}
                        sm={10}
                        md={10}
                        lg={10}
                        xl={10}
                        className="cont-text"
                    >
                        Aguardando pagamento
                    </Grid>
                    <Grid item xs={2} sm={2} md={2} lg={2} xl={2}>
                        <img src={waitPayment} alt="" srcSet="" />
                    </Grid>
                </Grid> */}
            </CardTwo>
        </Link>
    );
};

export default CardMobileTwo;
