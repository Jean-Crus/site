import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Grid } from "@material-ui/core";
import * as UserActions from "@store/modules/user/actions";
import history from "@routes/history";
import { Spinner } from "react-bootstrap";
import { SelectDefault } from "@components";
import { Container, MeusCursosMobile, CardIcon, IconCard } from "./styles";
import CardMobileOne from "./CardMobileOne";
import CardMobileTwo from "./CardMobileTwo";

const MeusCursos = () => {
    const [screen, setScreen] = useState({
        active: "cardOne"
    });

    const dispatch = useDispatch();
    const loading = useSelector(state => state.user.loading);

    useEffect(() => {
        dispatch(UserActions.getMyCoursesRequest());
    }, [dispatch]);

    const ativo = useSelector(state => state.user.ativo);
    const myCourses = useSelector(state => state.user.data);
    const typeCourse = useSelector(state => state.user.typeCourse);

    const courseExpired = useSelector(state =>
        state.user.data.filter(item => item.expired)
    );
    const courseActive = useSelector(state =>
        state.user.data.filter(item => !item.expired)
    );

    const profile = useSelector(state => state.user.profile);
    const handleScreen = active => {
        setScreen({ active });
    };

    const handleClick = id => {
        history.push(`/curso-apresentacao/${id}`);
    };

    return loading ? (
        <Grid container justify="center" className="mt-4 mb-4">
            <Spinner animation="border" role="status" />
        </Grid>
    ) : (
        <>
            <Container className="account-session meus-cursos">
                <Grid className="title-session" container>
                    <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
                        <h1 className="title-1 black size-25">
                            Meus Cursos
                            <span className="count-alert">
                                ({myCourses.length})
                            </span>
                        </h1>
                        <p className="description-1">Olá {profile.name}.</p>
                    </Grid>
                    <Grid item container xs={6} sm={6} md={6} lg={6} xl={6}>
                        <Grid
                            item
                            xs={12}
                            sm={12}
                            md={12}
                            lg={12}
                            xl={12}
                            className="toggle-buttons"
                        >
                            <SelectDefault
                                campos={typeCourse}
                                label="Estado dos cursos"
                                value={ativo}
                                onChange={event =>
                                    dispatch(
                                        UserActions.getAtivos(
                                            event.target.value
                                        )
                                    )
                                }
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <div className="content-session">
                    <div className="row">
                        {ativo === 0 &&
                            courseActive.map(course => (
                                <>
                                    <CardMobileOne
                                        item={course}
                                        active={screen.active}
                                    />
                                    <CardMobileTwo
                                        item={course}
                                        active={screen.active}
                                    />
                                </>
                            ))}
                        {ativo === 1 &&
                            courseExpired.map(course => (
                                <>
                                    <CardMobileOne
                                        item={course}
                                        active={screen.active}
                                    />
                                    <CardMobileTwo
                                        item={course}
                                        active={screen.active}
                                    />
                                </>
                            ))}
                        {ativo === 2 &&
                            courseExpired.map(course => (
                                <>
                                    <CardMobileOne
                                        item={course}
                                        active={screen.active}
                                    />
                                    <CardMobileTwo
                                        item={course}
                                        active={screen.active}
                                    />
                                </>
                            ))}
                    </div>
                </div>
            </Container>
            <MeusCursosMobile>
                <header>Meus cursos</header>
                <div className="apresentacao mt-3">
                    <span className="span-apresentacao">
                        Olá {profile.name}.
                    </span>
                </div>
                <Grid className="mt-3" container justify="center">
                    <Grid
                        item
                        xs={10}
                        sm={10}
                        md={10}
                        lg={10}
                        xl={10}
                        className="toggle-buttons"
                    >
                        <SelectDefault
                            campos={typeCourse}
                            label="Estado dos cursos"
                            value={ativo}
                            onChange={event =>
                                dispatch(
                                    UserActions.getAtivos(event.target.value)
                                )
                            }
                        />
                    </Grid>
                </Grid>
                <div className="apresentacao mt-2">
                    <Grid container justify="center">
                        <IconCard
                            fontSize="large"
                            className="fas fa-th-list"
                            onClick={() => handleScreen("cardOne")}
                            attr={screen.active}
                        />
                        <CardIcon
                            fontSize="large"
                            className="fas fa-th"
                            onClick={() => handleScreen("cardTwo")}
                            attr={screen.active}
                        />
                    </Grid>
                </div>
                <main>
                    {ativo === 0 &&
                        courseActive.map(item => {
                            return (
                                <>
                                    <CardMobileOne
                                        item={item}
                                        active={screen.active}
                                    />
                                    <CardMobileTwo
                                        item={item}
                                        active={screen.active}
                                    />
                                </>
                            );
                        })}
                    {ativo === 1 &&
                        courseExpired.map(item => {
                            return (
                                <>
                                    <CardMobileOne
                                        item={item}
                                        active={screen.active}
                                    />
                                    <CardMobileTwo
                                        item={item}
                                        active={screen.active}
                                    />
                                </>
                            );
                        })}
                </main>
            </MeusCursosMobile>
        </>
    );
};

export default MeusCursos;
