import styled from "styled-components";
import { Icon } from "@material-ui/core";

export const IconCard = styled(Icon)`
    cursor: pointer;
    background: ${props =>
        props.attr === "cardOne"
            ? "linear-gradient(0deg,#ef9300 0%,#ef7300 100%)"
            : "none"};
    color: ${props => (props.attr === "cardOne" ? "#fff" : "#0058a8")};
    width: 40px !important;
    height: 40px !important;
    display: flex !important;
    justify-content: center !important;
    overflow: auto !important;
    align-items: center !important;
    border-radius: 5px !important;
`;
export const CardIcon = styled(Icon)`
    cursor: pointer;
    width: 40px !important;
    height: 40px !important;
    display: flex !important;
    justify-content: center !important;
    overflow: auto !important;
    align-items: center !important;
    border-radius: 5px !important;

    background: ${props =>
        props.attr === "cardTwo"
            ? "linear-gradient(0deg,#ef9300 0%,#ef7300 100%)"
            : "none"};
    color: ${props => (props.attr === "cardTwo" ? "#fff" : "#0058a8")};
`;

export const Container = styled.div`
    @media only screen and (max-width: 999px) {
        display: none !important;
    }
    .MuiLinearProgress-root {
        width: 200px;
    }
    .MuiLinearProgress-barColorPrimary {
        background-color: ${props => props.theme.primary};
    }
    .name {
        min-height: 180px !important;
        .description-3 {
            font-size: 12px;
            color: ${props => props.theme.primary};
        }
        .MuiGrid-root {
            /* min-height: 80px; */
        }
    }

    .link {
        width: 100%;
    }
    .content-session {
        .row {
            margin: 0;
            justify-content: center;
        }
    }
    .card-curso {
        cursor: pointer;
        background-color: transparent;
        border: none;
    }
    .tr-card {
        .container-waiting {
            background: #1f1f1f;
            padding: 10px 15px;
            .cont-text {
                color: #fff;
                font-family: Montserrat;
                font-weight: bold;
                font-size: 0.9rem;
            }
        }
    }
`;

export const MeusCursosMobile = styled.div`
    margin-top: 10px;
    header {
        border-bottom: ${props => `1px solid ${props.theme.letter2}`};
        display: flex;
        height: 40px;
        justify-content: center;
        align-items: center;
        background: linear-gradient(0deg, #0058a8 0%, #0058a8 100%);

        font-family: Montserrat, sans-serif;
        font-size: 16px;
        font-weight: bold;
        color: #ffffff;
    }
    .sub {
        display: flex;
        div {
            cursor: pointer;
            width: 100%;
            display: flex;
            justify-content: center;
            box-shadow: 0 2px 0 0 rgba(0, 0, 0, 0.02);
            border-bottom: 1px solid #ececec;
            padding: 15px 0;
            span {
                font-family: Montserrat, sans-serif;
                font-size: 12px;
                text-align: center;
            }
        }
    }
    .MuiLinearProgress-root {
        width: 200px;
    }
    .MuiLinearProgress-barColorPrimary {
        background-color: ${props => props.theme.primary};
    }
    .apresentacao {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        .span-apresentacao {
            font-family: Montserrat, sans-serif;
            color: ${props => props.theme.letter2};
            font-size: 12px;
            font-weight: 300;
            line-height: 20px;
        }
        div {
            margin-top: 5px;
            margin-bottom: 25px;
            background-color: ${props => props.theme.letter};
            /* border: 1px solid #ececec; */
            /* border-radius: 5px; */
            /* box-shadow: 0 2px 1px 0 rgba(0, 0, 0, 0.02); */
        }
    }
    main {
        padding: 20px 20px 40px 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;
    }
    @media only screen and (min-width: 999px) {
        display: none;
    }
`;

export const CardOne = styled.div`
    margin-right: 20px;
    display: ${props => (props.active === "cardOne" ? "flex" : "none")};
    justify-content: center;
    align-items: center;
    flex-direction: column;
    border-radius: 5px;
    background-color: ${props => props.theme.letter};
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14),
        0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2);
    transition: box-shadow 0.25s;
    margin-bottom: 10px;
    max-width: 265px;
    .image {
        position: relative;
        display: flex;
        align-items: flex-end;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        background-image: ${props => `url(${props.img})`};
        width: 265px;
        height: 260px;

        background-position: center center;

        background-repeat: no-repeat;
        background-size: cover;
        background-color: #464646;

        .tag {
            position: absolute;
            bottom: -12.5px;
            left: 10px;
            span {
                background-color: ${props =>
                    props.tag ? props.tag : "violet"};
                color: ${props => props.theme.letter};
                font-family: Montserrat, sans-serif;
                font-size: 12px;
                text-align: center;
                padding: 4px;
                border-radius: 3px;
            }
        }
    }
    .title-progress {
        margin: 20px 10px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;
        text-align: center;

        span {
            color: ${props => props.theme.background};
            font-family: Montserrat, sans-serif;
            font-size: 15px;
            font-weight: 500;
            text-align: center;
            line-height: 19px;
            padding-bottom: 20px;
            &:last-child {
                color: ${props => props.theme.primary};
            }
        }
    }
    .container-waiting {
        background: #1f1f1f;
        padding: 10px 15px;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;

        .cont-text {
            color: #fff;
            font-family: Montserrat;
            font-weight: bold;
            font-size: 0.9rem;
        }
    }
    @media only screen and (max-width: 569px) {
        margin-right: 0;
    }
`;

export const CardTwo = styled.div`
    margin-right: 20px;
    display: ${props => (props.active === "cardTwo" ? "flex" : "none")};
    border-radius: 5px;
    background-color: #ffffff;
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14),
        0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2);
    transition: box-shadow 0.25s;
    margin-bottom: 10px;
    width: 285px;
    flex-direction: column;
    .img-course {
        display: flex;
        justify-content: center;
    }
    div {
        display: flex;

        .back-course {
            border-radius: 5px;
            width: 70px;
            height: 69px;
        }
        div {
            display: flex;
            .tag {
                display: flex;
                justify-content: center;
                align-items: center;
                background-color: ${props =>
                    props.tag ? props.tag : "violet"};
                color: ${props => props.theme.letter};
                font-family: Montserrat, sans-serif;
                font-size: 10px;
                text-align: center;
                border-radius: 3px;
            }
            .title-progress {
                display: flex;
                justify-content: center;
                align-items: center;
                flex-wrap: wrap;

                color: ${props => props.theme.background};
                font-family: Montserrat, sans-serif;
                font-size: 14px;
                font-weight: 500;
                line-height: 18px;
            }
        }
    }
    .barra {
        display: flex;
        flex-direction: column;
        width: 100%;
        align-items: center;
        justify-content: center;
        margin-top: 20px;
        span {
            margin-top: 10px;
            font-family: Montserrat;
            font-size: 15px;
            color: ${props => props.theme.primary};
        }
    }
    .container-waiting {
        background: #1f1f1f;
        padding: 10px 15px;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
        .cont-text {
            color: #fff;
            font-family: Montserrat;
            font-weight: bold;
            font-size: 0.9rem;
        }
    }
    @media only screen and (max-width: 569px) {
        margin-right: 0;
    }
`;
