import styled from 'styled-components';

export const Container = styled.div`
    @media only screen and (max-width: 999px) {
        margin: 0 !important;
        width: 100%;
        .container {
            max-width: unset !important;
        }
        .col-sm-3 {
            display: none;
        }
        .col-sm-9 {
            flex: 0 0 100%;
            max-width: unset !important;
            padding: 0;
        }
    }
`;
