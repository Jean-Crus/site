import styled from "styled-components";
import { Form } from "formik";
import InsertDriveFileIcon from "@material-ui/icons/InsertDriveFile";

export const IconFile = styled(InsertDriveFileIcon)``;
export const Container = styled.div`
    header {
        display: none;
    }
    .content-session {
        margin-top: 0 !important;
    }
    @media only screen and (max-width: 999px) {
        .title-session {
            margin-top: 10px;
            justify-content: center !important;
            .title-1 {
                display: none;
            }
        }
        .content-session {
            display: flex;
            justify-content: center;
        }
        padding: 0 !important;
        header {
            margin-top: 10px;
            border-bottom: ${props => `1px solid ${props.theme.letter2}`};
            display: flex;
            height: 40px;
            justify-content: center;
            align-items: center;
            background: linear-gradient(0deg, #0058a8 0%, #0058a8 100%);
            font-family: Montserrat, sans-serif;
            font-size: 16px;
            font-weight: bold;
            color: ${props => props.theme.letter};
            text-align: center;
        }
    }
`;

export const FormField = styled(Form)`
    max-width: 80%;
    margin: 30px;
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
    border-radius: 5px;
    /* background: linear-gradient(0deg, #012a50 0%, #0058a8 100%); */
    box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14),
        0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2);
    transition: box-shadow 0.25s;
    border: 1px solid #ccc;
    .box {
        display: flex;
        max-width: 500px;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        width: 100%;
        padding: 10px;
        input::placeholder {
            color: #ef7300 !important;
        }
        h2 {
            margin: 10px;
            color: ${props => "#1f1f1f"};
            font-family: Montserrat, sans-serif;
            font-size: 16px;
            font-weight: bold;
            line-height: 19px;
            align-self: flex-start;
        }
        .image {
            margin: 25px 0;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            img {
                border: 1px solid #fff;
                width: 87px;
                height: 87px;
                border-radius: 50%;
            }
            span {
                color: ${props => props.theme.letter};
                font-family: Montserrat, sans-serif;
                font-size: 12px;
                line-height: 15px;
                text-decoration: underline;
            }
        }

        .add-photo {
            margin-top: 20px;
            width: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            span {
                color: ${props => props.theme.letter};
                font-family: Montserrat, sans-serif;
                font-size: 11px;
                line-height: 14px;
                line-height: 14px;
                text-align: justify;
            }
        }

        button {
            margin-bottom: 30px;
        }
    }
`;

export const Divider = styled.div`
    width: 100%;
    border-bottom: 1px solid #fff;
    margin: 40px 0 0 0;
`;
