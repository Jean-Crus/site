import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as Yup from "yup";
import InputMask from "react-input-mask";
import { Formik } from "formik";
import * as UserActions from "@store/modules/user/actions";
import { ButtonTriade, Loading, TextDefault } from "@components";
import {
    Container,
    MobileConfigurar,
    TextInput,
    FormField,
    ErrorMsg,
    Divider,
    IconFile
} from "./styles";

const schema = Yup.object().shape({
    name: Yup.string().required("Nome obrigatório"),
    lastname: Yup.string().required("Nome obrigatório"),
    email: Yup.string()
        .email("Insira um e-mail válido")
        .required("E-mail obrigatório"),
    tel: Yup.string()
        .matches(
            /^\([0-9]{2}\)[0-9]?[0-9]{4}-[0-9]{4}$/,
            "Telefone inválido. Ex: (xx)xxxxx-xxxx"
        )
        .required("Número de telefone celular obrigatório"),
    password: Yup.string()
        .min(8, "Senha deve conter no mínimo 8 caracteres")
        .required("Senha obrigatória"),
    repassword: Yup.string("Coloque sua senha")
        .required("Confirme sua senha")
        .oneOf([Yup.ref("password")], "Senhas diferentes")
});

const Configurar = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user);
    const profile = useSelector(state => state.user.profile);
    const initialValues = {
        name: profile.name,
        email: profile.email,
        tel: profile.telephone,
        lastname: profile.lastname,
        password: "",
        repassword: ""
    };
    useEffect(() => {
        dispatch(UserActions.getUserRequest());
    }, [dispatch]);
    return (
        <>
            <Container className="account-session configurar-conta">
                <header>Configurar Conta</header>
                <div className="title-session">
                    <div>
                        <h1 className="title-1 black size-25">
                            Configurar Conta
                        </h1>
                        <p className="description-1">
                            Olá {user.profile.name}.
                        </p>
                    </div>
                </div>
                <div className="content-session">
                    <Formik
                        initialValues={initialValues}
                        enableReinitialize
                        validationSchema={schema}
                        className="tr-form"
                        onSubmit={({
                            name,
                            email,
                            tel,
                            lastname,
                            password
                        }) => {
                            dispatch(
                                UserActions.updateUserRequest(
                                    name,
                                    email,
                                    tel,
                                    lastname,
                                    password
                                )
                            );
                        }}
                    >
                        {({
                            handleBlur,
                            handleChange,
                            handleSubmit,
                            values,
                            errors,
                            touched,
                            validateField
                        }) => {
                            return user.loading ? (
                                <Loading />
                            ) : (
                                <FormField onSubmit={handleSubmit}>
                                    <div className="box mt-4">
                                        <h2>Dados pessoais</h2>
                                        {/* <div className="image">
                                            <img
                                                src="https://images.pexels.com/photos/3048454/pexels-photo-3048454.jpeg?cs=srgb&dl=agua-andando-ao-ar-livre-3048454.jpg&fm=jpg"
                                                alt=""
                                            />
                                            <span>Alterar foto</span>
                                        </div> */}
                                        <TextDefault
                                            placeholder="Nome"
                                            type="text"
                                            name="name"
                                            onChange={handleChange}
                                            errorColor="#1f1f1f"
                                            onBlur={handleBlur}
                                            value={values.name}
                                            helperText={
                                                touched.name && errors.name
                                            }
                                            error={
                                                !!touched.name && !!errors.name
                                            }
                                        />

                                        <TextDefault
                                            placeholder="Sobrenome"
                                            type="text"
                                            name="lastname"
                                            onChange={handleChange}
                                            errorColor="#1f1f1f"
                                            onBlur={handleBlur}
                                            value={values.lastname}
                                            helperText={
                                                touched.lastname &&
                                                errors.lastname
                                            }
                                            error={
                                                !!touched.lastname &&
                                                !!errors.lastname
                                            }
                                            className="mt-3"
                                        />

                                        <TextDefault
                                            placeholder="E-mail"
                                            type="email"
                                            name="email"
                                            onChange={handleChange}
                                            errorColor="#1f1f1f"
                                            onBlur={handleBlur}
                                            value={values.email}
                                            helperText={
                                                touched.email && errors.email
                                            }
                                            error={
                                                !!touched.email &&
                                                !!errors.email
                                            }
                                            className="mt-3"
                                        />

                                        <InputMask
                                            mask="(99)99999-9999"
                                            maskChar=" "
                                            placeholder="Celular"
                                            name="tel"
                                            onChange={handleChange}
                                            errorColor="#1f1f1f"
                                            onBlur={handleBlur}
                                            value={values.tel}
                                            helperText={
                                                touched.tel && errors.tel
                                            }
                                            error={
                                                !!touched.tel && !!errors.tel
                                            }
                                            className="mt-3"
                                        >
                                            {props => (
                                                <TextDefault {...props} />
                                            )}
                                        </InputMask>
                                    </div>
                                    {/* <Divider /> */}

                                    <div className="box">
                                        <h2>Senha</h2>
                                        <TextDefault
                                            placeholder="Senha"
                                            type="password"
                                            name="password"
                                            onChange={handleChange}
                                            errorColor="#1f1f1f"
                                            onBlur={handleBlur}
                                            value={values.password}
                                            helperText={
                                                touched.password &&
                                                errors.password
                                            }
                                            error={
                                                !!touched.password &&
                                                !!errors.password
                                            }
                                        />

                                        <TextDefault
                                            placeholder="Confirme a senha"
                                            type="password"
                                            name="repassword"
                                            onChange={handleChange}
                                            errorColor="#1f1f1f"
                                            onBlur={handleBlur}
                                            value={values.repassword}
                                            helperText={
                                                touched.repassword &&
                                                errors.repassword
                                            }
                                            error={
                                                !!touched.repassword &&
                                                !!errors.repassword
                                            }
                                            className="mt-3"
                                        />
                                    </div>
                                    {/* <Divider /> */}

                                    <div className="box">
                                        {/* <h2>Documentos</h2>
                                        <TextInput
                                            placeholder="CRM*"
                                            type="text"
                                            name="crm"
                                            onChange={handleChange}
                                            errorColor="#1f1f1f"
                                            onBlur={handleBlur}
                                            value={values.crm}
                                        />
                                        <ErrorMsg name="crm" component="div" />
                                        <div className="add-photo">
                                            <IconFile />
                                            <span>
                                                Adicione uma foto do seu CRM
                                            </span>
                                        </div> */}
                                        <ButtonTriade
                                            type="submit"
                                            button="Modelo 02"
                                            className="mt-5"
                                            letterColor="Modelo 02"
                                            cor="Modelo 02"
                                            text="Salvar"
                                        />
                                    </div>
                                </FormField>
                            );
                        }}
                    </Formik>
                </div>
            </Container>
        </>
    );
};

export default Configurar;
