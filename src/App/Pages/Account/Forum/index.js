import React from 'react';
import { Link } from 'react-router-dom';
import { compress } from '@images';


const Forum = (props, state) => {
    
    const style = {
        compress:{
            backgroundImage: "url('" + compress + "')"
        }
    }
    
    state = {
        cursos:[
            {},{},{},{}
        ]
    }

	return(
        <div className="account-session forum-account">
            <div className="title-session">
                <div>
                    <h1 className="title-1 black size-25">Fórum </h1>
                    <p className="description-1">Olá Raul Soares Silveira. Lorem ipsum dolor asit amet.</p>
                </div>
            </div>
            <div className="content-session">
                <div className="forum-topics">
                    <div className="top">
                        <ul>
                            <li className="description-3">Tópico</li>
                            <li className="description-3">Respostas</li>
                            <li className="description-3">Visualizações</li>
                            <li className="description-3">Última Resposta</li>
                        </ul>
                    </div>
                    <div className="middle">
                        {
                            state.cursos.map((item, index) => ( 
                                <ul className="topic">
                                    <li>
                                        <div className="topic-meta">
                                            <div className="avatar" style={style.compress}>
                                                <span className="sr-only">Rogério Santos</span>
                                            </div>
                                            <div className="author-meta">
                                                <Link className="title-topic" to="#">Dificuldades no módulo 3. Não consigo fazer a 4</Link>
                                                <strong className="black title-6">Rogério Santos, <span>30/03/2018</span></strong>
                                            </div>
                                        </div>
                                    </li>
                                    <li><span className="count-topic">2</span></li>
                                    <li><span className="count-topic">1622</span></li>
                                    <li>
                                        <div className="author-meta">
                                            <strong className="black title-6">Rogério Santos <span>30/03/2018</span></strong>
                                        </div>
                                    </li>
                                </ul>
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
	);	
}

export default Forum;