import React from 'react';
import { compress, conquista } from '@images';

const Conquistas = (props, state) => {

    state = {
        feed:[
            {},{},{},{},{},{},{},{}
        ],
        nome: [
            {},{}
        ],
        ranking:[
            {},{},{}
        ]
    }
	
	return(
        <div className="account-session conquistas">
            <div className="title-session">
                <div>
                    <h1 className="title-1 black size-25">Suas Conquistas</h1>
                    <p className="description-1">Olá Raul Soares Silveira. Lorem ipsum dolor asit amet.</p>
                </div>
            </div>
            <div className="content-session">
                <div className="row">
                    {
                        state.nome.map((item, index) => ( 
                        <div className="col-sm-6">
                            <div className="conquista-item">
                                <img src={conquista} alt="conquista" />
                                <div className="content">
                                    <h2 className="title-2 black">Nome do Curso</h2>
                                    <p className="description-1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt, architecto beatae delectus repellendus, dolorem illum provident obcaecati in, molestias nulla recusandae accusantium vero?</p>
                                    <small className="description-3">1 week ago</small>
                                </div>
                            </div>
                        </div>
                        ))
                    }
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <h2 className="title-1 black size-15">Feed</h2>
                        <div className="list-feed-badge">
                            {
                                state.feed.map((item, index) => ( 
                                <div className="list-feed-item">
                                    <div className="badge-list">
                                        <img className="badge-icon" src={conquista} alt="conquista"/>
                                        <img className="badge-owner" src={compress} alt="compress"/>
                                    </div>
                                    <div className="badge-meta">
                                        <h3 className="title-1 black">Wellington Trojan 🏆</h3>
                                        <p className="description-3">Unlock the award abcd.</p>
                                    </div>
                                    <div className="badge-time">
                                        <small className="description-2">20 min ago</small>
                                    </div>
                                </div>
                                ))
                            }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <h2 className="title-1 black size-15">Ranking Geral</h2>
                        <div className="list-ranking">
                            {
                                state.ranking.map((item, index) => ( 
                                <div className="list-ranking-item">
                                    <div className="position">
                                        <img src={compress} alt="compress"/>
                                        <span className="number">1</span>
                                    </div>
                                    <h2 className="title-1 black">Wellington Trojan 🥇</h2>
                                    <small className="description-2">210 pontos</small>
                                </div>
                                ))
                            }
                            <div className="list-ranking-item you">
                                <div className="position">
                                    <img src={compress} alt="compress"/>
                                    <span className="number">82</span>
                                </div>
                                <h2 className="title-1 black">Wellington Trojan</h2>
                                <small className="description-2">65 pontos</small>
                            </div>
                        </div>
                        <h2 className="title-1 black size-15 mt-5">Ranking Semanal</h2>
                        <div className="list-ranking">
                            {
                                state.ranking.map((item, index) => ( 
                                <div className="list-ranking-item">
                                    <div className="position">
                                        <img src={compress} alt="compress" />
                                        <span className="number">1</span>
                                    </div>
                                    <h2 className="title-1 black">Wellington Trojan 🥇</h2>
                                    <small className="description-2">210 pontos</small>
                                </div>
                                ))
                            }
                            <div className="list-ranking-item you">
                                <div className="position">
                                    <img src={compress} alt="compress" />
                                    <span className="number">82</span>
                                </div>
                                <h2 className="title-1 black">Wellington Trojan</h2>
                                <small className="description-2">65 pontos</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	);	
}

export default Conquistas;