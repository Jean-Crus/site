import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as Yup from "yup";
import { Formik } from "formik";
import { Grid } from "@material-ui/core";
import { Link } from "react-router-dom";
import InputMask from "react-input-mask";
import * as AuthActions from "@store/modules/auth/actions";
import logo from "@images/logo.png";
import { ButtonTriade, TextDefault } from "@components";
import useStyles from "./useStyle";
import { Container } from "./styles";

const initialValues = {
    name: "",
    lastname: "",
    cpf: "",
    email: "",
    tel: "",
    password: "",
    confirmPassword: ""
};

const schema = Yup.object().shape({
    name: Yup.string().required("Nome obrigatório"),
    lastname: Yup.string().required("Sobrenome obrigatório"),
    cpf: Yup.string().required("Cpf obrigatório"),
    email: Yup.string()
        .email("Insira um e-mail válido")
        .required("E-mail obrigatório"),
    tel: Yup.string()
        .matches(
            /^\([0-9]{2}\)[0-9]?[0-9]{4}-[0-9]{4}$/,
            "Telefone inválido. Ex: (xx)xxxxx-xxxx"
        )
        .required("Número de telefone celular obrigatório"),
    password: Yup.string()
        .min(8, "Senha deve conter no mínimo 8 caracteres")
        .required("Senha obrigatória"),
    confirmPassword: Yup.string("Coloque sua senha")
        .required("Confirme sua senha")
        .oneOf([Yup.ref("password")], "Senhas diferentes")
});

export default function CreateAccount() {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.app.loading);
    return (
        <Container className="bg-black tr-criar-conta">
            <div className="container">
                <Grid container className="content">
                    <div className="information">
                        <img
                            src={logo}
                            alt="Professor Carlos André"
                            srcSet=""
                        />
                        <div className="divider" />
                        <div className="title-1 ff-18">Criar conta</div>
                    </div>
                    <Formik
                        initialValues={initialValues}
                        enableReinitialize
                        validationSchema={schema}
                        className="tr-form"
                        onSubmit={({
                            name,
                            lastname,
                            email,
                            tel,
                            password,
                            cpf
                        }) => {
                            dispatch(
                                AuthActions.signUpRequest(
                                    name,
                                    lastname,
                                    email,
                                    tel,
                                    password,
                                    cpf
                                )
                            );
                        }}
                    >
                        {({
                            handleBlur,
                            handleChange,
                            handleSubmit,
                            values,
                            errors,
                            touched
                        }) => {
                            return (
                                <form onSubmit={handleSubmit}>
                                    <TextDefault
                                        fullWidth
                                        helperText={touched.name && errors.name}
                                        error={!!touched.name && !!errors.name}
                                        label="Nome*"
                                        type="text"
                                        variant="filled"
                                        name="name"
                                        onChange={handleChange}
                                        errorColor="#fff"
                                        className="mt-3"
                                        onBlur={handleBlur}
                                        value={values.name}
                                    />
                                    <TextDefault
                                        fullWidth
                                        helperText={
                                            touched.lastname && errors.lastname
                                        }
                                        error={
                                            !!touched.lastname &&
                                            !!errors.lastname
                                        }
                                        label="Sobrenome*"
                                        type="text"
                                        variant="filled"
                                        name="lastname"
                                        onChange={handleChange}
                                        errorColor="#fff"
                                        className="mt-3"
                                        onBlur={handleBlur}
                                        value={values.lastname}
                                    />

                                    <InputMask
                                        mask="999.999.999-99"
                                        maskChar=" "
                                        fullWidth
                                        helperText={touched.cpf && errors.cpf}
                                        error={!!touched.cpf && !!errors.cpf}
                                        label="CPF*"
                                        variant="filled"
                                        name="cpf"
                                        onChange={handleChange}
                                        errorColor="#fff"
                                        className="mt-3"
                                        onBlur={handleBlur}
                                        value={values.cpf}
                                    >
                                        {props => <TextDefault {...props} />}
                                    </InputMask>

                                    <TextDefault
                                        fullWidth
                                        helperText={
                                            touched.email && errors.email
                                        }
                                        error={
                                            !!touched.email && !!errors.email
                                        }
                                        label="E-mail*"
                                        type="email"
                                        variant="filled"
                                        name="email"
                                        onChange={handleChange}
                                        errorColor="#fff"
                                        className="mt-3"
                                        onBlur={handleBlur}
                                        value={values.email}
                                    />
                                    <InputMask
                                        mask="(99)99999-9999"
                                        maskChar=" "
                                        fullWidth
                                        helperText={touched.tel && errors.tel}
                                        error={!!touched.tel && !!errors.tel}
                                        label="Telefone celular*"
                                        variant="filled"
                                        name="tel"
                                        onChange={handleChange}
                                        errorColor="#fff"
                                        className="mt-3"
                                        onBlur={handleBlur}
                                        value={values.tel}
                                    >
                                        {props => <TextDefault {...props} />}
                                    </InputMask>

                                    <TextDefault
                                        fullWidth
                                        helperText={
                                            touched.password && errors.password
                                        }
                                        error={
                                            !!touched.password &&
                                            !!errors.password
                                        }
                                        label="Senha*"
                                        type="password"
                                        variant="filled"
                                        name="password"
                                        onChange={handleChange}
                                        errorColor="#fff"
                                        className="mt-3"
                                        onBlur={handleBlur}
                                        value={values.password}
                                    />
                                    <TextDefault
                                        fullWidth
                                        helperText={
                                            touched.confirmPassword &&
                                            errors.confirmPassword
                                        }
                                        error={
                                            !!touched.confirmPassword &&
                                            !!errors.confirmPassword
                                        }
                                        label="Repetir senha*"
                                        type="password"
                                        variant="filled"
                                        name="confirmPassword"
                                        onChange={handleChange}
                                        errorColor="#fff"
                                        className="mt-3"
                                        onBlur={handleBlur}
                                        value={values.confirmPassword}
                                    />
                                    {/* <div className="form-group pt-2 pb-3">
                                        <input
                                            type="checkbox"
                                            checked="checked"
                                            name="manager"
                                            id="manager"
                                        />
                                        <label
                                            htmlFor="manager"
                                            className="checkbox"
                                        >
                                            Lembre-me
                                        </label>
                                    </div> */}
                                    <div className="submit">
                                        <ButtonTriade
                                            type="submit"
                                            button="Modelo 02"
                                            letterColor="Modelo 02"
                                            cor="Modelo 02"
                                            icon="fas fa-user"
                                            text={
                                                loading
                                                    ? "Carregando..."
                                                    : "Criar Conta"
                                            }
                                        />
                                    </div>
                                </form>
                            );
                        }}
                    </Formik>
                </Grid>
                <Grid container justify="center" className="acc-already mt-2">
                    <span>Ja tem conta?</span>
                    <Link to="/login">Acesse aqui</Link>
                </Grid>
            </div>
        </Container>
    );
}
