import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    acc: {
        marginTop: theme.spacing(3),
        '& span': {
            fontFamily: 'Montserrat',
            fontSize: '14px',
            color: '#AFAFAF',
            // fontWeight: 'bold',
            // lineHeight: '18px',
        },
        '& a': {
            color: '#AFAFAF',
            fontSize: '14px',
            fontWeight: 'bold',
            textDecoration: 'underline',
        },
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginBottom: theme.spacing(1),
        borderRadius: '5px',
        color: '#fff',
        '& .MuiInputBase-input': {
            color: '#FFFFFFFF',
        },
        '& .MuiFormLabel-root': {
            color: '#AFAFAF',
        },
        '& .MuiOutlinedInput-notchedOutline': {
            border: '1px solid #AFAFAF',
        },
        '& .MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline': {
            border: `2px solid #3f51b5`,
            borderRadius: '5px',
        },
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
}));
export default useStyles;
