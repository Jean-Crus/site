import React from "react";
import { compress, download } from "@images";
import { Link } from "react-router-dom";

const Certificado = (props, state) => {
    state = {
        cursos: [{}, {}, {}, {}]
    };

    const style = {
        compress: {
            backgroundImage: `url('${compress}')`
        }
    };

    return (
        <div className="account-session certificados">
            <div className="title-session">
                <div>
                    <h1 className="title-1 black size-25">Certificado</h1>
                    <p className="description-1">
                        Olá Raul Soares Silveira. Lorem ipsum dolor asit amet.
                    </p>
                </div>
            </div>
            <div className="content-session">
                {state.cursos.map((item, index) => (
                    <div className="certificado-item">
                        <div className="info">
                            <div className="avatar" style={style.compress}>
                                <span className="sr-only">Rogério Santos</span>
                            </div>
                            <Link to="#" target="_blank">
                                <img src={download} alt="Download" />
                                Curso Avançado Lorem Ipsum
                            </Link>
                        </div>
                        <div className="author-meta">
                            <strong className="black title-6">
                                Rogério Santos <span>30/03/2018</span>
                            </strong>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Certificado;
