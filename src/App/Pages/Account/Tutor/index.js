import React from 'react';
import { compress } from '@images';
import { Link } from 'react-router-dom';

const Tutor = (props, state) => {

    state = {
        cursos:[
            {},{},{},{}
        ]
    }

    const style = {
        compress:{
            backgroundImage: "url('" + compress + "')"
        }
    }
	
	return(
        <div className="account-session falar-com-tutor">
            <div className="title-session">
                <div>
                    <h1 className="title-1 black size-25">Falar com o tutor <span className="count-alert">(3)</span></h1>
                    <p className="description-1">Olá Raul Soares Silveira. Lorem ipsum dolor asit amet.</p>
                </div>
            </div>
            <div className="content-session">
                <div className="falar-com-tutor">
                    {
                        state.cursos.map((item, index) => ( 
                        <div className="falar-item">
                            <div className="falar-meta">
                                <div className="avatar" style={style.compress}>
                                    <span className="sr-only">Rogério Santos</span>
                                    <div className="new-messages">2</div>
                                </div>
                                <div className="author-meta">
                                    <small>Curso Avançado de Bibliotecaria</small>
                                    <Link className="title-topic" to="#">Dificuldades no módulo 3. Não consigo fazer a 4</Link>
                                    <strong className="black title-6">Rogério Santos, <span>30/03/2018</span></strong>
                                </div>
                            </div>
                            <div className="falar-count">
                                <span>5</span>
                            </div>
                        </div>
                        ))
                    }
                </div>
            </div>
        </div>
	);	
}

export default Tutor;