import React from 'react';
import { bannerFull } from '@images';
import { BannerPage, Newsletter } from '@components';

const Artigos = (props) => {
	
	return(
		<React.Fragment>
			{/* Header Eventos */}
			<div className="tr-header-course">
				<BannerPage title="Artigos" banner={bannerFull}/>
			</div>

			{/* NEWSLETTER */}
			<div className="tr-newsletter purple mt-5">
				<Newsletter/>
			</div>
		</React.Fragment>
	);	
}

export default Artigos;