const rewireAliases = require("react-app-rewire-aliases");
const { paths } = require("react-app-rewired");
const path = require("path");

module.exports = function override(config, env) {
    config = rewireAliases.aliasesOptions({
        "@components": path.resolve(__dirname, `${paths.appSrc}/components`),
        "@routes": path.resolve(__dirname, `${paths.appSrc}/routes`),
        "@store": path.resolve(__dirname, `${paths.appSrc}/store`),
        "@api": path.resolve(__dirname, `${paths.appSrc}/services/api`),
        "@util": path.resolve(__dirname, `${paths.appSrc}/util`),
        "@assets": path.resolve(__dirname, `${paths.appSrc}/Assets`),
        "@slim": path.resolve(__dirname, `${paths.appSrc}/Assets/slim/slim`),

        "@theme": path.resolve(__dirname, `${paths.appSrc}/config/theme`),
        // '@globalStyle': path.resolve(__dirname, `${paths.appSrc}/Assets/css/style.css`),

        "@images": path.resolve(__dirname, `${paths.appSrc}/Assets/images`),
        "@app": path.resolve(__dirname, `${paths.appSrc}/App`)
    })(config, env);
    return config;
};
